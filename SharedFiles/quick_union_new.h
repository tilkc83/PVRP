#ifndef QUICK_UNION_NEW_H
#define QUICK_UNION_NEW_H

#include <vector>
#include <set>
///////////////////////////////////////////////////
//CT:Adapted to save flow values inside components
///////////////////////////////////////////////////
namespace quick_union_new {

	class c_QuickUnionFind_new {
		//
		// code taken from presentation
		// http://www.cs.princeton.edu/~rs/AlgsDS07/01UnionFind.pdf 
		// 
		std::vector<int> id;
		std::vector<int> sz;
		std::vector<double> v_flow;
	public:
		c_QuickUnionFind_new(int N) : id(N), sz(N), v_flow(N)
		{
			reset();
		}
		void reset()
		{
			for (int i = 0; i < (int)id.size(); i++)
			{
				id[i] = i;
				sz[i] = 1;
				v_flow[i] = 0;
			}
		}
		void reset(int i)
		{
			id[i] = i;
			sz[i] = 1;
			v_flow[i] = 0.0;
		}
		double GetFlow(int p)
		{
			int i = root(p);
			return v_flow[i];
		}
		int root(int i)
		{
			while (i != id[i])
			{
				id[i] = id[id[i]];
				i = id[i];
			}
			return i;
		}
		bool find(int p, int q)
		{
			return root(p) == root(q);
		}
		void merge(int p, int q, double flow)
		{
			int i = root(p);
			int j = root(q);
			if (i == j)
			{
				if (sz[i] < sz[j])
					v_flow[j] += flow;
				else
					v_flow[i] += flow;
				return;
			}

			if (sz[i] < sz[j])
			{
				id[i] = j;
				sz[j] += sz[i];
				v_flow[j] += flow + v_flow[i];
				v_flow[i] = 0;
			}
			else
			{
				id[j] = i;
				sz[i] += sz[j];
				v_flow[i] += flow + v_flow[j];
				v_flow[j] = 0;
			}
		}
		int component_size(int i) { return sz[root(i)]; }
		std::set<int> component(int p)
		{
			int j = root(p);
			std::set<int> comp;
			for (int q = 0; q < (int)id.size(); q++)
				if (root(q) == j)
					comp.insert(q);
			return comp;
		}
	};

} // end of namespace

#endif // of QUICK_UNION_H

