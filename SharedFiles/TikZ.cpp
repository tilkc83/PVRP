#include "TikZ.h"

#include <fstream>
#include <iostream>



c_Color::c_Color()
{
	std::vector<std::string> allcolor {"black", "blue", "brown", "cyan", "darkgray", "gray", "green", "lightgray", "lime", "magenta", "olive", "orange", "pink", "purple", "red", "teal", "violet", "white", "yellow"};

	std::random_device rd1;
	std::mt19937 rng1(rd1());
	std::uniform_int_distribution<> color1(0, allcolor.size()-1);
	std::uniform_int_distribution<> number1(0, 100);


	s_colorOne = allcolor[color1(rng1)];
	d_numberTwo = number1(rng1);

	std::random_device rd2;
	std::mt19937 rng2(rd2());
	std::uniform_int_distribution<> color2(0, allcolor.size() - 1);
	std::uniform_int_distribution<> number2(0, 100);


	s_colorTwo = allcolor[color2(rng2)];
	d_numberOne = number2(rng2); 


}

c_Color::~c_Color()
{
}


void c_NodeElement::Draw(std::ofstream& file, double scale) const
{

	file << "\\node[draw=black, fill = color" << i_color <<"] (" << i_id << ") at (\\x*" <<  XCoord() << ", \\x*" << YCoord() << ") {" << i_id << "}; \n";
}

void c_ArcElement::Draw(std::ofstream & file)
{
	if (d_weight != 9999)
	{
		if (b_curved)
			file << "\\draw[color = color" << i_color << ", line width = 1pt,latex-,bend angle=10,bend right,-](" << i_head << ") to node [right] {" << d_weight << "} (" << i_tail << ") ; \n";
		else
			file << "\\draw[color = color" << i_color << ", line width = 1pt,latex-,-](" << i_head << ") to node [right] {" << d_weight << "} (" << i_tail << ") ; \n";
	}
	else {
		if (b_curved)
			file << "\\draw[color = route" << i_color << ", line width = 1pt,latex-,bend angle=10,bend right, dotted,-](" << i_head << ") to node [right] {} (" << i_tail << ") ; \n";
		else
			file << "\\draw[color = route" << i_color << ", line width = 1pt,latex-, dotted,-](" << i_head << ") to node [right] {} (" << i_tail << ") ; \n";
	}
}

c_DrawTikz::c_DrawTikz(std::string filename, int numClu) : 
	s_filename(filename), 
	d_scale(1.0), 
	i_max_id(-1),
	v_cluster_colors(0),
	v_route_color(0)
{
	AddColor(numClu); 
}

void c_DrawTikz::AddColor(int numClu)
{
	v_cluster_colors.reserve(numClu); 
	for (int i = 0; i < numClu; ++i)
		v_cluster_colors.emplace_back(c_Color());
	

	v_cluster_colors.emplace_back(c_Color("black", "white", 50, 100));
	v_cluster_colors.emplace_back(c_Color("white", "white", 100, 100));
}

void c_DrawTikz::AddNode(double x_coord, double y_coord, int cluster, int id, bool depot, string additional_node_info)
{
	if (id > i_max_id)
		i_max_id = id;
	c_NodeElement new_node = c_NodeElement(x_coord, y_coord, cluster, additional_node_info, depot, id);
	if (depot)
	{
		for (int i = 0; i < (int)v_depot_nodes.size(); i++)
		{
			if (v_depot_nodes[i].XCoord() == new_node.XCoord() && v_depot_nodes[i].YCoord() == new_node.YCoord())
			{
				v_p_identical_depots.push_back(make_pair(id, v_depot_nodes[i].Id()));
				return;
			}
		}
		v_depot_nodes.push_back(new_node);
	}
	v_nodes.push_back(new_node);
}

void c_DrawTikz::AddRoute(vector<int> route)
{
	v_route_color.emplace_back(c_Color());

	for (int i = 0; i < (int)route.size() - 1; i++)
	{
		if(route[i] < route[i+1])
			v_arcs.push_back(c_ArcElement(route[i], route[i + 1], v_route_color.size()-1, 9999));
		else
			v_arcs.push_back(c_ArcElement(route[i+1], route[i], v_route_color.size()-1, 9999));
	}
}
void c_DrawTikz::AddArc(int i, int j, int color, double weight)
{
	v_arcs.push_back(c_ArcElement(i, j, color, weight));
}



void c_DrawTikz::Draw()
{
	// The LaTeX output...
	std::ofstream file("toy.tex");
	if (!file.is_open())
	{
		std::cout << "Can't open TikZ-file!" << std::endl;
	}
	// preamble
	file << "\\documentclass{standalone} \n";
	file << "\\usepackage{tikz} \n";
	file << "\\usetikzlibrary{calc} \n";
	
	for (int i = 0; i < v_cluster_colors.size(); ++i)
		file << "\\colorlet{color" << i << "}{" << v_cluster_colors[i].get_sColorOne() << "!" << v_cluster_colors[i].getNumberOne() << "!" << v_cluster_colors[i].get_sColorTwo() << "!" << v_cluster_colors[i].getNumberTwo() << "}" << std::endl;


	for (int i = 0; i < v_route_color.size(); ++i)
		file << "\\colorlet{route" << i << "}{" << v_route_color[i].get_sColorOne() << "!" << v_route_color[i].getNumberOne() << "!" << v_route_color[i].get_sColorTwo() << "!" << v_route_color[i].getNumberTwo() << "}" << std::endl;

	file << "\\def\\x{" << d_scale << "}" << std::endl; 

	file << "\\begin{document} \n";
	file << "\\begin{tikzpicture} \n";


	//double depot node?
	vector<bool> double_depot(i_max_id + 1, false);
	vector<int> change_id(i_max_id + 1, -1);
	for (int i = 0; i < (int)v_p_identical_depots.size(); i++)
	{
		double_depot[v_p_identical_depots[i].first] = true;
		change_id[v_p_identical_depots[i].first] = v_p_identical_depots[i].second;
	}

	//draw nodes
	for (int i = 0; i < (int)v_nodes.size(); i++)
		if (!double_depot[v_nodes[i].Id()])
			v_nodes[i].Draw(file, d_scale);

	//draw arcs
	/*for (int i = 0; i < (int)v_arcs.size(); i++)
	{
		if (double_depot[v_arcs[i].GetTail()])
			v_arcs[i].SetTail(change_id[v_arcs[i].GetTail()]);
		if (double_depot[v_arcs[i].GetHead()])
			v_arcs[i].SetHead(change_id[v_arcs[i].GetHead()]);
	}*/

	for (int i = 0; i < (int)v_arcs.size(); i++)
	{
		for (int j = i + 1; j < (int)v_arcs.size(); j++)
		{
			if (v_arcs[i].GetHead() == v_arcs[j].GetTail() && v_arcs[j].GetHead() == v_arcs[i].GetTail())
			{
				v_arcs[i].SetCurved();
				v_arcs[j].SetCurved();
			}
		}
	}
	for (int i = 0; i < (int)v_arcs.size(); i++)
		v_arcs[i].Draw(file);

	// postable
	file << "\\end{tikzpicture} \n";
	file << "\\end{document} \n";
	file.close();
}

