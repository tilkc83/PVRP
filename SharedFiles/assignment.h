#ifndef ASSIGNMENT_H
#define ASSIGNMENT_H

namespace assignment {

typedef double t_assignment_cost;

class c_AssignmentSolver
{
	int i_dimension;
	t_assignment_cost** pi_costs;
	int* pi_assigned_row;
	int* pi_assigned_column;
	t_assignment_cost *pc_dual_row; 
	t_assignment_cost *pc_dual_col; 
	t_assignment_cost Solve( int dim, t_assignment_cost **assigncost, int *rowsol, int *colsol, t_assignment_cost *u, t_assignment_cost *v );
 public:
	c_AssignmentSolver( int num_rows, int num_cols );
	~c_AssignmentSolver( void );

	void SetCost( int row, int col, t_assignment_cost cost );
	t_assignment_cost Solve( void );
	// Retrieval of solution
	t_assignment_cost Cost( int row, int col ) const { return pi_costs[row][col]; }
	int AssignedRow( int col ) const;
	int AssignedColumn( int row ) const;
	t_assignment_cost DualRow( int row ) const { return pc_dual_row[row]; }
	t_assignment_cost DualColumn( int col ) const { return pc_dual_col[col]; }
	t_assignment_cost ReducedCost( int row, int col ) const;
};
																																				
}; // of namespace assignment

#endif // ASSIGNMENT_H


 
