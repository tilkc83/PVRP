#ifndef MAX_FLOW_MIN_CUT_H
#define MAX_FLOW_MIN_CUT_H

#include <vector>
#include <cassert>
#include <tuple>

// encapsulates the Min-Cut code by Georg Skorobohatyj, ZIB
// elib.zib.de/pub/Packages/mathprog/mincut/all-pairs/index.html
namespace maxflow_mincut
{

/**********************************************************************************************************************
ZIB code
**********************************************************************************************************************/
typedef enum 
  { FALSE = 0,
    TRUE  = 1
  } BOOL;

typedef struct Node
  { long         id;
    struct Edge  *first_edge;
		 /* in list of incident edges */
    struct Edge  *scan_ptr;
		 /* next edge to be scanned when node 
		    will be visited again */
    struct Node  *parent;   /* pointer of Gomory-Hu cut tree */
    double       mincap;    /* capacity of minimum cut between
		               node and parent in GH cut tree */
      /* subsequent entries for use by maxflow */
    long         dist;
    double       excess;
    struct Node  *bfs_link;     /* for one way BFS working queue */
    struct Node  *stack_link;   /* for stack of active nodes */
    BOOL         alive;    
    BOOL         unmarked;      /* while BFS in progress */
  } node;

typedef struct Edge
  { node         *adjac; /* pointer to adjacent node */
    struct Edge  *next;  /* in incidence list of node 
			    from which edge is emanating */
    struct Edge  *back;  /* pointer to reverse edge */
    double       cap;
    double       rcap;   /* residual capacity */
  } edge;

typedef struct Graph
  { long    n_nodes;
    node    *nodes;
    long    n_edges;     /* number of edges with non-zero capacity */
    long    n_edges0;    /* number of all edges */
    edge    *edges;
  } graph;

#define  NILN (node *) 0
#define  NILE (edge *) 0
#define  EPS  1.0E-10
/**********************************************************************************************************************
end ZIB code
**********************************************************************************************************************/


/**********************************************************************************************************************
class CGomoryHuMinCutAlg
**********************************************************************************************************************/

class c_WeightedEdge {
public:
	int i_tail;
	int i_head;
	double d_weight;
	c_WeightedEdge(int tail, int head, double weight) : i_tail(tail), i_head(head), d_weight(weight) { assert(weight >= 0.0); }
};


class c_GomoryHuMinCutAlg
{
	graph* gr;

	node **active;
	long *number;
	long max_dist;
	long bound;
	BOOL co_check;

	BOOL init_maxflow(long n);
	void fini_maxflow();
	void global_relabel(graph* gr, node* tptr);
	double maxflow(graph* gr, node* s_ptr, node* t_ptr);
	BOOL ghc_tree(graph* gr);

	std::vector<int> old_idx_to_new_idx;
	std::vector<int> new_idx_to_old_idx;
	bool b_tree_computed;
	const std::vector<c_WeightedEdge>& v_weighted_edges;
public:
	c_GomoryHuMinCutAlg( const std::vector<c_WeightedEdge>& weighted_edges);
	virtual ~c_GomoryHuMinCutAlg();

	// returns true if all goes well
	double MaxFlow(int vertex_s, int vertex_t, std::vector<int>& S);
	bool ComputeMinCut( std::vector<int>& set_with_s, std::vector<int>& set_with_t, double& dMaxFlow );
	bool GetShore( int vertex, std::vector<bool>& in_S );
};


class c_LetchfordReineltTheisAlg {
	std::vector<bool>& v_odd_vertices;
	const std::vector<c_WeightedEdge>& v_weighted_edges;
public:
	c_LetchfordReineltTheisAlg(std::vector<bool>& odd_vertices, const std::vector<c_WeightedEdge>& weighted_edges);

	// minimum odd cuts with value < 1.0
	bool ComputeMinOddCut(bool only_components, std::vector<int>& S, std::vector<int>& F, double& dMaxFlow);
	bool ComputeMinOddCut(bool only_components, int max_num_results, std::vector<std::tuple<double, 
												std::vector<int>,std::vector<int> > >& violation_S_F, double& dMaxFlow);
};

} // namespace 

#endif // of MAX_FLOW_MIN_CUT_H
