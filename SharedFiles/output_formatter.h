#ifndef OUTPUT_FORMATTER_H
#define OUTPUT_FORMATTER_H

#include <string>
#include <list>
#include <vector>
#include <iostream>

#include "stopwatch.h"

typedef c_Stopwatch c_TimeInfo;

namespace formatted_output {

	class c_OutputController;
	class c_OutputField;
	class c_IntegerField;
	class c_DoubleField;
	class c_BoolField;
	class c_OnOffField;
	class c_YesNoField;
	class c_HostnameField;
	class c_UnsignedField;
	class c_Unsigned64Field;

	class c_OutputField {
		static c_OutputController* p_ctlr;
		int i_pos;
		std::vector<std::string> vs_field_header_line;
		bool b_print;
		int i_width;
		char c_fill;
	protected:
		c_OutputField();
	public:
		c_OutputField(int pos, const std::string& header_line, c_OutputController* ctlr = nullptr);
		virtual ~c_OutputField();

		int Position() const { return i_pos; }
		virtual void Output(std::ostream& stream) const = 0;
		virtual std::string HeaderLine(int i) const { return vs_field_header_line[i]; }
		virtual int NumHeaderLines() const { return (int)vs_field_header_line.size(); }
		// general IO
		void PrintSeparator(std::ostream& stream) const;
		c_OutputField& PrintIf(bool flag);
		bool Print() const { return b_print; }
		c_OutputField& SetWidth(int width);
		int Width() const { return i_width; }
		c_OutputField& SetFill(char fill);
		char Fill() const { return c_fill; }
		// trigger output of all fields
		void OutputToFile(const std::string& filename);

		c_OutputController* OutputController(){ return p_ctlr; }
	};

	class c_IntegerField : public c_OutputField {
		int i_value;
	public:
		c_IntegerField(int pos, const std::string& header_line, int value, c_OutputController* ctlr = nullptr);
		void Output(std::ostream& stream) const;
	};

	class c_DoubleField : public c_OutputField {
		double d_value;
	public:
		c_DoubleField(int pos, const std::string& header_line, double value, c_OutputController* ctlr = nullptr);
		void Output(std::ostream& stream) const;
	};

	class c_BoolField : public c_OutputField {
	protected:
		bool b_value;
	public:
		c_BoolField(int pos, const std::string& header_line, bool value);
		void Output(std::ostream& stream) const;
	};

	class c_OnOffField : public c_BoolField {
	public:
		c_OnOffField(int pos, const std::string& header_line, bool value) : c_BoolField(pos, header_line, value) {}
		void Output(std::ostream& stream) const { stream << (b_value ? "on" : "off"); }
	};

	class c_YesNoField : public c_BoolField {
	public:
		c_YesNoField(int pos, const std::string& header_line, bool value) : c_BoolField(pos, header_line, value) {}
		void Output(std::ostream& stream) const { stream << (b_value ? "yes" : "no"); }
	};

	class c_StringField : public c_OutputField {
		std::string s_text;
	public:
		c_StringField(int pos, const std::string& header_line, const std::string& text);
		void Output(std::ostream& stream) const;
	};

	class c_TimeInfoField : public c_OutputField {
		const c_TimeInfo& o_timer;
	public:
		c_TimeInfoField(int pos, const std::string& header_line, const c_TimeInfo& timer);
		void Output(std::ostream& stream) const;
		std::string HeaderLine(int i) const;
		int NumHeaderLines() const;
	};

	class c_TimeStampField : public c_OutputField {
	public:
		c_TimeStampField(int pos, const std::string& header_line);
		void Output(std::ostream& stream) const;
	};

	class c_HostnameField : public c_OutputField {
	public:
		c_HostnameField(int pos, const std::string& header_line);
		void Output(std::ostream& stream) const;
	};

	class c_OutputController {
		std::string s_filename;
		std::list<std::pair<int, const c_OutputField*> > l_field;
		std::string s_sep;
	public:
		c_OutputController();
		~c_OutputController() {}
		void Add(const c_OutputField* field);
		void SetFilename(const std::string& filename) { s_filename = filename; }
		void Output();
		void PrintSeparator(std::ostream& stream) const;
	};

	class c_UnsignedField : public c_OutputField {
		unsigned int i_value;
		public:
			c_UnsignedField(int pos, const std::string& header_line, unsigned int value, c_OutputController* ctlr=nullptr);
			void Output(std::ostream& stream) const;
	};

	class c_Unsigned64Field : public c_OutputField {
		unsigned long long i_value;
		public:
			c_Unsigned64Field(int pos, const std::string& header_line, unsigned long long value, c_OutputController* ctlr=nullptr);
			void Output(std::ostream& stream) const;
	};

}; // of namespace formatted_output

#endif