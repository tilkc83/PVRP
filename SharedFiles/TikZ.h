#ifndef TikZ_H
#define TikZ_H

#include <vector>
#include <string>
#include <random>
using namespace std;

class c_Color
{
	std::string s_colorOne; 
	std::string s_colorTwo; 
	int d_numberOne; 
	int d_numberTwo; 
public:
	c_Color();
	c_Color(std::string colorOne, std::string colorTwo, int numberOne, int numberTwo) : s_colorOne(colorOne), s_colorTwo(colorTwo), d_numberOne(numberOne), d_numberTwo(numberTwo) {}; 
	~c_Color();

	std::string get_sColorOne() { return s_colorOne; }
	std::string get_sColorTwo() { return s_colorTwo; }
	int getNumberOne() { return d_numberOne; }
	int getNumberTwo() { return d_numberTwo; }
};


class c_NodeElement
{
	bool b_depot;
	double d_x_coord;
	double d_y_coord;
	string v_additional_node_info;
	int i_id;
	int i_color; 
public:
	c_NodeElement(double x_coord, double y_coord, int color, string additional_node_info, bool depot, int id)
		: b_depot(depot), d_x_coord(x_coord), d_y_coord(y_coord), i_color(color), v_additional_node_info(additional_node_info), i_id(id) {};
	bool IsDepot() const { return b_depot; }
	int Id() const { return i_id; }
	double XCoord() const { return d_x_coord; }
	double YCoord() const { return d_y_coord; }
	string AdditionalNodeInfo() const { return v_additional_node_info; }
	int Color() { return i_color;  }
	void Draw(std::ofstream& file, double scale) const;
};

class c_ArcElement
{
	int i_head;
	int i_tail;
	double d_weight;
	bool b_curved;
	int i_color;
public:
	c_ArcElement(int head, int tail, int cluster,double weight = -1.0)
		:i_head(head), i_tail(tail), i_color(cluster), d_weight(weight), b_curved(false) {};
	void Draw(std::ofstream& file);
	void SetCurved() { b_curved = true; }
	int GetHead() { return i_head; }
	int GetTail() { return i_tail; }
	void SetHead(int head) { i_head=head; }
	void SetTail(int tail) { i_tail = tail; }
	int Color() { return i_color;  }
	double GetWeight() { return d_weight; }
};

class c_DrawTikz
{
	std::string s_filename;
	double d_scale;
	vector<c_ArcElement> v_arcs;
	vector<c_NodeElement> v_nodes;
	vector<c_NodeElement> v_depot_nodes;
	vector<pair<int, int> > v_p_identical_depots;
	int i_max_id;
	std::vector<c_Color> v_cluster_colors; 
	std::vector<c_Color> v_route_color;
public:
	c_DrawTikz(std::string filename, int numClu);
	//
	void AddColor(int numClu); 
	//add node
	void AddNode(double x_coord, double y_coord, int cluster, int id,  bool depot = false, string additional_node_info = "");
	//add route
	void AddRoute(vector<int> route);
	//add arc
	void AddArc(int i, int j, int color,  double weight);
	//general settings
	void SetScale(double scale) { d_scale = scale; };
	//draw
	void Draw();
};


#endif // of TikZ_H

