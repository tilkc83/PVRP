#ifndef STOPWATCH_H
#define STOPWATCH_H

/*
STOPWATCH CLASS - JB 2018/03/16
Measures time with the ability to chrono in 'laps'
Multiple formatting options for time duration using TimeFormat enum
Support of C++ <chrono>
Reproduces the c_TimeInfo class
	Although methods for Hours...Milliseconds, are renamed with small 's' and prefixed with Total
		Seconds() -> TotalSeconds()
		MilliSeconds() -> TotalMilliseconds()
	Likewise the string output methods are derived in analogous names
		FormattedTime() -> FormattedTotalChrono()
		FormattedAvgTime() -> FormattedAvgChrono()
	Backward compatible methods are defined at the end

Lap() method to do Stop() and Restart() functionality --> use Restart() once, and then Lap(), and finish with Stop()
Fixed potential bug in AvgTimer when dividing by 0 a timer that was never used.

Debug asserts also ensure a certain order in the calls Start/Restart/Stop/Lap
For instance, you may not use the method .Restart() unless the c_ChronoInfo object has first used .Stop()
Likewise, you may not use .Stop() before using .Start(), nor use consecutive .Start() without an intermediate .Reset()

c_DisposableStopwatch: intended for one time use Start/Stop
c_Stopwatch: inherits from the Disposable version and allows Restart/Lap functionality
*/

#include <iostream>
#include <sstream>
#include <iomanip>
#include <chrono>
#include <assert.h>

class c_DisposableStopwatch
{
protected:
	const std::chrono::steady_clock::duration _zeroDuration = std::chrono::steady_clock::duration::zero();
	//const std::chrono::steady_clock::duration _minDuration = std::chrono::steady_clock::duration::min();
	const std::chrono::steady_clock::duration _maxDuration = std::chrono::steady_clock::duration::max();

	std::chrono::steady_clock::time_point _start;
	std::chrono::steady_clock::time_point _stop;
	std::chrono::steady_clock::duration _totalTime;
	std::chrono::steady_clock::duration _lapTime;
	bool _isPaused;

public:
	typedef std::chrono::duration<int, std::ratio_multiply<std::chrono::hours::period, std::ratio<24>>::type> days;
	enum TimeFormat { _dd, _hh, _mm, _ss, _ms, _us, _ns }; // largest time unit for formatting: days, hours, minutes, seconds, milliseconds, microseconds, nanoseconds

	c_DisposableStopwatch() : 
		_isPaused(true),
		_totalTime(_zeroDuration)
	{}

	~c_DisposableStopwatch() {}

	void Start()
	{
		assert(_isPaused);
		_isPaused = false;
		_start = std::chrono::steady_clock::now();
	}

	virtual void Stop()
	{
		assert(!_isPaused);
		_stop = std::chrono::steady_clock::now();
		_lapTime = _stop - _start;
		_totalTime += _lapTime;
		_isPaused = true;
	}

	virtual void Reset()
	{
		_isPaused = true;
		_totalTime = _zeroDuration;
	}

	bool isPaused(void) const { return _isPaused; }

protected:
	inline auto _totalChronosI() const
	{
		if (_isPaused)		return _totalTime;
		else				return _totalTime + (std::chrono::steady_clock::now() - _start); // running time including current run if any
	}

	inline auto _totalChronosE() const
	{
		return _totalTime; // running time excluding current run if any
	}

	template<typename T>						struct is_chrono_duration { static constexpr bool value = false; };
	template<typename Rep, typename Period>		struct is_chrono_duration<std::chrono::duration<Rep, Period>> { static constexpr bool value = true; };

	std::string FormattedDuration(std::chrono::steady_clock::duration t, const TimeFormat f = _hh, const std::streamsize setwsize = 2) const
	{
		std::ostringstream buffer;
		if (f == _dd)
		{
			days dd = std::chrono::duration_cast<days>(t);
			std::chrono::hours hh = std::chrono::duration_cast<std::chrono::hours>(t % days(1));
			std::chrono::minutes mm = std::chrono::duration_cast<std::chrono::minutes>(t % std::chrono::hours(1));
			std::chrono::seconds ss = std::chrono::duration_cast<std::chrono::seconds>(t % std::chrono::minutes(1));
			buffer << std::setfill('0') << std::setw(setwsize) << dd.count() << ":";
			buffer << std::setfill('0') << std::setw(2) << hh.count() << ":";
			buffer << std::setfill('0') << std::setw(2) << mm.count() << ":";
			buffer << std::setfill('0') << std::setw(2) << ss.count();
		}
		else if (f == _hh)
		{
			std::chrono::hours hh = std::chrono::duration_cast<std::chrono::hours>(t);
			std::chrono::minutes mm = std::chrono::duration_cast<std::chrono::minutes>(t % std::chrono::hours(1));
			std::chrono::seconds ss = std::chrono::duration_cast<std::chrono::seconds>(t % std::chrono::minutes(1));
			std::chrono::milliseconds ms = std::chrono::duration_cast<std::chrono::milliseconds>(t % std::chrono::seconds(1));
			buffer << std::setfill('0') << std::setw(setwsize) << hh.count() << ":";
			buffer << std::setfill('0') << std::setw(2) << mm.count() << ":";
			buffer << std::setfill('0') << std::setw(2) << ss.count() << ".";
			buffer << std::setfill('0') << std::setw(3) << ms.count();
		}
		else if (f == _mm)
		{
			std::chrono::minutes mm = std::chrono::duration_cast<std::chrono::minutes>(t);
			std::chrono::seconds ss = std::chrono::duration_cast<std::chrono::seconds>(t % std::chrono::minutes(1));
			std::chrono::milliseconds ms = std::chrono::duration_cast<std::chrono::milliseconds>(t % std::chrono::seconds(1));
			buffer << std::setfill('0') << std::setw(setwsize) << mm.count() << ":";
			buffer << std::setfill('0') << std::setw(2) << ss.count() << ".";
			buffer << std::setfill('0') << std::setw(3) << ms.count();
		}
		else if (f == _ss)
		{
			std::chrono::seconds ss = std::chrono::duration_cast<std::chrono::seconds>(t);
			std::chrono::milliseconds ms = std::chrono::duration_cast<std::chrono::milliseconds>(t % std::chrono::seconds(1));
			std::chrono::microseconds us = std::chrono::duration_cast<std::chrono::microseconds>(t % std::chrono::milliseconds(1));
			buffer << std::setfill('0') << std::setw(setwsize) << ss.count() << ".";
			buffer << std::setfill('0') << std::setw(3) << ms.count() << ",";
			buffer << std::setfill('0') << std::setw(3) << us.count();
		}
		else if (f == _ms)
		{
			std::chrono::milliseconds ms = std::chrono::duration_cast<std::chrono::milliseconds>(t % std::chrono::seconds(1));
			std::chrono::microseconds us = std::chrono::duration_cast<std::chrono::microseconds>(t % std::chrono::milliseconds(1));
			std::chrono::nanoseconds ns = std::chrono::duration_cast<std::chrono::nanoseconds>(t % std::chrono::microseconds(1));
			buffer << std::setfill('0') << std::setw(setwsize) << ms.count() << ",";
			buffer << std::setfill('0') << std::setw(3) << us.count() << ",";
			buffer << std::setfill('0') << std::setw(3) << ns.count();
		}
		else if (f == _us)
		{
			std::chrono::microseconds us = std::chrono::duration_cast<std::chrono::microseconds>(t);
			std::chrono::nanoseconds ns = std::chrono::duration_cast<std::chrono::nanoseconds>(t % std::chrono::microseconds(1));
			buffer << std::setfill('0') << std::setw(setwsize) << us.count() << ",";
			buffer << std::setfill('0') << std::setw(3) << ns.count();
		}
		else if (f == _ns)
		{
			std::chrono::nanoseconds ns = std::chrono::duration_cast<std::chrono::nanoseconds>(t);
			buffer << std::setfill('0') << std::setw(setwsize) << ns.count() << ",";
		}
		else
			throw;

		return buffer.str();
	}

public:
	template<typename Dur = std::chrono::duration<double, std::chrono::seconds::period>>
	inline typename Dur::rep TotalChrono() const
	{
		static_assert(is_chrono_duration<Dur>::value, "template Duration must be a std::chrono::duration");
		return std::chrono::duration_cast<Dur>(_totalChronosI()).count();
	}
	inline double TotalDays() const { return TotalChrono<std::chrono::duration<double, days::period>>(); }
	inline double TotalHours() const { return TotalChrono<std::chrono::duration<double, std::chrono::hours::period>>(); }
	inline double TotalMinutes() const { return TotalChrono<std::chrono::duration<double, std::chrono::minutes::period>>(); }
	inline double TotalSeconds() const { return TotalChrono<std::chrono::duration<double, std::chrono::seconds::period>>(); }
	inline double TotalMilliseconds() const { return TotalChrono<std::chrono::duration<double, std::chrono::milliseconds::period>>(); }
	inline double TotalMicroseconds() const { return TotalChrono<std::chrono::duration<double, std::chrono::microseconds::period>>(); }
	inline double TotalNanoseconds() const { return TotalChrono<std::chrono::duration<double, std::chrono::nanoseconds::period>>(); }

	template<typename Dur = std::chrono::duration<double, std::chrono::seconds::period>>
	inline typename Dur::rep TotalChronoWithoutCurrentRun() const
	{
		static_assert(is_chrono_duration<Dur>::value, "template Duration must be a std::chrono::duration");
		return std::chrono::duration_cast<Dur>(_totalChronosE()).count();
	}
	inline double TotalDaysWithoutCurrentRun() const { return TotalChronoWithoutCurrentRun<std::chrono::duration<double, days::period>>(); }
	inline double TotalHoursWithoutCurrentRun() const { return TotalChronoWithoutCurrentRun<std::chrono::duration<double, std::chrono::hours::period>>(); }
	inline double TotalMinutesWithoutCurrentRun() const { return TotalChronoWithoutCurrentRun<std::chrono::duration<double, std::chrono::minutes::period>>(); }
	inline double TotalSecondsWithoutCurrentRun() const { return TotalChronoWithoutCurrentRun<std::chrono::duration<double, std::chrono::seconds::period>>(); }
	inline double TotalMillisecondsWithoutCurrentRun() const { return TotalChronoWithoutCurrentRun<std::chrono::duration<double, std::chrono::milliseconds::period>>(); }
	inline double TotalMicrosecondsWithoutCurrentRun() const { return TotalChronoWithoutCurrentRun<std::chrono::duration<double, std::chrono::microseconds::period>>(); }
	inline double TotalNanosecondsWithoutCurrentRun() const { return TotalChronoWithoutCurrentRun<std::chrono::duration<double, std::chrono::nanoseconds::period>>(); }

	std::string FormattedDurationLegend(const TimeFormat f = _hh) const
	{
		if (f == _dd)			return "dd:hh:mm:ss";
		else if (f == _hh)		return "hh:mm:ss.ms";
		else if (f == _mm)		return "mm,ss.ms";
		else if (f == _ss)		return "ss.ms,us";
		else if (f == _ms)		return "ms,us,ns";
		else if (f == _us)		return "us,ns";
		else if (f == _ns)		return "ns";
		else					throw;
	}

	std::string FormattedTotalChrono(const TimeFormat format = _hh, const std::streamsize st = 2) const { return FormattedDuration(_totalChronosI(), format, st); }
	std::string FormattedTotalChronoWithoutCurrentRun(const TimeFormat format = _hh, const std::streamsize st = 2) const { return FormattedDuration(_totalChronosE(), format, st); }

	// For backward compatibility ... //
	double Hours() const { return TotalHours(); }
	double Minutes() const { return TotalMinutes(); }
	double Seconds() const { return TotalSeconds(); }
	double MilliSeconds() const { return TotalMilliseconds(); }
	std::string FormattedTime() const { return FormattedTotalChrono(); }
};

class c_Stopwatch : public c_DisposableStopwatch
{
private:
	size_t _i_num_stops;
	std::chrono::steady_clock::duration _d_min_time;
	std::chrono::steady_clock::duration _d_max_time;

public:
	c_Stopwatch() : c_DisposableStopwatch(),
		_i_num_stops(static_cast<size_t>(0)),
		_d_max_time(_zeroDuration),
		_d_min_time(_maxDuration)
	{}

	~c_Stopwatch() {}
	
	void Restart()
	{
		assert(_isPaused);
		_isPaused = false;
		_start = std::chrono::steady_clock::now();
	}

	void Lap()
	{
		assert(!_isPaused);
		_stop = std::chrono::steady_clock::now();
		++_i_num_stops;
		_lapTime = _stop - _start;
		if (_lapTime < _d_min_time) _d_min_time = _lapTime;
		if (_lapTime > _d_max_time) _d_max_time = _lapTime;
		_totalTime += _lapTime;
		_start = _stop;
	}

	void Stop()
	{
		assert(!_isPaused);
		_stop = std::chrono::steady_clock::now();
		++_i_num_stops;
		_lapTime = _stop - _start;
		if (_lapTime < _d_min_time) _d_min_time = _lapTime;
		if (_lapTime > _d_max_time) _d_max_time = _lapTime;
		_totalTime += _lapTime;
		_isPaused = true;
	}

	void Reset()
	{
		_isPaused = true;
		_totalTime = _zeroDuration;
		_i_num_stops = static_cast<size_t>(0);
		_d_min_time = _maxDuration;
		_d_max_time = _zeroDuration;
	}

	inline size_t NumStops() const { return _i_num_stops; }

public:
	template<typename Dur = std::chrono::duration<double, std::chrono::seconds::period>>
	inline typename Dur::rep AvgChrono() const
	{
		static_assert(is_chrono_duration<Dur>::value, "template Duration must be a std::chrono::duration");
		//static_assert(std::numeric_limits<Dur::rep>::has_quiet_NaN, "template Duration::rep should be quiet NaN ready"); // program will crash if NaN not available for type Dur::rep.
		return (_i_num_stops == 0) ?
			//std::numeric_limits<Dur::rep>::quiet_NaN() :
			10000000:
			std::chrono::duration_cast<Dur>(_totalChronosE()).count() / _i_num_stops;
	}
	inline double AvgDays() const { return AvgChrono<std::chrono::duration<double, days::period>>(); }
	inline double AvgHours() const { return AvgChrono<std::chrono::duration<double, std::chrono::hours::period>>(); }
	inline double AvgMinutes() const { return AvgChrono<std::chrono::duration<double, std::chrono::minutes::period>>(); }
	inline double AvgSeconds() const { return AvgChrono<std::chrono::duration<double, std::chrono::seconds::period>>(); }
	inline double AvgMilliseconds() const { return AvgChrono<std::chrono::duration<double, std::chrono::milliseconds::period>>(); }
	inline double AvgMicroseconds() const { return AvgChrono<std::chrono::duration<double, std::chrono::microseconds::period>>(); }
	inline double AvgNanoseconds() const { return AvgChrono<std::chrono::duration<double, std::chrono::nanoseconds::period>>(); }

	template<typename Dur = std::chrono::duration<double, std::chrono::seconds::period>>
	inline typename Dur::rep MinChrono() const
	{
		static_assert(is_chrono_duration<Dur>::value, "duration must be a std::chrono::duration");
		return std::chrono::duration_cast<Dur>(_d_min_time).count();
	}
	inline double MinDays() const { return MinChrono<std::chrono::duration<double, days::period>>(); }
	inline double MinHours() const { return MinChrono<std::chrono::duration<double, std::chrono::hours::period>>(); }
	inline double MinMinutes() const { return MinChrono<std::chrono::duration<double, std::chrono::minutes::period>>(); }
	inline double MinSeconds() const { return MinChrono<std::chrono::duration<double, std::chrono::seconds::period>>(); }
	inline double MinMilliseconds() const { return MinChrono<std::chrono::duration<double, std::chrono::milliseconds::period>>(); }
	inline double MinMicroseconds() const { return MinChrono<std::chrono::duration<double, std::chrono::microseconds::period>>(); }
	inline double MinNanoseconds() const { return MinChrono<std::chrono::duration<double, std::chrono::nanoseconds::period>>(); }

	template<typename Dur = std::chrono::duration<double, std::chrono::seconds::period>>
	inline typename Dur::rep MaxChrono() const
	{
		static_assert(is_chrono_duration<Dur>::value, "duration must be a std::chrono::duration");
		return std::chrono::duration_cast<Dur>(_d_max_time).count();
	}
	inline double MaxDays() const { return MaxChrono<std::chrono::duration<double, days::period>>(); }
	inline double MaxHours() const { return MaxChrono<std::chrono::duration<double, std::chrono::hours::period>>(); }
	inline double MaxMinutes() const { return MaxChrono<std::chrono::duration<double, std::chrono::minutes::period>>(); }
	inline double MaxSeconds() const { return MaxChrono<std::chrono::duration<double, std::chrono::seconds::period>>(); }
	inline double MaxMilliseconds() const { return MaxChrono<std::chrono::duration<double, std::chrono::milliseconds::period>>(); }
	inline double MaxMicroseconds() const { return MaxChrono<std::chrono::duration<double, std::chrono::microseconds::period>>(); }
	inline double MaxNanoseconds() const { return MaxChrono<std::chrono::duration<double, std::chrono::nanoseconds::period>>(); }

	template<typename Dur = std::chrono::duration<double, std::chrono::seconds::period>>
	inline typename Dur::rep LapChrono() const
	{
		assert(_i_num_stops > 0);
		static_assert(is_chrono_duration<Dur>::value, "duration must be a std::chrono::duration");
		return std::chrono::duration_cast<Dur>(_lapTime).count();
	}
	inline double LapDays() const { return LapChrono<std::chrono::duration<double, days::period>>(); }
	inline double LapHours() const { return LapChrono<std::chrono::duration<double, std::chrono::hours::period>>(); }
	inline double LapMinutes() const { return LapChrono<std::chrono::duration<double, std::chrono::minutes::period>>(); }
	inline double LapSeconds() const { return LapChrono<std::chrono::duration<double, std::chrono::seconds::period>>(); }
	inline double LapMilliseconds() const { return LapChrono<std::chrono::duration<double, std::chrono::milliseconds::period>>(); }
	inline double LapMicroseconds() const { return LapChrono<std::chrono::duration<double, std::chrono::microseconds::period>>(); }
	inline double LapNanoseconds() const { return LapChrono<std::chrono::duration<double, std::chrono::nanoseconds::period>>(); }

	std::string FormattedAvgChrono(const TimeFormat format = _hh, const std::streamsize st = 2) const { return FormattedDuration((_i_num_stops != 0) ? std::chrono::duration_cast<std::chrono::nanoseconds>(_totalChronosE() / static_cast<double>(_i_num_stops)) : _zeroDuration, format, st); } // accept fractional nanoseconds loss.
	std::string FormattedMinChrono(const TimeFormat format = _hh, const std::streamsize st = 2) const { return FormattedDuration(_d_min_time, format, st); }
	std::string FormattedMaxChrono(const TimeFormat format = _hh, const std::streamsize st = 2) const { return FormattedDuration(_d_max_time, format, st); }

	// For backward compatibility
	double AvgMilliSeconds() const { return AvgMilliseconds(); }
	double MinMilliSeconds() const { return MinMilliseconds(); }
	double MaxMilliSeconds() const { return MaxMilliseconds(); }
	std::string FormattedAvgTime() const { return FormattedAvgChrono(); }
	std::string FormattedMinTime() const { return FormattedMinChrono(); }
	std::string FormattedMaxTime() const { return FormattedMaxChrono(); }
};

#endif // STOPWATCH_H