/*************** Includes **************************************************/

#include "settings_reader.h"

/**************** Implementation *******************************************/

#include <sstream>
#if defined(_WIN32) || defined(_WIN64)
#include <windows.h>
#include<tchar.h>
#endif

using namespace std;

c_Parameter::c_Parameter( const string param_name, int  def_val,	bool required )
: type( eInt ),
  s_name( param_name ),
	o_obj( new int( def_val ) ),
	b_required( required ),
	b_is_read( false )
{}


c_Parameter::c_Parameter( const string param_name, bool def_val,	bool required )
: type( eBool ),
  s_name( param_name ),
	o_obj( new bool( def_val ) ),
	b_required( required ),
	b_is_read( false )
{}

c_Parameter::c_Parameter( const string param_name, string def_val, bool required )
: type( eString ),
  s_name( param_name ),
	o_obj( new string(def_val) ),
	b_required( required ),
	b_is_read( false )
{}

c_Parameter::c_Parameter( const string param_name, double def_val,	bool required )
: type( eDouble ),
  s_name( param_name ),
	o_obj( new double( def_val ) ),
	b_required( required ),
	b_is_read( false )
{}

c_Parameter::c_Parameter( const string param_name, const list<int>& def_list, bool required )
: type( eIntList ),
  s_name( param_name ),
	o_obj( new list<int>( def_list ) ),
	b_required( required ),
	b_is_read( false )
{}

c_Parameter::c_Parameter(const string param_name, const list<double>& def_list, bool required)
	: type(eDoubleList),
	s_name(param_name),
	o_obj(new list<double>(def_list)),
	b_required(required),
	b_is_read(false)
{}


c_Parameter::c_Parameter( const c_Parameter& other ) 
: type( other.type ),
  s_name( other.s_name ),
	o_obj( NULL ),
	b_required( other.b_required ),
	b_is_read( other.b_is_read )
{
	switch( other.type ) {
		case eInt: 
			o_obj = new int( *((int*) other.o_obj) );
			break;
		case eDouble:
			o_obj = new double( *((double*) other.o_obj) );
			break;
		case eBool:
			o_obj = new bool( *((bool*) other.o_obj) );
			break;
		case eString:
			o_obj = new std::string( *(std::string*) other.o_obj );
			break;
		case eIntList:
			o_obj = new list<int>( *((list<int>*) other.o_obj) );
			break;
		case eDoubleList:
			o_obj = new list<double>(*((list<double>*) other.o_obj));
			break;
	}
}

c_Parameter::~c_Parameter() {
	switch ( type ) {
		case eInt:
			delete ((int*) o_obj);
			break;
		case eDouble:
			delete ((double*) o_obj);
			break;
		case  eBool:
			delete ((bool*) o_obj);
			break;
		case eString:
			delete ((string*) o_obj);
			break;
		case eIntList:
			delete ((list<int>*) o_obj);
			break;
		case eDoubleList:
			delete ((list<double>*) o_obj);
			break;
	}
}

// SetValue
void c_Parameter::SetValue( int val ) 
{
	if ( type != eInt )
		throw "Type Integer expected.";
	int& to_set = *((int*) o_obj); 
	to_set = val;
}

void c_Parameter::SetValue( bool val )
{
	if ( type != eBool )
		throw "Type Boolean expected.";
	bool& to_set = *((bool*) o_obj); 
	to_set = val;
}

void c_Parameter::SetValue( const char* val )
{
	if ( type != eString )
		throw "Type string expected.";
	string& to_set = *((string*) o_obj); 
	to_set = string(val);
}

void c_Parameter::SetValue( double val )
{
	if ( type != eDouble )
		throw "Type Double expected.";
	double& to_set = *((double*) o_obj); 
	to_set = val;
}

void c_Parameter::SetValue( const list<int>& val )
{
	if ( type != eIntList )
		throw "Type list<int> expected.";
	list<int>& to_set = *((list<int>*) o_obj); 
	to_set = val;
}

void c_Parameter::SetValue(const list<double>& val)
{
	if (type != eDoubleList)
		throw "Type list<double> expected.";
	list<double>& to_set = *((list<double>*) o_obj);
	to_set = val;
}

void c_Parameter::OutputInOStream( ostream& s ) const 
{
	if ( type != eIntList && type != eDoubleList )
		{
		switch( type ) {
			case eInt: 
				s << *((int*) o_obj);
				break;
			case eDouble:
				s << *((double*) o_obj);
				break;
			case eBool:
				s << (*((bool*) o_obj) ? "true" : "false" );
				break;
			case eString:
				s << "'" << *((string*) o_obj) << "'";
				break;
			}
		}
	else
	{
		if (type == eIntList)
		{
			list<int>& my_list = *((list<int>*) o_obj);
			list<int>::const_iterator i;
			for (i = my_list.begin(); i != my_list.end(); ++i)
				s << *i << " ";
		}
		else
		{
			list<double>& my_list = *((list<double>*) o_obj);
			list<double>::const_iterator i;
			for (i = my_list.begin(); i != my_list.end(); ++i)
				s << *i << " ";

		}
	}
}

//

ostream& operator<< ( ostream& os, const c_Parameter& param )
{
	param.OutputInOStream( os );
	return os;
}


/////////////////////////////////////////////////////////////////////////////

// Konstruktor
c_SettingsReader::c_SettingsReader( const string filename )
: b_unknown_parameters_allowed( false ),
  b_silent( true )
{
	s_filename = filename;
}


c_SettingsReader::~c_SettingsReader()
{
}


bool c_SettingsReader::ReadSettings() 
{
	// Open file for reading parameters
	ifstream file(s_filename.c_str());
	if (!file)
	{
		cout << "Can't read  settings file '" << s_filename << "'." << endl;
#if defined(_WIN32) || defined(_WIN64)
		TCHAR buffer[MAX_PATH];
		GetCurrentDirectory(MAX_PATH, buffer);
#if defined UNICODE
		wstring buffer_tmp(buffer);
		std::string whole_path(buffer_tmp.begin(), buffer_tmp.end());
		whole_path += "\\" + s_filename;
#else
		std::string whole_path = std::string(buffer) + "\\" + s_filename;
#endif
		std::size_t found = whole_path.find("/");
		while (found != std::string::npos)
		{
			whole_path.replace(found, 1, "\\");
			found = whole_path.find("/");
		}
		cout << "The whole path is '" << whole_path << "'." << endl;
#endif
		return false;
	}

	// Einlesen
	if (!b_silent)
		cout << "Reading parameters..." << endl << flush;
	string buffer;
	string param_name;
	string the_value;
	while ( !file.eof() ) 
	{
		getline(file,buffer,'\n');
		// empty lines
		if ( buffer.length() == 0 )
			continue;		
		// non-empty lines
		istringstream line( buffer, istringstream::in );
		line >> param_name;
		// comments
		if ( buffer[0] == '%' )
		{
			if ( !b_silent )
			{
				cout << "Line ignored..." << endl;
				cout << "'" << buffer << "'"<< endl;
			}
			continue;
		}

		// parameters...
		c_Parameter* param = Find( param_name.c_str() );
		if ( param == NULL )
		{
			// Error
			cout << "Parameter '" << param_name << "' unknown. Stop." << endl;
			if ( !b_unknown_parameters_allowed )
				throw;
		}
		if ( !param )
			continue;
		// is read!
		param->b_is_read = true;
		// Transfer value(s)
		line >> the_value;
		// Read value...
		istringstream valuestream(the_value);
		switch ( param->type ) {
			case c_Parameter::eInt:
			{
				int val = 0;
				valuestream >> val;
				param->SetValue( val );
				break;
			}
			case c_Parameter::eBool:
			{
				if ( the_value == "true" || the_value == "TRUE")
					param->SetValue( true );
				else if ( the_value == "false" || the_value == "FALSE")
					param->SetValue( false );
				break;
			}
			case c_Parameter::eDouble:
			{
				double val = 0;
				valuestream >> val;
				param->SetValue( val );
				break;
			}
			case c_Parameter::eString:
			{
				param->SetValue( the_value.c_str() );
				break;
			}
			case c_Parameter::eIntList:
			{
				list<int> liste;
				bool end = false;
				while ( !end )
				{	
					int number = -1;
					valuestream >> number;
					liste.push_back( number );
					end = line.eof();
					if ( !end )
					{
						line >> the_value;
						//FIXME do we need this line? makes problems!
						valuestream = istringstream(the_value);
					}
				}
				param->SetValue( liste );
				break;
			}
			case c_Parameter::eDoubleList:
			{
				list<double> liste;
				bool end = false;
				while (!end)
				{
					double number = -1;
					valuestream >> number;
					liste.push_back(number);
					end = line.eof();
					if (!end)
					{
						line >> the_value;
						//FIXME do we need this line? makes problems!
						valuestream = istringstream(the_value);
					}
				}
				param->SetValue(liste);
				break;
			}
		}
 		if ( !b_silent )
			cout << "SET " << param->s_name << " = " << *param << endl;
	}

	// check, if all required parameters are read
	bool error = false;
	set<c_Parameter*>::iterator par;
	for ( par=param.begin(); par!=param.end(); ++par )
		if ( !(*par)->b_is_read && (*par)->b_required )
		{
			cout << "Parameter '" << (*par)->s_name << "' is required." << endl;
			error = true;
		}
	if ( error )
	{
		cout << "I can't go on. Stop." << endl;
		file.close();
		throw;
	}
	file.close(); 

	if ( !b_silent )
		PrintAllParameters();
	return true;
}

// Neue Parameter hinzufügen
bool c_SettingsReader::AddParameter( c_Parameter* parameter )
{
	if ( param.find( parameter) != param.end() )
		{
		cout << "Parameter already defined." << endl;
		return false;
		}
	else
		param.insert( parameter );
	return true;
}


c_Parameter* c_SettingsReader::Find( const char* param_name )
{
	string search( param_name );
	set<c_Parameter*>::const_iterator par;
	for ( par=param.begin(); par!=param.end(); ++par )
		if ( search == string( (*par)->s_name ) )
			return *par;
	return NULL;
}

void c_SettingsReader::PrintAllParameters()
{
	// print all known parameters
	set<c_Parameter*>::const_iterator par;
	for ( par=param.begin(); par!=param.end(); ++par )
		{
		if ( (*par)->b_required ) 
			cout << "  ";
		else
			cout << " *";
		switch( (*par)->type ) {
		case c_Parameter::eInt:				cout << "int        ";
			break;
		case c_Parameter::eBool:			cout << "boolean    ";
			break;
		case c_Parameter::eString:		cout << "string";
			break;
		case c_Parameter::eDouble:		cout << "double     ";
			break;
		case c_Parameter::eIntList:	cout << "list<int>&   ";
			break;
		case c_Parameter::eDoubleList:	cout << "list<double>&   ";
			break;
		}
		cout << (*par)->s_name << " = " << *(*par) << endl;
		}
	cout << "* = optional parameter" << endl;
}
