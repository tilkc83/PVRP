#ifndef SETTINGS_READER_H
#define SETTINGS_READER_H


#pragma warning(disable:4786)

#include <list>
#include <set>
#include <string>
#include <fstream>
#include <iostream>


class c_Parameter {
	friend class c_SettingsReader;
	enum eType { eInt, eBool, eString, eDouble, eIntList, eDoubleList };
	eType type;
	std::string s_name;
	bool  b_required;
	bool  b_is_read;

	// Privat, also nicht erlaubt
	void operator=( const c_Parameter& other );

protected:
	void* o_obj;
public:
	// Konstruktoren
	c_Parameter( const std::string param_name, int  def_val,				bool required );
	c_Parameter( const std::string param_name, bool def_val,				bool required );
	c_Parameter( const std::string param_name, std::string def_val, bool required );
	c_Parameter( const std::string param_name, double def_val,			bool required );
	c_Parameter( const std::string param_name, const std::list<int>& def_list, bool required );
	c_Parameter(const std::string param_name, const std::list<double>& def_list, bool required);

	c_Parameter( const c_Parameter& other );
	~c_Parameter();

	
	// SetValue
	void SetValue( int val );
	void SetValue( bool val );
	void SetValue( const char* val );
	void SetValue( double val );
	void SetValue( const std::list<int>& val );
	void SetValue(const std::list<double>& val);

	virtual void OutputInOStream( std::ostream& s ) const;

	bool operator<( const c_Parameter& other )  { return s_name < other.s_name; }
	bool operator<( const c_Parameter& other ) const { return s_name < other.s_name; }
	bool operator==( const c_Parameter& other ) { return s_name == other.s_name; }
	bool operator!=( const c_Parameter& other ) { return s_name != other.s_name; }
	bool operator<=( const c_Parameter& other ) { return s_name <= other.s_name; }
};

class c_BoolParameter : public c_Parameter {
public:
	c_BoolParameter( std::string	param_name, bool def_val, bool required )
		: c_Parameter( param_name, def_val, required )
	{}

	bool operator()(void) const
	{ 
		return *((bool*) o_obj);
	}
};

class c_IntParameter : public c_Parameter {
public:
	c_IntParameter( std::string param_name, int def_val, bool required )
		: c_Parameter( param_name, def_val, required )
	{}

	int operator()(void) const
	{ 
		return *((int*) o_obj);
	}
};

class c_DoubleParameter : public c_Parameter {
public:
	c_DoubleParameter( std::string	param_name, double def_val, bool required )
		: c_Parameter( param_name, def_val, required )
	{}

	double operator()(void) const
	{ 
		return *((double*) o_obj);
	}
};

class c_StringParameter : public c_Parameter {
public:
	c_StringParameter( std::string param_name, std::string def_val, bool required )
		: c_Parameter( param_name, def_val, required )
	{}

	const std::string operator()(void) const
	{ 
		return *((std::string*) o_obj);
	}
};

class c_IntListParameter : public c_Parameter {
public:
	c_IntListParameter( std::string param_name, const std::list<int>& def_val, bool required )
		: c_Parameter( param_name, def_val, required )
	{}

	const std::list<int>& operator()(void) const
	{ 
		return *((std::list<int>*) o_obj);
	}
};

class c_DoubleListParameter : public c_Parameter {
public:
	c_DoubleListParameter(std::string param_name, const std::list<double>& def_val, bool required)
		: c_Parameter(param_name, def_val, required)
	{}

	const std::list<double>& operator()(void) const
	{
		return *((std::list<double>*) o_obj);
	}
};


class c_SettingsReader {
	bool b_unknown_parameters_allowed;
	std::string s_filename;
	bool b_silent;
	std::set<c_Parameter*> param;

	c_Parameter* Find( const char* param_name );
public:
	// Konstruktor
	c_SettingsReader( const std::string filename = "./settings.txt" );
	~c_SettingsReader();

	// Neue Parameter hinzufügen
	bool AddParameter( c_Parameter* param );

	// Eigentliches Lesen
	bool ReadSettings();
	void SetSilentMode( bool flag ) { b_silent = flag; }
	void SetUnknownParametersAllowed( bool flag ) { b_unknown_parameters_allowed = flag; }

	// Anzeigen aller verfügbaren Parameter
	void PrintAllParameters();
};

std::ostream& operator<< ( std::ostream& os, const c_Parameter& param );

#endif
