#include "output_formatter.h"

#include <fstream>
#include <time.h>

#if defined(_WIN32) || defined(_WIN64)
#include <winsock.h>
#include <tchar.h>
#else
#include <unistd.h>
#include <string.h>
#endif


namespace formatted_output {

	using namespace std;

	c_OutputField::c_OutputField(int pos, const std::string& header_line, c_OutputController* ctlr)
		: i_pos(pos),
		b_print(true),
		i_width(-1),
		c_fill(' ')
	{
		if (!p_ctlr)
			p_ctlr = new c_OutputController();
		p_ctlr->Add(this);
		// decompose into field and header(s)
		string work(header_line);
		for (auto idx = work.find('|'); idx != string::npos; /**/)
		{
			vs_field_header_line.push_back(work.substr(0, idx));
			work = work.substr(idx + 1, work.size() - idx - 1);
			idx = work.find('|');
		}
		if (!work.empty())
			vs_field_header_line.push_back(work);
	}

	void c_OutputField::OutputToFile(const std::string& filename)
	{
		if (p_ctlr)
		{
			p_ctlr->SetFilename(filename);
			p_ctlr->Output();
		}
	}

	void c_OutputField::PrintSeparator(ostream& stream) const
	{
		if (p_ctlr)
			p_ctlr->PrintSeparator(stream);
		else
			stream << "#";
	}

	c_OutputField& c_OutputField::PrintIf(bool flag)
	{
		b_print = flag;
		return *this;
	}

	c_OutputField& c_OutputField::SetWidth(int width)
	{
		i_width = width;
		return *this;
	}

	c_OutputField& c_OutputField::SetFill(char fill)
	{
		c_fill = fill;
		return *this;
	}

	c_OutputField::~c_OutputField()
	{
		delete p_ctlr; p_ctlr = NULL;
	}

	c_IntegerField::c_IntegerField(int pos, const string& header_line, int value, c_OutputController* ctlr)
		: c_OutputField(pos, header_line, ctlr),
		i_value(value)
	{
	}

	void c_IntegerField::Output(ostream& stream) const
	{
		stream << dec << i_value;
	}

	c_UnsignedField::c_UnsignedField(int pos, const std::string& header_line, unsigned int value, c_OutputController* ctlr)
		 : c_OutputField(pos, header_line),
		i_value(value)
	 {	}
	
	void c_UnsignedField::Output(ostream& stream) const
	{
		stream << dec << i_value;
	}

	c_Unsigned64Field::c_Unsigned64Field(int pos, const std::string& header_line, unsigned long long value, c_OutputController* ctlr)
		 : c_OutputField(pos, header_line),
		i_value(value)
		 {
	}
	
		void c_Unsigned64Field::Output(ostream& stream) const
		 {
		stream << dec << i_value;
		}

	c_BoolField::c_BoolField(int pos, const string& header_line, bool value)
		: c_OutputField(pos, header_line),
		b_value(value)
	{
	}

	void c_BoolField::Output(ostream& stream) const
	{
		stream << (b_value ? "true" : "false");
	}

	c_DoubleField::c_DoubleField(int pos, const string& header_line, double value, c_OutputController* ctlr)
		: c_OutputField(pos, header_line, ctlr),
		d_value(value)
	{
	}

	void c_DoubleField::Output(ostream& stream) const
	{
		stream << d_value;
	}


	c_TimeInfoField::c_TimeInfoField(int pos, const std::string& header_line, const c_TimeInfo& timer)
		: c_OutputField(pos, header_line),
		o_timer(timer)
	{
	}

	void c_TimeInfoField::Output(ostream& stream) const
	{
		stream << o_timer.NumStops();
		PrintSeparator(stream);
		stream << o_timer.AvgMilliSeconds();
		PrintSeparator(stream);
		stream << o_timer.MinMilliSeconds();
		PrintSeparator(stream);
		stream << o_timer.MaxMilliSeconds();
		PrintSeparator(stream);
		stream << o_timer.MilliSeconds();
		PrintSeparator(stream);
		stream << "[ms]";
		//PrintSeparator( stream );
	}

	std::string c_TimeInfoField::HeaderLine(int i) const
	{
		stringstream ss;
		if (i == 0)
		{
			ss << "Num";
			PrintSeparator(ss);
			ss << "Avg";
			PrintSeparator(ss);
			ss << "Min";
			PrintSeparator(ss);
			ss << "Max";
			PrintSeparator(ss);
			ss << "Total";
			PrintSeparator(ss);
			ss << "Units";
			//PrintSeparator( ss );
		}
		else
		{
			ss << c_OutputField::HeaderLine(i - 1);
			PrintSeparator(ss);
			PrintSeparator(ss);
			PrintSeparator(ss);
			PrintSeparator(ss);
			PrintSeparator(ss);
			//PrintSeparator( ss );
		}
		string s(ss.str());
		return s;
	}

	int c_TimeInfoField::NumHeaderLines() const
	{
		return (int)c_OutputField::NumHeaderLines() + 1;
	}

	c_StringField::c_StringField(int pos, const std::string& header_line, const string& text)
		: c_OutputField(pos, header_line),
		s_text(text)
	{
	}

	void c_StringField::Output(ostream& stream) const
	{
		stream << s_text;
	}

	c_TimeStampField::c_TimeStampField(int pos, const std::string& header_line)
		: c_OutputField(pos, header_line)
	{}

	void c_TimeStampField::Output(std::ostream& stream) const
	{
		//time_t x = time(NULL);
		//struct tm* tmptr = localtime(&x);
		//stream << setw(3) << int(tmptr->tm_year+1900)/10 << setw(1) << int(tmptr->tm_year+1900)%10 << "-";
		//stream << setfill('0') << setw(2) << tmptr->tm_mon+1 << "-";
		//stream << setfill('0') << setw(2) << tmptr->tm_mday << " ";
		//stream << setfill('0') << setw(2) << tmptr->tm_hour << ":";
		//stream << setfill('0') << setw(2) << tmptr->tm_min << ":";
		//stream << setfill('0') << setw(2) << tmptr->tm_sec; 

		time_t x;
		time(&x);
		struct tm tmptr;
		#if defined(_WIN32) || defined(_WIN64)
		localtime_s(&tmptr, &x);
		#else
						// e.g. on Linux
			localtime_r(&x, &tmptr);
		#endif

		stream << setw(3) << int(tmptr.tm_year + 1900) / 10 << setw(1) << int(tmptr.tm_year + 1900) % 10 << "-";
		stream << setfill('0') << setw(2) << tmptr.tm_mon + 1 << "-";
		stream << setfill('0') << setw(2) << tmptr.tm_mday << " ";
		stream << setfill('0') << setw(2) << tmptr.tm_hour << ":";
		stream << setfill('0') << setw(2) << tmptr.tm_min << ":";
		stream << setfill('0') << setw(2) << tmptr.tm_sec;

		// TODO: ms missing
		/* << ",";
		stream << setfill('0') << setw(3) << 0; */
	}

	c_HostnameField::c_HostnameField(int pos, const std::string& header_line)
		: c_OutputField(pos, header_line)
	{}


	void c_HostnameField::Output(ostream& stream) const
	{
		char Name[150];
		int i = 0;
#if defined(_WIN32) || defined(_WIN64)
		TCHAR infoBuf[150];
		DWORD bufCharCount = 150;
		memset(Name, 0, 150);
		if (GetComputerName(infoBuf, &bufCharCount))
			for (i = 0; i<150; i++)
				Name[i] = (char)infoBuf[i];
		else
			strcpy_s(Name, "Unknown_Host_Name");
#else
		memset(Name, 0, 150);
		gethostname(Name, 150);
#endif
		stream << Name;
	}

	c_OutputController::c_OutputController()
		: s_filename(""),
		s_sep(";")
	{
	}

	void c_OutputController::Output()
	{
		bool new_file = false;
		ifstream temp(s_filename.c_str());
		if (!temp) new_file = true;
		else temp.close();

		ofstream file(s_filename.c_str(), ios_base::app);
		locale myloc("");
		file.imbue(myloc);

#if defined(_WIN32) || defined(_WIN64)
		//check if output directory exists
		TCHAR buffer[MAX_PATH];
		GetCurrentDirectory(MAX_PATH, buffer);
#if defined UNICODE
		wstring buffer_tmp(buffer);
		std::string working_directory(buffer_tmp.begin(), buffer_tmp.end());
#else
		std::string working_directory = std::string(buffer);
#endif
		std::string missing_directory = working_directory + "\\" + s_filename;
		std::size_t found = missing_directory.find("/");
		while (found != std::string::npos)
		{
			missing_directory.replace(found, 1, "\\");
			found = missing_directory.find("/");
		}
		std::string whole_path = missing_directory;
		found = missing_directory.rfind("\\");
		missing_directory = missing_directory.substr(0, found);
#if defined UNICODE
		std::wstring missing_directory_tmp(missing_directory.begin(),missing_directory.end());
		const TCHAR* my_file = missing_directory_tmp.c_str();
#else
		const char* my_file = missing_directory.c_str();
#endif
		DWORD dwAttrib = GetFileAttributes(my_file);
		if (dwAttrib != INVALID_FILE_ATTRIBUTES && (dwAttrib & FILE_ATTRIBUTE_DIRECTORY))//directory exists
		{
			if (!file.is_open())
			{
				cout << "Attention: Can't change file '" << s_filename << "'!    -->    Is it write-protected?" << endl;
				cout << "           The whole path is '" << whole_path << "'." << endl;
				cout << "           Please close the file if you want to change it!" << endl;
				cout << "Press enter to continue!" << endl;
				cin.get();
				file.open(s_filename.c_str(), ios_base::app);
			}
		}
		else //directory doesn't exist
		{
			GetModuleFileName(NULL, buffer, MAX_PATH);
#if defined UNICODE
			wstring buffer_tmp(buffer);
			std::string f(buffer_tmp.begin(), buffer_tmp.end());
#else
			std::string f = std::string(buffer);
#endif
			cout << "error:               Output directory does not exist!" << endl;
			cout << "missing directory:   " << missing_directory << endl;
			cout << "working directory:   " << working_directory << endl;
			cout << "filename:            " << s_filename << endl;
			cout << "exe-path:            " << f << endl;
			throw;
		}
#endif

		l_field.sort();
		// headers
		if (new_file)
		{
			file << "sep=;" << endl;
			int max_header_lines = 0;
			for (list<pair<int, const c_OutputField*> >::const_iterator ii = l_field.begin(); ii != l_field.end(); ++ii)
				max_header_lines = max(max_header_lines, ii->second->NumHeaderLines());
			for (int line = max_header_lines - 1; line >= 0; line--)
			{
				for (list<pair<int, const c_OutputField*> >::const_iterator ii = l_field.begin(); ii != l_field.end(); ++ii)
				{
					const c_OutputField* field = ii->second;
					if (line < field->NumHeaderLines())
					{
						file << setfill(field->Fill());
						if (field->Width() >= 1)
							file << setw(field->Width());
						file << field->HeaderLine(line);
					}
					// separator only if another entry is following
					list<pair<int, const c_OutputField*> >::const_iterator test = ii; test++;
					if (test != l_field.end())
						file << s_sep;
				}
				file << endl;
			}
		}
		// contents
		for (list<pair<int, const c_OutputField*> >::const_iterator ii = l_field.begin(); ii != l_field.end(); ++ii)
		{
			const c_OutputField* field = ii->second;
			file << setfill(field->Fill());
			if (field->Width() >= 1)
				file << setw(field->Width());
			if (field->Print())
				field->Output(file);
			else
				file << "";
			// separator only if another entry is following
			list<pair<int, const c_OutputField*> >::const_iterator test = ii; test++;
			if (test != l_field.end())
				file << s_sep;
		}
		file << endl;
		// end
		file.close();
	}

	void c_OutputController::PrintSeparator(ostream& stream) const
	{
		stream << s_sep;
	}

	void c_OutputController::Add(const c_OutputField* field)
	{
		l_field.push_back(make_pair(field->Position(), field));
	}

	c_OutputController* c_OutputField::p_ctlr = NULL;


}; // of namespace formatted_output