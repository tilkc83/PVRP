#include "maxflow_mincut.h"

#include <deque>
#include <limits>
#include <iostream>
#include <algorithm>
#include <functional>


#include "quick_union.h"

// FOR DEBUGGING
//#include <sstream>
//#include "../Graphml/graphml.h"

using namespace std;

namespace maxflow_mincut
{

/**********************************************************************************************************************
class CGomoryHuMinCutAlg
**********************************************************************************************************************/

c_GomoryHuMinCutAlg::c_GomoryHuMinCutAlg( const vector<c_WeightedEdge>& VecEdges ) 
:	active( 0 ), 
	number( 0 ), 
	max_dist( 0L ), 
	bound( 0L ), 
	co_check( TRUE ),
	gr( 0 ),
	b_tree_computed(false),
	v_weighted_edges( VecEdges)
{
	if ( VecEdges.size() == 0 )
		return;

	int max_node_idx = 0;
	for ( unsigned v=0; v<VecEdges.size(); v++ )
		max_node_idx = std::max( max_node_idx, std::max( VecEdges[v].i_tail, VecEdges[v].i_head ) );
	int num_nodes = 0;
	old_idx_to_new_idx = vector<int>( max_node_idx+1, -1 );
	for ( unsigned e=0; e<VecEdges.size(); e++ )
	{
		if ( old_idx_to_new_idx[VecEdges[e].i_tail] < 0 )
			old_idx_to_new_idx[VecEdges[e].i_tail] = num_nodes++;
		if ( old_idx_to_new_idx[VecEdges[e].i_head] < 0 )
			old_idx_to_new_idx[VecEdges[e].i_head] = num_nodes++;
	}
	new_idx_to_old_idx = vector<int>( num_nodes );
	for ( unsigned v=0; v<old_idx_to_new_idx.size(); v++ )
		if ( old_idx_to_new_idx[v] >= 0 )
			new_idx_to_old_idx[old_idx_to_new_idx[v]] = v;

	long n = num_nodes;
	long m = (long) VecEdges.size();
	long m0 = 0L;
	long i = 0L;
	long j = 0L;
	long nod1 = 0L;
	long nod2 = 0L;
	double cap = 0.0;
	node* nptr = 0;
	node* nptr1 = 0;
	node* nptr2 = 0;
	edge* eptr1 = 0;
	edge* eptr2 = 0;

	gr = (graph*) malloc( sizeof( graph ) );
	if (!gr)
	{
		cerr << "Memory allocation failed in CGomoryHuMinCutAlg::CGomoryHuMinCutAlg()";
		throw;
	}
	gr->nodes = (node*) malloc( n * sizeof( node ) );
	if (!gr->nodes)
	{
		cerr << "Memory allocation failed in CGomoryHuMinCutAlg::CGomoryHuMinCutAlg()";
		throw;
	}
	gr->edges = (edge*) malloc( 2L * m * sizeof( edge ) );
	if (!gr->edges)
	{
		cerr << "Memory allocation failed in CGomoryHuMinCutAlg::CGomoryHuMinCutAlg()";
		throw;
	}
	gr->n_nodes = n;
	gr->n_edges0 = m;

	for( i = n, nptr = &(gr->nodes[n - 1]); i > 0L; --nptr, --i )
	{
		nptr->id = i;
		nptr->first_edge = NILE;
	}

	eptr1 = &(gr->edges[0L]);
	eptr2 = &(gr->edges[m]);

	for( j = 0L; j < m; j++ )
	{
		cap = VecEdges[j].d_weight;
		if( cap > EPS )
		{
			nod1 = old_idx_to_new_idx[ VecEdges[j].i_tail ];
			nod2 = old_idx_to_new_idx[ VecEdges[j].i_head ];
			nptr1 = &(gr->nodes[nod1]);
			nptr2 = &(gr->nodes[nod2]);
			eptr1->adjac = nptr2;
			eptr2->adjac = nptr1;
			eptr1->cap = cap;
			eptr2->cap = cap;
			eptr1->back = eptr2;
			eptr2->back = eptr1;
			if( nptr1->first_edge == NILE )
			{
				nptr1->first_edge = eptr1;
				eptr1->next = NILE;
			}
			else
			{
				eptr1->next = nptr1->first_edge;
				nptr1->first_edge = eptr1;
			}
			if( nptr2->first_edge == NILE )
			{
				nptr2->first_edge = eptr2;
				eptr2->next = NILE;
			}
			else
			{
				eptr2->next = nptr2->first_edge;
				nptr2->first_edge = eptr2;
			}
			++eptr1;
			++eptr2;
		}
		else
		{
			// zero capacity edge not put into edge lists of its incident nodes, just counted
			m0++;
		}
		gr->n_edges = m - m0;
	}
}


c_GomoryHuMinCutAlg::~c_GomoryHuMinCutAlg()
{
	if ( gr )
	{
		free( gr->edges );
		free( gr->nodes );
		free( gr );
	}
}


bool c_GomoryHuMinCutAlg::GetShore( int vertex, vector<bool>& in_S )
{
	if( gr->n_nodes < 2 || gr->n_edges < 1 )
		return false;
	if ( vertex < 1 || vertex >= gr->n_nodes ) 
		return false; // root node does not represent an edge
	if( !b_tree_computed && !ghc_tree( gr ) )
		return false;
	b_tree_computed = true;

	long id = gr->nodes[vertex].id;
	vector<vector<int> > iVecVecStars( gr->n_nodes );
	// start from 1, because the parent of the first node is the first node, see ghc_tree function
	for( int i = 1; i < gr->n_nodes; ++i )
	{
		node* nptr = &gr->nodes[i];
		// do not add edge defining minimum cut
		if( nptr->id != id )
			iVecVecStars[nptr->parent->id - 1].push_back( i );
	}
	// now iVecStars contains all edges of the minimum cut tree, except for the edge defining the minimum cut

	// now, simple depth-first search
	// the vertices reachable from vertex MinCapVertexID - 1 form one side of the cut
	if (in_S.size() < old_idx_to_new_idx.size())
		in_S.resize(old_idx_to_new_idx.size());
	for (int i = 0;i<(int)in_S.size(); i++ )
		in_S[i] = false;
	in_S[new_idx_to_old_idx[id - 1]] = true;
	// container of indices of vertices whose stars still need to be examined
	deque<int> iDequeCurStars;
	iDequeCurStars.push_back( id - 1 );
	while( iDequeCurStars.size() )
	{
		int iCurStar = iDequeCurStars[0];
		iDequeCurStars.pop_front();
		int iCurStarSize = static_cast<int>( iVecVecStars[iCurStar].size() );
		for( int i = 0; i < iCurStarSize; ++i )
		{
			in_S[ new_idx_to_old_idx[ iVecVecStars[iCurStar][i] ] ] = true;
			iDequeCurStars.push_back( iVecVecStars[iCurStar][i] );
		}
	}
	return true;
}


bool c_GomoryHuMinCutAlg::ComputeMinCut(vector<int>& S, vector<int>& S_compl, double& dMaxFlow)
{
	S.clear();
	S_compl.clear();
	dMaxFlow = 0.0;

	if( gr->n_nodes == 1 )
		S.push_back( 0 );

	if( gr->n_nodes < 2 || gr->n_edges < 1 )
		return false;

	if( !ghc_tree( gr ) )
		return false;

	node* nptr = 0; 

	dMaxFlow = numeric_limits<double>::max();
	long MinCapVertexId = 0;

	for( nptr = &(gr->nodes[gr->n_nodes - 1L]); nptr >= &(gr->nodes[1L]); nptr-- )
	{
		if( dMaxFlow > nptr->mincap )
		{
			dMaxFlow = nptr->mincap;
			MinCapVertexId = nptr->id;
		}
	}

	vector<vector<int> > iVecVecStars( gr->n_nodes );

	// start from 1, because the parent of the first node is the first node, see ghc_tree function
	for( int i = 1; i < gr->n_nodes; ++i )
	{
		nptr = &gr->nodes[i];
		// do not add edge defining minimum cut
		if( nptr->id != MinCapVertexId )
			iVecVecStars[nptr->parent->id - 1].push_back( i );
	}
	// now iVecStars contains all edges of the minimum cut tree, except for the edge defining the minimum cut

	// now, simple depth-first search
	// the vertices reachable from vertex MinCapVertexID - 1 form one side of the cut
	vector<bool> bVecReachable( gr->n_nodes, false );
	bVecReachable[MinCapVertexId - 1] = true;
	// container of indices of vertices whose stars still need to be examined
	deque<int> iDequeCurStars;
	iDequeCurStars.push_back( MinCapVertexId - 1 );
	while( !iDequeCurStars.empty() )
	{
		int iCurStar = iDequeCurStars[0];
		iDequeCurStars.pop_front();
		int iCurStarSize = static_cast<int>( iVecVecStars[iCurStar].size() );
		for( int i = 0; i < iCurStarSize; ++i )
		{
			bVecReachable[iVecVecStars[iCurStar][i]] = true;
			iDequeCurStars.push_back( iVecVecStars[iCurStar][i] );
		}
	}

	int ibVecReachableSize = (int) bVecReachable.size();
	for( int i = 0; i < ibVecReachableSize; ++i )
	{
		if( bVecReachable[i] )
			S.push_back(new_idx_to_old_idx[i] );
		else
			S_compl.push_back(new_idx_to_old_idx[i] );
	}
	// DEBUGGING
	vector<bool> in_S(old_idx_to_new_idx.size(), false);
	for (auto s : S)
		in_S[s] = true;
	double value = 0.0;
	for (auto e : v_weighted_edges)
		if (in_S[e.i_tail] != in_S[e.i_head])
			value += e.d_weight;
	if (fabs(value - dMaxFlow) > 0.0001)
	{
		cout << "Flow value is incorrectly computed" << endl;
		throw;
	}
	return true;
}


BOOL c_GomoryHuMinCutAlg::init_maxflow( long n )
{
/**********************************************************************************************************************
ZIB code
**********************************************************************************************************************/
  active = (node **) malloc ((n+1L) * sizeof (node *));
    /* holds stacks of active nodes arranged by distances */ 
  if (active == (node **) 0)
   { printf ("Unable to allocate memory\n");
     return (FALSE);
   }
  number = (long *) malloc ((n+1L) * sizeof (long));
    /* counts occurences of node distances in set 
       of alive nodes, i.e. nodes not contained in
       the set of nodes disconnected from the sink */ 
  if (number == (long *) 0)
   { printf ("Unable to allocate memory\n");
     return (FALSE);
   }
  co_check = TRUE;
  return (TRUE);
/**********************************************************************************************************************
end ZIB code
**********************************************************************************************************************/
}

void c_GomoryHuMinCutAlg::fini_maxflow()
{
/**********************************************************************************************************************
ZIB code
**********************************************************************************************************************/
  free (active);
  free (number);
/**********************************************************************************************************************
end ZIB code
**********************************************************************************************************************/
}


void c_GomoryHuMinCutAlg::global_relabel( graph* gr, node* tptr )
{
/**********************************************************************************************************************
ZIB code
**********************************************************************************************************************/
  /* breadth first search to get exact distance labels
     from sink with reordering of stack of active nodes */

  node *front, *rear, *nptr, **ptr;
  edge *eptr;
  long n, level, count, i;

  n = gr->n_nodes;
  for (nptr = &(gr->nodes[n-1L]); nptr >= gr->nodes; nptr--)
    { nptr->unmarked = TRUE;
      nptr->stack_link = NILN;
      nptr->scan_ptr = nptr->first_edge;
    }
  tptr->unmarked = FALSE;
     /* initialize stack of active nodes */
  for (ptr = &(active[n]); ptr >= active; ptr--)
    *ptr = NILN;
  for (i = 0L; i <= n; i++)
    number[i] = 0L;
  max_dist = 0L;
  count = 1L;     /* number of alive nodes */
  front = tptr;
  rear = front;

 bfs_next:
  level = rear->dist + 1L;
  eptr = rear->first_edge;
  while (eptr != NILE)
     { nptr = eptr->adjac;
       if (nptr->alive && nptr->unmarked
	               && eptr->back->rcap > EPS) 
	{ nptr->unmarked = FALSE;
	  nptr->dist = level;
	  ++count;
	  ++number[level];
	  if (nptr->excess > EPS)
           { nptr->stack_link = active[level];
	     active[level] = nptr;
	     max_dist = level;
           }
	  front->bfs_link = nptr;
	  front = nptr;
        }
       eptr = eptr->next;
     }
  if (front == rear)
    goto bfs_ready;
       
  rear = rear->bfs_link;
  goto bfs_next;

 bfs_ready: 

  if (count < bound)
   { /* identify nodes that are marked alive but have
	not been reached by BFS and mark them as dead  */
     for (nptr = &(gr->nodes[n-1L]); nptr >= gr->nodes; nptr--)
       if (nptr->unmarked && nptr->alive)
        { nptr->dist = n;
          nptr->alive = FALSE;
        }
     bound = count;
   }
/**********************************************************************************************************************
end ZIB code
**********************************************************************************************************************/
}


double c_GomoryHuMinCutAlg::maxflow( graph* gr, node* s_ptr, node* t_ptr )
{
/**********************************************************************************************************************
ZIB code
**********************************************************************************************************************/
  /* Determines maximum flow and minimum cut between nodes
     s (= *s_ptr) and t (= *t_ptr) in an undirected graph  

     References:
     ----------
     A. Goldberg/ E. Tarjan: "A New Approach to the
     Maximum Flow Problem", in Proc. 18th ACM Symp. 
     on Theory of Computing, 1986.
  */
  node *aptr, *nptr, *q_front, *q_rear;
/**********************************************************************************************************************
end ZIB code
**********************************************************************************************************************/
	// removed
	//node **ptr;
/**********************************************************************************************************************
ZIB code
**********************************************************************************************************************/
  edge *eptr;
  long n, m, m0, level, i, n_discharge;
  double incre;
/**********************************************************************************************************************
end ZIB code
**********************************************************************************************************************/
	//long count, dist, dmin, *upper, *lower;
	// replaced by 
	long dmin;
/**********************************************************************************************************************
ZIB code
**********************************************************************************************************************/
  double cap;
/**********************************************************************************************************************
end ZIB code
**********************************************************************************************************************/
	// removed
	//char any;
/**********************************************************************************************************************
ZIB code
**********************************************************************************************************************/
   
  /* node ids range from 1 to n, node array indices  
     range from 0 to n-1                             */

  n = gr->n_nodes;
  for (nptr = &(gr->nodes[n-1L]); nptr >= gr->nodes; nptr--)
    {  nptr->scan_ptr = nptr->first_edge;
       if (nptr->scan_ptr == NILE)
	{ fprintf (stderr, "isolated node in input graph\n");
	  return (FALSE);
        }
       nptr->excess = 0.0L;
       nptr->stack_link = NILN;
       nptr->alive = TRUE;
       nptr->unmarked = TRUE;
    }
  m = gr->n_edges;
  m0 = gr->n_edges0;
  for (eptr = &(gr->edges[m-1L]); eptr >= gr->edges; eptr--)
     eptr->rcap = eptr->cap;
  for (eptr = &(gr->edges[m0+m-1L]); eptr >= &(gr->edges[m0]); eptr--)
     eptr->rcap = eptr->cap;
      
  for (i = n; i >= 0L; i--)
    { number[i] = 0L;
      active[i] = NILN;
    }
  t_ptr->dist = 0L;

    /* breadth first search to get exact distances 
       from sink and for test of graph connectivity */

  t_ptr->unmarked = FALSE; 
  q_front = t_ptr;
  q_rear = q_front;
 bfs_next:
  level = q_rear->dist + 1L;
  eptr = q_rear->first_edge;
  while (eptr != NILE)
     { if (eptr->adjac->unmarked && eptr->back->rcap > EPS)
        { nptr = eptr->adjac;
	  nptr->unmarked = FALSE;
	  nptr->dist = level;
	  ++number[level];
          q_front->bfs_link = nptr;
	  q_front = nptr;
        }
       eptr = eptr->next;
     }
  if (q_rear == q_front)
    goto bfs_ready;

  q_rear = q_rear->bfs_link;
  goto bfs_next;

 bfs_ready:
  if (co_check)
   { co_check = FALSE;
     for (nptr = &(gr->nodes[n-1]); nptr >= gr->nodes; --nptr)
       if (nptr->unmarked)
        { 
					fprintf (stderr,"Input graph not connected\n");
          return (-1.0L);
        }
   }


  s_ptr->dist = n; /* number[0] and number[n] not required */
  t_ptr->dist = 0L;
  t_ptr->excess = 1.0L;  /* to be subtracted again */


  /* initial preflow push from source node */

  max_dist = 0L;  /* = max_dist of active nodes */
  eptr = s_ptr->first_edge;
  while (eptr != NILE)
     { nptr = eptr->adjac;
       cap = eptr->rcap;
       nptr->excess += cap;
       s_ptr->excess -= cap;
       eptr->back->rcap += cap;
       eptr->rcap = 0.0L;

       if (nptr != t_ptr && nptr->excess <= cap + EPS) 
        { /* push node nptr onto stack for nptr->dist,
	     but only once in case of double edges     */
	  nptr->stack_link = active[nptr->dist];
	  active[nptr->dist] = nptr;
          if (nptr->dist > max_dist)
	    max_dist = nptr->dist;
        }
       eptr = eptr->next;
     }

  s_ptr->alive = FALSE;
  bound = n; 
  n_discharge = 0L;

  /* main loop */

  do { /* get maximum distance active node */
       aptr = active[max_dist];
       while (aptr != NILN)
	 { active[max_dist] = aptr->stack_link;
	   eptr = aptr->scan_ptr;

           edge_scan:  /* for current active node  */
	     nptr = eptr->adjac;
             if (nptr->dist == aptr->dist - 1L &&
		 eptr->rcap > EPS) 
              { incre = aptr->excess;
	        if (incre <= eptr->rcap)
	         { /* perform a non saturating push */
	           eptr->rcap -= incre;
	           eptr->back->rcap += incre;
		   aptr->excess = 0.0L;
                   nptr->excess += incre;
		   if (nptr->excess <= incre + EPS)
		    { /* push nptr onto active stack */
		      nptr->stack_link = active[nptr->dist];
		      active[nptr->dist] = nptr;
                    }
                   aptr->scan_ptr = eptr;
		   goto node_ready;
                 }
                else
	         { /* perform a saturating push */
		   incre = eptr->rcap;
		   eptr->back->rcap += incre; 
		   aptr->excess -= incre;
		   nptr->excess += incre;
		   eptr->rcap = 0.0L;
		   if (nptr->excess <= incre + EPS)
		    { /* push nptr onto active stack */
                      nptr->stack_link = active[nptr->dist];
		      active[nptr->dist] = nptr;
                    }
		   if (aptr->excess <= EPS)
		    { aptr->scan_ptr = eptr;
		      goto node_ready;
                    }
                 }
              }
             if (eptr->next == NILE) 
              { /* all incident arcs scanned, but node still
		   has positive excess, check if for all nptr       
		   nptr->dist != aptr->dist                  */

                if (number[aptr->dist] == 1L)
		 { /* put all nodes v with dist[v] >= dist[a] 
		      into the set of "dead" nodes since they
		      are disconnected from the sink          */
                     
		   for (nptr = &(gr->nodes[n-1L]);
			nptr >= gr->nodes; nptr--)
                     if (nptr->alive &&
			 nptr->dist > aptr->dist)
		      { --number[nptr->dist];
			active[nptr->dist] = NILN; 
		        nptr->alive = FALSE; 
			nptr->dist = n;
			--bound;
		      }
                   --number[aptr->dist];
		   active[aptr->dist] = NILN;
		   aptr->alive = FALSE;
		   aptr->dist = n;
		   --bound;
		   goto node_ready;
                 }
	        else
		 { /* determine new label value */
                   dmin = n;
		   aptr->scan_ptr = NILE;
		   eptr = aptr->first_edge;
		   while (eptr != NILE)
		      { if (eptr->adjac->dist < dmin
			    && eptr->rcap > EPS)
                         { dmin = eptr->adjac->dist;
			   if (aptr->scan_ptr == NILE)
			     aptr->scan_ptr = eptr;
                         }
                        eptr = eptr->next;
                      }
		   if (++dmin < bound)
		    { /* ordinary relabel operation */
		      --number[aptr->dist];
		      aptr->dist = dmin;
		      ++number[dmin];
		      max_dist = dmin;
                      eptr = aptr->scan_ptr;
		      goto edge_scan;
                    }
                   else
		    { aptr->alive = FALSE;
		      --number[aptr->dist];
		      aptr->dist = n;
		      --bound;
		      goto node_ready;
                    }
                 }
              }
             else
	      { eptr = eptr->next;
                goto edge_scan;
              }

          node_ready: 
	   ++n_discharge;
	   if (n_discharge == n)
	    { n_discharge = 0L;
	      global_relabel (gr, t_ptr);
            }
	   aptr = active[max_dist];
         } /* aptr != NILN */ 
       --max_dist;
     } 
  while (max_dist > 0L);  

  return (t_ptr->excess - 1.0L);
/**********************************************************************************************************************
end ZIB code
**********************************************************************************************************************/
}


BOOL c_GomoryHuMinCutAlg::ghc_tree( graph* gr )
{
/**********************************************************************************************************************
ZIB code
**********************************************************************************************************************/
  /* Determines Gomory/Hu cut tree for input graph with
     capacitated edges, the tree structures is represented
     by parent pointers which are part of the node structure,
     the capacity of a tree edge is stored at the child node,
     the root of the cut tree is the first node in the list
     of graph nodes (&gr->nodes[0]). The implementation is
     described in [1].
     
     References:
     ----------
     1) D. Gusfield: "Very Simple Algorithms and Programs for
        All Pairs Network Flow Analysis", Computer Science Di-
        vision, University of California, Davis, 1987.
     
     2) R.E. Gomory and T.C. Hu: "Multi-Terminal Network Flows",
	SIAM J. Applied Math. 9 (1961), 551-570.

   */

  node *nptr, *nptr1, *nptrn, *sptr, *tptr;
/**********************************************************************************************************************
end ZIB code
**********************************************************************************************************************/
	// removed
	//edge *eptr;
/**********************************************************************************************************************
ZIB code
**********************************************************************************************************************/
  long n, m;
  double maxfl;

  
  n = gr->n_nodes;
  m = gr->n_edges;

  if (! init_maxflow (n))
    return (FALSE);

  nptr1 = gr->nodes;
  nptrn = &(gr->nodes[n-1L]);
  for (nptr = nptrn; nptr >= nptr1; nptr--)
    nptr->parent = nptr1;

  for (sptr = &(gr->nodes[1L]); sptr <= nptrn; sptr++)
    { tptr = sptr->parent;
      maxfl = maxflow (gr, sptr, tptr);
      if (maxfl < 0L)
	return (FALSE);
      
      sptr->mincap = maxfl;
      for (nptr = &(gr->nodes[1L]); nptr <= nptrn; nptr++)
	if (nptr != sptr &&  
	    ! nptr->alive && nptr->parent == tptr)
          nptr->parent = sptr;
      if (! tptr->parent->alive)
       { sptr->parent = tptr->parent;
	 tptr->parent = sptr;
	 sptr->mincap = tptr->mincap;
	 tptr->mincap = maxfl;
       }
    }
  fini_maxflow ();
/**********************************************************************************************************************
end ZIB code
**********************************************************************************************************************/
	return (TRUE);
}


double c_GomoryHuMinCutAlg::MaxFlow( int node_s, int node_t, vector<int>& S )
{
	long n = gr->n_nodes;
	long m = gr->n_edges;

	if (!init_maxflow (n))
	    return 0.0;
	
	S.clear();
	long s = old_idx_to_new_idx[node_s];
	long t = old_idx_to_new_idx[node_t];
	double max_flow = maxflow( gr, &(gr->nodes[s]), &(gr->nodes[t]) );
	for ( int i=0; i<n; i++ )
	{
		if ( gr->nodes[i].dist == n )
			S.push_back( new_idx_to_old_idx[i] );
	}
	
	fini_maxflow();
	return max_flow;
}


c_LetchfordReineltTheisAlg::c_LetchfordReineltTheisAlg(std::vector<bool>& odd_vertices, const std::vector<c_WeightedEdge>& weighted_edges)
:	v_odd_vertices(odd_vertices), 
	v_weighted_edges(weighted_edges)
{
	// check that all edges have weights between 0 and 1
	int largest_vertex = -1;
	for (auto edge : weighted_edges)
	{
		if (edge.d_weight > 1.0 + 1E-6)
			cout << "edge.d_weight = " << edge.d_weight << endl;
		assert(edge.d_weight <= 1.0 + 1E-6);
		assert(edge.d_weight >= 0.0 - 1E-6);
		largest_vertex = max( max(largest_vertex, edge.i_tail), edge.i_head);
	}
	// check that b-values are available for all vertices and that the sum of b-values is even
	bool odd = false;
	assert(largest_vertex < (int)odd_vertices.size());
	for (int i = 0; i < (int)odd_vertices.size(); i++)
		if ( odd_vertices[i] )
			odd = !odd;
	assert(!odd);
	// FOR DEGUGGING
	//graphml::c_GraphML g("LRT.graphml");
	//for (int i = 0; i < (int)odd_vertices.size(); i++)
	//{
	//	graphml::nodeproperties np;
	//	if (odd_vertices[i])
	//		np.shape = graphml::nodeproperties::diamond;
	//	g.AddNode(i, np);
	//}
	//for (auto edge : weighted_edges)
	//{
	//	if (edge.d_weight > 0.00001)
	//	{
	//		graphml::linkproperties lp;
	//		//stringstream mytext;
	//		//mytext << edge.d_weight;
	//		//lp.text = mytext.str().c_str();
	//		g.AddEdge(edge.i_tail, edge.i_head, lp);
	//	}
	//}
}


bool c_LetchfordReineltTheisAlg::ComputeMinOddCut(bool only_components, int max_num_results, vector<tuple<double, vector<int>, vector<int> > >& violation_S_F, double& max_flow_val)
{
	if (max_num_results < 0)
		max_num_results = numeric_limits<int>::max();
	violation_S_F.clear();

	const double eps = 0.00001;
	// Determine components
	int n = (int)v_odd_vertices.size();
//	vector<int> degree(n, 0);
	quick_union::c_QuickUnionFind qu(n);
	vector<bool> vertex_exists(n, false);
	vector<bool> flipped(v_weighted_edges.size());
	for (int e_idx = 0; e_idx < v_weighted_edges.size(); e_idx++)
	{
		const c_WeightedEdge& edge = v_weighted_edges[e_idx];
		vertex_exists[edge.i_tail] = vertex_exists[edge.i_head] = true;
		flipped[e_idx] = (edge.d_weight > 1.0 - edge.d_weight);
		double flow_val = min(edge.d_weight, 1.0 - edge.d_weight);
		if (flow_val > eps) // eps
		{
			c_WeightedEdge e(edge);
			e.d_weight = flow_val;
			qu.merge(edge.i_tail, edge.i_head);
		}
	}
	// is there a component that is odd?
	for (int i = 0; i < n; i++)
	{
		if (qu.root(i) == i && vertex_exists[i])
		{
			bool odd = false;
			set<int> comp(qu.component(i));
			for (auto j : comp)
				if (v_odd_vertices[j])
					odd = !odd;
			vector<bool> in_S(n, false);
			for (auto s : comp)
				in_S[s] = true;
			int f_critical = -1;
			double f_critical_val = numeric_limits<double>::max();
			for (int e_idx = 0; e_idx < v_weighted_edges.size(); e_idx++)
			{
				const c_WeightedEdge& edge = v_weighted_edges[e_idx];
				if (flipped[e_idx] && in_S[edge.i_tail] != in_S[edge.i_head])
					odd = !odd;
				double val = fabs(2.0*edge.d_weight - 1.0);
				if (in_S[edge.i_tail] != in_S[edge.i_head] && val < f_critical_val)
				{
					f_critical = e_idx;
					f_critical_val = val;
				}
			}
			vector<int> S(comp.begin(), comp.end());
			vector<int> F;
			bool critical_is_excluded = false;
			for (int e_idx = 0; e_idx < v_weighted_edges.size(); e_idx++)
				if (flipped[e_idx] && in_S[v_weighted_edges[e_idx].i_tail] != in_S[v_weighted_edges[e_idx].i_head])
				{
					if (odd)
						F.push_back(e_idx);
					else
					{
						if (e_idx == f_critical)
							critical_is_excluded = true;
						else
							F.push_back(e_idx);
					}
				}
			if (!odd && !critical_is_excluded)
				F.push_back(f_critical);
			double violation = 0.0;
			if (odd)
			{
				max_flow_val = 0.0;
				violation = 1.0;
			}
			else
			{
				max_flow_val = f_critical_val;
				violation = 1.0 - f_critical_val;
			}
			const double min_violation = 0.1;
			if ( violation > min_violation )
				violation_S_F.push_back(make_tuple(violation, S, F));
			if (violation_S_F.size() > max_num_results)
			{
				sort(violation_S_F.begin(), violation_S_F.end(), std::greater<tuple<double, vector<int>, vector<int> > >());
				violation_S_F.resize(max_num_results);
			}
		}
	}
	if (only_components)
		return !violation_S_F.empty();

	// otherwise we have to apply the algorithm by Letchford et al.
	max_flow_val = 1.0;
	for (int i = 0; i < n; i++)
	{
		if (qu.root(i) == i && vertex_exists[i])
		{
			set<int> comp(qu.component(i));
			vector<bool> in_comp(n, false);
			for (auto i : comp)
				in_comp[i] = true;
			vector<c_WeightedEdge> new_edges;
			vector<int> new_flipped;
			vector<int> new_to_old_idx;
			for (int e_idx = 0; e_idx < v_weighted_edges.size(); e_idx++)
				if (in_comp[v_weighted_edges[e_idx].i_tail] && in_comp[v_weighted_edges[e_idx].i_tail])
				{
					const c_WeightedEdge& edge = v_weighted_edges[e_idx];
					double flow_val = min(edge.d_weight, 1.0 - edge.d_weight);
					if (flow_val > eps) // eps
					{
						c_WeightedEdge e(edge);
						e.d_weight = flow_val;
						new_edges.push_back(e);
						new_flipped.push_back(edge.d_weight > 1.0 - edge.d_weight);
						new_to_old_idx.push_back(e_idx);
					}
				}
			c_GomoryHuMinCutAlg gh(new_edges);
			vector<bool> in_S(n, false);
			for (int i = 1; i < comp.size(); i++)
			{
				gh.GetShore(i, in_S);
				bool odd = false;
				for (int i = 0; i < (int)in_S.size(); i++)
					if (in_S[i] && v_odd_vertices[i] )
						odd = !odd;
				double flow = 0.0;
				for (int e_idx = 0; e_idx < v_weighted_edges.size(); e_idx++)
				{
					const c_WeightedEdge& edge = v_weighted_edges[e_idx];
					if (in_S[edge.i_tail] != in_S[edge.i_head])
					{
						double flow_val = min(edge.d_weight, 1.0 - edge.d_weight);
						flow += flow_val;
						if (edge.d_weight > 1.0 - edge.d_weight)
							odd = !odd;
					}
				}
				//for (int new_edge_idx = 0; new_edge_idx < new_edges.size(); new_edge_idx++)
				//	if (in_S[new_edges[new_edge_idx].i_tail] != in_S[new_edges[new_edge_idx].i_head])
				//	{
				//		flow += new_edges[new_edge_idx].d_weight;
				//		if (new_flipped[new_edge_idx])
				//			odd = !odd;
				//	}

				// 3. Find the best set $F\subseteq\delta(S)$ with $S$ + $|F|$ odd
				//    Flow value must be < 1.0
				int f_critical = -1;				
				if (!odd)
				{
					double f_critical_val = numeric_limits<double>::max();
					for (int e_idx = 0; e_idx < v_weighted_edges.size(); e_idx++)
					{
						const c_WeightedEdge& edge = v_weighted_edges[e_idx];
						if (in_S[edge.i_tail] != in_S[edge.i_head])
						{
							double flow_val = min(edge.d_weight, 1.0 - edge.d_weight);
							double val = fabs(2.0*flow_val - 1.0);
							if (val < f_critical_val)
							{
								f_critical = e_idx;
								f_critical_val = val;
							}
						}
					}
					//for (int new_edge_idx = 0; new_edge_idx < new_edges.size(); new_edge_idx++)
					//	if (in_S[new_edges[new_edge_idx].i_tail] != in_S[new_edges[new_edge_idx].i_head])
					//	{
					//		double val = fabs(2.0*new_edges[new_edge_idx].d_weight - 1.0);
					//		if (val < f_critical_val)
					//		{
					//			f_critical = new_edge_idx;
					//			f_critical_val = val;
					//		}
					//	}
					flow += f_critical_val;
				}
				double violation = 1.0 - flow;
				if (violation > eps && flow < max_flow_val) // eps
				{
					// store flow value, S, and F
					max_flow_val = flow;
					vector<int> S;
					for (int s = 0; s < in_S.size(); s++)
						if ( in_S[s] )
							S.push_back(s);
					vector<int> F;
					for (int e_idx = 0; e_idx < v_weighted_edges.size(); e_idx++)
					{
						const c_WeightedEdge& edge = v_weighted_edges[e_idx];
						bool flipped = (edge.d_weight > 1.0 - edge.d_weight);
						if (in_S[edge.i_tail] != in_S[edge.i_head])
						{
							if ( odd && flipped )
								F.push_back(e_idx);
							if ( !odd && flipped && ( e_idx != f_critical) )
								F.push_back(e_idx);
						}
						if ( !odd && e_idx == f_critical && !flipped )
							F.push_back(f_critical);
					}
					//for (int new_edge_idx = 0; new_edge_idx < new_edges.size(); new_edge_idx++)
					//{
					//	if (in_S[new_edges[new_edge_idx].i_tail] != in_S[new_edges[new_edge_idx].i_head])
					//	{
					//		if (new_flipped[new_edge_idx] && (!odd || new_edge_idx != f_critical))
					//			F.push_back(new_to_old_idx[new_edge_idx]);
					//	}
					//	if (new_edge_idx == f_critical && !odd)
					//		F.push_back(new_to_old_idx[f_critical]);
					//}
					violation_S_F.push_back(make_tuple(violation, S, F));
					if ((int)violation_S_F.size() > max_num_results)
					{
						sort(violation_S_F.begin(), violation_S_F.end(), std::greater<tuple<double, vector<int>, vector<int> > >() );
						violation_S_F.resize(max_num_results);
					}
				}
			}
		}
	}
	return !violation_S_F.empty();
}


bool c_LetchfordReineltTheisAlg::ComputeMinOddCut(bool only_components, std::vector<int>& S, std::vector<int>& F, double& dMaxFlow)
{
	int max_num_results = 1;
	vector<tuple<double, vector<int>, vector<int> > > violation_S_F;
	double max_flow_val = 0.0;
	bool ret_val = ComputeMinOddCut(only_components, max_num_results, violation_S_F, max_flow_val);
	if (violation_S_F.empty())
		return false;
	else
	{
		dMaxFlow = get<0>(violation_S_F.front());
		S = get<1>(violation_S_F.front());
		F = get<2>(violation_S_F.front());
		return ret_val;
	}
}

} // maxflow_mincut
