#ifndef MATRIX_H
#define MATRIX_H

#include <cstdio>
#include <cstring>
#include <iostream>

/****************************************************************************/

/**** CLASS  c_Matrix **********************************************************

---- DESCRIPTION   -------------------------------------------------------------
Template f�r bis zu 4-dimensionale Matrizen.
Verwendung: c_Matrix<long> my_matrix( 4, 3 ); 
my_matrix( 1, 2 ) = 54;
long zahl = my_matrix( 1, 2 )
Bei mehr oder weniger Dimensionen werden einfach mehr oder weniger Parameter
angegeben.

---- ATTRIBUTES  ---------------------------------------------------------------
T* daten		-	Feld, in dem die Daten gehalten werden		
int i_dim1	- Gr��e der ersten Dimension
int i_dim2	- Gr��e der zweiten Dimension
int i_dim3	- Gr��e der dritten Dimension
int i_dim4	- Gr��e der vierten Dimension

---- METHODS  ------------------------------------------------------------------
T& operator()	- R�ckgabe der Referenz auf Element der Matrix.

*******************************************************************************/

template <class T>
class c_Matrix
{
	T* daten;
	int i_dim1;
	int i_dim2;
	int i_dim3;
	int i_dim4;
public:
	c_Matrix( void )
	{
		i_dim1 = 0;
		i_dim2 = 0;
		i_dim3 = 0;
		i_dim4 = 0;
		daten  = NULL;
	}
	c_Matrix( int dim1, int dim2 = 1, int dim3 = 1, int dim4 = 1 )
	{
		i_dim1 = dim1;
		i_dim2 = dim2;
		i_dim3 = dim3;
		i_dim4 = dim4;
		daten = new T[i_dim1*i_dim2*i_dim3*i_dim4];
	};
	c_Matrix( const c_Matrix<T>& matrix )
	{
		i_dim1 = matrix.i_dim1;
		i_dim2 = matrix.i_dim2;
		i_dim3 = matrix.i_dim3;
		i_dim4 = matrix.i_dim4;
		int anzahl = i_dim1*i_dim2*i_dim3*i_dim4;
		daten = new T[anzahl];
		for ( int i = 0 ; i  < anzahl; i++ )
			daten[i] = matrix.daten[i];
	}
	~c_Matrix( void )
	{
		delete[] daten;
	};

	void Initialisierung( T wert )
	{
		int anzahl = i_dim1*i_dim2*i_dim3*i_dim4;
		for( int i=0; i<anzahl; i++ )
			daten[i] = wert;
	};

	int Speichern( FILE* datei )
	{
		int retVal, status;

		std::fwrite( &i_dim1, sizeof( int ), 1, datei );
		std::fwrite( &i_dim2, sizeof( int ), 1, datei );
		std::fwrite( &i_dim3, sizeof( int ), 1, datei );
		std::fwrite( &i_dim4, sizeof( int ), 1, datei );
		retVal = i_dim1*i_dim2*i_dim3*i_dim4;
		status = std::fwrite( daten, sizeof( T ), retVal, datei );

		return( retVal );
	};

	int StringsSpeichern( FILE* datei )
	{
		int i, length, retVal, status;
		char** tmpText;

		tmpText = (char**) daten;
		retVal = i_dim1;
		std::fwrite( &retVal, sizeof( int ), 1, datei );
		for( i=0; i<retVal; i++ )
		{
			if( tmpText[i] != NULL )
			{
				length = std::strlen( tmpText[i] );
				status = std::fwrite( &length, sizeof( int ), 1, datei );
				status = std::fwrite( tmpText[i], sizeof( char ), length, datei );
			}
			else
			{
				length = std::strlen( "" );
				status = std::fwrite( &length, sizeof( int ), 1, datei );
				status = std::fwrite( "", sizeof( char ), length, datei );
			}
		}
		return( retVal );
	};

	static c_Matrix<T>* Laden( FILE* datei )
	{
		int i1, i2, i3, i4, anzahl, status;
		c_Matrix<T>* retVal;

		std::fread( &i1, sizeof( int ), 1, datei );
		std::fread( &i2, sizeof( int ), 1, datei );
		std::fread( &i3, sizeof( int ), 1, datei );
		std::fread( &i4, sizeof( int ), 1, datei );
		retVal = new c_Matrix<T>( i1, i2, i3, i4 );
		anzahl = i1 * i2 * i3 * i4;
		status = std::fread( retVal->daten, sizeof( T ), anzahl, datei );

		return( retVal );
	};

	static c_Matrix<T>* StringsLaden( FILE* datei )
	{
		int i, length, status, dim1;
		char tmpText[1000];
		char** helpPo;
		c_Matrix<T>* retVal;

		std::fread( &dim1, sizeof( int ), 1, datei );
		retVal = new c_Matrix<T>( dim1, 1, 1, 1 );
		helpPo = (char**) retVal->daten;
		for( i=0; i<dim1; i++ )
		{
			status = std::fread( &length, sizeof( int ), 1, datei );
			status = std::fread( &tmpText, sizeof( char ), length, datei );
			tmpText[length] = 0;
			helpPo[i] = strdup( tmpText );
		}
		return( retVal );
	};

#ifdef _DEBUG
	void Check( int pos1, int pos2=0, int pos3=0, int pos4=0 ) const
	{
		if( ( pos1 >= i_dim1 ) || ( pos1 < 0 ) 
			|| ( pos2 >= i_dim2 ) || ( pos2 < 0 ) 
			|| ( pos3 >= i_dim3 ) || ( pos3 < 0 ) 
			|| ( pos4 >= i_dim4 ) || ( pos4 < 0 ) )
		{
			std::cerr << "Index out of bounds" << std::endl;
			throw;
		}
	}
	T& operator() (int pos1 )
	{
		Check( pos1 );
		return daten[pos1];
	};
	T& operator() (int pos1, int pos2)
	{
		Check( pos1, pos2 );
		return daten[pos1+i_dim1*pos2];
	};
	T& operator() (int pos1, int pos2 , int pos3 )
	{ 
		Check( pos1, pos2, pos3 );
		return daten[pos1+i_dim1*(pos2+i_dim2*pos3)];
	};
	T& operator() (int pos1, int pos2, int pos3, int pos4 )
	{
		Check( pos1, pos2, pos3, pos4 );
		return daten[pos1+i_dim1*(pos2+i_dim2*(pos3+i_dim3*pos4))];
	};

	T& operator() (int pos1 ) const
	{ 
		Check( pos1 );
		return daten[pos1];
	};
	T& operator() (int pos1, int pos2) const
	{
		Check( pos1, pos2 );
		return daten[pos1+i_dim1*pos2];
	};
	T& operator() (int pos1, int pos2 , int pos3 ) const
	{ 
		Check( pos1, pos2, pos3 );
		return daten[pos1+i_dim1*(pos2+i_dim2*pos3)];
	};
	T& operator() (int pos1, int pos2, int pos3, int pos4 ) const
	{
		Check( pos1, pos2, pos3, pos4 );
		return daten[pos1+i_dim1*(pos2+i_dim2*(pos3+i_dim3*pos4))];
	};
#endif

#ifndef _DEBUG
	T& operator() (int pos1 )
	{ return daten[pos1];};
	T& operator() (int pos1, int pos2)
	{ return daten[pos1+i_dim1*pos2];};
	T& operator() (int pos1, int pos2 , int pos3 )
	{ return daten[pos1+i_dim1*(pos2+i_dim2*pos3)];};
	T& operator() (int pos1, int pos2, int pos3, int pos4 )
	{ return daten[pos1+i_dim1*(pos2+i_dim2*(pos3+i_dim3*pos4))]; };

	T& operator() (int pos1 ) const 
	{ return daten[pos1];};
	T& operator() (int pos1, int pos2) const 
	{ return daten[pos1+i_dim1*pos2];};
	T& operator() (int pos1, int pos2 , int pos3 ) const 
	{ return daten[pos1+i_dim1*(pos2+i_dim2*pos3)];};
	T& operator() (int pos1, int pos2, int pos3, int pos4 ) const 
	{ return daten[pos1+i_dim1*(pos2+i_dim2*(pos3+i_dim3*pos4))]; };
#endif

	c_Matrix<T>& operator = ( const c_Matrix<T>& matrix )
	{
		delete [] daten;
		i_dim1 = matrix.Dimension1();
		i_dim2 = matrix.Dimension2();
		i_dim3 = matrix.Dimension3();
		i_dim4 = matrix.Dimension4();
		int anzahl = i_dim1*i_dim2*i_dim3*i_dim4;
		daten = new T[anzahl];
		for ( int i = 0; i < anzahl; i++ )
			daten[i] = matrix.daten[i];
		return( *this );
	}

	inline int Dimension1( void ) const { return i_dim1; };
	inline int Dimension2( void ) const { return i_dim2; };
	inline int Dimension3( void ) const { return i_dim3; };
	inline int Dimension4( void ) const { return i_dim4; };
};

typedef c_Matrix<char> c_CharMatrix;
typedef c_Matrix<int> c_IntMatrix;
typedef c_Matrix<long> c_LongMatrix;
typedef c_Matrix<float> c_FloatMatrix;
typedef c_Matrix<double> c_DoubleMatrix;
typedef c_Matrix<char*> c_StringMatrix;
typedef c_Matrix<unsigned> c_UnsignedMatrix;
typedef c_Matrix<bool> c_BoolMatrix;

extern c_Matrix<char> dummyCharMatrix;
extern c_Matrix<int> dummyIntMatrix;
extern c_Matrix<long> dummyLongMatrix;
extern c_Matrix<float> dummyFloatMatrix;
extern c_Matrix<double> dummyDoubleMatrix;
extern c_Matrix<char*> dummyStringMatrix;
extern c_Matrix<unsigned> dummyUnsignedMatrix;



#endif

//*** EOF *************/
