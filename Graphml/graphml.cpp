#include "graphml.h"

#include <iostream>
#include <fstream>
#include <ios>
#include <iomanip>

using namespace std;
using namespace graphml;

c_GraphML::c_GraphML(const char* filename)
	: yed(filename)
{
}

c_GraphML::~c_GraphML()
{
	WritePreamble();
	WriteNodes();
	WriteLinks();
	WriteEnd();
}

void c_GraphML::WritePreamble()
{
	unsigned num_nodes = (int)nodes.size();
	unsigned num_edges = (int)edges.size();
	// xml
	yed << "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?> " << endl;
	yed << "<graphml xmlns=\"http://graphml.graphdrawing.org/xmlns/graphml\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:y=\"http://www.yworks.com/xml/graphml\" xsi:schemaLocation=\"http://graphml.graphdrawing.org/xmlns/graphml http://www.yworks.com/xml/schema/graphml/1.0/ygraphml.xsd\"> " << endl;
	yed << "  <key for=\"node\" id=\"d0\" yfiles.type=\"nodegraphics\"/> " << endl;
	yed << "  <key attr.name=\"description\" attr.type=\"string\" for=\"node\" id=\"d1\"/> " << endl;
	yed << "  <key for=\"edge\" id=\"d2\" yfiles.type=\"edgegraphics\"/> " << endl;
	yed << "  <key attr.name=\"description\" attr.type=\"string\" for=\"edge\" id=\"d3\"/> " << endl;
	yed << "  <key for=\"graphml\" id=\"d4\" yfiles.type=\"resources\"/> " << endl;
	yed << "  <graph edgedefault=\"directed\" id=\"G\" parse.edges=\"" << num_edges << "\" parse.nodes=\"" << num_nodes << "\" parse.order=\"free\"> " << endl;
}

ostream& operator<<(ostream& out, const Color& col)
{
	out << "#";
	unsigned red = col.red();
	unsigned green = col.green();
	unsigned blue = col.blue();
	out << (red < 16 ? "0" : "") << hex << red;
	out << (green < 16 ? "0" : "") << hex << green;
	out << (blue < 16 ? "0" : "") << hex << blue;
	out << dec;
	return out;
}

void c_GraphML::WriteNodes()
{
	for (const auto& node : nodes)
	{
		int id = node.first;
		const nodeproperties& np = node.second;
		string shape("ellipse");
		switch (np.shape)
		{
		case nodeproperties::ellipse:			shape = "ellipse";			break;
		case nodeproperties::rectangle3d:		shape = "rectangle3d";		break;
		case nodeproperties::diamond:			shape = "diamond";			break;
		case nodeproperties::triangle:			shape = "triangle";			break;
		case nodeproperties::roundrectangle:	shape = "roundrectangle";	break;
		}
		// xml
		yed << "    <node id=\"n" << id << "\"> " << endl;
		yed << "      <data key=\"d0\"> " << endl;
		yed << "        <y:ShapeNode> " << endl;
		yed << "          <y:Geometry height=\"" << np.height << "\" width=\"" << np.width << "\" x=\"" << np.coord_x << "\" y=\"" << np.coord_y << "\"/>" << endl;
		yed << "          <y:Fill color=\"" << np.fillcolor << "\" color2=\"" << np.fillcolor2 << "\" transparent=\"false\"/> " << endl;
		yed << "          <y:BorderStyle color=\"" << np.bordercolor << "\" type=\"line\" width=\"1.0\"/> " << endl;
		yed << "          <y:NodeLabel alignment=\"center\" autoSizePolicy=\"content\" fontFamily=\"Dialog\" fontSize=\"12\" fontStyle=\"plain\" hasBackgroundColor=\"false\" hasLineColor=\"false\" height=\"18.701171875\" modelName=\"internal\" modelPosition=\"c\" textColor=\"#000000\" visible=\"true\" width=\"10.673828125\" x=\"9.6630859375\" y=\"5.6494140625\"> ";
		if (np.text.empty()) yed << id; else yed << np.text;
		yed << "</y:NodeLabel> " << endl;
		yed << "          <y:Shape type=\"" << shape << "\"/> " << endl;
		yed << "        </y:ShapeNode> " << endl;
		yed << "      </data> " << endl;
		yed << "      <data key=\"d1\"/> " << endl;
		yed << "    </node>" << endl;
	}
}

void c_GraphML::WriteLinks()
{
	int index = 0;
	for (const auto& edge : edges)
	{
		int tail = edge.first.first;
		int head = edge.first.second;
		const linkproperties& lp = edge.second;
		const string source_arrow((lp.orientation == linkproperties::bidir) ? "standard" : "none");
		const string target_arrow((lp.orientation == linkproperties::directed) ? "standard" : "none");
		// xml
		yed << "    <edge id=\"e" << index++ << "\" source=\"n" << tail << "\" target=\"n" << head << "\">" << endl;
		yed << "      <data key=\"d2\"> " << endl;
		yed << "        <y:PolyLineEdge>" << endl;
		yed << "          <y:Path sx=\"0.0\" sy=\"0.0\" tx=\"0.0\" ty=\"0.0\"/>" << endl;
		yed << "          <y:LineStyle color=\"" << lp.linecolor << "\" type=\"line\" width=\"" << lp.linewidth << "\"/> " << endl;
		yed << "          <y:Arrows source=\"" << source_arrow << "\" target=\"" << target_arrow << "\"/>" << endl;
		yed << "          <y:EdgeLabel alignment=\"center\" distance=\"2.0\" fontFamily=\"Dialog\" fontSize=\"12\" fontStyle=\"plain\" hasBackgroundColor=\"false\" hasLineColor=\"false\" height=\"2.0\" modelName=\"centered\" modelPosition=\"center\" preferredPlacement=\"anywhere\" ratio=\"0.5\" textColor=\"#000000\" visible=\"true\" width=\"2.0\" x=\"0.0\" y=\"0.0\">" << lp.text << "</y:EdgeLabel>" << endl;
		yed << "          <y:BendStyle smoothed=\"false\"/> " << endl;
		yed << "        </y:PolyLineEdge>" << endl;
		yed << "      </data>" << endl;
		yed << "      <data key=\"d3\"/> " << endl;
		yed << "    </edge>" << endl;
	}
}

void c_GraphML::WriteEnd()
{
	// xml
	yed << " </graph> " << endl;
	yed << " <data key=\"d4\"> " << endl;
	yed << " <y:Resources/> " << endl;
	yed << " </data> " << endl;
	yed << " </graphml> " << endl;
}

void c_GraphML::AddNode(int id, nodeproperties& p)
{
	nodes.emplace_back(id, p);
	//nodes.push_back( make_pair( id, p ) );
}

void c_GraphML::AddEdge(int id1, int id2, linkproperties& lp)
{
	lp.orientation = linkproperties::undir;
	edges.emplace_back(make_pair(id1, id2), lp);
	//edges.push_back( make_pair( make_pair(id1, id2), lp ) );
}

void c_GraphML::AddArc(int id1, int id2, linkproperties& lp)
{
	lp.orientation = linkproperties::directed;
	edges.emplace_back(make_pair(id1, id2), lp);
	//edges.push_back( make_pair( make_pair(id1, id2), lp ) );
}

