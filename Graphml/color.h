#ifndef COLOR_H_
#define COLOR_H_

#include <string>
#include <set>
#include <vector>

namespace graphml
{
	typedef unsigned char		BYTE;
	typedef unsigned long		COLORREF;

	class Color
	{
	public:
		Color(); 
		Color(unsigned char red, unsigned char green, unsigned char blue);
		Color(COLORREF);
		Color(const std::string&);
		/* added */
		BYTE red() const { return static_cast<BYTE>(color_ & 0xff); }
		BYTE green() const { return static_cast<BYTE>((color_ >> 8) & 0xff); }
		BYTE blue() const { return static_cast<BYTE>((color_ >> 16) & 0xff); }
		/* end of added */
	private:
		COLORREF color_;
	};

	class InterpolatedColor
	{
		std::vector<std::pair<double, Color> > sampling_points;
		Color below;
		Color above;
	public:
		InterpolatedColor(Color below_lowest = Color("White"), Color above_highest = Color("White"))
			: below(below_lowest), above(above_highest)
		{}

		void AddSamplingPoint( double value, Color color );
		Color GetColor(double value);
	};

	const int NumStandardColors = 8;
	static const Color StandardColors[] = {
		Color("Blue"), Color("Red"), Color("Yellow"), Color("Cyan"),
		Color("Green"), Color("Magenta"), Color("Orange"), Color("Violet")
	};

	static const std::vector<std::vector<Color> > StandardColorClasses{
		{ Color("Red"),		Color("Firebrick"),			Color("Indian Red"),	Color("Medium Violet Red"),		Color("Pink"),				Color("Plum"),					Color("Magenta"),			Color("Orange Red") },
		{ Color("Blue"),	Color("Aquamarine"),		Color("Cadet Blue"),	Color("Corn Flower Blue"),		Color("Dark Slate Blue"),	Color("Light Blue"),			Color("Light Steel Blue"),	Color("Medium Aquamarine"),		Color("Medium Blue") },
		{ Color("Green"),	Color("Cyan"),				Color("Dark Green"),	Color("Dark Green Copper"),		Color("Dark Olive Green"),	Color("Forest Green"),			Color("Lime Green"),		Color("Medium Forest Green"),	Color("Medium Sea Green"), Color("Medium Spring Green"), Color("Green Copper"), Color("Hunter Green") },
		{ Color("Yellow"),	Color("Bright Gold"),		Color("Green Yellow"),	Color("Dark Yellow"),			Color("Orange"),			Color("Mandarian Orange") },
		{ Color("Brown"),	Color("Bronze"),			Color("Bronze II"),		Color("Cool Copper"),			Color("Copper"),			Color("Dark Brown"),			Color("Khaki"),				Color("Very Dark Brown") },
		{ Color("Grey"),	Color("Dark Slate Grey"),	Color("Dim Grey"),		Color("Light Grey"),			Color("Silver"),			Color("Slate Grey") }, 
	};
} // end of namespace graphml

#endif // COLOR_H_