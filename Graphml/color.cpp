#include "color.h"

#include <sstream>
#include <algorithm>

using namespace std;

namespace graphml
{
	COLORREF RGB(unsigned char red, unsigned char green, unsigned char blue)
	{
		return static_cast<COLORREF>(red) | (static_cast<COLORREF>(green) << 8) | (static_cast<COLORREF>(blue) << 16);
	}

	Color::Color()
		: Color(RGB(0, 0, 0))
	{}

	Color::Color(unsigned char red, unsigned char green, unsigned char blue)
		: color_(RGB(red, green, blue))
	{}

	Color::Color(COLORREF cr) : color_(cr)
	{}

	Color::Color(const std::string& colorName)
	{
		if (colorName == "Aquamarine")
		{
			color_ = RGB(0x70, 0xDB, 0x93);
		}
		else if (colorName == "Baker's Chocolate")
		{
			color_ = RGB(0x5C, 0x33, 0x17);
		}
		else if (colorName == "Black")
		{
			color_ = RGB(0x00, 0x00, 0x00);
		}
		else if (colorName == "Blue")
		{
			color_ = RGB(0x00, 0x00, 0xFF);
		}
		else if (colorName == "Blue Violet")
		{
			color_ = RGB(0x9F, 0x5F, 0x9F);
		}
		else if (colorName == "Brass")
		{
			color_ = RGB(0xB5, 0xA6, 0x42);
		}
		else if (colorName == "Bright Gold")
		{
			color_ = RGB(0xD9, 0xD9, 0x19);
		}
		else if (colorName == "Brown") {
			color_ = RGB(0xA6, 0x2A, 0x2A);
		}
		else if (colorName == "Bronze")
		{
			color_ = RGB(0x8C, 0x78, 0x53);
		}
		else if (colorName == "Bronze II")
		{
			color_ = RGB(0xA6, 0x7D, 0x3D);
		}
		else if (colorName == "Cadet Blue")
		{
			color_ = RGB(0x5F, 0x9F, 0x9F);
		}
		else if (colorName == "Cool Copper")
		{
			color_ = RGB(0xD9, 0x87, 0x19);
		}
		else if (colorName == "Copper")
		{
			color_ = RGB(0xB8, 0x73, 0x33);
		}
		else if (colorName == "Coral")
		{
			color_ = RGB(0xFF, 0x7F, 0x00);
		}
		else if (colorName == "Corn Flower Blue")
		{
			color_ = RGB(0x42, 0x42, 0x6F);
		}
		else if (colorName == "Cyan")
		{
			color_ = RGB(0x00, 0xFF, 0xFF);
		}
		else if (colorName == "Dark Brown")
		{
			color_ = RGB(0x5C, 0x40, 0x33);
		}
		else if (colorName == "Dark Green")
		{
			color_ = RGB(0x2F, 0x4F, 0x2F);
		}
		else if (colorName == "Dark Green Copper")
		{
			color_ = RGB(0x4A, 0x76, 0x6E);
		}
		else if (colorName == "Dark Olive Green")
		{
			color_ = RGB(0x4F, 0x4F, 0x2F);
		}
		else if (colorName == "Dark Orchid")
		{
			color_ = RGB(0x99, 0x32, 0xCD);
		}
		else if (colorName == "Dark Purple")
		{
			color_ = RGB(0x87, 0x1F, 0x78);
		}
		else if (colorName == "Dark Slate Blue")
		{
			color_ = RGB(0x6B, 0x23, 0x8E);
		}
		else if (colorName == "Dark Slate Grey")
		{
			color_ = RGB(0x2F, 0x4F, 0x4F);
		}
		else if (colorName == "Dark Tan")
		{
			color_ = RGB(0x97, 0x69, 0x4F);
		}
		else if (colorName == "Dark Turquoise")
		{
			color_ = RGB(0x70, 0x93, 0xDB);
		}
		else if (colorName == "Dark Wood")
		{
			color_ = RGB(0x85, 0x5E, 0x42);
		}
		else if (colorName == "Dim Grey")
		{
			color_ = RGB(0x54, 0x54, 0x54);
		}
		else if (colorName == "Dusty Rose")
		{
			color_ = RGB(0x85, 0x63, 0x63);
		}
		else if (colorName == "Feldspar")
		{
			color_ = RGB(0xD1, 0x92, 0x75);
		}
		else if (colorName == "Firebrick")
		{
			color_ = RGB(0x8E, 0x23, 0x23);
		}
		else if (colorName == "Forest Green")
		{
			color_ = RGB(0x23, 0x8E, 0x23);
		}
		else if (colorName == "Gold")
		{
			color_ = RGB(0xCD, 0x7F, 0x32);
		}
		else if (colorName == "Goldenrod")
		{
			color_ = RGB(0xDB, 0xDB, 0x70);
		}
		else if (colorName == "Grey")
		{
			color_ = RGB(0xC0, 0xC0, 0xC0);
		}
		else if (colorName == "Grey10")
		{
			color_ = RGB(0x1A, 0x1A, 0x1A);
		}
		else if (colorName == "Grey20")
		{
			color_ = RGB(0x30, 0x30, 0x30);
		}
		else if (colorName == "Grey30")
		{
			color_ = RGB(0x4D, 0x4D, 0x4D);
		}
		else if (colorName == "Grey40")
		{
			color_ = RGB(0x66, 0x66, 0x66);
		}
		else if (colorName == "Grey50")
		{
			color_ = RGB(0x7F, 0x7F, 0x7F);
		}
		else if (colorName == "Grey60")
		{
			color_ = RGB(0x99, 0x99, 0x99);
		}
		else if (colorName == "Grey70")
		{
			color_ = RGB(0xB3, 0xB3, 0xB3);
		}
		else if (colorName == "Grey80")
		{
			color_ = RGB(0xCC, 0xCC, 0xCC);
		}
		else if (colorName == "Grey90")
		{
			color_ = RGB(0xE5, 0xE5, 0xE5);
		}
		else if (colorName == "Green")
		{
			color_ = RGB(0x00, 0xFF, 0x00);
		}
		else if (colorName == "Green Copper")
		{
			color_ = RGB(0x52, 0x7F, 0x76);
		}
		else if (colorName == "Green Yellow")
		{
			color_ = RGB(0x93, 0xDB, 0x70);
		}
		else if (colorName == "Hunter Green")
		{
			color_ = RGB(0x21, 0x5E, 0x21);
		}
		else if (colorName == "Indian Red")
		{
			color_ = RGB(0x4E, 0x2F, 0x2F);
		}
		else if (colorName == "Khaki")
		{
			color_ = RGB(0x9F, 0x9F, 0x5F);
		}
		else if (colorName == "Light Blue")
		{
			color_ = RGB(0xC0, 0xD9, 0xD9);
		}
		else if (colorName == "Light Grey")
		{
			color_ = RGB(0xA8, 0xA8, 0xA8);
		}
		else if (colorName == "Light Steel Blue")
		{
			color_ = RGB(0x8F, 0x8F, 0xBD);
		}
		else if (colorName == "Light Wood")
		{
			color_ = RGB(0xE9, 0xC2, 0xA6);
		}
		else if (colorName == "Lime Green")
		{
			color_ = RGB(0x32, 0xCD, 0x32);
		}
		else if (colorName == "Magenta")
		{
			color_ = RGB(0xFF, 0x00, 0xFF);
		}
		else if (colorName == "Mandarian Orange")
		{
			color_ = RGB(0xE4, 0x78, 0x33);
		}
		else if (colorName == "Maroon")
		{
			color_ = RGB(0x8E, 0x23, 0x6B);
		}
		else if (colorName == "Medium Aquamarine")
		{
			color_ = RGB(0x32, 0xCD, 0x99);
		}
		else if (colorName == "Medium Blue")
		{
			color_ = RGB(0x32, 0x32, 0xCD);
		}
		else if (colorName == "Medium Forest Green")
		{
			color_ = RGB(0x6B, 0x8E, 0x23);
		}
		else if (colorName == "Medium Goldenrod")
		{
			color_ = RGB(0xEA, 0xEA, 0xAE);
		}
		else if (colorName == "Medium Orchid")
		{
			color_ = RGB(0x93, 0x70, 0xDB);
		}
		else if (colorName == "Medium Sea Green")
		{
			color_ = RGB(0x42, 0x6F, 0x42);
		}
		else if (colorName == "Medium Slate Blue")
		{
			color_ = RGB(0x7F, 0x00, 0xFF);
		}
		else if (colorName == "Medium Spring Green")
		{
			color_ = RGB(0x7F, 0xFF, 0x00);
		}
		else if (colorName == "Medium Turquoise")
		{
			color_ = RGB(0x70, 0xDB, 0xDB);
		}
		else if (colorName == "Medium Violet Red")
		{
			color_ = RGB(0xDB, 0x70, 0x93);
		}
		else if (colorName == "Medium Wood")
		{
			color_ = RGB(0xA6, 0x80, 0x64);
		}
		else if (colorName == "Midnight Blue")
		{
			color_ = RGB(0x2F, 0x2F, 0x4F);
		}
		else if (colorName == "Navy Blue")
		{
			color_ = RGB(0x23, 0x23, 0x8E);
		}
		else if (colorName == "Neon Blue")
		{
			color_ = RGB(0x4D, 0x4D, 0xFF);
		}
		else if (colorName == "Neon Pink")
		{
			color_ = RGB(0xFF, 0x6E, 0xC7);
		}
		else if (colorName == "New Midnight Blue")
		{
			color_ = RGB(0x00, 0x00, 0x9C);
		}
		else if (colorName == "New Tan")
		{
			color_ = RGB(0xEB, 0xC7, 0x9E);
		}
		else if (colorName == "Old Gold")
		{
			color_ = RGB(0xCF, 0xB5, 0x3B);
		}
		else if (colorName == "Orange")
		{
			color_ = RGB(0xFF, 0x7F, 0x00);
		}
		else if (colorName == "Orange Red")
		{
			color_ = RGB(0xFF, 0x24, 0x00);
		}
		else if (colorName == "Orchid")
		{
			color_ = RGB(0xDB, 0x70, 0xDB);
		}
		else if (colorName == "Pale Green")
		{
			color_ = RGB(0x8F, 0xBC, 0x8F);
		}
		else if (colorName == "Pink")
		{
			color_ = RGB(0xBC, 0x8F, 0x8F);
		}
		else if (colorName == "Plum")
		{
			color_ = RGB(0xEA, 0xAD, 0xEA);
		}
		else if (colorName == "Quartz")
		{
			color_ = RGB(0xD9, 0xD9, 0xF3);
		}
		else if (colorName == "Red")
		{
			color_ = RGB(0xFF, 0x00, 0x00);
		}
		else if (colorName == "Rich Blue")
		{
			color_ = RGB(0x59, 0x59, 0xAB);
		}
		else if (colorName == "Salmon")
		{
			color_ = RGB(0x6F, 0x42, 0x42);
		}
		else if (colorName == "Scarlet")
		{
			color_ = RGB(0x8C, 0x17, 0x17);
		}
		else if (colorName == "Sea Green")
		{
			color_ = RGB(0x23, 0x8E, 0x68);
		}
		else if (colorName == "Semi-Sweet Chocolate")
		{
			color_ = RGB(0x6B, 0x42, 0x26);
		}
		else if (colorName == "Sienna")
		{
			color_ = RGB(0x8E, 0x6B, 0x23);
		}
		else if (colorName == "Silver")
		{
			color_ = RGB(0xE6, 0xE8, 0xFA);
		}
		else if (colorName == "Sky Blue")
		{
			color_ = RGB(0x32, 0x99, 0xCC);
		}
		else if (colorName == "Slate Blue")
		{
			color_ = RGB(0x00, 0x7F, 0xFF);
		}
		else if (colorName == "Slate Grey")
		{
			color_ = RGB(0x70, 0x80, 0x90);
		}
		else if (colorName == "Spicy Pink")
		{
			color_ = RGB(0xFF, 0x1C, 0xAE);
		}
		else if (colorName == "Spring Green")
		{
			color_ = RGB(0x00, 0xFF, 0x7F);
		}
		else if (colorName == "Steel Blue")
		{
			color_ = RGB(0x23, 0x6B, 0x8E);
		}
		else if (colorName == "Summer Sky")
		{
			color_ = RGB(0x38, 0xB0, 0xDE);
		}
		else if (colorName == "Tan")
		{
			color_ = RGB(0xDB, 0x93, 0x70);
		}
		else if (colorName == "Thistle")
		{
			color_ = RGB(0xD8, 0xBF, 0xD8);
		}
		else if (colorName == "Turquoise")
		{
			color_ = RGB(0xAD, 0xEA, 0xEA);
		}
		else if (colorName == "Very Dark Brown")
		{
			color_ = RGB(0x5C, 0x40, 0x33);
		}
		else if (colorName == "Very Light Grey")
		{
			color_ = RGB(0xCD, 0xCD, 0xCD);
		}
		else if (colorName == "Violet")
		{
			color_ = RGB(0x4F, 0x2F, 0x4F);
		}
		else if (colorName == "Violet Red")
		{
			color_ = RGB(0xCC, 0x32, 0x99);
		}
		else if (colorName == "Wheat")
		{
			color_ = RGB(0xD8, 0xD8, 0xBF);
		}
		else if (colorName == "White")
		{
			color_ = RGB(0xFF, 0xFF, 0xFF);
		}
		else if (colorName == "Yellow")
		{
			color_ = RGB(0xFF, 0xFF, 0x00);
		}
		else if (colorName == "Yellow Green")
		{
			color_ = RGB(0x99, 0xCC, 0x32);
		}
		else if (colorName == "Dark Yellow")
		{
			color_ = RGB(0x99, 0xCC, 0x32);
		}
		else
		{
			throw;
		}
	}


	void InterpolatedColor::AddSamplingPoint(double value, Color color)
	{
		sampling_points.push_back(std::make_pair(value, color));
	}


	Color InterpolatedColor::GetColor(double value)
	{
		if (sampling_points.empty())
			return above;
		sort(sampling_points.begin(), sampling_points.end(), [](const pair<double, Color>& a, const pair<double, Color>& b) { return (a.first < b.first); });

		if (value < sampling_points.begin()->first)
			return below;
		if (value > sampling_points.rbegin()->first)
			return above;

		Color next_below("Black");
		double value_below;
		for (auto ii = std::as_const(sampling_points).begin(), iiend = std::as_const(sampling_points).end(); ii != iiend; ++ii)
			if (ii->first <= value)
			{
				value_below = ii->first;
				next_below = ii->second;
			}

		Color next_above("Black");
		double value_above;
		for (auto jj = std::as_const(sampling_points).rbegin(), jjend = std::as_const(sampling_points).rend(); jj != jjend; ++jj)
			if (jj->first >= value)
			{
				value_above = jj->first;
				next_above = jj->second;
			}

		if (value_above - value_below < 0.01)
			return next_below;

		double lambda = (value - value_below) / (value_above - value_below);
		double red_d = (1.0 - lambda) * double(next_below.red()) + lambda * double(next_above.red());
		double green_d = (1.0 - lambda) * double(next_below.green()) + lambda * double(next_above.green());
		double blue_d = (1.0 - lambda) * double(next_below.blue()) + lambda * double(next_above.blue());
		return Color(
			static_cast<unsigned char>(red_d),
			static_cast<unsigned char>(green_d),
			static_cast<unsigned char>(blue_d)
		);
	}

} // end of namespace graphml
