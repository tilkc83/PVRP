#ifndef GRAPHML_H
#define GRAPHML_H

#include <string>
#include <fstream>
#include <vector>

#include "color.h"

namespace graphml
{
	struct nodeproperties
	{
		nodeproperties()
			: shape(ellipse),
			bordercolor("Black"),
			fillcolor("White"),
			fillcolor2("Yellow"),
			coord_x(0), coord_y(0),
			text(""),
			height(30.0),
			width(30.0)
		{}

		enum Shape { ellipse, rectangle3d, diamond, triangle, roundrectangle };
		Shape shape;
		Color bordercolor;
		Color fillcolor;
		Color fillcolor2;
		int coord_x;
		int coord_y;
		std::string text;
		double height;
		double width;
	};

	struct linkproperties
	{
		linkproperties()
			: linecolor("Black"),
			textcolor("Blue"),
			text(""),
			linetype("line"),
			linewidth(1.0),
			orientation(undir)
		{}

		enum Orientation { undir, directed, bidir };
		Orientation orientation;
		Color linecolor;
		Color textcolor;
		double linewidth;
		std::string text;
		std::string linetype;
	};

	class c_GraphML
	{
		std::ofstream yed;
		std::vector<std::pair<int, nodeproperties> > nodes;
		std::vector<std::pair<std::pair<int, int>, linkproperties> > edges;
		void WritePreamble();
		void WriteNodes();
		void WriteLinks();
		void WriteEnd();
	public:
		c_GraphML(const char* filename);
		virtual ~c_GraphML();

		void AddNode(int id, nodeproperties& p);
		void AddEdge(int id1, int id2, linkproperties& p);
		void AddArc(int id1, int id2, linkproperties& p);
	};
} // end of namespace graphml

#endif 

