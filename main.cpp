#include "bcp_cvrptw.h"

#include "visualization.h"

//#include <vld.h>

using namespace std;

///////////////////////////////////////////////////////////////////////////////
// main
///////////////////////////////////////////////////////////////////////////////

int main(int argc, char** argv )
{
	if ( argc < 2 )
	{
		//cout << "Usage: " << argv[0] << " <instance> " << endl;
		cerr << "Too less input parameters. (Instance name missing.)" << endl;
		return 0;
	}

	const char* instancename = argv[1];
	stringstream ssInstance;
	ssInstance << instancename;

	cout << "Initializing CG for " << instancename;

	stringstream ssSchedule;
	if( argc >= 3 ){
		const char* schedulename = argv[2];
		ssSchedule << schedulename;
		cout << " with schedule " << schedulename;
	}
	cout << "... " << endl;

	c_ControllerCVRPTW ctlr( ssInstance.str().c_str(), ssSchedule.str().c_str(),"../settings/settings.txt");
	ctlr.ReadSettings();

	/*stringstream tikzName;
	tikzName << "Tikz_" << ctlr.InstanceName() << ".tikz";
	c_TikzVisualization* tikz = new c_TikzVisualization( tikzName.str().c_str() , ctlr.NumNodes()-1, ctlr.GetXCoords(), ctlr.GetYCoords(), ctlr.GetDemands(), ctlr.GetStarts(), ctlr.GetEnds(), ctlr.GetFrequencies());
	*/
	ctlr.Go();

	/*c_SolutionCVRPTW* sol = (c_SolutionCVRPTW*) ctlr.BestSolution();
	if(sol != NULL){
	vector<vector<int>> routes = vector<vector<int>>((int)sol->getRoutes().size());
	vector<vector<int>> times = vector<vector<int>>((int)sol->getRoutes().size());
	for ( int i=0; i<(int) sol->getRoutes().size(); i++ ) {
	int cur_time = 0;
	const vector<int>& route = sol->getRoutes()[i].first;
	for ( int idx=0; idx<(int)route.size(); idx++ ) {
	int node = (idx == route.size()-1)? ctlr.OrigDepot() : ctlr.GetNode(route[idx]);
	routes[i].push_back(node);
	int time_way = (idx==0)? 0 : ctlr.Time(ctlr.GetNode(route[idx-1]), ctlr.GetNode(route[idx]));
	cur_time = max(cur_time + time_way, ctlr.StartTime(ctlr.GetNode(route[idx])));
	times[i].push_back(cur_time);
	}
	}
	tikz->addRoutes(routes, times);
	}*/

	ctlr.StoreUB();
	if( ctlr.TimeOut() ){
		ctlr.SolveMIPWithOriginalMP();
		//ctlr.GreedyRepair();
	}

	ctlr.WriteSolutionInFile( "solution.csv" );
	//system("pause");
	return 0;

	//Separate cuts (only) aggregated
	//Branch with breaking ties for customer with most active schedules, among them the one with the most fractional schedule column value, and then branch on its first fractional day
};