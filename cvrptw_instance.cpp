#include "cvrptw_instance.h"

bool taskNetwork;
bool schedulePartNetwork;
using namespace std;

/////////////////////////////////////////////////////////////////////////////
// c_CVRPTW_Instance
/////////////////////////////////////////////////////////////////////////////


c_CVRPTW_Instance::c_CVRPTW_Instance( string instanceFilename, string scheduleFilename )
{
	if( scheduleFilename == "" ){ //Cordeau instances have all infos in one file
		ifstream sfile( instanceFilename, ios_base::in );
		if ( !sfile.is_open() ) 
		{
			cerr << "File kann nicht ge�ffnet werden: " << instanceFilename << endl;
			return;
		}
		instanceName = instanceFilename.substr(10);

		bool entry_ok;
		int variante, fahrzeug_anzahl, kunden_anzahl, planungshorizont;

		int cust_no = 0, service = 0, demand = 0, frequency = 0, schedules_no = 0, dec_schedule = 0, ready = 0, due = 0;
		double x = 0, y = 0;

		LeseInteger( sfile, variante ) &&
			LeseInteger( sfile, fahrzeug_anzahl ) &&
			LeseInteger( sfile, kunden_anzahl ) &&
			LeseInteger( sfile, planungshorizont );

		i_numVehiclesPerDay = fahrzeug_anzahl; // wird sie �berhaupt ben�tigt?
		i_planningHorizon = planungshorizont;
		i_numCustomers = kunden_anzahl;
		i_numNodes = kunden_anzahl +2;
		i_origDepot = 0;
		i_destDepot = i_numCustomers+1;
		m_costs = c_DoubleMatrix( i_numNodes, i_numNodes );
		m_times = c_IntMatrix( i_numNodes, i_numNodes );
		v_maxDurationPerRoute.resize(i_planningHorizon);
		//v_capacityPerVehPerDay.resize(i_planningHorizon);
		v_serviceTime.resize(i_numNodes);
		v_startTime.resize(i_numNodes);
		v_endTime.resize(i_numNodes);
		v_xCoord.resize(i_numNodes);
		v_yCoord.resize(i_numNodes);
		v_demand.resize(i_numNodes);
		v_frequency.resize(i_numNodes);
		v_schedulesPerCustomer.resize(i_numNodes);
		v_schedulePartsPerCustomer.resize(i_numNodes);

		for (int i = 0; i < planungshorizont; i++) {
			LeseInteger( sfile, v_maxDurationPerRoute[i]) &&
				LeseInteger( sfile, i_capacity);
		}

		// je Knoten kommen jetzt die folgenden Werte
		// CUST_NO. XCOORD. YCOORD. ServiceTime Demand Frequency NumSchedules Schedules READY_TIME DUE_DATE
		//knoten_nr = 0;
		while( true )
		{
			entry_ok = LeseInteger( sfile, cust_no ) &&
				LeseDouble( sfile, v_xCoord[cust_no] ) && 
				LeseDouble( sfile, v_yCoord[cust_no] ) &&
				LeseInteger( sfile, v_serviceTime[cust_no] ) &&
				LeseInteger( sfile, demand ) &&
				LeseInteger( sfile, v_frequency[cust_no] ) &&
				LeseInteger( sfile, schedules_no );
			if ( !entry_ok )
				break;
			v_demand[cust_no] = (demand * v_frequency[cust_no]) / i_planningHorizon; // calculating demand per day per customer
			if( cust_no == OrigDepot() ){
				//LeseInteger( sfile, dec_schedule );
				vector<bool> bin_schedule = vector<bool>(i_planningHorizon);
				for (int i = 0; i < i_planningHorizon; i++) {
					bin_schedule[i] = 1;
				}
				v_schedulesPerCustomer[cust_no].push_back(bin_schedule);
				v_schedulesPerCustomer[i_destDepot] = v_schedulesPerCustomer[i_origDepot];
			} else{
				for (int counter = 0; counter < schedules_no; counter++ ) { //transforming decimal schedule to binary schedule
					LeseInteger( sfile, dec_schedule );
					int decimal = dec_schedule;
					int rest;
					vector<bool> bin_schedule = vector<bool>(i_planningHorizon);
					for (int i = i_planningHorizon - 1; i >= 0; i--) {
						rest = decimal % 2;
						bin_schedule[i] = rest;
						decimal = decimal / 2;
					}
					v_schedulesPerCustomer[cust_no].push_back(bin_schedule);
				}
			}
			
			entry_ok = LeseInteger( sfile, v_startTime[cust_no] ) &&
				LeseInteger( sfile, v_endTime[cust_no] );
			if ( cust_no == OrigDepot() ) //die Zeile 0 geh�rt zu OrigDepot und DestDepot
			{
				v_xCoord[DestDepot()] = v_xCoord[cust_no];
				v_yCoord[DestDepot()]  = v_yCoord[cust_no] ;
				v_serviceTime[DestDepot()] = v_serviceTime[cust_no];
				v_demand[DestDepot()] = v_demand[cust_no];
				v_frequency[DestDepot()] = v_frequency[cust_no];
				v_startTime[DestDepot()] = v_startTime[cust_no];
				v_endTime[DestDepot()] =  v_endTime[cust_no];
			}
			
		}

		//Scale times
		for(int n=0; n<i_numNodes; n++){
			v_startTime[n] *= timeScaleFactor; 
			v_endTime[n] *= timeScaleFactor; 
			v_serviceTime[n] *= timeScaleFactor; 
		}
		for(int p=0; p<i_planningHorizon; p++){
			v_maxDurationPerRoute[p] *= timeScaleFactor;
		}

		if (cust_no != kunden_anzahl) { // check if cust_no is equal to kunden_anzahl, otherwise reading file went wrong
			cout << "Durchlaufene Anzahl an Kunden entspricht nicht in Instanz zu Beginn angegebener Anzahl an Kunden." << endl;
			return;
		}

		sfile.close();
	} else { //Solomon instances combined with additional schedule file

		ifstream sfile( instanceFilename, ios_base::in );
		if ( !sfile.is_open() ) 
		{
			cerr << "File kann nicht ge�ffnet werden: " << instanceFilename << endl;
			return;
		}

		int fahrzeug_anzahl, fahrzeug_kapazitaet, knoten_nr;
		int cust_no = 0, x = 0, y = 0, demand = 0, ready = 0, due = 0, service = 0;
		bool header_ok, entry_ok;

		char name[1000];

		// Header weglesen:
		header_ok = 
			( LeseBez( sfile, name )              
			&& LeseString( sfile, "VEHICLE" ) 
			&& LeseString( sfile, "NUMBER" ) 
			&& LeseString( sfile, "CAPACITY" ) );
		instanceName = name;
		if ( header_ok )
		{
			LeseInteger( sfile, fahrzeug_anzahl ) 
				&& LeseInteger( sfile, fahrzeug_kapazitaet );

			// Lese alles weg, bis einschlie�lich "SERVICE TIME"
			while ( !LeseString( sfile, "SERVICE", true ) );
			LeseString( sfile, "TIME" );

			// je Knoten kommen jetzt die folgenden Werte
			// CUST_NO. XCOORD. YCOORD. DEMAND READY_TIME DUE_DATE SERVICE_TIME
			knoten_nr = 0;
			while( true )
			{
				entry_ok =  LeseInteger( sfile, cust_no ) && 
					LeseInteger( sfile, x )       && 
					LeseInteger( sfile, y )       && 
					LeseInteger( sfile, demand )  && 
					LeseInteger( sfile, ready )   && 
					LeseInteger( sfile, due )     && 
					LeseInteger( sfile, service );
				if ( !entry_ok )
					break;
			}
			int kundenzahl = cust_no;

			i_numCustomers = kundenzahl;
			i_numNodes = kundenzahl +2;
			i_origDepot = 0;
			i_destDepot = i_numCustomers+1;
			m_costs = c_DoubleMatrix( i_numNodes, i_numNodes );
			m_times = c_IntMatrix( i_numNodes, i_numNodes );
			v_serviceTime.resize(i_numNodes);
			v_startTime.resize(i_numNodes);
			v_endTime.resize(i_numNodes);
			v_xCoord.resize(i_numNodes);
			v_yCoord.resize(i_numNodes);
			v_demand.resize(i_numNodes);
			v_schedulesPerCustomer.resize(i_numNodes);
			v_schedulePartsPerCustomer.resize(i_numNodes);

			sfile.close();

			ifstream zfile( instanceFilename, ios_base::in );
			if ( !zfile.is_open() ) 
			{
				cerr << "File kann nicht ge�ffnet werden: " << instanceFilename << endl;
				return;
			}

			cust_no = 0, x = 0, y = 0, demand = 0, ready = 0, due = 0, service = 0;

			// Header weglesen:
			header_ok = 
				( LeseBez( zfile, name )              
				&& LeseString( zfile, "VEHICLE" ) 
				&& LeseString( zfile, "NUMBER" ) 
				&& LeseString( zfile, "CAPACITY" ) );

			if ( header_ok )
			{
				LeseInteger( zfile, fahrzeug_anzahl ) 
					&& LeseInteger( zfile, fahrzeug_kapazitaet );
				i_numVehiclesPerDay = fahrzeug_anzahl;
				i_capacity = fahrzeug_kapazitaet;

				// Lese alles weg, bis einschlie�lich "SERVICE TIME"
				while ( !LeseString( zfile, "SERVICE", true ) );
				LeseString( zfile, "TIME" );

				// je Knoten kommen jetzt die folgenden Werte
				// CUST_NO. XCOORD. YCOORD. DEMAND READY_TIME DUE_DATE SERVICE_TIME
				knoten_nr = 0;
				while( true )
				{
					entry_ok =  LeseInteger( zfile, cust_no );
					entry_ok = entry_ok && LeseDouble( zfile, v_xCoord[cust_no] ) && 
						LeseDouble( zfile, v_yCoord[cust_no] ) && 
						LeseInteger( zfile, demand  ) && 
						LeseInteger( zfile, v_startTime[cust_no] ) && 
						LeseInteger( zfile, v_endTime[cust_no]) && 
						LeseInteger( zfile, v_serviceTime[cust_no] );
					v_demand[cust_no] = demand;
					if ( cust_no == OrigDepot() ) //die Zeile 0 geh�rt zu OrigDepot und DestDepot
					{
						v_xCoord[DestDepot()] = v_xCoord[cust_no];
						v_yCoord[DestDepot()]  = v_yCoord[cust_no] ;
						v_demand[DestDepot()] = v_demand[cust_no];
						v_startTime[DestDepot()] = v_startTime[cust_no];
						v_endTime[DestDepot()] =  v_endTime[cust_no];
						v_serviceTime[DestDepot()] = v_serviceTime[cust_no];
					}
					if ( !entry_ok )
						break;
				}
			}
		}
		else
		{
			sfile.close();
			cerr << "Instance could not be read." << endl;
			return;
		}	

		for(int n=0; n<i_numNodes; n++){
			v_startTime[n] *= timeScaleFactor; 
			v_endTime[n] *= timeScaleFactor; 
			v_serviceTime[n] *= timeScaleFactor; 
		}

		//Einlesen von ganzen Schedules, statt Frequenzen: PVRP
		ifstream schfile( scheduleFilename, ios_base::in );
		if ( !schfile.is_open() ) 
		{
			//error( "File kann nicht ge�ffnet werden.", filename );
			cerr << "File kann nicht ge�ffnet werden: " << scheduleFilename << endl;
			return;
		}
		// Header weglesen:
		bool periodic_header_ok = 
			( LeseBez( schfile, name )              
			&& LeseString( schfile, "PLANNINGHORIZON" ) );

		scheduleName = name;

		if ( periodic_header_ok )
		{
			LeseInteger( schfile, i_planningHorizon ); 

			bool day;
			// Lese alles weg, bis einschlie�lich "DAY6"
			while ( !LeseString( schfile, "ENDDAY", true ) );
			while( true ) {
				bool periodic_entry_ok =  LeseInteger( schfile, cust_no );
				if(!periodic_entry_ok)
					break;

				vector<bool> schedule = vector<bool>(i_planningHorizon);
				for (int i=0;i<i_planningHorizon;i++) {
					LeseBoolean( schfile, day );
					schedule[i]=day;
				}
				v_schedulesPerCustomer[cust_no].push_back(schedule);
			}
			//Copy schedules of starDepot to endDepot
			v_schedulesPerCustomer[i_destDepot] = v_schedulesPerCustomer[i_origDepot];
			schfile.close();
		} else {
			schfile.close();
			cerr << "ScheduleInfo could not be read." << endl;
			return;
		}	
	
	////Read additional info of customer visit frequences
	//ifstream ffile( scheduleFilename, ios_base::in );
	//if ( !ffile.is_open() ) 
	//{
	//	cerr << "File kann nicht ge�ffnet werden: " << scheduleFilename << endl;
	//	return;
	//}

	//// Header weglesen:
	//header_ok = 
	//	( LeseBez( ffile, name )              
	//	&& LeseString( ffile, "PLANNINGHORIZON" ) );
	//scheduleName = name;
	//if ( header_ok )
	//{
	//	LeseInteger( ffile, i_planningHorizon );

	//	// Lese alles weg, bis einschlie�lich "SERVICE TIME"
	//	while ( !LeseString( ffile, "FREQUENCE", true ) );

	//	v_frequence.resize(i_numNodes);
	//	v_maxDaysUntilNextVisit.resize(i_numNodes);
	//	// je Knoten kommen jetzt die folgenden Werte
	//	// CUST_NO. FREQUENCE
	//	knoten_nr = 0;
	//	while( true )
	//	{
	//		entry_ok =  LeseInteger( ffile, cust_no ) && 
	//			LeseInteger( ffile, v_frequence[cust_no] );
	//		if ( !entry_ok )
	//			break;
	//	}
	//	v_frequence[DestDepot()] = v_frequence[OrigDepot()];

	//	if( cust_no != i_numCustomers ){
	//		cerr << "ScheduleFile has not as many customers than InstanceFile" << endl;
	//		throw;
	//	}
	//	
	//	for ( int i = 0; i < NumNodes(); i++){
	//		if( i_planningHorizon%v_frequence[i] != 0){
	//			cerr << "Planning horizon is not multiple of all frequences: PH = " << i_planningHorizon << ", Frequence of node " << i << " = " << v_frequence[i] << endl;
	//			throw;
	//		} else{
	//			v_maxDaysUntilNextVisit[i] = i_planningHorizon/v_frequence[i];
	//		}

	//	}
	//	ffile.close();
	//}
	//else
	//{
	//	ffile.close();
	//	cerr << "ScheduleInfo could not be read." << endl;
	//	return;
	//}	

		sfile.close();
	}

	Go();
}

bool c_CVRPTW_Instance::LeseString
	( ifstream& file, const char* string, bool weglesen )
{
	static char buffer[1000];
	file >> buffer;
	if ( strcmp( buffer, string ) == 0 )
		return true;
	else
	{
		if( !weglesen )
			cerr << "noch nicht ausprogrammiert!" << endl;
			//error( "noch nicht ausprogrammiert!" );
		return false;
	}
}

bool c_CVRPTW_Instance::LeseInteger( ifstream& file, int& i1 )
{
	// ev. f�hrende Whitespaces l�schen
	//file.eatwhite(); 

	int i0;
	if ( file >> i0 )
	{	
		i1 = i0;
		return true;
	}
	else
		return false;
}

bool c_CVRPTW_Instance::LeseBoolean( ifstream& file, bool& b1 )
{
	// ev. f�hrende Whitespaces l�schen
	//file.eatwhite(); 

	bool b0;
	if ( file >> b0 )
	{	
		b1 = b0;
		return true;
	}
	else
		return false;
}

bool c_CVRPTW_Instance::LeseDouble( ifstream& file, double& i1 )
{
	// ev. f�hrende Whitespaces l�schen
	//file.eatwhite(); 

	double i0;
	if ( file >> i0 )
	{	
		i1 = i0;
		return true;
	}
	else
		return false;
}


bool c_CVRPTW_Instance::LeseBez( ifstream& file, char* buffer )
{
	// ev. f�hrende Whitespaces l�schen
	//file.eatwhite(); 
	file >> buffer;
	return true;
}



const int c_CVRPTW_Instance::GetVertex(int cust, int day, int endDay/*=-1*/)
{
	if ( taskNetwork ) {
		//first is depot. Is followed by all customer vertices of first day, then by all of second day and so on 
		if( cust == OrigDepot() ){
			return cust;
		} else if ( cust == DestDepot() ){
			return DestDepotVertex();
		} else{ //customer
			return cust + NumCustomers() * (day % i_planningHorizon);
		}
	} else if( schedulePartNetwork ) {
		//depot has length of PlanningHorizon many schedule parts
		//every customer has also length of Planning Horizon many schedule parts in the regular case (one schedule part starting at every day, 
		//but the order among those of one customer is not always by increasing start day
		if( cust == OrigDepot() ){
			return day;
		} else if( cust == DestDepot() ){
			return DestDepotVertex(day);
		} else{ //customer
			for (auto spId = GetAllowedScheduleParts(cust).begin(); spId != GetAllowedScheduleParts(cust).end();spId) {
				if( GetSchedulePart(*spId)->getStartDay() == day )
					if( endDay == -1 )
						return *spId;
					else if( GetSchedulePart(*spId)->getEndDay() == endDay )
						return *spId;
			}
			//return cust * PlanningHorizon() + day;
		}	
		cout << "Error in GetVertex. No schedulePart with given location, start and end period found." << endl;
		throw;
	} else{
		cerr << "Invalid network type. Error in GetVertex." << endl;
		throw;
		return -1;
	}
}

int c_CVRPTW_Instance::OrigDepotVertex(int day /*= -1*/) const
{
	if( day == -1 ) 
		return i_origDepotVertex; //taskNetwork
	else{
		const vector<int>& schedulePartIds = GetAllowedScheduleParts(i_origDepot);
		int schedulePartVertex = schedulePartIds[day];
		return schedulePartVertex;
	}
}

int c_CVRPTW_Instance::DestDepotVertex(int day /*= -1*/) const
{
	if( day == -1 ) 
		return i_destDepotVertex; //taskNetwork
	else{
		const vector<int>& schedulePartIds = GetAllowedScheduleParts(i_destDepot);
		int schedulePartVertex = schedulePartIds[day];
		return schedulePartVertex ;
	}
}

const int c_CVRPTW_Instance::GetNode(int vertex) const
{
	return v_nodesOfVertices[vertex];
	//if( taskNetwork ){
	//	if( vertex == 0) //first
	//		return OrigDepot();
	//	if( vertex == (NumVertices()-1)  ) //last
	//		return DestDepot();

	//	return (vertex % NumCustomers() == 0)? NumCustomers() : (vertex % NumCustomers()); //for last customer modulo would be 0
	//}
	//else if( schedulePartNetwork ){
	//	return GetSchedulePart(vertex)->getCustomer();
	//} else {
	//	cerr << "Error in GetNode. No network type chosen." << endl;
	//	throw;
	//}
}

const int c_CVRPTW_Instance::GetDay(int vertex) const
{
	return v_daysOfVertices[vertex];
	//if( taskNetwork ){
	//	if( vertex == 0) //first
	//		return 0;
	//	if( vertex == (NumVertices()-1)  ) //last
	//		return 0;

	//	return (vertex % NumCustomers() == 0)? (vertex / NumCustomers()) - 1 : vertex / NumCustomers();
	//}
	//else if( schedulePartNetwork ){
	//	return GetSchedulePart(vertex)->getStartDay();
	//} else {
	//	cerr << "Error in GetDay. No network type chosen." << endl;
	//	throw;
	//}
}



const int c_CVRPTW_Instance::GetNodeOfTask(int task) const{
	return v_nodesOfTasks[task];
}

const int c_CVRPTW_Instance::GetDayOfTask(int task) const{
	return v_daysOfTasks[task];
}


const int c_CVRPTW_Instance::GetTask(int cust, int day) const{
	if( cust == OrigDepot() ){
		return i_startTask;
	} else if ( cust == DestDepot() ){
		return i_endTask;
	} else{ //customer
		return cust + NumCustomers() * (day % i_planningHorizon);
	}
}

const bool c_CVRPTW_Instance::TaskOfSubsetContainedInSP(const vector<int>& subset, int SPId) const
{
	for (auto task = subset.begin(); task!=subset.end();task++ ) {
		if(	GetSchedulePart(SPId)->getCustomer() == GetNodeOfTask(*task) ){
			if( SPContainsDay( SPId, GetDayOfTask(*task) ) ){
				return true;
			}
		}
	}
	return false;
}

const int c_CVRPTW_Instance::GetNumTasksCoveredBySP(int SPId) const
{
	return ( (GetSchedulePart(SPId)->getEndDay() - GetSchedulePart(SPId)->getStartDay()+ PlanningHorizon() ) % PlanningHorizon()) + 1;
}

const int c_CVRPTW_Instance::GetNumTasksCovered(int startDay, int endDay) const
{
	return ( (endDay - startDay + PlanningHorizon() ) % PlanningHorizon()) + 1;
}

vector<int> c_CVRPTW_Instance:: GetDemandsForVertices() const
{
	vector<int> demandsVertices;
	demandsVertices.push_back(0); //starts access at 1
	if ( taskNetwork ) {
		for (int i = 1; i <= NumCustomerVertices(); i++) {
			demandsVertices.push_back( DemandPerDay(GetNode(i)) );
		}
		return demandsVertices;
	}
	else if( schedulePartNetwork ) {
		vector<int> demandsVertices;
		//number of schedule parts for start and end depot is both equal to length of planning horizon
		for (int i = PlanningHorizon();  i < GetNumScheduleParts() - PlanningHorizon(); i++) {
			demandsVertices.push_back(DemandSchedulePart(i));
		}
		return demandsVertices;
	}
	else
		throw;
}

const int c_CVRPTW_Instance::GetSepVertexNumber(int origVertexNumber) const{
	if ( taskNetwork ) {
		if (origVertexNumber == 0 ) {
			return (NumCustomerVertices() + 1);
		} else {
			return origVertexNumber;
		}
	} else if( schedulePartNetwork ) {
		throw;
	} else{
		cerr << "Invalid network type. Error in GetSepVertexNumber." << endl;
		throw;
		return -1;
	}
}

void c_CVRPTW_Instance::Go( void ) 
{
	// Rundung
	double test = OffsetForTriangleInequality();

	// Rechnen...
	int i, j;
	for ( i=0; i<i_numNodes; i++ )
	{
		/*v_startTime[i] = 0;
		v_endTime[i] = 100;*/
		for ( j=0; j<i_numNodes; j++ )
		{
			// costs
			double dist = sqrt( (double) (v_xCoord[i]-v_xCoord[j])*(v_xCoord[i]-v_xCoord[j]) + (v_yCoord[i]-v_yCoord[j])*(v_yCoord[i]-v_yCoord[j]) ); 
			m_costs(i,j) = dist; //(int) ( ceil ( timeScaleFactor * dist ) );

			//times
			if ( ( ( i != j ) && ( v_startTime[i] + timeScaleFactor * m_costs(i,j) + v_serviceTime[i] <= v_endTime[j] ) )   
				&& ( i != DestDepot() ) 
				&& ( j != OrigDepot() ) 
				&& ( i != OrigDepot() || j!= DestDepot() ) ){
				//cout << i << "-" << j << ": " << (int) (m_costs(i,j) / rounding_factor) + v_serviceTime[i] << endl;
				m_times(i,j) = ((int) ( ceil ( timeScaleFactor * m_costs(i,j) ) ) )+ v_serviceTime[i]; //m_costs(i,j) + v_serviceTime[i];//(int) (m_costs(i,j) / rounding_factor) + v_serviceTime[i];

			//if (  ( i != j )    
			//	&& ( i != DestDepot() ) 
			//	&& ( j != OrigDepot() ) 
			//	&& ( i != OrigDepot() || j!= DestDepot() ) ){
			//		m_times(i,j) = 1;
			} else if ( i == j ){ //no time between vertices of same customer at different days, only additional service time not stored in the time matrix
				m_times(i,j) = AdditionalServiceTime();
			} else {
				m_times(i,j) = (int) CGBase::INFTY;
			}
		}
	}
	i_numVertices = 2 + i_planningHorizon * i_numCustomers;
	i_origDepotVertex = 0;
	i_destDepotVertex = i_numVertices - 1;


	i_numTasks = 2 + i_planningHorizon * i_numCustomers;
	i_startTask = 0;
	i_endTask = i_numVertices - 1;
	v_nodesOfTasks = vector<int>(i_numVertices);
	v_daysOfTasks = vector<int>(i_numVertices);
	for( int t = 0; t < i_numVertices; t++){
		if( t == 0){ //first
			v_nodesOfTasks[t] = OrigDepot();
			v_daysOfTasks[t] = 0;
		} else if( t == (i_numVertices-1)  ){ //last
			v_nodesOfTasks[t] = DestDepot();
			v_daysOfTasks[t] = 0;
		} else if( t % NumCustomers() == 0  ){ //last
			v_nodesOfTasks[t] = NumCustomers();
			v_daysOfTasks[t] = (t / NumCustomers()) - 1;
		} else {
			v_nodesOfTasks[t] = t % NumCustomers();
			v_daysOfTasks[t] = t / NumCustomers();
		}
	}
}

void c_CVRPTW_Instance::createScheduleParts()
{
	int id = 0;
	v_schedulePartsPerCustomerAndScheduleId = vector<vector<vector<int>>>( NumNodes() );
	//create all allowed schedule parts beginning with visit up to (not including) next visit
	for ( int i = 0 ; i < i_numNodes; i++){
		//if( IsCustomerNode(i) ){ //also depot has schedule
			v_schedulePartsPerCustomerAndScheduleId[i] = vector<vector<int>>( (int)GetAllowedSchedules(i).size() );
			int scheduleId = 0;
			for (auto schedule = GetAllowedSchedules(i).begin(); schedule != GetAllowedSchedules(i).end();schedule++) {
				for( int d = 0; d < PlanningHorizon(); d++ ){
					if( (*schedule)[d] == true ){
						//check how many following days can be delivered at this visit
						int dBeforeNextVisit = d;
						int dEnd = (d + 1)%PlanningHorizon();
						while((*schedule)[dEnd] == false ){
							dBeforeNextVisit = dEnd;
							dEnd = (dEnd+ 1)%PlanningHorizon();
						}

						//don't create same schedulePart (defined by customer, start and endPeriod) twice
						int sp_id = -1;
						for (auto SPId = v_schedulePartsPerCustomer[i].begin(); SPId != v_schedulePartsPerCustomer[i].end();SPId++) {
							if( GetSchedulePart(*SPId)->getStartDay() == d && GetSchedulePart(*SPId)->getEndDay() == dBeforeNextVisit ){
								sp_id = *SPId;
								break;
							}
						}
						//if schedulePart does not exist already, create it and store it in v_scheduleParts and store pointer in v_schedulePartsPerCustomer
						if( sp_id == -1 ){
							//calculate demand and serviceTime (negativeNumber % PlanningHorizon = negativeNumber, thus add PlanningHorizon once)
							int numberCoveredPeriods = ((dBeforeNextVisit - d) + PlanningHorizon()) % PlanningHorizon() + 1;
							int demand = (int) floor( numberCoveredPeriods * DemandPerDay(i) + 0.001 ); //Ensure no rounding errors
							int addServiceTime = (numberCoveredPeriods - 1) * AdditionalServiceTime(); 
							v_scheduleParts.push_back(new c_SchedulePart(id, i, d, dBeforeNextVisit, demand, addServiceTime));
							v_schedulePartsPerCustomer[i].push_back(id);
							sp_id = id;
							id++;
						}
						//several schedules of one customer can contain the same schedule part
						v_schedulePartsPerCustomerAndScheduleId[i][scheduleId].push_back( sp_id );
					}
				}
				scheduleId++;
			}
		//} 
	} 
}

void c_CVRPTW_Instance::storeVertexInfos()
{
	if( taskNetwork ){ 
		i_numVertices = 2 + i_planningHorizon * i_numCustomers;
		v_nodesOfVertices = vector<int>(i_numVertices);
		v_daysOfVertices = vector<int>(i_numVertices);
		for( int v = 0; v < i_numVertices; v++){
			if( v == 0){ //first
				v_nodesOfVertices[v] = OrigDepot();
				v_daysOfVertices[v] = 0;
			} else if( v == (i_numVertices-1)  ){ //last
				v_nodesOfVertices[v] = DestDepot();
				v_daysOfVertices[v] = 0;
			} else if( v % NumCustomers() == 0  ){ //last
				v_nodesOfVertices[v] = NumCustomers();
				v_daysOfVertices[v] = (v / NumCustomers()) - 1;
			} else {
				v_nodesOfVertices[v] = v % NumCustomers();
				v_daysOfVertices[v] = v / NumCustomers();
			}
		}
	} else if( schedulePartNetwork ){
		i_numVertices = GetNumScheduleParts();
		v_nodesOfVertices = vector<int>(i_numVertices);
		v_daysOfVertices = vector<int>(i_numVertices);
		for( int v = 0; v < i_numVertices; v++){
			v_nodesOfVertices[v] = GetSchedulePart(v)->getCustomer();
			v_daysOfVertices[v] = GetSchedulePart(v)->getStartDay();
		}
	} else{
		cerr << "Error in storeVertexInfos. No network type chosen." << endl;
		throw;
	}
}

const bool c_CVRPTW_Instance::SPContainsDay(int SPId, int day) const
{
	int d = GetSchedulePart(SPId)->getStartDay();
	int endDay = GetSchedulePart(SPId)->getEndDay();
	if( d == day )
		return true;

	while( d != endDay ){
		d = (d+1)%PlanningHorizon();
		if( d == day )
			return true;
	}
	return false;
}

//-1 as endDay as wildcard, useful for non-overlapping schedules such that the starting vertex suffices
const int c_CVRPTW_Instance::GetSchedulePartId(int customer, int startDay, int endDay) const
{
	for (auto SPId = GetAllowedScheduleParts(customer).begin(); SPId != GetAllowedScheduleParts(customer).end();SPId++) {
		if( GetSchedulePart(*SPId)->getStartDay() == startDay && (GetSchedulePart(*SPId)->getEndDay() == endDay || endDay == -1) ){
			return *SPId;
		}
	}
	cerr << "Error. No schedule part id found for customer " << customer << ", startPeriod " << startDay << " and endPeriod " << endDay << endl;
	throw;
}

const bool c_CVRPTW_Instance::IsAllowedSchedulePart(pair<int,int>& schedulepart, vector<bool>& schedule)
{
	if( !schedule[schedulepart.first] )
		return false;
	int day = schedulepart.first;
	while ( day != schedulepart.second ){
		if( schedule[day] )
			return false;
		 day = (day + 1)%PlanningHorizon();
	}
	return true;
}

const int c_CVRPTW_Instance::NumVertices() const
{
	//Attention: method can be used only after settings are read
	if( taskNetwork ) 
		return i_numVertices; 
	else if( schedulePartNetwork )
		return GetNumScheduleParts();
	else
		throw;
}

vector<bool> c_CVRPTW_Instance::createSchedule(int maxDaysUntilNextVisit, int startDay)
{
	vector<bool> schedule = vector<bool>(i_planningHorizon);
	for( int i = 0; i < i_planningHorizon; i++){
		if( (i - startDay)% maxDaysUntilNextVisit == 0 ){
			schedule[i] = true;
		} else{
			schedule[i] = false;
		}
	}
	return schedule;
}


void c_CVRPTW_Instance::FixCustToFirstDay(int customer)
{
	vector<int> positionsToKeep;
	//Erase all schedules that do not include a visit on the first day
	for (int i = 0; i < v_schedulesPerCustomer[customer].size(); i++ ){
		if( v_schedulesPerCustomer[customer][i][0] ){
			positionsToKeep.push_back(i);
		}
	}
	for( int i = ((int)v_schedulesPerCustomer[customer].size())-1; i >= 0; i--){
		if( !v_contains(positionsToKeep, i) )
			v_schedulesPerCustomer[customer].erase(v_schedulesPerCustomer[customer].begin()+ i);
	}
	return;
	//In case of the standard Cordeau-instances, the following suffices by deleting all but the last schedule for the chosen customer
	v_schedulesPerCustomer[customer].erase(v_schedulesPerCustomer[customer].begin(),v_schedulesPerCustomer[customer].end()-1);
}

const double c_CVRPTW_Instance::getDistToDepot(int node) const
{
	int depot = OrigDepot();
	return sqrt( (double) (v_xCoord[node]-v_xCoord[depot])*(v_xCoord[node]-v_xCoord[depot]) + (v_yCoord[node]-v_yCoord[depot])*(v_yCoord[node]-v_yCoord[depot]) ); 
}

bool c_CVRPTW_Instance::IsCustomerNode( int i ) const
{
	return ( i != OrigDepot() && i != DestDepot() );
}


//int c_CVRPTW_Instance::Frequence(int i) const
//{
//	return v_frequence[i];
//}
//
//int c_CVRPTW_Instance::MaxDaysUntilNextVisit(int i) const
//{
//	return v_maxDaysUntilNextVisit[i];
//}

int c_CVRPTW_Instance::ArcExists( int n1, int n2 ) const
{
	if ( n1 == n2 )
		return false;
	if ( n1 == DestDepot() )
		return false;
	if ( n2 == OrigDepot() )
		return false;
	if ( n1 == OrigDepot() && n2 == DestDepot() )
		return false;
	if( StartTime(n1) + Time(n1,n2) > EndTime(n2) )
		return false;
	return ( DemandPerDay(n1) + DemandPerDay(n2) <= Capacity() + CGBase::CG_EPS );
}


bool c_CVRPTW_Instance::ArcExistsOnDay(int v1, int v2, int day) const
{
	if( taskNetwork ){
	//Mein Code:
		if( v1 == v2 )
			return false; //no loop
		int node1 = GetNode(v1);
		int node2 = GetNode(v2);
		if( ArcExists( node1, node2 ) ){ //different nodes that can succeed each other
			int day2 = GetDay(v2);
			bool destDepot = (node2 == DestDepot());
			if( day2 != day && !destDepot)
				return false; // start of customer visit has to be the vertex belonging to the day of the current network, depot belongs always to day 0 per definition

			bool node2VisitableAtThisDay = false;
			for (auto schedule = GetAllowedSchedules(node2).begin(); schedule != GetAllowedSchedules(node2).end();schedule++) {
				if( (*schedule)[day] ){
					node2VisitableAtThisDay = true;
					break;
				}
			}

			if( v1 == OrigDepot() && node2VisitableAtThisDay )
				return true; //all arcs from depot to vertices of network day are allowed

			if( !node2VisitableAtThisDay && !destDepot )
				return false; //node2 has to be visitable at this day
			for (auto schedule = GetAllowedSchedules(node1).begin(); schedule != GetAllowedSchedules(node1).end();schedule++) {
				//if at least one schedule, that is active at the network-day, allows visit at the day after day2, 
				if( (*schedule)[day%PlanningHorizon()] ){ //check all schedules that allow visit at the network day
					//if node1 belongs to network day and schedule allows new visit at next day, visitation of this customer can stop here
					int day1 = GetDay(v1);
					if( day1 == day ){
						if( (*schedule)[(day+1)%PlanningHorizon()] )
							return true;
					} else { //if node1 does not belong to network day, but there is a schedule that allows visit at day1+1
						if ( (*schedule)[(day1+1)%PlanningHorizon()] ){
							//and does not include visits at all days in (network day; day1], 
							int d_between = (day + 1) % PlanningHorizon();
							bool allBetweenNotVisitable = true;
							do{
								if( (*schedule)[d_between] )
									allBetweenNotVisitable = false;
								d_between = (d_between + 1) % PlanningHorizon();
							} while (d_between != (day1+1)%PlanningHorizon() ) ;
							if( allBetweenNotVisitable)
								return true; //visitation of customer node1 can stop here 
						}
					}
				}
			} 
		} else if ( node1 == node2 ){ //same node, but other days
			for (auto schedule = GetAllowedSchedules(node1).begin(); schedule != GetAllowedSchedules(node1).end();schedule++) {
				//if at least one schedule that is active at the network-day
				if( (*schedule)[day] ){
					//forbids visit at the day after d1 and d1+1 = d2
					int day1 = GetDay(v1);
					if( !(*schedule)[(day1+1)%PlanningHorizon()] && ((day1+1)%PlanningHorizon() == GetDay(v2)) ){
						return true; //if at least one schedule, that is active at this day, forbids visit at the following day, further demand fulfillment has to be allowed
					}
				}
			}
		}
		return false;

		//Sarahs Code
		//if (v1 == v2) 
		//	return false;
		//if (v2 == OrigDepotVertex())
		//	return false;
		//if (v1 == DestDepotVertex()) 
		//	return false;
		//if (v1 == OrigDepotVertex() && v2 == DestDepotVertex())
		//	return false;
		//if ((GetNode(v1) == GetNode(v2)) && ((GetDay(v2) % i_planningHorizon) != ((GetDay(v1)+1) % i_planningHorizon))) // keine B�gen zwischen Vertices des gleichen Kunden, die mehr als ein Tag auseinander liegen, au�er letzter Tag & erster Tag
		//	return false;
		//if ((GetNode(v1) != GetNode(v2)) && GetDay(v2) != day && v2 != DestDepotVertex()) // wenn unterschiedliche Kunden, dann darf es nur Bogen zu Vertex des zu betrachtenden Tages geben
		//	return false;
		////if (v1 == OrigDepotVertex() && GetDay(v2) == day) {// eigentlich unn�tige & teilweise falsche Nebenbedingung (da alle B�gen von Startdepot zu Kundenknoten erlaubt werden), aber anders Fehlermeldung
		////	return true;
		////} else 
		//if (v2 == DestDepotVertex()) { // B�gen zu DestDepotVertex
		//	for each (const vector <bool>& startSchedule in GetAllowedSchedules(GetNode(v1))) {// teste jeden zugeordneten schedule des Kunden & wenn f�r einen scheudle=true, return true
		//		for each (const vector <bool>& endSchedule in GetAllowedSchedules(OrigDepotVertex())) {
		//			if (startSchedule[day] == true) {
		//				if (GetDay(v1) == day) {
		//					if (startSchedule[((day+1) % i_planningHorizon)] == true) {
		//						return true;
		//					}
		//				} else { // wenn GetDay(v1) != day 
		//					if (startSchedule[GetDay(v1)] == false && startSchedule[(GetDay(v1)+1) % i_planningHorizon] == true) {
		//						//Schleife pr�ft ab day+1 bis v1-1 durch, ob alle Stellen in schedule gleich false
		//						int counter = day + 1;
		//						while ((counter % i_planningHorizon) != GetDay(v1)) {
		//							if (startSchedule[(counter % i_planningHorizon)] == false) {
		//								counter++;
		//							} else {
		//								return false;
		//							}
		//						}
		//						return true;
		//					}
		//				}
		//			}
		//		}
		//	}
		//	return false;
		//}
		//else { //wenn v2 kein DestDepotVertex
		//	for each (const vector <bool>& startSchedule in GetAllowedSchedules(GetNode(v1))) {// teste jeden zugeordneten schedule des betreffenden Kunden von v1...
		//		for each (const vector <bool>& endSchedule in GetAllowedSchedules(GetNode(v2))) {// ...mit schedules des zugeordneten Kunden von v2
		//			if (v1 == OrigDepotVertex() && GetDay(v2) == day && endSchedule[GetDay(v2)] == true) {//alle B�gen von StartDepot zu Knoten des zu betrachtenden Tages (=true) sind erlaubt
		//				return true;
		//			}
		//			if (startSchedule[day] == true && v1 != OrigDepotVertex() ) {//allgemein k�nnen B�gen nur an day existieren, wenn startSchedule[day] = true

		//				if (GetNode(v1) == GetNode(v2)) {// wenn Vertices von gleichem Kunden

		//					if (GetDay(v1) == day && GetDay(v2) == (day+1) % i_planningHorizon) {//...von v1 = day zu v2 von darauffolgendem Tag geh�rt
		//						if (GetDay(v1) == day && startSchedule[GetDay(v2)] == false)//...an Tag von v2=0
		//							return true;
		//					}

		//					if ((GetDay(v1) != day) && (startSchedule[GetDay(v1)] == false) && (startSchedule[GetDay(v2)] == false)) {
		//						//Schleife pr�ft ab day+1 bis v1-1 durch, ob alle Stellen in schedule gleich false
		//						int counter = day + 1;
		//						while ((counter % i_planningHorizon) != GetDay(v1)) {
		//							if (startSchedule[(counter % i_planningHorizon)] == false) {
		//								counter++;
		//							} else {
		//								return false;
		//							}
		//						}
		//						return true;

		//					}

		//				} else { // wenn Vertices von unterschiedlichen Kunden

		//					if (endSchedule[day] == true) { //nur B�gen zu Vertices m�glich, deren schedule an day = true
		//						if (GetDay(v1) == day) {//wenn Tag des Vertex v1 dem zu betrachtenden Tag day entspricht; GetDay(v2) != day wurde oben schon ausgeschlossen
		//							if (startSchedule[((day+1) % i_planningHorizon)] == true)//v1 (1,1,..) & v2 (1,..) 
		//								return true;
		//						} else { // wenn Tag von v1 ! = day ist
		//							if (startSchedule[GetDay(v1)] == false && startSchedule[(GetDay(v1)+1) % i_planningHorizon] == true) {
		//								//Schleife pr�ft ab day+1 bis v1-1 durch, ob alle Stellen in schedule gleich false
		//								int counter = day + 1;
		//								while ((counter % i_planningHorizon) != GetDay(v1)) {
		//									if (startSchedule[(counter % i_planningHorizon)] == false) {
		//										counter++;
		//									} else {
		//										return false;
		//									}
		//								}
		//								return true;
		//							}
		//						}
		//					}
		//				}
		//			}
		//		}
		//	}
		//} 
		//return false;
	} else if( schedulePartNetwork ){
		//if connection between nodes exist and both schedule parts belong to day, arc exists
		if( ArcExists(GetSchedulePart(v1)->getCustomer(), GetSchedulePart(v2)->getCustomer() ) ){
			if( GetSchedulePart(v1)->getStartDay() == day && GetSchedulePart(v2)->getStartDay() == day ){
				return true;
			}
		} 
		return false;
	} else{
		cerr << "Error. One network type has to be chosen." << endl;
		throw;
	}
}

/////////////////////////////////////////////////////////////////////////////
// c_CVRP_Instance_With_RDC
/////////////////////////////////////////////////////////////////////////////


c_CVRPTW_Instance_With_RDC::c_CVRPTW_Instance_With_RDC( string instanceFile, string scheduleFile) 
:	c_CVRPTW_Instance( instanceFile, scheduleFile  ),
	//m_rdc( NumVertices(), NumVertices(), PlanningHorizon() ),
	v_ng( NumNodes() ),
	v_ng_index( NumNodes() )
	/*v_enforcedPartnersPerNode( NumNodes()),
	v_forbiddenPartnersPerNode( NumNodes() )*/
{
	for ( int i=0; i<NumNodes(); i++ )
		v_ng_index[i].resize( NumNodes(), -1 );
}


void c_CVRPTW_Instance_With_RDC::InitializeRDCMatrix()
{
	m_rdc = c_DoubleMatrix(NumVertices(), NumVertices(), PlanningHorizon());
}

//void c_CVRPTW_Instance_With_RDC::ResetTogetherness()
//{
//	for ( int i = 0; i < NumNodes(); i++ ){
//		v_enforcedPartnersPerNode[i].clear();
//		v_forbiddenPartnersPerNode[i].clear();
//	}
//}
//
//void c_CVRPTW_Instance_With_RDC::SetTogetherness(int cust1, int cust2, bool together)
//{
//	if( together ){
//		v_enforcedPartnersPerNode[cust1].push_back(cust2);
//		v_enforcedPartnersPerNode[cust2].push_back(cust1);
//	} else{
//		v_forbiddenPartnersPerNode[cust1].push_back(cust2);
//		v_forbiddenPartnersPerNode[cust2].push_back(cust1);
//	}
//}

void c_CVRPTW_Instance_With_RDC::SetRDC( int i, int j, int day, double rdc )
{
	m_rdc(i, j, day) = rdc;
}


double c_CVRPTW_Instance_With_RDC::RDC( int i, int j ,int day ) const
{
	return m_rdc(i, j, day);
}


const int c_CVRPTW_Instance_With_RDC::NumberOfVehicles()
{
	return NumVehiclesPerDay() * PlanningHorizon();
}

void c_CVRPTW_Instance_With_RDC::SetNGNeighborhood( int i, const vector<int>& ng_i )
{
	v_ng[i] = ng_i;
	int n = (int)v_ng.size();
	for ( int j=0; j<n; j++ )
		v_ng_index[i][j] = -1;
	for ( int idx =0; idx<(int)v_ng[i].size(); idx++ )
		v_ng_index[i][ v_ng[i][idx] ] = idx;
}

int c_CVRPTW_Instance_With_RDC::NGNeighborhoodSize( int i ) const
{
	return (int)v_ng[i].size();
}

int c_CVRPTW_Instance_With_RDC::NGNeighbor( int i, int idx ) const
{
	return v_ng[i][idx];
}

int c_CVRPTW_Instance_With_RDC::NGIndex( int i, int j ) const
{
	return v_ng_index[i][j];
}

bool c_CVRPTW_Instance_With_RDC::AddToNeighborhood(int v, int nodeToBeAdded, int maxNgSize)
{
	//if max ng size not exceeded and not set before return true
	if( (int) v_ng[v].size() < maxNgSize && v_ng_index[v][nodeToBeAdded] == -1 ){
		v_ng[v].push_back(nodeToBeAdded);
		v_ng_index[v][nodeToBeAdded] = v_ng[v].size() - 1; //the new neighbor is added at the last position
		return true;
	} else{
		return false;
	}
}