#ifndef ESPPRC_LABEL_H
#define ESPPRC_LABEL_H

#include "ref.h"

////////////////////////////////////////////////////////////////////////////////////////////////////////
// c_Label
////////////////////////////////////////////////////////////////////////////////////////////////////////
namespace espprc
{

template <class ResVect, class REF>
class c_Label : public ResVect {
public:
	typedef c_Label<ResVect,REF> tLabel;
protected:
	// the predecessor label in the partial path
	const c_Label* p_pred;
	// the REF with which this label has been created
	const REF* p_REF;	
	// the next label of this node for singly-linked list, e.g., used in dominance algorithm
	c_Label<ResVect,REF>* p_next;
	int i_id;
public:
	c_Label( int id, ResVect&& res_vector, const tLabel* pred, const REF* ref );
	// TODO: do we really need a virtual destructor?
	virtual ~c_Label() {}
	// getter
	const tLabel* predecessor() const { return p_pred; }
	const REF* generator_REF() const { return p_REF; }
	tLabel* next() const { return p_next; } // use this to iterate over singly-linked lists
	int Id() const { return i_id; }
	constexpr bool is_extended() const { return false; }
	// setter
	void set_extended() {}
	void set_next( c_Label* label ) { p_next = label; }
#ifdef SPPRC_CHECK_PATH
	bool b_check_required;
#endif
};

////////////////////////////////////////////////////////////////////////////////////////////////////////
// implementation
////////////////////////////////////////////////////////////////////////////////////////////////////////

template <class ResVect, class REF>
c_Label<ResVect,REF>::c_Label( int id, ResVect&& res_vector, const tLabel* pred, const REF* ref)
:	ResVect(std::move(res_vector)), 
	p_pred(pred),
	p_REF(ref),
	p_next(NULL),
	i_id(id)
{}

}

#endif // of ESPPRC_LABEL_H