#ifndef ESPPRC_REF_H
#define ESPPRC_REF_H

#include <vector>
#include <bitset>

////////////////////////////////////////////////////////////////////////////////////////////////////////
// c_REF
////////////////////////////////////////////////////////////////////////////////////////////////////////
namespace espprc
{

/* It is recommended that you derive your problem-specific REFs from this base class */
/* - In many applications, the forward and backward resource vectors are identical.  */
/*   However, sometime they have different components.								 */ 
/* - Make sure that only negative rdc labels are created at the sink                 */

template <class ResVectFw, class ResVectBw>
class c_REF {
public:
	typedef ResVectFw ResVectFW;
	typedef ResVectBw ResVectBW;
protected:
	int i_tail; 
	int i_head;
public:
	c_REF( int tail_id, int head_id );
	// getter
	int Tail() const { return i_tail; }
	int Head() const { return i_head; }
	// propagation: the derived class must specify these methods
	//		bool PropagateFw( const ResVectFW& old_res, ResVectFW& new_res ) const;
	//		bool PropagateBw( const ResVectBW& old_res, ResVectBW& new_res ) const;
};

////////////////////////////////////////////////////////////////////////////////////////////////////////
// implementation
////////////////////////////////////////////////////////////////////////////////////////////////////////

template <class ResVectFw, class ResVectBw>
c_REF<ResVectFw,ResVectBw>::c_REF(int tail_id, int head_id)
:	i_tail(tail_id),
	i_head(head_id)
{}

};
#endif // of ESPPRC_REF_H