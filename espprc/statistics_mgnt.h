#ifndef STATISTICS_MGNT_H
#define STATISTICS_MGNT_H

#include <vector>

#include "../SharedFiles/stopwatch.h"

namespace espprc
{
	using namespace std;

	// for everything related to statistics

	////////////////////////////////////////////////////////////////////////////////////////////////////////
	// c_EmptyStatisticsManager
	////////////////////////////////////////////////////////////////////////////////////////////////////////

	class c_EmptyStatisticsManager {
	public:
		c_EmptyStatisticsManager() {}
		void resize(int num_nodes) {}
		// the following member functions must be provided, possible to leave them blank
		// this way the compiler can ompletely disregard the member function so that 
		// (true!!!) zero overhead results.
			// fw
		void OnStartFwLabeling() {}
		void OnEndFwLabeling() {}
		void OnStartFwPropagate() {}
		void OnEndFwPropagate() {}
		void OnStartFwDominance() {}
		void OnEndFwDominance() {}
		// bw
		void OnStartBwLabeling() {}
		void OnEndBwLabeling() {}
		void OnStartBwPropagate() {}
		void OnEndBwPropagate() {}
		void OnStartBwDominance() {}
		void OnEndBwDominance() {}
		// bidir	
		void OnStartBidirLabeling() {}
		void OnEndBidirLabeling() {}
		void OnStartBidirFwLabeling() {} // fw
		void OnEndBidirFwLabeling() {}
		void OnStartBidirFwPropagate() {}
		void OnEndBidirFwPropagate() {}
		void OnStartBidirFwDominance() {}
		void OnEndBidirFwDominance() {}
		void OnStartBidirBwLabeling() {} //bw
		void OnEndBidirBwLabeling() {}
		void OnStartBidirBwPropagate() {}
		void OnEndBidirBwPropagate() {}
		void OnStartBidirBwDominance() {}
		void OnEndBidirBwDominance() {}
		void OnStartBidirMerge() {} // merge
		void OnEndBidirMerge() {}
		// dominance manager
		void OnLabelInserted(bool fw, int node) {}
		void OnDominanceTest(bool fw, int node) {}

		void ResetStatistics() {}
		void OutputInStream(std::ostream& s) const { s << "No statistics" << endl; }
	};


	////////////////////////////////////////////////////////////////////////////////////////////////////////
	// c_StatisticsManager
	////////////////////////////////////////////////////////////////////////////////////////////////////////


	class c_StatisticsManager {
	public:
		c_Stopwatch o_timer_fw_labeling;
		c_Stopwatch o_timer_fw_labeling_propagate;
		c_Stopwatch o_timer_fw_labeling_dominance;
		//
		c_Stopwatch o_timer_bw_labeling;
		c_Stopwatch o_timer_bw_labeling_propagate;
		c_Stopwatch o_timer_bw_labeling_dominance;
		//
		c_Stopwatch o_timer_bidir_labeling;
		c_Stopwatch o_timer_bidir_labeling_fw_part;
		c_Stopwatch o_timer_bidir_labeling_bw_part;
		c_Stopwatch o_timer_bidir_labeling_merge;
		c_Stopwatch o_timer_bidir_labeling_fw_part_propagate;
		c_Stopwatch o_timer_bidir_labeling_fw_part_dominance;
		c_Stopwatch o_timer_bidir_labeling_bw_part_propagate;
		c_Stopwatch o_timer_bidir_labeling_bw_part_dominance;
		// 
		std::vector<int> v_cnt_fw_labels_inserted;
		std::vector<int> v_cnt_bw_labels_inserted;
		std::vector<int> v_cnt_fw_dominance_tests;
		std::vector<int> v_cnt_bw_dominance_tests;
		//
		void OutputInStream(const c_Stopwatch& timer, std::string text, std::ostream& s) const;
	public:
		c_StatisticsManager() : v_cnt_fw_labels_inserted(0), v_cnt_bw_labels_inserted(0), v_cnt_fw_dominance_tests(0), v_cnt_bw_dominance_tests(0) {}
		void resize(int num_nodes);
		// fw
		void OnStartFwLabeling() { o_timer_fw_labeling.Restart(); }
		void OnEndFwLabeling() { o_timer_fw_labeling.Stop(); }
		void OnStartFwPropagate() { o_timer_fw_labeling_propagate.Restart(); }
		void OnEndFwPropagate() { o_timer_fw_labeling_propagate.Stop(); }
		void OnStartFwDominance() { o_timer_fw_labeling_dominance.Restart(); }
		void OnEndFwDominance() { o_timer_fw_labeling_dominance.Stop(); }
		// bw
		void OnStartBwLabeling() { o_timer_bw_labeling.Restart(); }
		void OnEndBwLabeling() { o_timer_bw_labeling.Stop(); }
		void OnStartBwPropagate() { o_timer_bw_labeling_propagate.Restart(); }
		void OnEndBwPropagate() { o_timer_bw_labeling_propagate.Stop(); }
		void OnStartBwDominance() { o_timer_bw_labeling_dominance.Restart(); }
		void OnEndBwDominance() { o_timer_bw_labeling_dominance.Stop(); }
		// bidir	
		void OnStartBidirLabeling() { o_timer_bidir_labeling.Restart(); }
		void OnEndBidirLabeling() { o_timer_bidir_labeling.Stop(); }
		//
		void OnStartBidirFwLabeling() { o_timer_bidir_labeling_fw_part.Restart(); } // fw
		void OnEndBidirFwLabeling() { o_timer_bidir_labeling_fw_part.Stop(); }
		void OnStartBidirFwPropagate() { o_timer_bidir_labeling_fw_part_propagate.Restart(); }
		void OnEndBidirFwPropagate() { o_timer_bidir_labeling_fw_part_propagate.Stop(); }
		void OnStartBidirFwDominance() { o_timer_bidir_labeling_fw_part_dominance.Restart(); }
		void OnEndBidirFwDominance() { o_timer_bidir_labeling_fw_part_dominance.Stop(); }
		//
		void OnStartBidirBwLabeling() { o_timer_bidir_labeling_bw_part.Restart(); } //bw
		void OnEndBidirBwLabeling() { o_timer_bidir_labeling_bw_part.Stop(); }
		void OnStartBidirBwPropagate() { o_timer_bidir_labeling_bw_part_propagate.Restart(); }
		void OnEndBidirBwPropagate() { o_timer_bidir_labeling_bw_part_propagate.Stop(); }
		void OnStartBidirBwDominance() { o_timer_bidir_labeling_bw_part_dominance.Restart(); }
		void OnEndBidirBwDominance() { o_timer_bidir_labeling_bw_part_dominance.Stop(); }
		//
		void OnStartBidirMerge() { o_timer_bidir_labeling_merge.Restart(); } // merge
		void OnEndBidirMerge() { o_timer_bidir_labeling_merge.Stop(); }
		// dominance manager
		void OnLabelInserted(bool fw, int node);
		void OnDominanceTest(bool fw, int node);
		//
		void ResetStatistics();
		void OutputInStream(std::ostream& s) const;
	};


} // of namespace espprc

#endif // of STATISTICS_MGNT_H
