#ifndef ESPPRC_DOMINANCE_MGNT_H
#define ESPPRC_DOMINANCE_MGNT_H

#include <iostream>
#include <functional>

#include "statistics_mgnt.h"

/////////////////////////////////////////////////////////////////////////////
// c_Dominance_Manager_ESPPC
/////////////////////////////////////////////////////////////////////////////
namespace espprc
{

template <class Label, class StatManager = c_EmptyStatisticsManager>
class c_Dominance_Manager {
protected:
	StatManager o_initial_stat;
	StatManager* p_stat;
	Label* p_first_label; /* singly-linked list with those "old" labels on which the dominance algorithm has already been run */
	Label* p_first_added_label; /* singly-linked list with "new" labels, dominance algorithm has NOT been run */
	std::function<bool(Label*, Label*)>* f_sorter;
	std::function<bool(Label*, Label*)>* f_breakingCriterion;
	int i_node;
	int i_added_labels;
	bool b_fw;
	// private member functions
	void Sort(Label*& start, std::function<bool(Label*, Label*)>* sorter);
	// void Reverse(Label*& start); /* needed? */
public:
	c_Dominance_Manager();
	void SetInfo(bool fw, int node) { b_fw = fw; i_node = node;	}

	void insert( Label* );
	Label* apply_dominance(int bm_bucket_index = -1); // return value is first label to delete
	Label* apply_final_dominance(int bm_bucket_index = -1); // return value is first label to delete
	Label* begin(); // iterate over all undominated labels
	Label* next(Label* current);
	void clear();
	int size() const { return i_added_labels; }

	void OutputInStream(std::ostream& s ) const; // Output all Label of this dominance manager
	void Sort( std::function<bool (Label*,Label*)>* sorter );
	void SetSortingFunction(std::function<bool(Label*, Label*)>* sorter) { f_sorter = sorter; }
	void SetBreakingCriterionFunction(std::function<bool(Label*, Label*)>* breakingCriterion) { f_breakingCriterion = breakingCriterion; }
	void SetStatisticsManager(StatManager& stat) { p_stat = &stat; }
	StatManager& StatisticsManager() { return *p_stat; }
	void PrepareForWarmstart(int currentExtensionManagerBucket) {};
#ifdef SPPRC_CHECK_PATH
	void CheckDominance(Label* label1, Label* label2);
#endif
};


template <class Label, class StatManager>
c_Dominance_Manager<Label, StatManager>::c_Dominance_Manager()
:	o_initial_stat(),
	p_stat(&o_initial_stat),
	p_first_label(nullptr),
	p_first_added_label(nullptr),
	f_sorter(nullptr),	
	f_breakingCriterion(nullptr),
	i_node(0),	
	i_added_labels(0),
	b_fw(false)
{}


template <class Label, class StatManager>
void c_Dominance_Manager<Label, StatManager>::Sort( std::function<bool (Label*,Label*)>* sorter )
{
	Sort( p_first_label, sorter );
}


template <class Label, class StatManager>
void c_Dominance_Manager<Label, StatManager>::Sort( Label*& from, std::function<bool (Label*,Label*)>* sorter )
{
	// trivial cases
	if ( from == nullptr || from->next() == nullptr)
		return;
	if ( from->next()->next() == nullptr ) // only two labels
	{
		if ( !(*sorter)( from, from->next() ) )
		{
			Label* new_start = from->next();
			from->next()->set_next( from );
			from->set_next(nullptr);
			from = new_start;
		}
		return;
	}
	// Split in the middle
	int length = 0;
	Label* it  = from;
	Label* mid = from;
	while ( it )
	{
		it = it->next();
		length++;
		if ( length % 2 == 0 && it )
			mid = mid->next();
	}

	// Sort each part 
	Label* right = mid->next();
	mid->set_next(nullptr);
	Sort( from, sorter );
	Sort( right, sorter );
	// Merge both parts together
	Label* result = nullptr;
	Label* last = nullptr;
	while ( from && right )
	{
		if (result == nullptr)
		{
			if ((*sorter)(from, right))
				result = from;
			else
				result = right;
		}
		if ( (*sorter)( from, right ) )
		{
			if ( last )
				last->set_next( from );
			last = from;
			from = from->next();
		}
		else
		{
			if ( last )
				last->set_next( right );
			last = right;
			right = right->next();
		}
	}
	// add remaining parts
	if ( from )
		last->set_next( from );
	if ( right )
		last->set_next( right );
	// return resulting start label
	from = result;
}


template <class Label, class StatManager>
void c_Dominance_Manager<Label, StatManager>::clear()
{
	i_added_labels=0;
	while (p_first_label)  
	{
		Label* help= p_first_label;
		p_first_label=p_first_label->next();		
		delete help;
	}
	while (p_first_added_label)  
	{
		Label* help=p_first_added_label;
		p_first_added_label=p_first_added_label->next();		
		delete help;
	}
	p_first_label = nullptr;
	p_first_added_label = nullptr;
}


template <class Label, class StatManager>
Label* c_Dominance_Manager<Label, StatManager>::next(Label* current)
{
	return current->next();
}


template <class Label, class StatManager>
Label* c_Dominance_Manager<Label, StatManager>::begin()
{
	return p_first_label;
}


template <class Label, class StatManager>
void c_Dominance_Manager<Label, StatManager>::OutputInStream(std::ostream& s) const
{
	Label* label=p_first_label;
	while (label)
	{
#ifdef SPPRC_CHECK_PATH
		if (label->b_check_required)
			s << "### ";
#endif
		label->OutputInStream(s);
		s << "Id=" << label->Id();
		if (label->predecessor())
			s << " pred_Id=" << label->predecessor()->Id();
		s << endl;
		label=label->next();
	}
	s << "________________" << endl;
}


template <class Label, class StatManager>
void c_Dominance_Manager<Label, StatManager>::insert( Label* new_label )
{
	p_stat->OnLabelInserted(b_fw,i_node);
	// insert into singly-linked list right at the beginning
	i_added_labels++;
	new_label->set_next( p_first_added_label );
	p_first_added_label = new_label;
}


template <class Label, class StatManager>
Label* c_Dominance_Manager<Label, StatManager>::apply_final_dominance(int bm_bucket_index)
{
	return apply_dominance(bm_bucket_index);
}

/* 
Test later empirically:
1. What is faster? First check dominance between old and new labels, second new vs. new labels; or vice versa. 
2. ...
*/

///* return value is singly-linked list of all those labels that need to be deleted */
template <class Label, class StatManager>
Label* c_Dominance_Manager<Label, StatManager>::apply_dominance(int bm_bucket_index)
{
	// if there are no additional labels then there are no labels in dominance
	if (!p_first_added_label)
		return nullptr;
	Label* ret_labels_to_del = nullptr;
	// sort new labels (old ones are already sorted)
	if ( f_sorter )
		Sort(p_first_added_label, f_sorter);
	// dominance between two new labels (quick solution: test in both directions)
	Label* pred_new1 = nullptr;
	for (Label* curr_new1 = p_first_added_label;
		curr_new1;
		pred_new1 = curr_new1, curr_new1 = (curr_new1 ? curr_new1->next() : nullptr))
	{
		Label* pred_new2 = curr_new1;
		for (Label* curr_new2 = curr_new1->next(); curr_new2; /**/)
		{
			p_stat->OnDominanceTest(b_fw,i_node);
			if (*curr_new1 <= *curr_new2)
			{ // new1 dominates new2
#ifdef SPPRC_CHECK_PATH
				CheckDominance(curr_new1, curr_new2);
#endif
				pred_new2->set_next(curr_new2->next());
				curr_new2->set_next(ret_labels_to_del);
				ret_labels_to_del = curr_new2;
				curr_new2 = pred_new2->next();
				i_added_labels--;
			}
			else 
			{
				p_stat->OnDominanceTest(b_fw, i_node);
				if (*curr_new2 <= *curr_new1)
				{ // new2 dominates new1
#ifdef SPPRC_CHECK_PATH
					CheckDominance(curr_new2, curr_new1);
#endif
					if (pred_new1)
						pred_new1->set_next(curr_new1->next());
					else
						p_first_added_label = curr_new1->next();
					curr_new1->set_next(ret_labels_to_del);
					ret_labels_to_del = curr_new1;
					curr_new1 = (pred_new1 ? pred_new1->next() : p_first_added_label);
					curr_new2 = curr_new1->next();
					pred_new2 = curr_new1;
					i_added_labels--;
				}
				else // no domination
				{
					pred_new2 = curr_new2;
					curr_new2 = curr_new2->next();
				}
			}
		}
	}
	// old dominates new labels
	Label* pred_old = nullptr;
	for (Label* curr_old = p_first_label; curr_old;
		pred_old = curr_old, curr_old = (curr_old ? curr_old->next() : nullptr))
	{
		Label* pred_new = nullptr;
		for (Label* curr_new = p_first_added_label; curr_new; /**/)
		{
			if (f_breakingCriterion && (*f_breakingCriterion)(curr_old, curr_new))
				break;
			p_stat->OnDominanceTest(b_fw, i_node);
			if (*curr_old <= *curr_new)
			{ // old dominates new
#ifdef SPPRC_CHECK_PATH
				CheckDominance(curr_old, curr_new);
#endif
				if (pred_new)
					pred_new->set_next(curr_new->next());
				else
					p_first_added_label = curr_new->next();
				curr_new->set_next(ret_labels_to_del);
				ret_labels_to_del = curr_new;
				curr_new = (pred_new ? pred_new->next() : p_first_added_label);
				i_added_labels--;
			}
			else // no dominance
			{
				pred_new = curr_new;
				curr_new = curr_new->next();
			}
		}
	}
	// new dominates old labels
	Label* pred_new = nullptr;
	for (Label* curr_new = p_first_added_label; curr_new;
		pred_new = curr_new, curr_new = (curr_new ? curr_new->next() : nullptr))
	{
		Label* pred_old = nullptr;
		for (Label* curr_old = p_first_label; curr_old; /**/)
		{
			if (f_breakingCriterion && (*f_breakingCriterion)(curr_new, curr_old))
				break;
			p_stat->OnDominanceTest(b_fw, i_node);
			if (*curr_new <= *curr_old)
			{ // new dominates old
#ifdef SPPRC_CHECK_PATH
				CheckDominance(curr_new, curr_old);
#endif
				if (pred_old)
					pred_old->set_next(curr_old->next());
				else
					p_first_label = curr_old->next();
				curr_old->set_next(ret_labels_to_del);
				ret_labels_to_del = curr_old;
				curr_old = (pred_old ? pred_old->next() : p_first_label);
				i_added_labels--;
			}
			else // no dominance
			{
				pred_old = curr_old;
				curr_old = curr_old->next();
			}
		}
	}
	// concatenate
	if (f_sorter && p_first_added_label && p_first_label)
	{
		// splice (both lists are sorted)
		Label* result = nullptr;
		Label* last = nullptr;
		while (p_first_added_label && p_first_label)
		{
			if (result == nullptr)
			{
				if ((*f_sorter)(p_first_added_label, p_first_label))
					result = p_first_added_label;
				else
					result = p_first_label;
			}
			if ((*f_sorter)(p_first_added_label, p_first_label))
			{
				if (last)
					last->set_next(p_first_added_label);
				last = p_first_added_label;
				p_first_added_label = p_first_added_label->next();
			}
			else
			{
				if (last)
					last->set_next(p_first_label);
				last = p_first_label;
				p_first_label = p_first_label->next();
			}
		}
		// add remaining parts
		if (p_first_added_label)
			last->set_next(p_first_added_label);
		if (p_first_label)
			last->set_next(p_first_label);
		p_first_label = result;
	}
	else if (!f_sorter && p_first_added_label && p_first_label)
	{
		// concatenate without sorting; first new, then old
		Label* last = p_first_added_label;
		while (last->next())
			last = last->next();
		last->set_next(p_first_label);
		p_first_label = p_first_added_label;
	}
	else 
	{
		if (p_first_added_label)
			p_first_label = p_first_added_label;
	}
	p_first_added_label = nullptr;
	// those labels to delete
	return ret_labels_to_del;
}


#ifdef SPPRC_CHECK_PATH
template<class Label, class StatManager>
inline void c_Dominance_Manager<Label, StatManager>::CheckDominance(Label * label1, Label * label2)
{
	if (label2->b_check_required)
	{
		cout << "### Label Id=" << label1->Id() << " eliminates Label Id=" << label2->Id() << "\n";
	}
}
#endif

};


#endif // ESPPRC_DOMINANCE_MGNT_H