#ifndef ESPPTW_EXTENSION_MGNT_H
#define ESPPTW_EXTENSION_MGNT_H

/*
This is what used to be a "bucket manager".

It is however responsible to control which label 
is selected next to be *extended*. Therefore, the
new terminology is "extension manager"
*/

#include <iostream>
#include <vector>
#include <map>


namespace espprc
{

/////////////////////////////////////////////////////////////////////////////
// c_Extension_Manager 
// for ESPPRC with only positive (>0) critical resource consumptions
/////////////////////////////////////////////////////////////////////////////
//
// * should be the fastest extension manager
// * works only for instances with no REF with value 0 on the critical resource
// * LIFO: labels that are inserted last are extended first
// * dominance is invoked as soon as the last label of a bucket is finished


template <class Label, bool increasingRes>
class c_Extension_Manager {
protected:
	int i_curr_bucket_idx;
	int i_last_id;
	std::vector<std::vector<Label*> > o_buckets;
	bool b_invoke_dom_alg;
	std::vector<int> v_position; // position for label id
	// statistics
	int i_added_labels;
	int i_erased_labels;
public:
	c_Extension_Manager();
	typedef Label tLabel;

	void insert(Label*);
	void erase(Label*);
	bool is_erased(int id) const;
	Label* get_next(); // return and set next label to propagate
	bool invoke_dominance_algorithm();
	void clear();

	void OutputInStream(std::ostream& s) const; // IO: number of labels in current bucket
	// statistics
	int AddedLabels() const { return i_added_labels; }
	int ErasedLabels() const { return i_erased_labels; }
	int OpenLabels() const { return (i_added_labels - i_erased_labels); }
	int GetBucketIndex() const { return i_curr_bucket_idx; }
	void ResetBucketIndex() { increasingRes ? i_curr_bucket_idx = 0 : i_curr_bucket_idx = (int)o_buckets.size() - 1; }
	void IncreaseBucketIndex();
};


template <class Label, bool increasingRes>
bool c_Extension_Manager<Label, increasingRes>::is_erased(int id) const
{
	return (v_position[id] == -1);
}


template <class Label, bool increasingRes>
void c_Extension_Manager<Label, increasingRes>::clear()
{
	int buckets_size = (int)o_buckets.size();
	for (int i = 0; i < buckets_size; i++)
		o_buckets[i].clear();
	i_curr_bucket_idx = 0;
	b_invoke_dom_alg = true;
	i_added_labels = 0;
	i_erased_labels = 0;
	i_last_id = -1;
}


template <class Label, bool increasingRes>
void c_Extension_Manager<Label, increasingRes>::OutputInStream(std::ostream& s) const
{
	s << "Labels in Bucket " << i_curr_bucket_idx << ":" << o_buckets[i_curr_bucket_idx].size() << endl;
}


template <class Label, bool increasingRes>
c_Extension_Manager<Label, increasingRes>::c_Extension_Manager()
:	i_curr_bucket_idx(0),
	i_last_id(-1),
	b_invoke_dom_alg(true),
	i_added_labels(0),
	i_erased_labels(0)
{}


template <class Label, bool increasingRes>
bool c_Extension_Manager<Label, increasingRes>::invoke_dominance_algorithm()
{
	if (b_invoke_dom_alg)
	{
		b_invoke_dom_alg = false;
		return true;
	}
	return false;
}


template <class Label, bool increasingRes>
Label* c_Extension_Manager<Label, increasingRes>::get_next()
{
	Label* ret = nullptr;
	while (!ret)
	{
		if (increasingRes)
		{
			while (i_curr_bucket_idx < (int)o_buckets.size() && o_buckets[i_curr_bucket_idx].empty())
			{
				i_curr_bucket_idx++;
				b_invoke_dom_alg = true;
			}
		}
		else
		{
			while (i_curr_bucket_idx >= 0 && o_buckets[i_curr_bucket_idx].empty())
			{
				i_curr_bucket_idx--;
				b_invoke_dom_alg = true;
			}
		}
		// nothing left?
		if (i_curr_bucket_idx < 0 || i_curr_bucket_idx >= (int)o_buckets.size())
		{
			i_last_id = -1;
			return nullptr;
		}
		// otherwise erase and return first element
		vector<Label*>& the_bucket = o_buckets[i_curr_bucket_idx];
		while (!the_bucket.empty() && !the_bucket.back())
			the_bucket.pop_back();
		if (the_bucket.empty())
			continue;
		ret = the_bucket.back();
		the_bucket.pop_back();
		i_last_id = ret->Id();
		//// Find out if the next non-null label is still in the same bucket
		//// If not, invoke the dominance algorithm
		//while (!the_bucket.empty() && !the_bucket.back())
		//	the_bucket.pop_back();
		//if (the_bucket.empty())
		//	b_invoke_dom_alg = true;
	}
	i_erased_labels++;
	return ret;
}


template <class Label, bool increasingRes>
void c_Extension_Manager<Label, increasingRes>::erase(Label* label)
{
	int id = label->Id();
	int idx = v_position[id];
	v_position[id] = -1;
	int res_val = (*label)();

	if ((int)o_buckets[res_val].size() > idx)
	{
		o_buckets[res_val][idx] = nullptr;
		i_erased_labels++;
	}
}


// insert element to bucket container
template <class Label, bool increasingRes>
void c_Extension_Manager<Label, increasingRes>::insert(Label* label)
{
	i_added_labels++;
	int res_val = (*label)();
	if (increasingRes)
	{
		//for an increasing resource, extend the bucket-container
		if (res_val >= (int)o_buckets.size())
			o_buckets.resize(2 * res_val + 1);
	}
	else
	{
		//set the current bucket index to the end, when insert is done for the first time
		if (res_val >= (int)o_buckets.size())
			o_buckets.resize(res_val + 1);
		//decreasing resource can never be less than the current bucket index, but when a new pricing iteration is started
		if (res_val > i_curr_bucket_idx)
			i_curr_bucket_idx = res_val;
	}
	int idx = (int)o_buckets[res_val].size();
	o_buckets[res_val].push_back(label);
	if ((int)v_position.size() <= label->Id())
	{
		v_position.resize(2 * label->Id() + 1, -1);
	}
	v_position[label->Id()] = idx;
}


template <class Label, bool increasingRes>
void c_Extension_Manager<Label, increasingRes>::IncreaseBucketIndex()
{
	do
	{
		if (increasingRes)
		{
			while (i_curr_bucket_idx < (int)o_buckets.size() && o_buckets[i_curr_bucket_idx].empty())
				i_curr_bucket_idx++;
		}
		else
		{
			while (i_curr_bucket_idx >= 0 && o_buckets[i_curr_bucket_idx].empty())
				i_curr_bucket_idx--;
		}
		// nothing left?
		if (i_curr_bucket_idx < 0 || i_curr_bucket_idx >= (int)o_buckets.size())
			return;
		// otherwise check if there are non-eliminated labels in this bucket
		vector<Label*>& the_bucket = o_buckets[i_curr_bucket_idx];
		while (!the_bucket.empty() && !the_bucket.back())
			the_bucket.pop_back();
	} while (o_buckets[i_curr_bucket_idx].empty());
}



}; // of namespace

#endif // ESPPTW_EXTENSION_MGNT_H