#ifndef ESPPTW_EXTENSION_MGNT_ZERO_EXT_H
#define ESPPTW_EXTENSION_MGNT_ZERO_EXT_H

/*
This is what used to be a "bucket manager".

It is however responsible to control which label 
is selected next to be *extended*. Therefore, the
new terminology is "extension manager"
*/

#include <iostream>
#include <vector>
#include <map>


namespace espprc
{

/////////////////////////////////////////////////////////////////////////////
// c_Extension_Manager_ZeroConsumption 
// for ESPPRC with critical resource consuming 0 units
/////////////////////////////////////////////////////////////////////////////
//
// * should be the second fastest bucket manager, much faster than the c_Bucket_Manager_UsingUnorderedMap
// * works for instances with REFs with value 0 on the critical resource
//
// * Differs from c_Bucket_Manager_UsingVector by: 
// * 1. FIFO: labels that are inserted first are extended first (a little bit less efficient)
// * 2. Dominance is invoked after every label extension (more costly)


/* This is the only implementation of a extension manager that can handle REFs with increase 0 in the critical 
resource defining the bucket index. It is guaranteed that for identical critical resource values a label
with smaller ID is extended before any other label with larger ID (same critical resource). */


template <class Label, bool increasingRes>
class c_Extension_Manager_ZeroConsumption {
protected:
	int i_curr_bucket_idx;
	std::vector<std::vector<Label*> > o_buckets;
	std::vector<int> v_position; // position for label id
	// statistics
	int i_added_labels;
	int i_erased_labels;
	int i_pos_in_curr_bucket;
public:
	c_Extension_Manager_ZeroConsumption();
	typedef Label tLabel;

	void insert( Label* );
	void erase( Label* );
	bool is_erased(int id) const;
	Label* get_next(); // return and set next label to propagate
	bool invoke_dominance_algorithm();
	void clear();

	void OutputInStream( std::ostream& s ) const; // IO: number of labels in current bucket
	// statistics
	int AddedLabels() const { return i_added_labels; }
	int ErasedLabels() const { return i_erased_labels; }
	int OpenLabels() const { return (i_added_labels - i_erased_labels); }
	int GetBucketIndex() const { return i_curr_bucket_idx; }
};

////////////////////////////////////////////////////////////////////////////////////////////////////////
// implementation
////////////////////////////////////////////////////////////////////////////////////////////////////////


template <class Label, bool increasingRes>
c_Extension_Manager_ZeroConsumption<Label, increasingRes>::c_Extension_Manager_ZeroConsumption()
:	i_curr_bucket_idx (0),
	i_added_labels(0),
	i_erased_labels(0),
	i_pos_in_curr_bucket(0)
{}


template <class Label, bool increasingRes>
bool c_Extension_Manager_ZeroConsumption<Label, increasingRes>::invoke_dominance_algorithm()
{
	//Dominance has to be checked after every label extension in case of REFs with value 0 on critical resource:
	//Several labels inside the same bucket could be created when the bucket is already started, which dominate each other.
	//But they are already extended when dominance is invoked only at the end of a bucket
	// -> dominance of an extended label leads to access on deleted memory!
	return true;
}


template <class Label, bool increasingRes>
bool c_Extension_Manager_ZeroConsumption<Label, increasingRes>::is_erased(int id) const
{
	return (v_position[id] == -1);
}


template <class Label, bool increasingRes>
void c_Extension_Manager_ZeroConsumption<Label, increasingRes>::clear()
{
	int buckets_size = (int)o_buckets.size();
	for (int i = 0; i < buckets_size;i++)
		o_buckets[i].clear();
	i_curr_bucket_idx = 0;
	i_added_labels = 0;
	i_erased_labels = 0;
	i_pos_in_curr_bucket = 0;
}


template <class Label, bool increasingRes>
void c_Extension_Manager_ZeroConsumption<Label, increasingRes>::OutputInStream(std::ostream& s) const
{
	s << "Labels in Bucket " << i_curr_bucket_idx << ":" << o_buckets[i_curr_bucket_idx].size() << endl;
}


//more clear and efficient version of Jean-Bertrand:
template <class Label, bool increasingRes>
Label* c_Extension_Manager_ZeroConsumption<Label, increasingRes>::get_next()
{
	if (increasingRes)
	{
		int obucketsize = (int)o_buckets.size();
		//if the end of the current bucket is passed, go to the next bucket
		if (i_pos_in_curr_bucket >= (int)o_buckets[i_curr_bucket_idx].size())
		{
			o_buckets[i_curr_bucket_idx].clear();
			i_pos_in_curr_bucket = 0;
			do
			{
				if (++i_curr_bucket_idx >= obucketsize) return nullptr;
			} while (o_buckets[i_curr_bucket_idx].empty());
		}
		//if there is no label at the current position in the bucket, increase it (and go to next bucket if the end of the bucket is passed)
		while (!(o_buckets[i_curr_bucket_idx][i_pos_in_curr_bucket]))
			if (++i_pos_in_curr_bucket >= (int)o_buckets[i_curr_bucket_idx].size())
			{
				o_buckets[i_curr_bucket_idx].clear();
				i_pos_in_curr_bucket = 0;
				do
				{
					if (++i_curr_bucket_idx >= obucketsize) return nullptr;
				} while (o_buckets[i_curr_bucket_idx].empty());
			}
	}
	else
	{
		if (i_pos_in_curr_bucket >= (int)o_buckets[i_curr_bucket_idx].size())
		{
			o_buckets[i_curr_bucket_idx].clear();
			i_pos_in_curr_bucket = 0;
			do
			{
				if (--i_curr_bucket_idx < 0) 
					return nullptr;
			} while (o_buckets[i_curr_bucket_idx].empty());
		}
		while (!(o_buckets[i_curr_bucket_idx][i_pos_in_curr_bucket]))
			if (++i_pos_in_curr_bucket >= (int)o_buckets[i_curr_bucket_idx].size())
			{
				o_buckets[i_curr_bucket_idx].clear();
				i_pos_in_curr_bucket = 0;
				do
				{
					if (--i_curr_bucket_idx < 0) 
						return nullptr;
				} while (o_buckets[i_curr_bucket_idx].empty());
			}
	}
	Label* ret = o_buckets[i_curr_bucket_idx][i_pos_in_curr_bucket];
	++i_pos_in_curr_bucket;
	++i_erased_labels;
	return ret;
}


template <class Label, bool increasingRes>
void c_Extension_Manager_ZeroConsumption<Label, increasingRes>::erase( Label* label )
{
	if ((int)v_position.size() <= label->Id())
		return;
	i_erased_labels++;
	int idx=v_position[label->Id()];
	v_position[label->Id()] = -1;
	int res_val = (*label)();
	if (idx>-1)
		o_buckets[res_val][idx] = nullptr;
}


// insert element to bucket container
template <class Label, bool increasingRes> 
void c_Extension_Manager_ZeroConsumption<Label, increasingRes>::insert( Label* label )
{
	i_added_labels++;
	int res_val = (*label)();
	if( increasingRes )
	{
		//for an increasing resource, extend the bucket-container
		if ( res_val >= (int)o_buckets.size() ) 
			o_buckets.resize( 2*res_val+1 );
	} 
	else
	{
		//set the current bucket index to the end, when insert is done for the first time
		if ( res_val >= (int)o_buckets.size() ) 			
			o_buckets.resize( res_val+1 );
		//decreasing resource can never be less than the current bucket index, but when a new pricing iteration is started
		if( res_val > i_curr_bucket_idx )
			i_curr_bucket_idx = res_val;
	}
	int idx=(int) o_buckets[res_val].size();
	o_buckets[res_val].push_back(label);
	if ((int)v_position.size()<=label->Id())
	{
		v_position.resize(2*label->Id()+1,-1);
	}
	v_position[label->Id()]=idx;
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////


}; // of namespace

#endif // ESPPTW_EXTENSION_MGNT_ZERO_EXT_H