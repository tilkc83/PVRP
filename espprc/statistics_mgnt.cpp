#include "statistics_mgnt.h"

namespace espprc
{
	using namespace std;

	// for everything related to statistics

	void c_StatisticsManager::OnLabelInserted(bool fw, int node) 
	{ 
		if (fw) 
			v_cnt_fw_labels_inserted[node]++; 
		else 
			v_cnt_bw_labels_inserted[node]++; 
	}

	void c_StatisticsManager::OnDominanceTest(bool fw, int node)
	{
		if (fw)
			v_cnt_fw_dominance_tests[node]++;
		else
			v_cnt_bw_dominance_tests[node]++;
	}


	void c_StatisticsManager::OutputInStream(const c_Stopwatch& timer, std::string text, std::ostream& s) const
	{
		if (timer.NumStops() == 1)
		{
			s << text << " " << timer.FormattedTime() << "  " << timer.TotalNanoseconds() << " ns" << endl;
		}
		if (timer.NumStops() > 1)
		{
			s << text << " (#=" << timer.NumStops() << ")" << endl;
			s << "\tavg: " << timer.FormattedAvgTime() << "  " << timer.AvgNanoseconds() << " ns" << endl;
			s << "\tmin: " << timer.FormattedMinTime() << "  " << timer.MinNanoseconds() << " ns" << endl;
			s << "\tmax: " << timer.FormattedMaxTime() << "  " << timer.MaxNanoseconds() << " ns" << endl;
		}
	}

	void c_StatisticsManager::resize(int num_nodes) 
	{
		v_cnt_fw_labels_inserted.resize(num_nodes, 0); 
		v_cnt_bw_labels_inserted.resize(num_nodes, 0); 
		v_cnt_fw_dominance_tests.resize(num_nodes, 0); 
		v_cnt_bw_dominance_tests.resize(num_nodes, 0);
	}


	void c_StatisticsManager::OutputInStream(std::ostream& s) const
	{
		s << "Statistics:" << endl;
		OutputInStream(o_timer_fw_labeling, "fw labeling", s);
		OutputInStream(o_timer_fw_labeling_propagate, "fw propagate", s);
		OutputInStream(o_timer_fw_labeling_dominance, "fw dominance", s);
		//
		OutputInStream(o_timer_bw_labeling, "bw labeling", s);
		OutputInStream(o_timer_bw_labeling_propagate, "bw propagate", s);
		OutputInStream(o_timer_bw_labeling_dominance, "bw dominance", s);
		//
		OutputInStream(o_timer_bidir_labeling, "bidir labeling", s);
		OutputInStream(o_timer_bidir_labeling_fw_part, "bidir fw labeling", s);
		OutputInStream(o_timer_bidir_labeling_fw_part_propagate, "bidir fw propagate", s);
		OutputInStream(o_timer_bidir_labeling_fw_part_dominance, "bidir fw dominance", s);
		OutputInStream(o_timer_bidir_labeling_bw_part, "bidir bw labeling", s);
		OutputInStream(o_timer_bidir_labeling_bw_part_propagate, "bidir bw propagate", s);
		OutputInStream(o_timer_bidir_labeling_bw_part_dominance, "bidir bw dominance", s);
		OutputInStream(o_timer_bidir_labeling_merge, "bidir merge", s);
		//
		s << "Labels inserted fw: ";
		for (int i = 0; i < (int)v_cnt_fw_labels_inserted.size(); i++)
			if (v_cnt_fw_labels_inserted[i])
				s << "[" << i << "]:" << v_cnt_fw_labels_inserted[i] << " ";
		s << endl;
		s << "Labels inserted bw: ";
		for (int i = 0; i < (int)v_cnt_bw_labels_inserted.size(); i++)
			if (v_cnt_bw_labels_inserted[i])
				s << "[" << i << "]:" << v_cnt_bw_labels_inserted[i] << " ";
		s << endl;
		//
		s << "Dominance tests fw: ";
		for (int i = 0; i < (int)v_cnt_fw_dominance_tests.size(); i++)
			if (v_cnt_fw_dominance_tests[i])
				s << "[" << i << "]:" << v_cnt_fw_dominance_tests[i] << " ";
		s << endl;
		s << "Dominance tests bw: ";
		for (int i = 0; i < (int)v_cnt_bw_dominance_tests.size(); i++)
			if (v_cnt_bw_dominance_tests[i] )
			s << "[" << i << "]:" << v_cnt_bw_dominance_tests[i] << " ";
		s << endl;
	}

	void c_StatisticsManager::ResetStatistics()
	{
		// timers
		o_timer_fw_labeling.Reset();
		o_timer_bw_labeling.Reset();
		o_timer_bidir_labeling.Reset();
		o_timer_bidir_labeling_fw_part.Reset();
		o_timer_bidir_labeling_bw_part.Reset();
		o_timer_bidir_labeling_merge.Reset();
		o_timer_fw_labeling_propagate.Reset();
		o_timer_fw_labeling_dominance.Reset();
		o_timer_bw_labeling_propagate.Reset();
		o_timer_bw_labeling_dominance.Reset();
		o_timer_bidir_labeling_fw_part_propagate.Reset();
		o_timer_bidir_labeling_fw_part_dominance.Reset();
		o_timer_bidir_labeling_bw_part_propagate.Reset();
		o_timer_bidir_labeling_bw_part_dominance.Reset();
		o_timer_bidir_labeling_merge.Reset();
		// counters
		v_cnt_fw_labels_inserted.assign(v_cnt_fw_labels_inserted.size(), 0);
		v_cnt_bw_labels_inserted.assign(v_cnt_bw_labels_inserted.size(), 0);
		v_cnt_fw_dominance_tests.assign(v_cnt_fw_dominance_tests.size(), 0);
		v_cnt_bw_dominance_tests.assign(v_cnt_fw_dominance_tests.size(), 0);
	}

} // of namespace espprc

