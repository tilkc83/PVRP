﻿#ifndef ESPPRC_SOLVER_H
#define ESPPRC_SOLVER_H

#include "statistics_mgnt.h"

#include <vector>
#include <functional>
#include <algorithm>
#include <iostream>

#include "../graphml/graphml.h"

////////////////////////////////////////////////////////////////////////////////////////////////////////
// c_Solver
////////////////////////////////////////////////////////////////////////////////////////////////////////

namespace espprc
{

using namespace std;


template <class REF, class ExtManagerFW, class DManagerFW, class ExtManagerBW, class DManagerBW, 
		class MManager, class StatManager = c_EmptyStatisticsManager>
class c_SPPRC_Solver {
protected:
	typedef typename REF::ResVectFW ResVectFW;
	typedef typename REF::ResVectBW ResVectBW;
	typedef typename ExtManagerFW::tLabel t_labelFW;
	typedef typename ExtManagerBW::tLabel t_labelBW;
	double d_EPS;
	vector<vector<REF*> > v_fw_star; 
	vector<vector<REF*> > v_bw_star; 
	ExtManagerFW o_extension_manager_fw;
	vector<DManagerFW> v_dominance_manager_fw;
	ExtManagerBW o_extension_manager_bw;
	vector<DManagerBW> v_dominance_manager_bw;
	MManager* p_merge_manager; 
	void apply_dominance_FW( int node );
	void apply_dominance_FW();
	void apply_dominance_BW( int node );
	void apply_dominance_BW();
	void apply_final_dominance_FW(int node);
	void apply_final_dominance_FW();
	void apply_final_dominance_BW(int node);
	void apply_final_dominance_BW();
	int i_extended_fw;
	int i_extended_bw;
	int i_hw;
	int i_gen_paths;
	int i_id_fwd;
	int i_id_bwd;
	StatManager o_initial_stat;
	StatManager* p_stat;
public:
	c_SPPRC_Solver( int max_nodes );
	~c_SPPRC_Solver();

	void Add( REF* ref );
	void Add_Fw( REF* ref );
	void Add_Bw( REF* ref );
	void RemoveAllREFs();
	void SetEPS( double eps ) { d_EPS = eps; }
	// forward
	bool SolveFw( int source, int sink, ResVectFW&& init_res, int max_num_paths = numeric_limits<int>::max(), int maxTime = numeric_limits<int>::max() );
	bool GetPathsFW( const int node, vector<t_labelFW*>& paths, function<bool (t_labelFW*,t_labelFW*)>* pointer_sorter = nullptr, int max_num_paths = numeric_limits<int>::max() );
	void ClearFW();
	bool WarmstartSolveFw(int source, int sink, int max_num_paths = numeric_limits<int>::max(), int maxTime = numeric_limits<int>::max());
	DManagerFW& DominanceManagerFW(int i) { return v_dominance_manager_fw[i]; }
	std::vector<DManagerFW>& GetLabelsFW() { return v_dominance_manager_fw; }
	// backward
	bool SolveBw( int source, int sink, ResVectBW&& init_res, int max_num_paths = numeric_limits<int>::max(), int maxTime = numeric_limits<int>::max() );
	bool GetPathsBW(const int node, vector<t_labelBW*>& paths, function<bool(t_labelBW*, t_labelBW*)>* pointer_sorter = nullptr, int max_num_paths = numeric_limits<int>::max());
	void ClearBW();
	bool WarmstartSolveBw(int source, int sink, int max_num_paths = numeric_limits<int>::max(), int maxTime = numeric_limits<int>::max());
	DManagerBW& DominanceManagerBW(int i) { return v_dominance_manager_bw[i]; }
	std::vector<DManagerBW>& GetLabelsBW() { return v_dominance_manager_bw; }
	// bi-directional
	bool SolveBidir( MManager& merger, int source, int sink, ResVectFW&& init_res_FW, ResVectBW&& init_res_bw, int max_num_paths = numeric_limits<int>::max(), int maxTime = numeric_limits<int>::max() );
	bool SolveBidir_DynamicHalfway( MManager& merger, int source, int sink, ResVectFW&& init_res_FW, ResVectBW&& init_res_bw, int max_num_paths = numeric_limits<int>::max(), int maxTime = numeric_limits<int>::max() );
	bool GetPathsBidir(const int sink, vector<pair<double, pair<t_labelFW*, t_labelBW*> > >& paths, function<bool(pair<double, pair<t_labelFW*, t_labelBW*> >, pair<double, pair<t_labelFW*, t_labelBW*> >)>* pointer_sorter = nullptr, int max_num_paths = numeric_limits<int>::max());
	bool GetPathsBidirBuckets(const int sink, vector<pair<double, pair<t_labelFW*, t_labelBW*> > >& paths, function<bool(pair<double, pair<t_labelFW*, t_labelBW*> >, pair<double, pair<t_labelFW*, t_labelBW*> >)>* pointer_sorter = nullptr, int max_num_paths = numeric_limits<int>::max());
	const MManager* MergeManager() const { return p_merge_manager; }
	StatManager& StatisticsManager() { return *p_stat; }
	/* this can be useful to have e.g. different statistics per network size */
	void SetStatisticsManager(StatManager& stat);
	// bi-directional for symmetric ESPPRCs
	//   Pay attention: GetPathsBidir_Symmetric may return source-source-paths as well as source-sink-paths
	bool SolveBidir_Symmetric( MManager& merger, int source, int sink, ResVectFW&& init_res, int max_num_paths = numeric_limits<int>::max(), int maxTime = numeric_limits<int>::max() );
	bool GetPathsBidir_Symmetric(const int sink, vector<pair<double, pair<t_labelFW*, t_labelBW*> > >& paths, function<bool(t_labelFW*, t_labelBW*)>* pointer_sorter_merge = nullptr, function<bool(pair<double, pair<t_labelFW*, t_labelBW*> >, pair<double, pair<t_labelFW*, t_labelBW*> >)>* pointer_sorter = nullptr, int max_num_paths = numeric_limits<int>::max());
	bool GetPathsBidirBuckets_Symmetric(const int sink, vector<pair<double, pair<t_labelFW*, t_labelBW*> > >& paths, function<bool(t_labelFW*, t_labelBW*)>* pointer_sorter_merge = nullptr, function<bool(pair<double, pair<t_labelFW*, t_labelBW*> >, pair<double, pair<t_labelFW*, t_labelBW*> >)>* pointer_sorter = nullptr, int max_num_paths = numeric_limits<int>::max());
	// IO
	void OutputInStream( ostream& s ) const;  // output all labels grouped by node
	void OutputGraphML( string filename );
	int InfoExtendedFW() const { return i_extended_fw; } // counter is reset in clearFW
	int InfoExtendedBW() const { return i_extended_bw; } // counter is reset in clearBW
	int InfoAddedFW() const { return o_extension_manager_fw.AddedLabels(); }
	int InfoAddedBW() const { return o_extension_manager_bw.AddedLabels(); }
	int InfoHw() const { return i_hw; }
	// The following section is for debug purposes
#ifdef SPPRC_CHECK_PATH
	// Find out why a given path is missing, because
	// (1) it is (incorrectly?) dominated, (2) or it is not generated, (3) or it is not found in the merge procedure
private:
	bool b_check_path;
	bool b_check_verbose;
	vector<REF*> v_path_to_check;
	bool CheckNewLabelFW(t_labelFW* label);
	bool CheckNewLabelBW(t_labelBW* label);
public:
	bool CheckPath(const vector<REF*>& path, bool verbose);
#endif
};

////////////////////////////////////////////////////////////////////////////////////////////////////////
// implementation
////////////////////////////////////////////////////////////////////////////////////////////////////////

template <class REF, class ExtManagerFW, class DManagerFW, class ExtManagerBW, class DManagerBW, class MManager, class StatManager>
c_SPPRC_Solver<REF, ExtManagerFW, DManagerFW, ExtManagerBW, DManagerBW, MManager, StatManager>
	::c_SPPRC_Solver(int max_nodes)
:	d_EPS( 0.00001 ),
	v_fw_star(max_nodes),
	v_bw_star(max_nodes),
	v_dominance_manager_fw(max_nodes),
	v_dominance_manager_bw(max_nodes),
	p_merge_manager(nullptr),
	i_extended_fw(0),
	i_extended_bw(0),
	o_initial_stat(),
	p_stat(&o_initial_stat)
#ifdef SPPRC_CHECK_PATH
	,b_check_path(false),
	b_check_verbose(false)
#endif

{
	p_stat->resize(max_nodes);
	int i = 0;
	for (auto&& dm : v_dominance_manager_fw)
	{
		dm.SetInfo(true, i++);
		dm.SetStatisticsManager(*p_stat);
	}
	i = 0;
	for (auto&& dm : v_dominance_manager_bw)
	{
		dm.SetInfo(false, i++);
		dm.SetStatisticsManager(*p_stat);
	}
}


template <class REF, class ExtManagerFW, class DManagerFW, class ExtManagerBW, class DManagerBW, class MManager, class StatManager>
void c_SPPRC_Solver<REF, ExtManagerFW, DManagerFW, ExtManagerBW, DManagerBW, MManager, StatManager>::SetStatisticsManager(StatManager& stat) 
{ 
	p_stat = &stat; 
	p_stat->resize((int)v_fw_star.size());
	for (auto&& dm : v_dominance_manager_fw)
		dm.SetStatisticsManager(stat);
	for (auto&& dm : v_dominance_manager_bw)
		dm.SetStatisticsManager(stat);
}


template <class REF, class ExtManagerFW, class DManagerFW, class ExtManagerBW, class DManagerBW, class MManager, class StatManager>
c_SPPRC_Solver<REF, ExtManagerFW, DManagerFW, ExtManagerBW, DManagerBW, MManager, StatManager>::~c_SPPRC_Solver()
{
	ClearFW();
	ClearBW();
}


template <class REF, class ExtManagerFW, class DManagerFW, class ExtManagerBW, class DManagerBW, class MManager, class StatManager>
void c_SPPRC_Solver<REF, ExtManagerFW, DManagerFW, ExtManagerBW, DManagerBW, MManager, StatManager>
	::Add( REF* ref )
{
	v_fw_star[ref->Tail()].push_back(ref);
	v_bw_star[ref->Head()].push_back(ref);
}


template <class REF, class ExtManagerFW, class DManagerFW, class ExtManagerBW, class DManagerBW, class MManager, class StatManager>
void c_SPPRC_Solver<REF, ExtManagerFW, DManagerFW, ExtManagerBW, DManagerBW, MManager, StatManager>::Add_Fw( REF* ref )
{
	v_fw_star[ref->Tail()].push_back(ref);
}


template <class REF, class ExtManagerFW, class DManagerFW, class ExtManagerBW, class DManagerBW, class MManager, class StatManager>
void c_SPPRC_Solver<REF, ExtManagerFW, DManagerFW, ExtManagerBW, DManagerBW, MManager, StatManager>::Add_Bw( REF* ref )
{
	v_bw_star[ref->Head()].push_back(ref);
}


template <class REF, class ExtManagerFW, class DManagerFW, class ExtManagerBW, class DManagerBW, class MManager, class StatManager>
void c_SPPRC_Solver<REF, ExtManagerFW, DManagerFW, ExtManagerBW, DManagerBW, MManager, StatManager>
	::RemoveAllREFs()
{
	for( int i = 0; i < (int)v_fw_star.size(); i++)
	{
		v_fw_star[i].clear();
		v_bw_star[i].clear();
	}
}

// forward labeling

template <class REF, class ExtManagerFW, class DManagerFW, class ExtManagerBW, class DManagerBW, class MManager, class StatManager>
void c_SPPRC_Solver<REF, ExtManagerFW, DManagerFW, ExtManagerBW, DManagerBW, MManager, StatManager>
	::ClearFW()
{
	o_extension_manager_fw.clear();
	int dom_size=(int) v_dominance_manager_fw.size();
	for (int i=0;i< dom_size;i++)
		v_dominance_manager_fw[i].clear();
	i_extended_fw = 0;
	i_hw = -1;
	i_gen_paths = -1;
}


template <class REF, class ExtManagerFW, class DManagerFW, class ExtManagerBW, class DManagerBW, class MManager, class StatManager>
bool c_SPPRC_Solver<REF, ExtManagerFW, DManagerFW, ExtManagerBW, DManagerBW, MManager, StatManager>
	::SolveFw( int source, int sink, ResVectFW&& init_res, int max_num_paths, int maxTime )
{
	c_Stopwatch timer;
	timer.Start();
	p_stat->OnStartFwLabeling();
	ClearFW();
	i_id_fwd = 0;
	t_labelFW* p_label = new t_labelFW(i_id_fwd++, std::move(init_res), nullptr, nullptr);
#ifdef SPPRC_CHECK_PATH
	if (b_check_path)
		CheckNewLabelFW(p_label);
#endif
	o_extension_manager_fw.insert( p_label );
	v_dominance_manager_fw[source].insert( p_label );
	// iterate over the labels
	ResVectFW new_res;
	while (( p_label = o_extension_manager_fw.get_next() ))
	{
		int id = p_label->Id();
		// dominance can be delayed to any point in time, the extension manager decides when
		if (o_extension_manager_fw.invoke_dominance_algorithm())
		{
			p_stat->OnStartFwDominance();
			apply_dominance_FW();
			p_stat->OnEndFwDominance();
			if (v_dominance_manager_fw[sink].size() >= max_num_paths)
			{
				p_stat->OnStartFwDominance();
				apply_final_dominance_FW();
				p_stat->OnEndFwDominance();
				p_stat->OnEndFwLabeling();
				return false;
			}
		}
		// has p_label been deleted?
		if (o_extension_manager_fw.is_erased(id))
			continue;
		// extension...
		i_extended_fw++;
		const ResVectFW& curr_res( *p_label );
		int node = ( p_label->generator_REF() ? p_label->generator_REF()->Head() : source );
		// iterate over forward star of the node
		p_stat->OnStartFwPropagate();
		int max_idx = (int)v_fw_star[node].size();
		for ( int idx=0; idx<max_idx; idx++ )
		{
			REF* p_ref = v_fw_star[node][idx];
			if ( p_ref->PropagateFw( curr_res, new_res ) )
			{
				p_label->set_extended();
				t_labelFW* new_label = new t_labelFW(i_id_fwd++, std::move(new_res), p_label, p_ref);
#ifdef SPPRC_CHECK_PATH
				if(b_check_path)
					CheckNewLabelFW(new_label);
#endif
				o_extension_manager_fw.insert( new_label );
				int new_node = p_ref->Head();
				v_dominance_manager_fw[new_node].insert( new_label );
			}
		}
		p_stat->OnEndFwPropagate();
		//check global time
		if (timer.Seconds() > maxTime)
		{
			p_stat->OnEndFwLabeling();
			ClearFW();
			return false;
		}
	}
	// final dominance
	p_stat->OnStartFwDominance();
	apply_final_dominance_FW();
	p_stat->OnEndFwDominance();
	p_stat->OnEndFwLabeling();
	return ( v_dominance_manager_fw[sink].size() < max_num_paths );
}


template <class REF, class ExtManagerFW, class DManagerFW, class ExtManagerBW, class DManagerBW, class MManager, class StatManager>
bool c_SPPRC_Solver<REF, ExtManagerFW, DManagerFW, ExtManagerBW, DManagerBW, MManager, StatManager>
::WarmstartSolveFw(int source, int sink, int max_num_paths, int maxTime)
{
	c_Stopwatch timer;
	timer.Start();
	p_stat->OnStartFwLabeling();
	int n = (int)v_dominance_manager_fw.size();
	if (!p_merge_manager)
	{
		cout << "Run SolveBidir(...) first. " << endl;
		throw;
	}
	for (int i = 0; i < n; i++)
		for (t_labelFW* label_fw = v_dominance_manager_fw[i].begin(); label_fw; label_fw = v_dominance_manager_fw[i].next(label_fw))
			if (!p_merge_manager->BeforeHalfWayPointFW(*label_fw))
				o_extension_manager_fw.insert(label_fw);
	o_extension_manager_fw.ResetBucketIndex();
	o_extension_manager_fw.IncreaseBucketIndex();
	for (int i = 0; i < n; i++)
		v_dominance_manager_fw[i].PrepareForWarmstart(o_extension_manager_fw.GetBucketIndex());

	t_labelFW* p_label = nullptr;
	// iterate over the labels
	ResVectFW new_res;
	while ((p_label = o_extension_manager_fw.get_next()))
	{
		int id = p_label->Id();
		// dominance can be delayed to any point in time, the bucket manager decides when
		if (o_extension_manager_fw.invoke_dominance_algorithm())
		{
			p_stat->OnStartFwDominance();
			apply_dominance_FW();
			p_stat->OnEndFwDominance();
			if (v_dominance_manager_fw[sink].size() >= max_num_paths)
			{
				p_stat->OnStartFwDominance();
				apply_final_dominance_FW();
				p_stat->OnEndFwDominance();
				p_stat->OnEndFwLabeling();
				return false;
			}
		}
		// has p_label been deleted?
		if (o_extension_manager_fw.is_erased(id))
			continue;
		// extension...
		i_extended_fw++;
		const ResVectFW& curr_res(*p_label);
		int node = (p_label->generator_REF() ? p_label->generator_REF()->Head() : source);
		// iterate over forward star of the node
		p_stat->OnStartFwPropagate();
		int max_idx = (int)v_fw_star[node].size();
		for (int idx = 0; idx<max_idx; idx++)
		{
			REF* p_ref = v_fw_star[node][idx];
			if (p_ref->PropagateFw(curr_res, new_res))
			{
				p_label->set_extended();
				t_labelFW* new_label = new t_labelFW(i_id_fwd++, std::move(new_res), p_label, p_ref);
#ifdef SPPRC_CHECK_PATH
				if ( b_check_path)
					CheckNewLabelFW(new_label);
#endif
				o_extension_manager_fw.insert(new_label);
				int new_node = p_ref->Head();
				v_dominance_manager_fw[new_node].insert(new_label);
			}
		}
		p_stat->OnEndFwPropagate();
		//check global time
		if (timer.Seconds() > maxTime)
		{
			p_stat->OnEndFwLabeling();
			ClearFW();
			return false;
		}
	}
	// final dominance
	p_stat->OnStartFwDominance();
	apply_final_dominance_FW();
	p_stat->OnEndFwDominance();
	p_stat->OnEndFwLabeling();
	return (v_dominance_manager_fw[sink].size() < max_num_paths);
}


template <class REF, class ExtManagerFW, class DManagerFW, class ExtManagerBW, class DManagerBW, class MManager, class StatManager>
void c_SPPRC_Solver<REF, ExtManagerFW, DManagerFW, ExtManagerBW, DManagerBW, MManager, StatManager>
	::apply_dominance_FW( int new_node )
{
	t_labelFW* label_to_delete = v_dominance_manager_fw[new_node].apply_dominance(o_extension_manager_fw.GetBucketIndex());
	while ( label_to_delete )
	{
		if (!label_to_delete->is_extended())
		{
			o_extension_manager_fw.erase(label_to_delete);
			t_labelFW* next_label = label_to_delete->next();
			delete label_to_delete;
			label_to_delete = next_label;
		}
		else
		{
			//TODO: Collect Labels for later deleting
			t_labelFW* next_label = label_to_delete->next();
			label_to_delete = next_label;
		}
	}
}


template <class REF, class ExtManagerFW, class DManagerFW, class ExtManagerBW, class DManagerBW, class MManager, class StatManager>
void c_SPPRC_Solver<REF, ExtManagerFW, DManagerFW, ExtManagerBW, DManagerBW, MManager, StatManager>
	::apply_dominance_FW()
{
	int max_idx=(int)v_dominance_manager_fw.size();
	for ( int i=0; i<max_idx; i++ )
		apply_dominance_FW(i);
}


template <class REF, class ExtManagerFW, class DManagerFW, class ExtManagerBW, class DManagerBW, class MManager, class StatManager>
void c_SPPRC_Solver<REF, ExtManagerFW, DManagerFW, ExtManagerBW, DManagerBW, MManager, StatManager>
::apply_final_dominance_FW(int new_node)
{
	t_labelFW* label_to_delete = v_dominance_manager_fw[new_node].apply_final_dominance(o_extension_manager_fw.GetBucketIndex());
	while (label_to_delete)
	{
		if (!label_to_delete->is_extended())
		{
			o_extension_manager_fw.erase(label_to_delete);
			t_labelFW* next_label = label_to_delete->next();
			delete label_to_delete;
			label_to_delete = next_label;
		}
		else
		{
			//TODO: Collect Labels for later deleting
			t_labelFW* next_label = label_to_delete->next();
			label_to_delete = next_label;
		}
	}
}


template <class REF, class ExtManagerFW, class DManagerFW, class ExtManagerBW, class DManagerBW, class MManager, class StatManager>
void c_SPPRC_Solver<REF, ExtManagerFW, DManagerFW, ExtManagerBW, DManagerBW, MManager, StatManager>
::apply_final_dominance_FW()
{
	int max_idx = (int)v_dominance_manager_fw.size();
	for (int i = 0; i<max_idx; i++)
		apply_final_dominance_FW(i);
}


template <class REF, class ExtManagerFW, class DManagerFW, class ExtManagerBW, class DManagerBW, class MManager, class StatManager>
bool c_SPPRC_Solver<REF, ExtManagerFW, DManagerFW, ExtManagerBW, DManagerBW, MManager, StatManager>
	::GetPathsFW( const int sink, vector<t_labelFW*>& paths, function<bool (t_labelFW*,t_labelFW*)>* pointer_sorter, int max_num_paths )
{
	// attention: max_nodes must be number of nodes!
	t_labelFW* iter = v_dominance_manager_fw[sink].begin();
	while(iter)
	{
		paths.push_back(iter);
		iter=v_dominance_manager_fw[sink].next(iter);
	}
	if ( (int)paths.size() > max_num_paths )
	{
		if ( pointer_sorter )
			sort( paths.begin(), paths.end(), *pointer_sorter );
		paths.resize( max_num_paths );
		if (!pointer_sorter)
			return false;
	}
	return true;
}

// backward

template <class REF, class ExtManagerFW, class DManagerFW, class ExtManagerBW, class DManagerBW, class MManager, class StatManager>
void c_SPPRC_Solver<REF, ExtManagerFW, DManagerFW, ExtManagerBW, DManagerBW, MManager, StatManager>
	::ClearBW()
{
	o_extension_manager_bw.clear();
	int max_idx=(int) v_dominance_manager_bw.size();
	for (int i=0;i< max_idx;i++)
		v_dominance_manager_bw[i].clear();
	i_extended_bw = 0;
}


template <class REF, class ExtManagerFW, class DManagerFW, class ExtManagerBW, class DManagerBW, class MManager, class StatManager>
bool c_SPPRC_Solver<REF, ExtManagerFW, DManagerFW, ExtManagerBW, DManagerBW, MManager, StatManager>
	::SolveBw(int source, int sink, ResVectBW&& init_res, int max_num_paths, int maxTime )
{
	c_Stopwatch timer_Solver;
	timer_Solver.Start();
	p_stat->OnStartBwLabeling();
	ClearBW();
	i_id_bwd = 0;
	t_labelBW* p_label = new t_labelBW(i_id_bwd++, std::move(init_res), nullptr, nullptr);
#ifdef SPPRC_CHECK_PATH
	if (b_check_path)
		CheckNewLabelBW(p_label);
#endif
	o_extension_manager_bw.insert( p_label );
	v_dominance_manager_bw[sink].insert( p_label );
	// iterate over the labels
	ResVectBW new_res;
	while ( (p_label = o_extension_manager_bw.get_next() ))
	{		
		int id = p_label->Id();
		// dominance can be delayed to any point in time, the bucket manager decides when
		if (o_extension_manager_bw.invoke_dominance_algorithm())
		{
			p_stat->OnStartBwDominance();
			apply_dominance_BW();
			p_stat->OnEndBwDominance();
			if (v_dominance_manager_bw[source].size() >= max_num_paths)
			{
				p_stat->OnStartBwDominance();
				apply_final_dominance_BW();
				p_stat->OnEndBwDominance();
				p_stat->OnStartBwLabeling();
				return false;
			}
		}
		// has p_label been deleted?
		if (o_extension_manager_bw.is_erased(id))
			continue;
		// entension
		i_extended_bw++;
		const ResVectBW& curr_res( *p_label );
		int node = ( p_label->generator_REF() ? p_label->generator_REF()->Tail() : sink );
		// iterate over backward star of the node
		p_stat->OnStartBwPropagate();
		int max_idx = (int)v_bw_star[node].size();
		for ( int idx=0; idx<max_idx; idx++ )
		{
			REF* p_ref = v_bw_star[node][idx];
			if ( p_ref->PropagateBw( curr_res, new_res ) )
			{
				p_label->set_extended();
				t_labelBW* new_label = new t_labelBW(i_id_bwd++, std::move(new_res), p_label, p_ref);
#ifdef SPPRC_CHECK_PATH
				if (b_check_path)
					CheckNewLabelBW(new_label);
#endif
				o_extension_manager_bw.insert( new_label );
				int new_node = p_ref->Tail();
				v_dominance_manager_bw[new_node].insert( new_label );
			}
		}
		p_stat->OnEndBwPropagate();
		// check global time
		if (timer_Solver.Seconds() > maxTime)
		{
			p_stat->OnEndBwLabeling();
			ClearBW();
			return false;
		}
	}
	// final dominance
	p_stat->OnStartBwDominance();
	apply_final_dominance_BW();
	p_stat->OnEndBwDominance();
	p_stat->OnEndBwLabeling();
	return ( v_dominance_manager_bw[source].size() < max_num_paths );
}


template <class REF, class ExtManagerFW, class DManagerFW, class ExtManagerBW, class DManagerBW, class MManager, class StatManager>
bool c_SPPRC_Solver<REF, ExtManagerFW, DManagerFW, ExtManagerBW, DManagerBW, MManager, StatManager>
::WarmstartSolveBw(int source, int sink, int max_num_paths, int maxTime)
{
	c_Stopwatch timer_Solver;
	timer_Solver.Start();
	p_stat->OnStartBwLabeling();
	int n = (int)v_dominance_manager_bw.size();
	if (!p_merge_manager)
	{
		cout << "Run SolveBidir(...) first. " << endl;
		throw;
	}
	for (int i = 0; i < n; i++)
		for (t_labelBW* label_bw = v_dominance_manager_bw[i].begin(); label_bw; label_bw = v_dominance_manager_bw[i].next(label_bw))
			if (!p_merge_manager->BeforeHalfWayPointBW(*label_bw))
				o_extension_manager_bw.insert(label_bw);
	o_extension_manager_bw.ResetBucketIndex();
	o_extension_manager_bw.IncreaseBucketIndex();
	for (int i = 0; i < n; i++)
		v_dominance_manager_bw[i].PrepareForWarmstart(o_extension_manager_bw.GetBucketIndex());

	t_labelBW* p_label = nullptr;
	// iterate over the labels
	ResVectBW new_res;
	while ((p_label = o_extension_manager_bw.get_next()))
	{
		int id = p_label->Id();
		// dominance can be delayed to any point in time, the bucket manager decides when
		if (o_extension_manager_bw.invoke_dominance_algorithm())
		{
			p_stat->OnStartBwDominance();
			apply_dominance_BW();
			p_stat->OnEndBwDominance();
			if (v_dominance_manager_bw[source].size() >= max_num_paths)
			{
				p_stat->OnStartBwDominance();
				apply_final_dominance_BW();
				p_stat->OnEndBwDominance();
				p_stat->OnEndBwLabeling();
				return false;
			}
		}
		// has p_label been deleted?
		if (o_extension_manager_bw.is_erased(id))
			continue;
		// extension
		i_extended_bw++;
		const ResVectBW& curr_res(*p_label);
		int node = (p_label->generator_REF() ? p_label->generator_REF()->Tail() : sink);
		// iterate over backward star of the node
		p_stat->OnStartBwPropagate();
		int max_idx = (int)v_bw_star[node].size();
		for (int idx = 0; idx<max_idx; idx++)
		{
			REF* p_ref = v_bw_star[node][idx];
			if (p_ref->PropagateBw(curr_res, new_res))
			{
				p_label->set_extended();
				t_labelBW* new_label = new t_labelBW(i_id_bwd++, std::move(new_res), p_label, p_ref);
#ifdef SPPRC_CHECK_PATH
				if (b_check_path)
					CheckNewLabelBW(new_label);
#endif
				o_extension_manager_bw.insert(new_label);
				int new_node = p_ref->Tail();
				v_dominance_manager_bw[new_node].insert(new_label);
			}
		}
		p_stat->OnEndBwPropagate();
		// check global time
		if (timer_Solver.Seconds() > maxTime)
		{
			p_stat->OnEndBwLabeling();
			ClearBW();
			return false;
		}
	}
	// final dominance
	p_stat->OnStartBwDominance();
	apply_final_dominance_BW();
	p_stat->OnEndBwDominance();
	p_stat->OnEndBwLabeling();
	return (v_dominance_manager_bw[source].size() < max_num_paths);
}


template <class REF, class ExtManagerFW, class DManagerFW, class ExtManagerBW, class DManagerBW, class MManager, class StatManager>
void c_SPPRC_Solver<REF, ExtManagerFW, DManagerFW, ExtManagerBW, DManagerBW, MManager, StatManager>
	::apply_dominance_BW()
{
	int max_idx=(int)v_dominance_manager_bw.size();
	for ( int i=0; i<max_idx; i++ )
		apply_dominance_BW(i);
}


template <class REF, class ExtManagerFW, class DManagerFW, class ExtManagerBW, class DManagerBW, class MManager, class StatManager>
void c_SPPRC_Solver<REF, ExtManagerFW, DManagerFW, ExtManagerBW, DManagerBW, MManager, StatManager>
	::apply_dominance_BW(int new_node)
{
	t_labelBW* label_to_delete = v_dominance_manager_bw[new_node].apply_dominance(o_extension_manager_bw.GetBucketIndex());
	
	while ( label_to_delete )
	{
		if (!label_to_delete->is_extended())
		{
			o_extension_manager_bw.erase(label_to_delete);
			t_labelBW* next_label = label_to_delete->next();
			delete label_to_delete;
			label_to_delete = next_label;
		}
		else
		{
			//TODO: Collect Labels for later deleting
			t_labelBW* next_label = label_to_delete->next();
			label_to_delete = next_label;
		}
	}
}


template <class REF, class ExtManagerFW, class DManagerFW, class ExtManagerBW, class DManagerBW, class MManager, class StatManager>
void c_SPPRC_Solver<REF, ExtManagerFW, DManagerFW, ExtManagerBW, DManagerBW, MManager, StatManager>
::apply_final_dominance_BW()
{
	int max_idx = (int)v_dominance_manager_bw.size();
	for (int i = 0; i<max_idx; i++)
		apply_final_dominance_BW(i);
}


template <class REF, class ExtManagerFW, class DManagerFW, class ExtManagerBW, class DManagerBW, class MManager, class StatManager>
void c_SPPRC_Solver<REF, ExtManagerFW, DManagerFW, ExtManagerBW, DManagerBW, MManager, StatManager>
::apply_final_dominance_BW(int new_node)
{
	t_labelBW* label_to_delete = v_dominance_manager_bw[new_node].apply_final_dominance(o_extension_manager_bw.GetBucketIndex());

	while (label_to_delete)
	{
		if (!label_to_delete->is_extended())
		{
			o_extension_manager_bw.erase(label_to_delete);
			t_labelBW* next_label = label_to_delete->next();
			delete label_to_delete;
			label_to_delete = next_label;
		}
		else
		{
			//TODO: Collect Labels for later deleting
			t_labelBW* next_label = label_to_delete->next();
			label_to_delete = next_label;
		}
	}
}


template <class REF, class ExtManagerFW, class DManagerFW, class ExtManagerBW, class DManagerBW, class MManager, class StatManager>
bool c_SPPRC_Solver<REF, ExtManagerFW, DManagerFW, ExtManagerBW, DManagerBW, MManager, StatManager>
	::GetPathsBW( const int sink, vector<t_labelBW*>& paths, function<bool (t_labelBW*,t_labelBW*)>* pointer_sorter, int max_num_paths )
{
	t_labelBW* iter= v_dominance_manager_bw[sink].begin();
	while(iter)
	{
		paths.push_back(iter);
		iter=v_dominance_manager_bw[sink].next(iter);
	}
	if ( (int)paths.size() > max_num_paths )
	{
		if ( pointer_sorter )
			sort( paths.begin(), paths.end(), *pointer_sorter );
		paths.resize( max_num_paths );
		if (!pointer_sorter)
			return false;
	}
	return true;
}

// bidirectional 

template <class REF, class ExtManagerFW, class DManagerFW, class ExtManagerBW, class DManagerBW, class MManager, class StatManager>
bool c_SPPRC_Solver<REF, ExtManagerFW, DManagerFW, ExtManagerBW, DManagerBW, MManager, StatManager>
	::GetPathsBidir( const int sink, vector<pair<double, pair<t_labelFW*,t_labelBW*> > >& paths, 
					 function<bool (pair<double,pair<t_labelFW*,t_labelBW*> >,pair<double,pair<t_labelFW*,t_labelBW*> >)>* pointer_sorter, int max_num_paths )
{
	p_stat->OnStartBidirMerge();
	if ( !p_merge_manager )
	{
		cout << "Run SolveBidir(...) first. " << endl;
		throw;
	}
	int n = (int)v_dominance_manager_fw.size();
	double rdc = 0.0;
	for (int i = 0; i < n; i++)
	{
		for (t_labelFW* label_fw = v_dominance_manager_fw[i].begin(); label_fw; label_fw = v_dominance_manager_fw[i].next(label_fw))
		{
			if (p_merge_manager->MergeableFW(*label_fw))
			{
				for (t_labelBW* label_bw = v_dominance_manager_bw[i].begin(); label_bw; label_bw = v_dominance_manager_bw[i].next(label_bw))
				{
					if (p_merge_manager->MergeableBW(*label_bw) && p_merge_manager->Mergeable(*label_fw, *label_bw, rdc))
						paths.push_back(make_pair(rdc, make_pair(label_fw, label_bw)));
					if ((int)paths.size()/2 > max_num_paths)
					{
						if (pointer_sorter)
							sort(paths.begin(), paths.end(), *pointer_sorter);
						paths.resize(max_num_paths);
					}
					if (!pointer_sorter && (int)paths.size() >= max_num_paths)
					{
						p_stat->OnEndBidirMerge();
						i_gen_paths = (int)paths.size();
						return false;
					}
				}
			}
		}
	}
	if ( (int)paths.size() > max_num_paths )
	{
		if ( pointer_sorter )
			sort( paths.begin(), paths.end(), *pointer_sorter );
		paths.resize( max_num_paths );
		if (!pointer_sorter)
		{
			p_stat->OnEndBidirMerge();
			return false;
		}
	}
	i_gen_paths = (int) paths.size();
	p_stat->OnEndBidirMerge();
	//sort(paths.begin(), paths.end(), *pointer_sorter);
	return true;
}


template <class REF, class ExtManagerFW, class DManagerFW, class ExtManagerBW, class DManagerBW, class MManager, class StatManager>
bool c_SPPRC_Solver<REF, ExtManagerFW, DManagerFW, ExtManagerBW, DManagerBW, MManager, StatManager>
::GetPathsBidirBuckets(const int sink, vector<pair<double, pair<t_labelFW*, t_labelBW*> > >& paths,
function<bool(pair<double, pair<t_labelFW*, t_labelBW*> >, pair<double, pair<t_labelFW*, t_labelBW*> >)>* pointer_sorter, int max_num_paths)
{
	p_stat->OnStartBidirMerge();
	if (!p_merge_manager)
	{
		cout << "Run SolveBidir(...) first. " << endl;
		throw;
	}
	int n = (int)v_dominance_manager_fw.size();
	double rdc = 0.0;
	int numBuckets = p_merge_manager->NumBuckets();
	for (int i = 0; i < n; i++)
	{
		//Labels are sorted, if reverse order: reverse labels
		v_dominance_manager_fw[i].ReverseAllBuckets();
		v_dominance_manager_bw[i].ReverseAllBuckets();
		for (int bucketI = 0; bucketI < numBuckets; ++bucketI)
		{
			for (t_labelFW* label_fw = v_dominance_manager_fw[i].begin(bucketI); label_fw; label_fw = v_dominance_manager_fw[i].next(bucketI, label_fw))
			{
				if (p_merge_manager->MergeableFW(*label_fw))
				{
					for (auto bucketJ : p_merge_manager->BucketsThatFitForMerge(i, bucketI))
					{
						for (t_labelBW* label_bw = v_dominance_manager_bw[i].begin(bucketJ); label_bw; label_bw = v_dominance_manager_bw[i].next(bucketJ, label_bw))
						{
							if (p_merge_manager->BreakingCriterion(*label_fw, *label_bw))
								break;
							if (p_merge_manager->MergeableBW(*label_bw) && p_merge_manager->Mergeable(*label_fw, *label_bw, rdc))
								paths.push_back(make_pair(rdc, make_pair(label_fw, label_bw)));
							if ((int)paths.size() > 2 * max_num_paths)
							{
								if (pointer_sorter)
									sort(paths.begin(), paths.end(), *pointer_sorter);
								paths.resize(max_num_paths);
							}
							if (!pointer_sorter && (int)paths.size() >= max_num_paths)
							{
								p_stat->OnEndBidirMerge();
								i_gen_paths = (int)paths.size();
								return false;
							}
						}
					}
				}
			}
		}
	}
	if ((int)paths.size() > max_num_paths)
	{
		if (pointer_sorter)
			sort(paths.begin(), paths.end(), *pointer_sorter);
		paths.resize(max_num_paths);
		if (!pointer_sorter)
		{
			p_stat->OnEndBidirMerge();
			return false;
		}
	}
	i_gen_paths = (int)paths.size();
	p_stat->OnEndBidirMerge();
	//sort(paths.begin(), paths.end(), *pointer_sorter);
	return true;
}


template <class REF, class ExtManagerFW, class DManagerFW, class ExtManagerBW, class DManagerBW, class MManager, class StatManager>
bool c_SPPRC_Solver<REF, ExtManagerFW, DManagerFW, ExtManagerBW, DManagerBW, MManager, StatManager>
	::SolveBidir( MManager& merger, int source, int sink, ResVectFW&& init_res_fw, ResVectBW&& init_res_bw, int max_num_paths, int maxTime ) // not generic!
{
	p_stat->OnStartBidirLabeling();
	c_Stopwatch timer_Solver;
	timer_Solver.Start();
	ClearFW();
	ClearBW();
	p_merge_manager = &merger;
	i_hw = p_merge_manager->HalfwayPointFW();
	// init FW
	i_id_fwd = 0;
	t_labelFW* p_label_fw = new t_labelFW(i_id_fwd++, std::move(init_res_fw), nullptr, nullptr);
#ifdef SPPRC_CHECK_PATH
	if(b_check_path)
		CheckNewLabelFW(p_label_fw);
#endif
	o_extension_manager_fw.insert( p_label_fw );
	v_dominance_manager_fw[source].insert( p_label_fw );
	// init BW
	i_id_bwd = 0;
	t_labelBW* p_label_bw = new t_labelBW(i_id_bwd++, std::move(init_res_bw), nullptr, nullptr);
#ifdef SPPRC_CHECK_PATH
	if (b_check_path)
		CheckNewLabelBW(p_label_bw);
#endif

	o_extension_manager_bw.insert(p_label_bw);
	v_dominance_manager_bw[sink].insert(p_label_bw);
	// iterate over the labels forward
	p_stat->OnStartBidirFwLabeling();
	ResVectFW new_res;
	while ( (p_label_fw = o_extension_manager_fw.get_next()) )
	{	
		int id = p_label_fw->Id();
		// dominance can be delayed to any point in time, the bucket manager decides when
		if (o_extension_manager_fw.invoke_dominance_algorithm())
		{
			p_stat->OnStartBidirFwDominance();
			apply_dominance_FW();
			p_stat->OnEndBidirFwDominance();
			if (v_dominance_manager_fw[sink].size() >= max_num_paths)
			{
				p_stat->OnStartBidirFwDominance();
				apply_final_dominance_FW();
				p_stat->OnEndBidirFwDominance();
				p_stat->OnEndBidirFwLabeling();
				return false;
			}
		}
		// has p_label been deleted?
		if (o_extension_manager_fw.is_erased(id))
			continue;
		// extension
		if (p_merge_manager->BeforeHalfWayPointFW(*p_label_fw))
		{
			i_extended_fw++;
			const ResVectFW& curr_res(*p_label_fw);
			int node = (p_label_fw->generator_REF() ? p_label_fw->generator_REF()->Head() : source);
			// iterate over forward star of the node
			p_stat->OnStartBidirFwPropagate();
			int max_idx = (int)v_fw_star[node].size();
			for (int idx = 0; idx < max_idx; idx++)
			{
				REF* p_ref = v_fw_star[node][idx];
				if (p_ref->PropagateFw(curr_res, new_res))
				{
					p_label_fw->set_extended();
					t_labelFW* new_label = new t_labelFW(i_id_fwd++, std::move(new_res), p_label_fw, p_ref);
#ifdef SPPRC_CHECK_PATH
					if(b_check_path)
						CheckNewLabelFW(new_label);
#endif
					o_extension_manager_fw.insert(new_label);
					int new_node = p_ref->Head();
					v_dominance_manager_fw[new_node].insert(new_label);
				}
			}
			p_stat->OnEndBidirFwPropagate();
		}
		// check global time
		if (timer_Solver.Seconds() > maxTime)
		{
			p_stat->OnEndBidirLabeling();
			p_stat->OnEndBidirFwLabeling();
			ClearFW();
			return false;
		}
	}
	// final dominance
	p_stat->OnStartBidirFwDominance();
	apply_final_dominance_FW();
	p_stat->OnEndBidirFwDominance();
	p_stat->OnEndBidirFwLabeling();
	//
	// iterate over the labels backward
	//
	p_stat->OnStartBidirBwLabeling();
	ResVectBW new_res_bw;
	while ( (p_label_bw = o_extension_manager_bw.get_next()) )
	{		
		int id = p_label_bw->Id();
		// dominance can be delayed to any point in time, the bucket manager decides when
		if (o_extension_manager_bw.invoke_dominance_algorithm())
		{
			p_stat->OnStartBidirBwDominance();
			apply_dominance_BW();
			p_stat->OnEndBidirBwDominance();
			if (v_dominance_manager_bw[source].size() >= max_num_paths)
			{
				p_stat->OnStartBidirBwDominance();
				apply_final_dominance_BW();
				p_stat->OnEndBidirBwDominance();
				p_stat->OnEndBidirBwLabeling();
				return false;
			}
		}
		// has p_label_bw been deleted?
		if (o_extension_manager_bw.is_erased(id))
			continue;
		// extension
		if ( p_merge_manager->BeforeHalfWayPointBW( *p_label_bw ) )
		{
			i_extended_bw++;
			const ResVectBW& curr_res( *p_label_bw );
			int node = ( p_label_bw->generator_REF() ? p_label_bw->generator_REF()->Tail() : sink );
			// iterate over backward star of the node
			p_stat->OnStartBidirBwPropagate();
			int max_idx = (int)v_bw_star[node].size();
			for ( int idx=0; idx<max_idx; idx++ )
			{
				REF* p_ref = v_bw_star[node][idx];
				if ( p_ref->PropagateBw( curr_res, new_res_bw ) )
				{
					p_label_bw->set_extended();
					t_labelBW* new_label = new t_labelBW(i_id_bwd++, std::move(new_res_bw), p_label_bw, p_ref);
#ifdef SPPRC_CHECK_PATH
					if (b_check_path)
						CheckNewLabelBW(new_label);
#endif
					o_extension_manager_bw.insert( new_label );
					int new_node = p_ref->Tail();
					v_dominance_manager_bw[new_node].insert( new_label );
				}
			}
			p_stat->OnEndBidirBwPropagate();
		}
		// check global time
		if (timer_Solver.Seconds() > maxTime)
		{
			p_stat->OnEndBidirBwLabeling();
			ClearFW();
			ClearBW();
			return false;
		}
	}
	// final dominance
	p_stat->OnStartBidirBwDominance();
	apply_final_dominance_BW();
	p_stat->OnEndBidirBwDominance();
	p_stat->OnEndBidirBwLabeling();
	p_stat->OnEndBidirLabeling();
	return true;
}


template <class REF, class ExtManagerFW, class DManagerFW, class ExtManagerBW, class DManagerBW, class MManager, class StatManager>
bool c_SPPRC_Solver<REF, ExtManagerFW, DManagerFW, ExtManagerBW, DManagerBW, MManager, StatManager>
	::SolveBidir_DynamicHalfway(MManager& merger, int source, int sink, ResVectFW&& init_res_fw, ResVectBW&& init_res_bw, int max_num_paths, int maxTime )
{
	c_Stopwatch timer_Solver;
	timer_Solver.Start();
	p_stat->OnStartBidirLabeling();
	ClearFW();
	ClearBW();
	p_merge_manager = &merger;
	// init FW
	i_id_fwd = 0;
	t_labelFW* p_label_fw = new t_labelFW(i_id_fwd++, std::move(init_res_fw), nullptr, nullptr);
#ifdef SPPRC_CHECK_PATH
	if ( b_check_path)
		CheckNewLabelFW(p_label_fw);
#endif
	o_extension_manager_fw.insert( p_label_fw );
	v_dominance_manager_fw[source].insert( p_label_fw );
	ResVectFW new_res_fw;
	// init BW
	i_id_bwd = 0;
	t_labelBW* p_label_bw = new t_labelBW(i_id_bwd++, std::move(init_res_bw), nullptr, nullptr);
#ifdef SPPRC_CHECK_PATH
	if (b_check_path)
		CheckNewLabelBW(p_label_bw);
#endif
	o_extension_manager_bw.insert( p_label_bw );
	v_dominance_manager_bw[sink].insert( p_label_bw );
	ResVectBW new_res_bw;
	// the dynamic part
	p_merge_manager->SetHalfwayPointFW( (*p_label_bw)() );
	p_merge_manager->SetHalfwayPointBW( (*p_label_fw)() );
	bool forward = true;
	do
	{
		if ( forward )
		{
			p_label_fw = o_extension_manager_fw.get_next();
			if (!p_label_fw)
			{
				forward = false;
				continue;
			}
			int id = p_label_fw->Id();
			// dominance can be delayed to any point in time, the bucket manager decides when
			if (o_extension_manager_fw.invoke_dominance_algorithm())
			{
				p_stat->OnStartBidirFwDominance();
				apply_dominance_FW();
				p_stat->OnEndBidirFwDominance();
				if (v_dominance_manager_fw[sink].size() >= max_num_paths)
				{
					p_stat->OnStartBidirFwDominance();
					apply_final_dominance_FW();
					p_stat->OnEndBidirFwDominance();
					p_stat->OnStartBidirBwDominance();
					apply_final_dominance_BW();
					p_stat->OnEndBidirBwDominance();
					p_stat->OnEndBidirLabeling();
					return false;
				}
			}
			// has p_label_fw been deleted?
			if (o_extension_manager_fw.is_erased(id))
				continue;
			// extension
			if ( p_merge_manager->BeforeHalfWayPointFW( *p_label_fw ) )
			{
				i_extended_fw++;
				const ResVectFW& curr_res( *p_label_fw );
				int node = ( p_label_fw->generator_REF() ? p_label_fw->generator_REF()->Head() : source );
				// iterate over forward star of the node
				p_stat->OnStartBidirFwPropagate();
				int max_idx = (int)v_fw_star[node].size();
				for ( int idx=0; idx<max_idx; idx++ )
				{
					REF* p_ref = v_fw_star[node][idx];
					if ( p_ref->PropagateFw( curr_res, new_res_fw ) )
					{
						p_label_fw->set_extended();
						t_labelFW* new_label = new t_labelFW(i_id_fwd++, std::move(new_res_fw), p_label_fw, p_ref);
#ifdef SPPRC_CHECK_PATH
						if (b_check_path)
							CheckNewLabelFW(new_label);
#endif
						o_extension_manager_fw.insert( new_label );
						int new_node = p_ref->Head();
						v_dominance_manager_fw[new_node].insert( new_label );
					}
				}
				p_stat->OnEndBidirFwPropagate();
			}
			p_merge_manager->SetHalfwayPointBW( min( (*p_label_fw)(), p_merge_manager->HalfwayPointFW() ) );
		}
		else 
		{	
			// backward
			p_label_bw = o_extension_manager_bw.get_next();
			if (!p_label_bw)
			{
				forward = true;
				continue;
			}
			int id = p_label_bw->Id();
			// dominance can be delayed to any point in time, the bucket manager decides when
			if (o_extension_manager_bw.invoke_dominance_algorithm())
			{
				p_stat->OnStartBidirBwDominance();
				apply_dominance_BW();
				p_stat->OnEndBidirBwDominance();
			}
			// has p_label_bw been deleted?
			if (o_extension_manager_bw.is_erased(id))
				continue;
			// extension...
			if ( p_merge_manager->BeforeHalfWayPointBW( *p_label_bw ) )
			{
				i_extended_bw++;
				const ResVectBW& curr_res( *p_label_bw );
				int node = ( p_label_bw->generator_REF() ? p_label_bw->generator_REF()->Tail() : sink );
				// iterate over backward star of the node
				p_stat->OnStartBidirBwPropagate();
				int max_idx = (int)v_bw_star[node].size();
				for ( int idx=0; idx<max_idx; idx++ )
				{
					REF* p_ref = v_bw_star[node][idx];
					if ( p_ref->PropagateBw( curr_res, new_res_bw ) )
					{
						p_label_bw->set_extended();
						t_labelBW* new_label = new t_labelBW(i_id_bwd++, std::move(new_res_bw), p_label_bw, p_ref);
#ifdef SPPRC_CHECK_PATH
						if (b_check_path)
							CheckNewLabelBW(new_label);
#endif
						o_extension_manager_bw.insert( new_label );
						int new_node = p_ref->Tail();
						v_dominance_manager_bw[new_node].insert( new_label );
					}
				}
				p_stat->OnEndBidirBwPropagate();
			}
			p_merge_manager->SetHalfwayPointFW( max( (*p_label_bw)(), p_merge_manager->HalfwayPointBW() ) );
		}
		// Decide on whether we go forward or backward
		forward = ( o_extension_manager_fw.OpenLabels() < o_extension_manager_bw.OpenLabels() );
		if ( forward && !p_label_fw )
			forward = false;
		if ( !forward && !p_label_bw )
			forward = true;
		// check global time
		if (timer_Solver.Seconds() > maxTime)
		{
			p_stat->OnEndBidirLabeling();
			ClearFW();
			ClearBW();
			return false;
		}
	}
	while (p_label_fw || p_label_bw);
	// final dominance
	p_stat->OnStartBidirFwDominance();
	apply_final_dominance_FW();
	p_stat->OnEndBidirFwDominance();
	p_stat->OnStartBidirBwDominance();
	apply_final_dominance_BW();
	p_stat->OnEndBidirBwDominance();
	i_hw = (p_merge_manager->HalfwayPointFW() + p_merge_manager->HalfwayPointBW()) /2;
	p_stat->OnEndBidirLabeling();
	return true;
}

// bidir symmetric

template <class REF, class ExtManagerFW, class DManagerFW, class ExtManagerBW, class DManagerBW, class MManager, class StatManager>
bool c_SPPRC_Solver<REF, ExtManagerFW, DManagerFW, ExtManagerBW, DManagerBW, MManager, StatManager>
	::SolveBidir_Symmetric( MManager& merger, int source, int sink, ResVectFW&& init_res, int max_num_paths, int maxTime )
{
	if ( !is_same<ExtManagerFW,ExtManagerBW>::value
		|| !is_same<DManagerFW,DManagerBW>::value 
		|| !is_same<ResVectFW,ResVectBW>::value 
		|| !is_same<t_labelFW,t_labelBW>::value )
	{	
		cout << "SolveBidir_Symmetric only applicable for symmetric SPPRCs" << endl;
		throw;
	}
	// safe now
	c_Stopwatch timer;
	timer.Start();
	p_stat->OnStartBidirLabeling();
	p_stat->OnStartBidirFwLabeling();
	p_merge_manager = &merger;
	ClearFW();
	ResVectFW copy_res( init_res );
	i_id_fwd = 0;
	t_labelFW* p_label = new t_labelFW(i_id_fwd++, std::move(init_res), nullptr, nullptr);
#ifdef SPPRC_CHECK_PATH
	if ( b_check_path)
		CheckNewLabelFW(p_label);
#endif
	o_extension_manager_fw.insert( p_label );
	v_dominance_manager_fw[source].insert( p_label );
	// iterate over the labels
	ResVectFW new_res;
	while (( p_label = o_extension_manager_fw.get_next() ))
	{		
		int id = p_label->Id();
		// dominance can be delayed to any point in time, the bucket manager decides when
		if (o_extension_manager_fw.invoke_dominance_algorithm())
		{
			apply_dominance_FW();
			if (v_dominance_manager_fw[sink].size() >= max_num_paths)
				break;
		}
		// has p_label been deleted?
		if (o_extension_manager_fw.is_erased(id))
			continue;
		// extension...
		if ( p_merge_manager->BeforeHalfWayPointFW( *p_label ) )
		{
			i_extended_fw++;
			// check global time
			if( timer.Seconds() > maxTime )
			{
				p_stat->OnEndBidirLabeling();
				p_stat->OnEndBidirFwLabeling();
				ClearFW();
				return false; 
			}
			const ResVectFW& curr_res( *p_label );
			int node = ( p_label->generator_REF() ? p_label->generator_REF()->Head() : source );
			// iterate over forward star of the node
			p_stat->OnStartBidirFwPropagate();
			int max_idx = (int)v_fw_star[node].size();
			for ( int idx=0; idx<max_idx; idx++ )
			{
				REF* p_ref = v_fw_star[node][idx];
				if ( p_ref->PropagateFw( curr_res, new_res ) )
				{
					p_label->set_extended();
					t_labelFW* new_label = new t_labelFW(i_id_fwd++, std::move(new_res), p_label, p_ref);
#ifdef SPPRC_CHECK_PATH
					if(b_check_path)
						CheckNewLabelFW(new_label);
#endif
					o_extension_manager_fw.insert( new_label );
					int new_node = p_ref->Head();
					v_dominance_manager_fw[new_node].insert( new_label );
				}
			}
			p_stat->OnEndBidirFwPropagate();
		}
	}
	// add one more label at sink
	t_labelFW* dest_label = new t_labelFW(i_id_fwd++, std::move(copy_res), nullptr, nullptr);
#ifdef SPPRC_CHECK_PATH
	if(b_check_path)
		CheckNewLabelFW(dest_label);
#endif
	o_extension_manager_fw.insert( dest_label );
	v_dominance_manager_fw[sink].insert( dest_label );
	// final dominance
	p_stat->OnStartBidirFwDominance();
	apply_dominance_FW();
	p_stat->OnEndBidirFwDominance();
	p_stat->OnEndBidirFwLabeling();
	p_stat->OnEndBidirLabeling();
	return (v_dominance_manager_fw[sink].size() < max_num_paths );
}


template <class REF, class ExtManagerFW, class DManagerFW, class ExtManagerBW, class DManagerBW, class MManager, class StatManager>
bool c_SPPRC_Solver<REF, ExtManagerFW, DManagerFW, ExtManagerBW, DManagerBW, MManager, StatManager>
	::GetPathsBidir_Symmetric( const int sink, vector<pair<double, pair<t_labelFW*,t_labelBW*> > >& paths, function<bool (t_labelFW*, t_labelBW*)>* pointer_sorter_merge, function<bool (pair<double,pair<t_labelFW*,t_labelBW*> >, pair<double,pair<t_labelFW*,t_labelBW*> > )>* pointer_sorter, int max_num_paths)
{
	if ( !is_same<ExtManagerFW,ExtManagerBW>::value
		|| !is_same<DManagerFW,DManagerBW>::value 
		|| !is_same<ResVectFW,ResVectBW>::value 
		|| !is_same<t_labelFW,t_labelBW>::value )
	{	
		cout << "GetPathsBidir_Symmetric only applicable for symmetric SPPRCs" << endl;
		throw;
	}
	// safe now
	p_stat->OnStartBidirMerge();
	if ( !p_merge_manager )
	{
		cout << "Run SolveBidir_Symmetric(...) first. " << endl;
		throw;
	}
	int n = (int)v_dominance_manager_fw.size();
	double rdc = 0.0;
	for (int i = 0; i < n; i++)
	{
 		v_dominance_manager_fw[i].Sort(pointer_sorter_merge);
		for (t_labelFW* label_fw = v_dominance_manager_fw[i].begin(); label_fw; label_fw = v_dominance_manager_fw[i].next(label_fw))
		{
			if (p_merge_manager->MergeableFW(*label_fw))
			{
				for (t_labelBW* label_bw = label_fw; label_bw; label_bw = v_dominance_manager_fw[i].next(label_bw))
				{
					if (p_merge_manager->BreakingCriterion(*label_fw, *label_bw))
						break;
					if (p_merge_manager->MergeableBW(*label_bw) && p_merge_manager->Mergeable(*label_fw, *label_bw, rdc))
						paths.push_back(make_pair(rdc, make_pair(label_fw, label_bw)));
					if ((int)paths.size() > 2 * max_num_paths)
					{
						if (pointer_sorter)
							sort(paths.begin(), paths.end(), *pointer_sorter);
						paths.resize(max_num_paths);
					}
					if (!pointer_sorter && (int)paths.size() >= max_num_paths)
					{
						p_stat->OnEndBidirMerge();
						i_gen_paths = (int)paths.size();
						return false;
					}
				}
			}
		}
	}
	// too many?
	if ( (int)paths.size() > max_num_paths )
	{
		if ( pointer_sorter )
			sort( paths.begin(), paths.end(), *pointer_sorter );
		paths.resize( max_num_paths );
		if (!pointer_sorter)
		{
			p_stat->OnEndBidirMerge();
			return false;
		}
	}
	i_gen_paths = (int)paths.size();
	p_stat->OnEndBidirMerge();
	return true;
}


template <class REF, class ExtManagerFW, class DManagerFW, class ExtManagerBW, class DManagerBW, class MManager, class StatManager>
bool c_SPPRC_Solver<REF, ExtManagerFW, DManagerFW, ExtManagerBW, DManagerBW, MManager, StatManager>
::GetPathsBidirBuckets_Symmetric(const int sink, vector<pair<double, pair<t_labelFW*, t_labelBW*> > >& paths, function<bool(t_labelFW*, t_labelBW*)>* pointer_sorter_merge, function<bool(pair<double, pair<t_labelFW*, t_labelBW*> >, pair<double, pair<t_labelFW*, t_labelBW*> >)>* pointer_sorter, int max_num_paths)
{
	if (!is_same<ExtManagerFW, ExtManagerBW>::value
		|| !is_same<DManagerFW, DManagerBW>::value
		|| !is_same<ResVectFW, ResVectBW>::value
		|| !is_same<t_labelFW, t_labelBW>::value)
	{
		cout << "GetPathsBidir_Symmetric only applicable for symmetric SPPRCs" << endl;
		throw;
	}
	// safe now
	p_stat->OnStartBidirMerge();
	if (!p_merge_manager)
	{
		cout << "Run SolveBidir_Symmetric(...) first. " << endl;
		throw;
	}
	int n = (int)v_dominance_manager_fw.size();
	double rdc = 0.0;
	int numBuckets = p_merge_manager->NumBuckets();
	for (int i = 0; i < n; i++)
	{
		//Labels are sorted, in reverse order: reverse labels
		v_dominance_manager_fw[i].ReverseAllBuckets();
		for (int bucketI = 0; bucketI < numBuckets; ++bucketI)
		{
			for (t_labelFW* label_fw = v_dominance_manager_fw[i].begin(bucketI); label_fw; label_fw = v_dominance_manager_fw[i].next(bucketI, label_fw))
			{
				if (p_merge_manager->MergeableFW(*label_fw))
				{
					for (auto bucketJ : p_merge_manager->BucketsThatFitForMerge(i, bucketI))
					{
						for (t_labelBW* label_bw = (bucketI == bucketJ ? label_fw : v_dominance_manager_fw[i].begin(bucketJ)); label_bw; label_bw = v_dominance_manager_fw[i].next(bucketJ, label_bw))
						{
							if (p_merge_manager->BreakingCriterion(*label_fw, *label_bw))
								break;
							if (p_merge_manager->MergeableBW(*label_bw) && p_merge_manager->Mergeable(*label_fw, *label_bw, rdc))
								paths.push_back(make_pair(rdc, make_pair(label_fw, label_bw)));
							if ((int)paths.size() > 2 * max_num_paths)
							{
								if (pointer_sorter)
									sort(paths.begin(), paths.end(), *pointer_sorter);
								paths.resize(max_num_paths);
							}

							if (!pointer_sorter && (int)paths.size() >= max_num_paths)
							{
								p_stat->OnEndBidirMerge();
								i_gen_paths = (int)paths.size();
								return false;
							}
						}
					}
				}
			}
		}
	}
	// too many?
	if ((int)paths.size() > max_num_paths)
	{
		if (pointer_sorter)
			sort(paths.begin(), paths.end(), *pointer_sorter);
		paths.resize(max_num_paths);
		if (!pointer_sorter)
		{
			p_stat->OnEndBidirMerge();
			return false;
		}
	}
	i_gen_paths = (int)paths.size();
	p_stat->OnEndBidirMerge();
	return true;
}


// IO

template <class REF, class ExtManagerFW, class DManagerFW, class ExtManagerBW, class DManagerBW, class MManager, class StatManager>
void c_SPPRC_Solver<REF, ExtManagerFW, DManagerFW, ExtManagerBW, DManagerBW, MManager, StatManager>
	::OutputInStream(ostream& s) const
{
	int max_idx_fw=(int)v_dominance_manager_fw.size();
	int	max_idx_bw=(int)v_dominance_manager_bw.size();
	for (int i=0;i<max(max_idx_fw, max_idx_bw);i++)
	{   
		s << "Node: " << i << endl; 
		if (i < max_idx_fw)
		{
			s << "Forward: " << endl; 
			v_dominance_manager_fw[i].OutputInStream(s);
		}
		if (i < max_idx_bw)
		{
			s << "Backward: " << endl; 
			v_dominance_manager_bw[i].OutputInStream(s);
		}
	}
}


template <class REF, class ExtManagerFW, class DManagerFW, class ExtManagerBW, class DManagerBW, class MManager, class StatManager>
void c_SPPRC_Solver<REF, ExtManagerFW, DManagerFW, ExtManagerBW, DManagerBW, MManager, StatManager>
	::OutputGraphML(string filename)
{
	using namespace graphml;
	c_GraphML g( filename);
	//Nodes
	int max_idx=(int)v_dominance_manager_fw.size();
	for (int i=0; i<max_idx;i++)
	{
		nodeproperties np;
		np.bordercolor = Color( "Black" );
		np.coord_x =  i*10;
		np.coord_y =  i*10;
		g.AddNode( i, np );
	}
	//Arcs
	for (int i=0; i<max_idx;i++)
	{
		int max_idx2=(int) v_fw_star[i].size();
		for (int j=0; j<max_idx2;j++)
		{		
			linkproperties lp;
			lp.linecolor = Color("Black");
			lp.linewidth = 1.5;		
			g.AddArc( v_fw_star[i][j]->Tail(), v_fw_star[i][j]->Head(), lp );
		}
	}
}

// DEBUGGING

#ifdef SPPRC_CHECK_PATH

template<class REF, class ExtManagerFW, class DManagerFW, class ExtManagerBW, class DManagerBW, class MManager, class StatManager>
inline bool c_SPPRC_Solver<REF, ExtManagerFW, DManagerFW, ExtManagerBW, DManagerBW, MManager, StatManager>::CheckNewLabelFW(t_labelFW* label)
{
	if (label->generator_REF())
	{
		if (label->predecessor()->b_check_required)
		{
			for ( int i=0; i<v_path_to_check.size(); i++ )
			{
				REF* ref = v_path_to_check[i];
				if (ref == label->generator_REF())
				{
					int ii = i-1;
					const t_labelFW* l = label->predecessor();
					while (ii >= 0 && label && label->generator_REF())
					{
						if (v_path_to_check[ii] != l->generator_REF())
							return false;
						ii--;
						l = l->predecessor();
					}
					label->b_check_required = (ii == -1 && l->generator_REF() == nullptr);
					if (b_check_verbose && label->b_check_required)
					{
						cout << "### FW label Id=" << label->Id() << " of partial path ( s";
						vector<int> vertices;
						const t_labelFW* l = label;
						while (l->generator_REF())
						{
							vertices.insert(vertices.begin(), l->generator_REF()->Head());
							l = l->predecessor();
						}
						for (auto v : vertices)
							cout << ", " << v;
						cout << " ) created";
						if (label->predecessor())
							cout << " from pred_Id=" << label->predecessor()->Id();
						cout << ".\n";
					}
					return label->b_check_required;
				}
			}
		}
	}
	else
	{
		label->b_check_required = true;
		if (b_check_verbose)
			cout << "### Initial FW label Id=" << label->Id() << " is created. \n";
		return true;
	}
	return false;
}


template<class REF, class ExtManagerFW, class DManagerFW, class ExtManagerBW, class DManagerBW, class MManager, class StatManager>
inline bool c_SPPRC_Solver<REF, ExtManagerFW, DManagerFW, ExtManagerBW, DManagerBW, MManager, StatManager>::CheckNewLabelBW(t_labelBW * label)
{
	if (label->generator_REF())
	{
		if (label->predecessor()->b_check_required)
		{
			for (int i=0; i<(int)v_path_to_check.size(); i++)
			{
				REF* ref = v_path_to_check[i];
				if (ref == label->generator_REF())
				{
					int ii = i + 1;
					const t_labelBW* l = label->predecessor();
					while (ii < v_path_to_check.size() && label && label->generator_REF())
					{
						if (v_path_to_check[ii] != l->generator_REF())
							return false;
						ii++;
						l = l->predecessor();
					}
					label->b_check_required = (ii == v_path_to_check.size() && l->generator_REF() == nullptr);
					if (b_check_verbose && label->b_check_required)
					{
						cout << "### BW label Id=" << label->Id() << " of partial path ( ";
						const t_labelBW* l = label;
						while (l->generator_REF())
						{
							cout << l->generator_REF()->Tail()<< ", ";
							l = l->predecessor();
						}
						cout << "t ) created";
						if (label->predecessor())
							cout << " from pred_Id=" << label->predecessor()->Id();
						cout << ".\n";
					}
					return label->b_check_required;
				}
			}
		}
	}
	else
	{
		label->b_check_required = true;
		if (b_check_verbose)
			cout << "### Initial BW label Id=" << label->Id() << " is created. \n";
		return true;
	}
	return false;
}


template<class REF, class ExtManagerFW, class DManagerFW, class ExtManagerBW, class DManagerBW, class MManager, class StatManager>
inline bool c_SPPRC_Solver<REF, ExtManagerFW, DManagerFW, ExtManagerBW, DManagerBW, MManager, StatManager>::CheckPath(const vector<REF*>& path, bool verbose)
{
	b_check_verbose = verbose;
	// Check that the path is well defined, i.e., sequence of REFs describes a path
	if (path.empty())
	{
		b_check_path = false;
		v_path_to_check.clear();
		return true;
	}
	int pred = path[0]->Tail();
	for (auto ref : path)
	{
		if (ref->Tail() != pred)
		{
			std::cerr << "### The given REFs don't represent a path. Error at REF for (" << ref->Tail() 
				<< ", " << ref->Head() << ") \n";
			return false;
		}
		pred = ref->Head();
	}
	b_check_path = true;
	v_path_to_check = path;
	return true;
}
#endif

};
#endif // of ESPPRC_SOLVER_H
