#ifndef ESPPTW_DOMINANCE_MGNT_WITH_BUCKET_H
#define ESPPTW_DOMINANCE_MGNT_WITH_BUCKET_H

#include <iostream>
#include <functional>
#include <tuple>
#include <cassert>
#include <algorithm>

#include "statistics_mgnt.h"

/////////////////////////////////////////////////////////////////////////////
// c_Dominance_Manager_WithBuckets
/////////////////////////////////////////////////////////////////////////////

namespace espprc
{

template <class Label>
struct s_Bucket {
	Label* p_first_label; /* singly-linked list with those "old" labels on which the dominance algorithm has already been run */
	Label* p_first_added_label; /* singly-linked list with "new" labels, dominance algorithm has NOT been run */
	bool b_old_labels_tested_interbucket;
};


template <class Label, class tTuple, class StatManager = c_EmptyStatisticsManager>
class c_Dominance_Manager_WithBuckets {
protected:
	bool b_fw;
	int i_node;
	int i_bm_bucket_index;
	int i_added_labels;
	tTuple t_streckfaktor;
	int i_startTime;
	tTuple t_numBuckets;
	std::vector<int> v_activeBuckets;
	int i_lastActiveBucket;
	//const my_tuple t_max_size;
	std::function<tTuple(const Label&, const tTuple&, int)> f_bucket_assign;
	std::function<int(int, int, int, int)> f_bm_bucket_index_assign;
	std::vector<s_Bucket<Label> > v_buckets;
	Label* apply_dominance(Label*& p_first_label, Label*& p_first_added_label, bool old_labels_tested_interbucket); // return value is first label to delete
	Label* apply_dominance_interbucket(Label*& p_labels_to_dominate, Label* p_dominator_labels);
	void Sort(Label*& start, std::function<bool(Label*, Label*)>* sorter);
	std::function<bool(Label*, Label*)>* f_breakingCriterion;
	std::function<bool(Label*, Label*)>* f_sorter;
	void Reverse(Label*& start);
	StatManager o_initial_stat;
	StatManager* p_stat;
public:
	c_Dominance_Manager_WithBuckets();
	void SetInfo(bool fw, int node) { b_fw = fw; i_node = node; }

	void insert(Label*);
	Label* apply_dominance(int bm_bucket_index = -1); // return value is first label to delete
	Label* apply_final_dominance(int bm_bucket_index = -1);
	Label* begin(); // iterate over all undominated labels
	Label* next(Label* current);
	void clear();
	int size() const { return i_added_labels; }
	void OutputInStream(std::ostream& s) const; // Output all Label of this dominance manager
	void Sort(std::function<bool(Label*, Label*)>* sorter);
	void SetAssignmentFunction(std::function<tTuple(const Label&, const tTuple&, int)> bucket_assign) { f_bucket_assign = bucket_assign; }
	void SetBMBucketIndexAssignmentFunction(std::function<int(int, int, int, int)> bm_bucket_index_assign) { f_bm_bucket_index_assign = bm_bucket_index_assign; }
	void SetStreckfaktor(tTuple fact, tTuple numBuckets);
	void SetStartTime(int time){ i_startTime = time; }
	const vector<int>& ActiveBucketsSorted();
	Label* begin(int bucket); // iterate over all undominated labels
	Label* next(int bucket, Label* current);
	void SortAllBuckets(std::function<bool(Label*, Label*)>* sorter);
	void SortBucket(std::function<bool(Label*, Label*)>* sorter, int bucket);
	void Splice(Label*& p_first_label, Label*& p_first_added_label);
	void SetSortingFunction(std::function<bool(Label*, Label*)>* sorter) { f_sorter = sorter; }
	void SetBreakingCriterionFunction(std::function<bool(Label*, Label*)>* breakingCriterion) { f_breakingCriterion = breakingCriterion; }
	void ReverseAllBuckets();
	void ReverseBucket(int bucket);
	void ResetOldLabelsTestedInterbucket(int currentExtensionManagerBucket);
	void OutputOldBucketsTested();
	void SetStatisticsManager(StatManager& stat) { p_stat = &stat; }
	StatManager& StatisticsManager() { return *p_stat; }
	void PrepareForWarmstart(int currentExtensionManagerBucket);
#ifdef SPPRC_CHECK_PATH
	void CheckDominance(Label* label1, Label* label2);
#endif
};


template <class Label, class tTuple, class StatManager>
c_Dominance_Manager_WithBuckets<Label, tTuple, StatManager>::c_Dominance_Manager_WithBuckets()
:	i_bm_bucket_index(-1),
	i_added_labels(0),
	i_lastActiveBucket(0),
	o_initial_stat(),
	p_stat(&o_initial_stat)
{}


template <class Label, class tTuple, class StatManager>
void c_Dominance_Manager_WithBuckets<Label, tTuple, StatManager>::SetStreckfaktor(tTuple fact, tTuple numBuckets)
{
	t_streckfaktor = fact;
	t_numBuckets = numBuckets;
	i_lastActiveBucket = 0;
	v_activeBuckets.clear();
	v_activeBuckets.resize(get<0>(t_numBuckets));
	v_buckets.resize(get<0>(t_numBuckets));
	for (int i = 0; i < (int)v_buckets.size(); ++i)
	{
		v_buckets[i].p_first_label = nullptr;
		v_buckets[i].p_first_added_label = nullptr;
		v_buckets[i].b_old_labels_tested_interbucket = false;
	}
}


template <class Label, class tTuple, class StatManager>
Label* c_Dominance_Manager_WithBuckets<Label, tTuple, StatManager>::apply_dominance(Label*& p_first_label, Label*& p_first_added_label, bool old_labels_tested_interbucket)
{
	// if there are no additional labels then there are no labels in dominance
	if (!p_first_added_label)
		return NULL;
	Label* ret_labels_to_del = NULL;
	//Sort new labels (old ones are already sorted)
	Sort(p_first_added_label, f_sorter);
	// dominance between two new labels (quick solution: test in both directions)
	Label* pred_new1 = NULL;
	for (Label* curr_new1 = p_first_added_label;
		curr_new1;
		pred_new1 = curr_new1, curr_new1 = (curr_new1 ? curr_new1->next() : NULL))
	{
		Label* pred_new2 = curr_new1;
		for (Label* curr_new2 = curr_new1->next(); curr_new2; /**/)
		{
			p_stat->OnDominanceTest(b_fw, i_node);
			if (*curr_new1 <= *curr_new2)
			{ // new1 dominates new2
#ifdef SPPRC_CHECK_PATH
				CheckDominance(curr_new1, curr_new2);
#endif
				pred_new2->set_next(curr_new2->next());
				curr_new2->set_next(ret_labels_to_del);
				ret_labels_to_del = curr_new2;

				curr_new2 = pred_new2->next();
				i_added_labels--;
			}
			else
			{
				p_stat->OnDominanceTest(b_fw, i_node);
				if (*curr_new2 <= *curr_new1)
				{ // new2 dominates new1
#ifdef SPPRC_CHECK_PATH
					CheckDominance(curr_new2, curr_new1);
#endif
					if (pred_new1)
						pred_new1->set_next(curr_new1->next());
					else
						p_first_added_label = curr_new1->next();
					curr_new1->set_next(ret_labels_to_del);
					ret_labels_to_del = curr_new1;

					curr_new1 = (pred_new1 ? pred_new1->next() : p_first_added_label);
					curr_new2 = curr_new1->next();
					pred_new2 = curr_new1;
					i_added_labels--;
				}
				else // no domination
				{
					pred_new2 = curr_new2;
					curr_new2 = curr_new2->next();
				}
			}
		}
	}
	// old dominates new labels
	Label* pred_old = NULL;
	for (Label* curr_old = p_first_label; curr_old;
		pred_old = curr_old, curr_old = (curr_old ? curr_old->next() : NULL))
	{
		Label* pred_new = NULL;
		for (Label* curr_new = p_first_added_label; curr_new; /**/)
		{
			if ((*f_breakingCriterion)(curr_old, curr_new))
				break;
			p_stat->OnDominanceTest(b_fw, i_node);
			if (*curr_old <= *curr_new)
			{ // old dominates new
#ifdef SPPRC_CHECK_PATH
				CheckDominance(curr_old, curr_new);
#endif
				if (pred_new)
					pred_new->set_next(curr_new->next());
				else
					p_first_added_label = curr_new->next();
				curr_new->set_next(ret_labels_to_del);
				ret_labels_to_del = curr_new;
				curr_new = (pred_new ? pred_new->next() : p_first_added_label);
				i_added_labels--;
			}
			else // no dominance
			{
				pred_new = curr_new;
				curr_new = curr_new->next();
			}
		}
	}
	// new dominates old labels
	Label* pred_new = NULL;
	for (Label* curr_new = p_first_added_label; curr_new;
		pred_new = curr_new, curr_new = (curr_new ? curr_new->next() : NULL))
	{
		Label* pred_old = NULL;
		for (Label* curr_old = p_first_label; curr_old; /**/)
		{
			if ((*f_breakingCriterion)(curr_new, curr_old))
				break;
			p_stat->OnDominanceTest(b_fw, i_node);
			if (*curr_new <= *curr_old)
			{ // new dominates old
#ifdef SPPRC_CHECK_PATH
				CheckDominance(curr_new, curr_old);
#endif
				if (pred_old)
					pred_old->set_next(curr_old->next());
				else
					p_first_label = curr_old->next();
				curr_old->set_next(ret_labels_to_del);
				ret_labels_to_del = curr_old;
				curr_old = (pred_old ? pred_old->next() : p_first_label);
				i_added_labels--;
			}
			else // no dominance
			{
				pred_old = curr_old;
				curr_old = curr_old->next();
			}
		}
	}
	//splice (both lists are sorted)
	//New labels stay new when the bm_bucket index is smaller than the index of this bucket, because they should be tested against other buckets; set them old after these tests
	if (!old_labels_tested_interbucket)
	{
		if (p_first_label && p_first_added_label)
			Splice(p_first_label, p_first_added_label);
		else if (p_first_added_label)
			p_first_label = p_first_added_label;
		p_first_added_label = NULL;
	}
	return ret_labels_to_del;
}


template <class Label, class tTuple, class StatManager>
Label* c_Dominance_Manager_WithBuckets<Label, tTuple, StatManager>::apply_dominance_interbucket(Label*& p_labels_to_dominate, Label* p_dominator_labels)
{
	// if there are no additional labels then there are no labels in dominance
	if (!p_labels_to_dominate || !p_dominator_labels)
		return NULL;
	Label* ret_labels_to_del = NULL;
	// dominance between old and new labels
	Label* pred_old = NULL;
	for (Label* curr_old = p_dominator_labels; curr_old;
		pred_old = curr_old, curr_old = (curr_old ? curr_old->next() : NULL))
	{
		Label* pred_new = NULL;
		for (Label* curr_new = p_labels_to_dominate; curr_new; /**/)
		{
			if ((*f_breakingCriterion)(curr_old, curr_new))
				break;
			p_stat->OnDominanceTest(b_fw, i_node);
			if (*curr_old < *curr_new)
			{ // old dominates new
#ifdef SPPRC_CHECK_PATH
				CheckDominance(curr_old, curr_new);
#endif
				if (pred_new)
					pred_new->set_next(curr_new->next());
				else
					p_labels_to_dominate = curr_new->next();
				curr_new->set_next(ret_labels_to_del);
				ret_labels_to_del = curr_new;
				curr_new = (pred_new ? pred_new->next() : p_labels_to_dominate);
				i_added_labels--;
			}
			else // no dominance
			{
				pred_new = curr_new;
				curr_new = curr_new->next();
			}
		}
	}
	// those labels to delete
	return ret_labels_to_del;
}


template <class Label, class tTuple, class StatManager>
Label* c_Dominance_Manager_WithBuckets<Label, tTuple, StatManager>::apply_dominance(int bm_bucket_index)
{
	i_bm_bucket_index = bm_bucket_index;
	Label* first_to_delete = nullptr;
	for (int k = 0; k < i_lastActiveBucket; ++k)
	{
		int i = v_activeBuckets[k];
		s_Bucket<Label>& bucket = v_buckets[i];
		Label* new_to_delete = apply_dominance(bucket.p_first_label, bucket.p_first_added_label, bucket.b_old_labels_tested_interbucket);
		if (new_to_delete)
		{
			// determine last element in singly-linked list
			Label* last = new_to_delete;
			while (last->next())
				last = last->next();
			last->set_next(first_to_delete);
			first_to_delete = new_to_delete;
		}
	}
	// if current i_bm_bucket_index is not in node TW
	int streckindex = f_bm_bucket_index_assign(i_bm_bucket_index, i_startTime, get<0>(t_streckfaktor), get<0>(t_numBuckets));
	if (streckindex < 0 || streckindex >= get<0>(t_numBuckets))
		return first_to_delete;
	// dominance between different buckets
	//for (int k = 0; k < i_lastActiveBucket;)
	//{
	//	int position = v_activeBuckets[k];
	//	if (position != streckindex)
	//	{
	//		++k;
	//		continue;
	//	}
	s_Bucket<Label>& bucket_to_dominate = v_buckets[streckindex];
	for (int k2 = 0; k2 < i_lastActiveBucket; ++k2)
	{
		int positionDominator = v_activeBuckets[k2];
		if (streckindex <= positionDominator)
			continue;
		s_Bucket<Label>& dominator_bucket = v_buckets[positionDominator];
		//compare new labels against old from dominator bucket
		Label* new_to_delete = apply_dominance_interbucket(bucket_to_dominate.b_old_labels_tested_interbucket ? bucket_to_dominate.p_first_added_label : bucket_to_dominate.p_first_label, dominator_bucket.p_first_label);
		if (new_to_delete)
		{
			Label* last = new_to_delete;
			while (last->next())
				last = last->next();
			last->set_next(first_to_delete);
			first_to_delete = new_to_delete;
		}
		if (!bucket_to_dominate.p_first_added_label && !bucket_to_dominate.p_first_label)
		{
			for (int i = 0; i < i_lastActiveBucket; ++i)
			{
				if (streckindex == v_activeBuckets[i])
				{
					--i_lastActiveBucket;
					swap(v_activeBuckets[i], v_activeBuckets[i_lastActiveBucket]);
					return first_to_delete;
				}
			}
		}
	}
	if (bucket_to_dominate.p_first_label && bucket_to_dominate.p_first_added_label)
		Splice(bucket_to_dominate.p_first_label, bucket_to_dominate.p_first_added_label);
	else if (bucket_to_dominate.p_first_added_label)
		bucket_to_dominate.p_first_label = bucket_to_dominate.p_first_added_label;
	bucket_to_dominate.p_first_added_label = NULL;
	bucket_to_dominate.b_old_labels_tested_interbucket = true;
	return first_to_delete;
}


template <class Label, class tTuple, class StatManager>
Label* c_Dominance_Manager_WithBuckets<Label, tTuple, StatManager>::apply_final_dominance(int bm_bucket_index)
{
	i_bm_bucket_index = bm_bucket_index;
	Label* first_to_delete = nullptr;
	for (int k = 0; k < i_lastActiveBucket; ++k)
	{
		int i = v_activeBuckets[k];
		s_Bucket<Label>& bucket = v_buckets[i];
		//bucket.b_old_labels_tested_interbucket = false;
		Label* new_to_delete = apply_dominance(bucket.p_first_label, bucket.p_first_added_label, bucket.b_old_labels_tested_interbucket);
		if (new_to_delete)
		{
			// determine last element in singly-linked list
			Label* last = new_to_delete;
			while (last->next())
				last = last->next();
			last->set_next(first_to_delete);
			first_to_delete = new_to_delete;
		}
	}
	int streckindex = f_bm_bucket_index_assign(i_bm_bucket_index, i_startTime, get<0>(t_streckfaktor), get<0>(t_numBuckets));
	if (streckindex < 0 || streckindex >= get<0>(t_numBuckets))
		return first_to_delete;
	// dominance between different buckets
	for (int k = 0; k < i_lastActiveBucket;)
	{
		int position = v_activeBuckets[k];
		s_Bucket<Label>& bucket_to_dominate = v_buckets[position];

		for (int k2 = 0; k2 < i_lastActiveBucket; ++k2)
		{
			if (k == k2)
				continue;
			int positionDominator = v_activeBuckets[k2];
			s_Bucket<Label>& dominator_bucket = v_buckets[positionDominator];
			if (position > positionDominator)
			{
				//compare new labels against old from dominator bucket
				//this is always done, for buckets in same column and others
				Label* new_to_delete = apply_dominance_interbucket(bucket_to_dominate.b_old_labels_tested_interbucket ? bucket_to_dominate.p_first_added_label : bucket_to_dominate.p_first_label, dominator_bucket.p_first_label);
				if (new_to_delete)
				{
					Label* last = new_to_delete;
					while (last->next())
						last = last->next();
					last->set_next(first_to_delete);
					first_to_delete = new_to_delete;
				}
				if (!bucket_to_dominate.p_first_added_label && !bucket_to_dominate.p_first_label)
				{
					--i_lastActiveBucket;
					swap(v_activeBuckets[k], v_activeBuckets[i_lastActiveBucket]);
					--k;
					break;
				}
			}
		}
		++k;
	}
	for (int k = 0; k < i_lastActiveBucket; ++k)
	{
		int position = v_activeBuckets[k];
		s_Bucket<Label>& bucket_to_dominate = v_buckets[position];
		if (bucket_to_dominate.p_first_label && bucket_to_dominate.p_first_added_label)
			Splice(bucket_to_dominate.p_first_label, bucket_to_dominate.p_first_added_label);
		else if (bucket_to_dominate.p_first_added_label)
			bucket_to_dominate.p_first_label = bucket_to_dominate.p_first_added_label;
		bucket_to_dominate.p_first_added_label = NULL;
		//bucket_to_dominate.b_old_labels_tested_interbucket = true;
	}
	return first_to_delete;
}


template <class Label, class tTuple, class StatManager>
void c_Dominance_Manager_WithBuckets<Label, tTuple, StatManager>::OutputInStream(std::ostream& s) const
{
	for (int i = 0; i < (int)v_buckets.size(); ++i)
	{
		Label* label = v_buckets[i].p_first_label;
		while (label)
		{
			label->OutputInStream(s);
			s << endl;
			label = label->next();
		}
	}
	s << "________________" << endl;
}


template <class Label, class tTuple, class StatManager>
Label* c_Dominance_Manager_WithBuckets<Label, tTuple, StatManager>::next(Label* current)
{
	if (current->next())
		return current->next();
	tTuple key(f_bucket_assign(*current, t_streckfaktor, get<0>(t_numBuckets)));
	assert(get<0>(key) >= 0);
	int position = get<0>(key);
	for (int i = position+1; i < (int)v_buckets.size(); ++i)
		if (v_buckets[i].p_first_label)
			return v_buckets[i].p_first_label;
	return nullptr;
}


template <class Label, class tTuple, class StatManager>
Label* c_Dominance_Manager_WithBuckets<Label, tTuple, StatManager>::begin()
{
	for (int i = 0; i < (int)v_buckets.size(); ++i)
		if (v_buckets[i].p_first_label)
			return v_buckets[i].p_first_label;
	return nullptr;
}


template <class Label, class tTuple, class StatManager>
void c_Dominance_Manager_WithBuckets<Label, tTuple, StatManager>::insert(Label* new_label)
{
	p_stat->OnLabelInserted(b_fw, i_node);
	// insert into singly-linked list right at the beginning
	i_added_labels++;
	tTuple key(f_bucket_assign(*new_label, t_streckfaktor, get<0>(t_numBuckets)));
	assert(get<0>(key) >= 0);
	int position = get<0>(key);
	if (v_buckets[position].p_first_added_label || v_buckets[position].p_first_label)
	{
		new_label->set_next(v_buckets[position].p_first_added_label);
		v_buckets[position].p_first_added_label = new_label;
	}
	else
	{
		new_label->set_next(nullptr);
		v_buckets[position].p_first_label = nullptr;
		v_buckets[position].p_first_added_label = new_label;
		int streckindex = f_bm_bucket_index_assign(i_bm_bucket_index, i_startTime, get<0>(t_streckfaktor), get<0>(t_numBuckets));
		if (i_bm_bucket_index >= 0 && get<0>(key) == streckindex)
			v_buckets[position].b_old_labels_tested_interbucket = true;
		else
			v_buckets[position].b_old_labels_tested_interbucket = false;
		v_activeBuckets[i_lastActiveBucket++] = position;
	}
}


template <class Label, class tTuple, class StatManager>
void c_Dominance_Manager_WithBuckets<Label, tTuple, StatManager>::clear()
{
	i_added_labels = 0;
	//for (int i = 0; i < (int)v_buckets.size(); ++i)
	for (int k = 0; k < i_lastActiveBucket; ++k)
	{
		int i = v_activeBuckets[k];
		s_Bucket<Label>& bucket = v_buckets[i];
		Label* p_first_label = bucket.p_first_label;
		while (p_first_label)
		{
			Label* help = p_first_label;
			p_first_label = p_first_label->next();
			delete help;
		}
		Label* p_first_added_label = bucket.p_first_added_label;
		while (p_first_added_label)
		{
			Label* help = p_first_added_label;
			p_first_added_label = p_first_added_label->next();
			delete help;
		}
		bucket.p_first_label = nullptr;
		bucket.p_first_added_label = nullptr;
		bucket.b_old_labels_tested_interbucket = false;
	}
	i_bm_bucket_index = -1;
	i_lastActiveBucket = 0;
}


template <class Label, class tTuple, class StatManager>
void c_Dominance_Manager_WithBuckets<Label,tTuple, StatManager>::Sort(std::function<bool(Label*, Label*)>* sorter)
{
	cout << "c_Bucket_Dominance_Manager does not allow global sorting of all labels." << endl;
	throw;
}


template <class Label, class tTuple, class StatManager>
const vector<int>& c_Dominance_Manager_WithBuckets<Label, tTuple, StatManager>::ActiveBucketsSorted()
{
	sort(v_activeBuckets.begin(), v_activeBuckets.begin() + i_lastActiveBucket);
	return v_activeBuckets;
}


template <class Label, class tTuple, class StatManager>
Label* c_Dominance_Manager_WithBuckets<Label, tTuple, StatManager>::begin(int bucket)
{
	return v_buckets[bucket].p_first_label;
}


template <class Label, class tTuple, class StatManager>
Label* c_Dominance_Manager_WithBuckets<Label, tTuple, StatManager>::next(int bucket, Label* current)
{
	return current->next();
}


template <class Label, class tTuple, class StatManager>
void c_Dominance_Manager_WithBuckets<Label, tTuple, StatManager>::SortAllBuckets(std::function<bool(Label*, Label*)>* sorter)
{
	for (int k = 0; k < i_lastActiveBucket; ++k)
		SortBucket(sorter, v_activeBuckets[k]);
}


template <class Label, class tTuple, class StatManager>
void c_Dominance_Manager_WithBuckets<Label, tTuple, StatManager>::SortBucket(std::function<bool(Label*, Label*)>* sorter, int bucket)
{
	Sort(v_buckets[bucket].p_first_label, sorter);
}


template <class Label, class tTuple, class StatManager>
void c_Dominance_Manager_WithBuckets<Label, tTuple, StatManager>::Sort(Label*& from, std::function<bool(Label*, Label*)>* sorter)
{
	// trivial cases
	if (from == NULL || from->next() == NULL)
		return;
	if (from->next()->next() == NULL) // only two labels
	{
		if (!(*sorter)(from, from->next()))
		{
			Label* new_start = from->next();
			from->next()->set_next(from);
			from->set_next(NULL);
			from = new_start;
		}
		return;
	}
	// Split in the middle
	int length = 0;
	Label* it = from;
	Label* mid = from;
	while (it)
	{
		it = it->next();
		length++;
		if (length % 2 == 0 && it)
			mid = mid->next();
	}
	// Sort each part 
	Label* right = mid->next();
	mid->set_next(NULL);
	Sort(from, sorter);
	Sort(right, sorter);
	// Merge both parts together
	Label* result = NULL;
	Label* last = NULL;
	while (from && right)
	{
		if (result == NULL)
		{
			if ((*sorter)(from, right))
				result = from;
			else
				result = right;
		}
		if ((*sorter)(from, right))
		{
			if (last)
				last->set_next(from);
			last = from;
			from = from->next();
		}
		else
		{
			if (last)
				last->set_next(right);
			last = right;
			right = right->next();
		}
	}
	// add remaining parts
	if (from)
		last->set_next(from);
	if (right)
		last->set_next(right);
	// return resulting start label
	from = result;
}


template <class Label, class tTuple, class StatManager>
void c_Dominance_Manager_WithBuckets<Label, tTuple, StatManager>::Splice(Label*& p_first_label, Label*& p_first_added_label)
{
	Label* result = NULL;
	Label* last = NULL;
	while (p_first_added_label && p_first_label)
	{
		if (result == NULL)
		{
			if ((*f_sorter)(p_first_added_label, p_first_label))
				result = p_first_added_label;
			else
				result = p_first_label;
		}
		if ((*f_sorter)(p_first_added_label, p_first_label))
		{
			if (last)
				last->set_next(p_first_added_label);
			last = p_first_added_label;
			p_first_added_label = p_first_added_label->next();
		}
		else
		{
			if (last)
				last->set_next(p_first_label);
			last = p_first_label;
			p_first_label = p_first_label->next();
		}
	}
	// add remaining parts
	if (p_first_added_label)
		last->set_next(p_first_added_label);
	if (p_first_label)
		last->set_next(p_first_label);
	p_first_label = result;
}


template <class Label, class tTuple, class StatManager>
void c_Dominance_Manager_WithBuckets<Label, tTuple, StatManager>::ReverseAllBuckets()
{
	for (int k = 0; k < i_lastActiveBucket; ++k)
		ReverseBucket(v_activeBuckets[k]);
}


template <class Label, class tTuple, class StatManager>
void c_Dominance_Manager_WithBuckets<Label, tTuple, StatManager>::ReverseBucket(int bucket)
{
	Reverse(v_buckets[bucket].p_first_label);
}


template <class Label, class tTuple, class StatManager>
void c_Dominance_Manager_WithBuckets<Label, tTuple, StatManager>::Reverse(Label*& start)
{
	Label* prev = NULL;
	Label* current = start;
	Label* next = NULL;
	while (current != NULL)
	{
		next = current->next();
		current->set_next(prev);
		prev = current;
		current = next;
	}
	start = prev;
}


template <class Label, class tTuple, class StatManager>
void c_Dominance_Manager_WithBuckets<Label, tTuple, StatManager>::ResetOldLabelsTestedInterbucket(int currentExtensionManagerBucket)
{
	int streckindex = f_bm_bucket_index_assign(currentExtensionManagerBucket, i_startTime, get<0>(t_streckfaktor), get<0>(t_numBuckets));
	//if (streckindex < 0 || streckindex >= get<0>(t_numBuckets))
	//	return;
	for (int k = 0; k < i_lastActiveBucket; ++k)
	{
		int i = v_activeBuckets[k];
		if (i > streckindex)
			v_buckets[i].b_old_labels_tested_interbucket = false;
		else
			v_buckets[i].b_old_labels_tested_interbucket = true;
	}
}


template <class Label, class tTuple, class StatManager>
void c_Dominance_Manager_WithBuckets<Label, tTuple, StatManager>::OutputOldBucketsTested()
{
	for (int k = 0; k < i_lastActiveBucket; ++k)
	{
		int i = v_activeBuckets[k];
		cout << "Bucket " << i << " b_old_labels_tested_interbucket is set to " << (v_buckets[i].b_old_labels_tested_interbucket ? "true" : "false") << endl;
	}
}

template <class Label, class tTuple, class StatManager>
void c_Dominance_Manager_WithBuckets<Label, tTuple, StatManager>::PrepareForWarmstart(int currentExtensionManagerBucket)
{
	ResetOldLabelsTestedInterbucket(currentExtensionManagerBucket);
}


#ifdef SPPRC_CHECK_PATH
template <class Label, class tTuple, class StatManager>
void c_Dominance_Manager_WithBuckets<Label, tTuple, StatManager>::CheckDominance(Label * label1, Label * label2)
{
	if (label2->b_check_required)
	{
		cout << "### Label Id=" << label1->Id() << " eliminates Label Id=" << label2->Id() << "\n";
	}
}
#endif


} // of namespace

#endif // of ESPPTW_DOMINANCE_MGNT_WITH_BUCKET_H