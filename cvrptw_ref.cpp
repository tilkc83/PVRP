#include "cvrptw_ref.h"
#include <functional>


extern bool taskNetwork;
extern bool schedulePartNetwork;

/////////////////////////////////////////////////////////////////////////////
// c_ResVectorCVRP
/////////////////////////////////////////////////////////////////////////////

c_ResVectorCVRPTW::c_ResVectorCVRPTW( const c_ResVectorCVRPTW& orig )
{
	// regular resources
	d_cost = orig.d_cost;
	i_load = orig.i_load;
	i_time = orig.i_time;
	i_dur = orig.i_dur;
	i_help = orig.i_help;
	l_visit = orig.l_visit; 
	bs_SubsetRowCutactive = orig.bs_SubsetRowCutactive; 
	
}

void c_ResVectorCVRPTW::OutputInStream(std::ostream& s) const
{
	s << "(" << Cost() << "," << Load() << "," << Time() << ")" << std::endl; 
}


/////////////////////////////////////////////////////////////////////////////
// c_REF_CVRPTW
/////////////////////////////////////////////////////////////////////////////


bool c_REF_CVRPTW::PropagateFw( const c_ResVectorCVRPTW_Fw& old_res, c_ResVectorCVRPTW_Fw& new_res ) const
{
	/*if( !IsInNetwork() )
	return false;*/
	if ( !IsActive() )
		return false;
	int idx = o_instance.NGIndex(i_tailNode, i_headNode);
	if ( idx >= 0 && (old_res.l_visit & (1<<idx)) && (i_tailNode != i_headNode) ) //man darf am selben Kunden Bedarfe weiterer Tage abdecken
		return false; 


	if( taskNetwork ){
		new_res.i_load = old_res.i_load + o_instance.DemandPerDay( i_headNode );
		if ( new_res.i_load > o_instance.Capacity() + CGBase::CG_EPS )
			return false;
		new_res.i_time = max( o_instance.StartTime( i_headNode ), old_res.i_time + o_instance.Time( i_tailNode, i_headNode ));
		if( new_res.i_time > o_instance.EndTime( i_headNode ) )
			return false;
	} else if( schedulePartNetwork ){
		new_res.i_load = old_res.i_load + o_instance.DemandSchedulePart( i_head );
		if ( new_res.i_load > o_instance.Capacity() + CGBase::CG_EPS )
			return false;
		new_res.i_time = max( o_instance.StartTime( i_headNode ), old_res.i_time + o_instance.Time( i_tailNode, i_headNode ) + o_instance.AdditionalServiceTimeSchedulePart(i_tail ));
		if( new_res.i_time > o_instance.EndTime( i_headNode ) )
			return false;
	}

	if( minTourDuration ){ //&& (i_tailNode != i_headNode) ){ //Attention: Assumes that there is no additional service time for an increased demand fulfillment
		new_res.i_dur = max( old_res.i_dur + o_instance.Time( i_tailNode, i_headNode ), old_res.i_help + o_instance.StartTime(i_headNode) );
		if( new_res.i_dur > o_instance.MaxTourDuration() )
			return false;
		new_res.i_help = max( old_res.i_dur + o_instance.Time( i_tailNode, i_headNode ) - o_instance.EndTime(i_headNode), old_res.i_help );
		if ( new_res.i_help < - o_instance.EndTime(o_instance.OrigDepot()))
			return false;
	} 
	// compute new resource values
	new_res.d_cost = old_res.d_cost+ o_instance.RDC( i_tail, i_head, i_day );

	//if SR-value changed to zero subtract dual price	
	new_res.bs_SubsetRowCutactive = old_res.bs_SubsetRowCutactive ^ bs_IncrementCutCounter_Fw ;	

	for (auto cutId = o_instance.getActiveCutIds().begin(); cutId != o_instance.getActiveCutIds().end();cutId++) {
		if( bs_IncrementCutCounter_Fw[*cutId] && !new_res.bs_SubsetRowCutactive[*cutId] ){ //Wenn Wechsel und Wert nun false
			new_res.d_cost -= o_instance.getSRIDual(*cutId);
		}
	}

	// no rdc>0 at enddepot!
	if ( i_headNode == o_instance.DestDepot() && new_res.d_cost > -0.00001 ) 
		return false;

	// elementarity, ng-path etc.
	new_res.l_visit = 0;
	int ng_size = o_instance.NGNeighborhoodSize(i_tailNode);
	for ( int idx=0; idx<ng_size; idx++ )
		if ( old_res.l_visit & (1<<idx) )
		{
			int node = o_instance.NGNeighbor( i_tailNode, idx );
			int idx_new = o_instance.NGIndex( i_headNode, node );
			if ( idx_new >= 0 )
		       new_res.l_visit |= (1<<idx_new);
		}
	new_res.l_visit |= (1<<0);
	return true;
}


bool c_REF_CVRPTW::PropagateBw(const c_ResVectorCVRPTW_Bw& old_res, c_ResVectorCVRPTW_Bw& new_res ) const
{

	//Branching and Network related
	if ( !IsActive() )
		return false;
	int idx = o_instance.NGIndex(i_headNode, i_tailNode);
	if ( idx>= 0 && (old_res.l_visit & (1<<idx)) && (i_tailNode != i_headNode) )
		return false; 

	// compute new resource values
	if( taskNetwork ){
		new_res.i_load = old_res.i_load + o_instance.DemandPerDay( i_tailNode );
		if ( new_res.i_load > o_instance.Capacity() + CGBase::CG_EPS )
			return false;
		new_res.i_time = min( o_instance.EndTime( i_tailNode ), old_res.i_time - o_instance.Time( i_tailNode, i_headNode ));
		if( new_res.i_time < o_instance.StartTime( i_tailNode ) )
			return false;
	} else if( schedulePartNetwork ){
		new_res.i_load = old_res.i_load + o_instance.DemandSchedulePart( i_tail );
		if ( new_res.i_load > o_instance.Capacity() + CGBase::CG_EPS )
			return false;
		new_res.i_time = min( o_instance.EndTime( i_tailNode ), old_res.i_time - o_instance.Time( i_tailNode, i_headNode ) - o_instance.AdditionalServiceTimeSchedulePart( i_tail ) );
		if( new_res.i_time < o_instance.StartTime( i_tailNode ) )
			return false;
	}
	if( minTourDuration ){ //&& (i_tailNode != i_headNode) ){ //Attention: Assumes that there is no additional service time for an increased demand fulfillment
		new_res.i_dur = min( old_res.i_dur - o_instance.Time( i_tailNode, i_headNode ), old_res.i_help - o_instance.Time( i_tailNode, i_headNode ) + o_instance.EndTime(i_headNode) );
		if( new_res.i_dur < 0 )
			return false;
		new_res.i_help = min( old_res.i_dur - o_instance.StartTime(i_headNode), old_res.i_help );
	}
	new_res.d_cost = old_res.d_cost + o_instance.RDC( i_tail, i_head, i_day );

	//Subset-row
	new_res.bs_SubsetRowCutactive = old_res.bs_SubsetRowCutactive ^ bs_IncrementCutCounter_Bw ;	

	for (auto cutId = o_instance.getActiveCutIds().begin(); cutId != o_instance.getActiveCutIds().end(); cutId++) {
		if( bs_IncrementCutCounter_Bw[*cutId] && !new_res.bs_SubsetRowCutactive[*cutId] ){
			new_res.d_cost -= o_instance.getSRIDual(*cutId);
		}
	}
	
	// no rdc>0 at enddepot!
	if ( i_tailNode == o_instance.OrigDepot() && new_res.d_cost > -0.00001 ) 
		return false;

	// elementarity, ng-path etc.
	new_res.l_visit = 0;
	int ng_size = o_instance.NGNeighborhoodSize(i_headNode);
	for ( int idx=0; idx<ng_size; idx++ )
		if ( old_res.l_visit & (1<<idx) )
		{
			int node = o_instance.NGNeighbor( i_headNode, idx );
			int idx_new = o_instance.NGIndex( i_tailNode, node );
			if ( idx_new >= 0 )
				new_res.l_visit |= (1<<idx_new);
		}
	new_res.l_visit |= (1<<0);
	return true;
}



bool c_LabelCVRPTW_Fw::operator<=(const c_LabelCVRPTW_Fw& second)
{
	//TODO: heuristic dominance by ignoring l_visit and/or Help?
	if  ( Time() > second.Time() || Load() > second.Load() + CGBase::CG_EPS ) 
		return false;
	if( minTourDuration ){
		if( Duration() > second.Duration() || (exactDom && Help() > second.Help()) )
			return false;
	}
	if(~second.l_visit & l_visit )
		return false;
	//TODO: Perhaps wrong. Visit refers to customers, not to vertices. Thus, two routes fulfilling different daily demands of the same customer are here considered to be comparable.
	//This is okay for standard instances (because only one schedule part starts at every day) but this should be false for irregular schedules. Therefore, the neighborhood has to consist of schedule parts. 
	//Probably it's indeed okay for all instances. The purpose of l_visit is only to ensure (Ng-)elementary and every customer can be visited only once. The difference of the customer visit is already expressed by other rdc and demand.

	double costForComparison = Cost();
	if( costForComparison > second.Cost() )
		return false;

	for (auto cutId = p_REF->o_instance.getActiveCutIds().begin(); cutId != p_REF->o_instance.getActiveCutIds().end(); cutId++) {
		if (bs_SubsetRowCutactive[*cutId] && !second.bs_SubsetRowCutactive[*cutId])
			costForComparison -= p_REF->o_instance.getSRIDual(*cutId);
	}
	return ( costForComparison <= second.Cost() ); // Label 1 ist besser als Label 2 trotz Bestrafung
}

bool c_LabelCVRPTW_Bw::operator<=(const c_LabelCVRPTW_Bw& second)
{	
	if  ( Time() < second.Time() || Load() > second.Load() + CGBase::CG_EPS )  //if  ( Load() > second.Load() ) //
		return false;	
	if( minTourDuration ){
		if( Duration() < second.Duration() || (exactDom && Help() < second.Help()) )
			return false;
	}
	if ( ~second.l_visit & l_visit )
		return false;
	double costForComparison = Cost();
	if( costForComparison > second.Cost() )
		return false;

	for (auto cutId = p_REF->o_instance.getActiveCutIds().begin(); cutId != p_REF->o_instance.getActiveCutIds().end(); cutId++) {
		if (bs_SubsetRowCutactive[*cutId] && !second.bs_SubsetRowCutactive[*cutId])
			costForComparison -= p_REF->o_instance.getSRIDual(*cutId);
	}
	return ( costForComparison <= second.Cost() ); 
}



bool c_LabelCVRPTW_Fw::containsRoute(const vector<int>& route, int& nextExpectedNode) const
{
	stringstream ss;
	ss << "Route of Label " << this->i_id << ": " << this->generator_REF()->Head() << " ";
	auto pos = route.begin();
	while( pos != route.end() ){

		pos = std::find(pos+1, route.end(), this->generator_REF()->Head());
		if(pos == route.end()){
			return false;
		}
		nextExpectedNode = *(pos+1);

		int i = 1;
		c_LabelCVRPTW_Fw actualLabel = *this;
		bool completeSubtourFound = true;
		ss.clear();
		while(actualLabel.predecessor()){ //predecessor von vorletztem Label einer Route ist letztes mit REF, von dieser wird ja schon Tail=Depot �berpr�ft
			if( !*(pos-i) && actualLabel.generator_REF()->Tail() != 0){
				completeSubtourFound = false;
				break;
			}
			if(actualLabel.generator_REF()->Tail() != *(pos-i)){
				completeSubtourFound = false;
				break;
			}
			ss << actualLabel.generator_REF()->Tail() << " ";
			actualLabel = *actualLabel.predecessor();
			i++;
		}
		if( completeSubtourFound ){
			ss << endl;
			cout << ss.str();
			return true;
		}
	}
	return false;
}

bool c_LabelCVRPTW_Bw::containsRoute(const vector<int>& route, int& nextExpectedNode) const
{
	stringstream ss;
	ss << "Route of Label " << this->i_id << ": " << this->generator_REF()->Tail() << " ";
	auto pos = route.begin();
	while( pos != route.end() ){
		pos = std::find(pos+1, route.end(), this->generator_REF()->Tail());
		if(pos == route.end()){
			return false;
		}

		nextExpectedNode = *(pos-1);
		int i = 1;
		c_LabelCVRPTW_Bw actualLabel = *this;
		bool completeSubtourFound = true;
		ss.clear();
		while(actualLabel.predecessor()){ //predecessor von vorletztem Label einer Route ist letztes mit REF, von dieser wird ja schon Head=Depot �berpr�ft

			if( !*(pos+i) ){
				completeSubtourFound = false;
				break;
			}
			if(actualLabel.generator_REF()->Head() != *(pos+i)){
				completeSubtourFound = false;
				break;
			}
			ss << actualLabel.generator_REF()->Head() << " ";
			actualLabel = *actualLabel.predecessor();
			i++;
		}
		if( completeSubtourFound ){
			ss << endl;
			cout << ss.str();
			return true;
		}
	} 
	return false;
}









//For Separation -> k-path,capacity cuts (not relevant atm)

/////////////////////////////////////////////////////////////////////////////
// c_ResVectorPVRPTW_Separation
/////////////////////////////////////////////////////////////////////////////

c_ResVectorPVRPTW_Separation::c_ResVectorPVRPTW_Separation(const c_ResVectorPVRPTW_Separation& orig)
{
	// regular resources
	i_load = orig.i_load;
	i_time = orig.i_time;
	bs_toVisit = orig.bs_toVisit;
}

void c_ResVectorPVRPTW_Separation::OutputInStream(std::ostream& s) const
{
	s << "(" << Load() << "," << Time() << ")" << std::endl;
}

/////////////////////////////////////////////////////////////////////////////
// c_REF_PVRPTW_Separation
/////////////////////////////////////////////////////////////////////////////

bool c_REF_PVRPTW_Separation::PropagateFw(const c_ResVectorPVRPTW_Separation_Fw& old_res, c_ResVectorPVRPTW_Separation_Fw& new_res) const
{
	if (!IsInNetwork())
		return false;
	if (i_head == o_instance.DestDepot() && old_res.bs_toVisit.any())
		return false;
	new_res.i_load = old_res.i_load + o_instance.DemandPerDay(i_head);
	if (new_res.i_load > o_instance.Capacity() + CGBase::CG_EPS )
		return false;
	new_res.i_time = max(o_instance.StartTime(i_head), old_res.i_time + o_instance.Time(i_tail, i_head));
	if (new_res.i_time > o_instance.EndTime(i_head))
		return false;

	new_res.bs_toVisit = old_res.bs_toVisit;
	new_res.bs_toVisit.set(i_head, false);

	return true;
}

bool c_REF_PVRPTW_Separation::PropagateBw(const c_ResVectorPVRPTW_Separation_Bw& old_res, c_ResVectorPVRPTW_Separation_Bw& new_res) const
{
	if (!IsInNetwork())
		return false;

	if (i_tail == o_instance.OrigDepot() && old_res.bs_toVisit.any())
		return false;

	new_res.i_load = old_res.i_load + o_instance.DemandPerDay(i_tail);
	if (new_res.i_load > o_instance.Capacity() + CGBase::CG_EPS)
		return false;
	new_res.i_time = min(o_instance.EndTime(i_tail), old_res.i_time - o_instance.Time(i_tail, i_head));
	if (new_res.i_time < o_instance.StartTime(i_tail))
		return false;

	new_res.bs_toVisit = old_res.bs_toVisit;
	new_res.bs_toVisit.set(i_tail, false);

	return true;
}

bool c_LabelPVRPTW_Separation_FW::operator<=(const c_LabelPVRPTW_Separation_FW& second)
{
	if (Time() > second.Time() || abs(Load() - second.Load()) > CGBase::CG_EPS )
		return false;
	if (second.bs_toVisit != bs_toVisit)
		return false;
	return true;
}

bool c_LabelPVRPTW_Separation_BW::operator<=(const c_LabelPVRPTW_Separation_BW& second)
{
	if (Time() < second.Time() || abs(Load() - second.Load()) > CGBase::CG_EPS )
		return false;
	if (second.bs_toVisit != bs_toVisit)
		return false;
	return true;
}