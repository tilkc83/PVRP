#include "data.h"


c_SchedulePart::c_SchedulePart(int id, int customer, int startDay, int endDay, int demand, int addServiceTime)
	: i_id( id ),
	i_customer( customer ),
	i_startDay ( startDay ),
	i_endDay ( endDay ),
	i_demand( demand ),
	i_additionalServiceTime( addServiceTime )
{}


