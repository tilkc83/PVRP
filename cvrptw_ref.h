#ifndef CVRP_REF_H
#define CVRP_REF_H

#include <bitset>
#include <vector>

#include "ESPPRC/ref.h"
#include "ESPPRC/label.h"
//#include "bcp_cvrptw.h"
#include "cvrptw_instance.h"

#include <algorithm> 

using namespace espprc;

//const int maxCutNumber = 100; // static version, just allow 300 at max
const int MaxSR = 300;
typedef std::bitset<MaxSR> sr_bitset;
//const int numNodes = 50;// TODO: 
//typedef std::bitset<numNodes> node_bitset;

extern bool minTourDuration;
extern bool exactDom;
extern bool ryanFosterActive;


//@Francesco: This file contains everything problem-specific for the labeling algorihm, especially the ressource vector for the labeling must be adapted (including its methods in the .cpp) and the methods PropagateFw and PropagateBw of the REF
class c_ResVectorCVRPTW
{
public: // not nice but effective
	double d_cost;
	double i_load;
	int i_time; //earliest possible visit time
	int i_dur; //minimum tour duration
	int i_help; //- latest possible departure time at depot
	long l_visit;
	sr_bitset bs_SubsetRowCutactive; 
	/*node_bitset bs_enforcedCusts;
	node_bitset bs_forbiddenCusts;*/

public:
	c_ResVectorCVRPTW() { d_cost=0.0; i_load=0; i_time=0; l_visit=0; i_dur=0; i_help = 0; }
	c_ResVectorCVRPTW( const c_ResVectorCVRPTW& orig );  
	~c_ResVectorCVRPTW( void ) {}    
	//setter
	void setHelp( int help){ i_help = help;}
	// getter
	double Cost() const {return d_cost;}
	double Load() const {return i_load;}
	int Time() const {return i_time;}
	int Duration() const {return i_dur;}
	int Help() const {return i_help;}
	int Bs_SRI(int cutId) const {return bs_SubsetRowCutactive[cutId];}
	// operators
	int operator()( void ) const { return i_time; }
	// IO
	void OutputInStream( std::ostream& s ) const;
};

class c_ResVectorCVRPTW_Fw : public c_ResVectorCVRPTW
{
public:
	c_ResVectorCVRPTW_Fw() { d_cost=0.0; i_load=0; i_time=0; l_visit=0; i_dur=0; i_help = 0; }
};

class c_ResVectorCVRPTW_Bw : public c_ResVectorCVRPTW
{
public:
	c_ResVectorCVRPTW_Bw() { d_cost=0.0; i_load=0; i_time= 10000; l_visit=0; i_dur=0; i_help = 0; }
	void setEndTime( int endTime ) { i_time = endTime; } 
	void setDuration( int dur){ i_dur = dur;}
};




class c_REF_CVRPTW : public c_REF<c_ResVectorCVRPTW_Fw,c_ResVectorCVRPTW_Bw> 
{
protected:
	bool b_active; //by branching
	int i_day;
	sr_bitset bs_IncrementCutCounter_Fw;
	sr_bitset bs_IncrementCutCounter_Bw;
	int i_tailNode; //tail is vertex, so copy of node per day. Info is stored per node.
	int i_headNode; //head is vertex, so copy of node per day. Info is stored per node.
	bool b_inNetwork;
public:
	c_CVRPTW_Instance_With_RDC& o_instance;
public:
	c_REF_CVRPTW( int tail, int head, int day, c_CVRPTW_Instance_With_RDC& instance )
	:	c_REF<c_ResVectorCVRPTW_Fw,c_ResVectorCVRPTW_Bw>( tail, head ),
		i_day(day),
		o_instance( instance ),
		b_active(true)
	{
		i_tailNode = o_instance.GetNode(tail);
		i_headNode = o_instance.GetNode(head);
	}

	virtual bool PropagateFw( const c_ResVectorCVRPTW_Fw& old_res, c_ResVectorCVRPTW_Fw& new_res ) const;
	virtual bool PropagateBw( const c_ResVectorCVRPTW_Bw& old_res, c_ResVectorCVRPTW_Bw& new_res ) const;

	void Activate( bool val ) { b_active=val; }
	bool IsActive() const { return b_active; }

	const int Day() const {return i_day;}

	/*void SetInNetwork(bool val) { b_inNetwork = val; }
	bool IsInNetwork() const { return b_inNetwork; }*/

	void SetCutIncrement_Fw( int cutId ) { bs_IncrementCutCounter_Fw[cutId] = true; }
	void ResetIncrementCutCounter_Fw() { bs_IncrementCutCounter_Fw.reset(); }

	void SetCutIncrement_Bw( int cutId ) { bs_IncrementCutCounter_Bw[cutId] = true; }
	void ResetIncrementCutCounter_Bw() { bs_IncrementCutCounter_Bw.reset(); }
};

class c_LabelCVRPTW_Fw : public c_Label<c_ResVectorCVRPTW_Fw,c_REF_CVRPTW>
{
public:
	c_LabelCVRPTW_Fw( int id, c_ResVectorCVRPTW_Fw&& res_vector, const tLabel* pred, const c_REF_CVRPTW* ref)
		:	c_Label<c_ResVectorCVRPTW_Fw,c_REF_CVRPTW>( id, std::move(res_vector), pred, ref ){};
	~c_LabelCVRPTW_Fw(){};

	bool operator<=( const c_LabelCVRPTW_Fw& second );

	const c_LabelCVRPTW_Fw* predecessor() const { return (c_LabelCVRPTW_Fw*) p_pred; }
	c_LabelCVRPTW_Fw* next() { return (c_LabelCVRPTW_Fw*) p_next; }

	bool containsRoute(const vector<int>& route, int& nextExpectedNode) const;
};

class c_LabelCVRPTW_Bw : public c_Label<c_ResVectorCVRPTW_Bw,c_REF_CVRPTW>
{
public:
	c_LabelCVRPTW_Bw( int id, c_ResVectorCVRPTW_Bw&& res_vector, const tLabel* pred, const c_REF_CVRPTW* ref)
		:	c_Label<c_ResVectorCVRPTW_Bw,c_REF_CVRPTW>( id, std::move(res_vector), pred, ref ){};
	~c_LabelCVRPTW_Bw(){};

	bool operator<=( const c_LabelCVRPTW_Bw& second );

	const c_LabelCVRPTW_Bw* predecessor() const { return (c_LabelCVRPTW_Bw*) p_pred; }
	c_LabelCVRPTW_Bw* next() { return (c_LabelCVRPTW_Bw*) p_next; }

	bool containsRoute(const vector<int>& route, int& nextExpectedNode) const;
};







//For Separation -> k-path,capacity cuts (not relevant atm)
class c_ResVectorPVRPTW_Separation
{
public: // not nice but effective
	int i_load;
	int i_time;
	customer_bitset bs_toVisit;
public:
	c_ResVectorPVRPTW_Separation() : i_load(0), i_time(0) {}
	c_ResVectorPVRPTW_Separation(customer_bitset toVisit) : i_load(0), i_time(0), bs_toVisit(toVisit) {}
	c_ResVectorPVRPTW_Separation(const c_ResVectorPVRPTW_Separation& orig);
	~c_ResVectorPVRPTW_Separation(void) {}
	// getter
	int Load() const { return i_load; }
	int Time() const { return i_time; }
	// operators
	int operator()(void) const { return i_time; }
	// IO
	void OutputInStream(std::ostream& s) const;
};

class c_ResVectorPVRPTW_Separation_Fw : public c_ResVectorPVRPTW_Separation
{
public:
	c_ResVectorPVRPTW_Separation_Fw(){}
	c_ResVectorPVRPTW_Separation_Fw(customer_bitset toVisit) : c_ResVectorPVRPTW_Separation(toVisit){}
};

class c_ResVectorPVRPTW_Separation_Bw : public c_ResVectorPVRPTW_Separation
{
public:
	c_ResVectorPVRPTW_Separation_Bw(){}
	c_ResVectorPVRPTW_Separation_Bw(customer_bitset toVisit) : c_ResVectorPVRPTW_Separation(toVisit){}
	void setEndTime(int endTime) { i_time = endTime; }
};

class c_REF_PVRPTW_Separation : public c_REF<c_ResVectorPVRPTW_Separation_Fw, c_ResVectorPVRPTW_Separation_Bw>
{
protected:
	bool b_inNetwork; //by reduced Network
public:
	c_CVRPTW_Instance_With_RDC& o_instance;
public:
	c_REF_PVRPTW_Separation(int tail, int head, c_CVRPTW_Instance_With_RDC& instance)
		: c_REF<c_ResVectorPVRPTW_Separation_Fw, c_ResVectorPVRPTW_Separation_Bw>(tail, head),
		o_instance(instance),
		b_inNetwork(true)
	{}

	virtual bool PropagateFw(const c_ResVectorPVRPTW_Separation_Fw& old_res, c_ResVectorPVRPTW_Separation_Fw& new_res) const;
	virtual bool PropagateBw(const c_ResVectorPVRPTW_Separation_Bw& old_res, c_ResVectorPVRPTW_Separation_Bw& new_res) const;

	void SetInNetwork(bool val) { b_inNetwork = val; }
	bool IsInNetwork() const { return b_inNetwork; }
};

class c_LabelPVRPTW_Separation_FW : public c_Label<c_ResVectorPVRPTW_Separation_Fw, c_REF_PVRPTW_Separation>
{
public:
	//bool deleted;
	c_LabelPVRPTW_Separation_FW(int id, c_ResVectorPVRPTW_Separation_Fw&& res_vector, const tLabel* pred, const c_REF_PVRPTW_Separation* ref)
		: c_Label<c_ResVectorPVRPTW_Separation_Fw, c_REF_PVRPTW_Separation>(id, std::move(res_vector), pred, ref)/*, deleted (false)*/{};
	~c_LabelPVRPTW_Separation_FW(){};

	bool operator<=(const c_LabelPVRPTW_Separation_FW& second);

	const c_LabelPVRPTW_Separation_FW* predecessor() const { return (c_LabelPVRPTW_Separation_FW*)p_pred; }
	c_LabelPVRPTW_Separation_FW* next() { return (c_LabelPVRPTW_Separation_FW*)p_next; }
};

class c_LabelPVRPTW_Separation_BW : public c_Label<c_ResVectorPVRPTW_Separation_Bw, c_REF_PVRPTW_Separation>
{
public:
	c_LabelPVRPTW_Separation_BW(int id, c_ResVectorPVRPTW_Separation_Bw&& res_vector, const tLabel* pred, const c_REF_PVRPTW_Separation* ref)
		: c_Label<c_ResVectorPVRPTW_Separation_Bw, c_REF_PVRPTW_Separation>(id, std::move(res_vector), pred, ref){};
	~c_LabelPVRPTW_Separation_BW(){};

	bool operator<=(const c_LabelPVRPTW_Separation_BW& second);

	const c_LabelPVRPTW_Separation_BW* predecessor() const { return (c_LabelPVRPTW_Separation_BW*)p_pred; }
	c_LabelPVRPTW_Separation_BW* next() { return (c_LabelPVRPTW_Separation_BW*)p_next; }
};

#endif // of CVRP_REF_H





