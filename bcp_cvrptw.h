#ifndef BAP_CVRP_h
#define BAP_CVRP_h

#include "ColumnGeneration/base.h"

#include "ESPPRC/extension_mgnt.h"
#include "ESPPRC/dominance_mgnt.h"
#include "ESPPRC/label.h"
#include "ESPPRC/solver.h"

#include "cvrptw_instance.h"
#include "cvrptw_ref.h"

#include "SharedFiles/matrix.h"
#include "SharedFiles/output_formatter.h"

//#include "KPathCutSeparator.h"
//#include "2PathCutSeparator.h"

#include <assert.h> //For testing

using namespace CGBase;

extern bool taskNetwork;
extern bool schedulePartNetwork;

class c_PricingSolverHierarchyCVRPTW;

//@Francesco: This file contains everything problem specific for the column generation algorithm (but not the labeling for the pricing problem). At first we probably do not need to touch it.

class c_ControllerCVRPTW : public c_Controller, public c_CVRPTW_Instance_With_RDC {
	c_Matrix<c_REF_CVRPTW*> m_REFs;
	int i_numSubsetRowCuts;
	int i_numCapacityCuts;
	int i_numArcSetCuts;
	int i_numNgIncreasements;
	int counterCutGeneration;
	int fixedCustomer;
	bool b_daySpecificBranching;
	int cnt_extendedFWLabels;
	int cnt_extendedBWLabels;
	int i_numAddedColumns;
	bool b_aggregated;
	double originalUB;
	int i_startHierarchyId;

	std::vector<c_PricingSolverHierarchyCVRPTW*> v_hierarchy_vec; //Work_around for ankas code in new framework

	vector<int> pricing_levels_in_hierarchies;
	vector<int> v_numFailures_hierarchy;
public:
	c_ControllerCVRPTW( string instanceFile, string scheduleFile, std::string settings_file = "settings/settings.txt" );
	virtual ~c_ControllerCVRPTW();
	// Getter
	c_REF_CVRPTW* REF(int tail, int head, int day) { return m_REFs(tail,head, day); }
	void ActivateREF( int i, int j, int day, bool flag=true );
	// these methods can be modified
	virtual void Go( void );
	virtual void WriteSolutionInFile( string FileName );	
	virtual void Pricing(int iteration, c_DualVariables* dual, list<c_Column*>& col_list, int NumPricersUsed = MaxNumPricers);
	// these methods must be provided by derived class    
	virtual c_RMP* CreateNewRMP();
	virtual c_BranchAndBoundNode* CreateRootNode();
	virtual c_DualVariables* CreateDualVariables();
	virtual c_Solution* CreateSolution( c_RMP*, c_DualVariables* );
	virtual void AddPricingProblems();
	virtual void AddSeparationProblems();
	
	void createREFs();
	int NumSubsetRowCuts() {return i_numSubsetRowCuts;}
	void IncreaseNumSubsetRowCuts(){i_numSubsetRowCuts++;}
	void ResetNumSubsetRowCuts(){i_numSubsetRowCuts = 0;}

	int NumNgIncreasements() {return i_numNgIncreasements;}
	void IncreaseNumNgIncreasements(){i_numNgIncreasements++;}

	int NumCapacityCuts() {return i_numCapacityCuts;}
	void IncreaseNumCapacityCuts(){i_numCapacityCuts++;}

	int NumArcSetConstraints() {return i_numArcSetCuts;}
	void IncreaseNumArcSetConstraints(){i_numArcSetCuts++;}

	void IncreaseCutGenerationCounter(){counterCutGeneration++;}
	void IncreaseColumnCounter(){ i_numAddedColumns++; }
	int NumAddedColumns(){ return i_numAddedColumns;}

	long long GetNumFWLabels() const { return cnt_extendedFWLabels;}
	void AddToExtendedFW(int numFwLabels ){ cnt_extendedFWLabels += numFwLabels;}
	long long GetNumBWLabels() const { return cnt_extendedBWLabels;}
	void AddToExtendedBW(int numBwLabels ){ cnt_extendedBWLabels += numBwLabels;}

	double calculateCostOfRoute( const vector<int>& route );
	int getNumArcs();
	void BreakSymmetryByScheduleSelection();

	bool DaySpecificBranching() {return b_daySpecificBranching;}
	void SetDaySpecificBranching(bool b){ b_daySpecificBranching = b;}
	int GetFixedCustomer() { return fixedCustomer;}
	void SetFixedCustomer(int cust) { fixedCustomer = cust;}
	vector<int> SwitchRouteToOtherDay(const vector<int>& tour, int numDaysToShift);
	vector<int> SwitchSubsetToOtherDay(const vector<int>& tour, int numDaysToShift);
	int GetSchedulePartShiftedToFirstPeriod(int spId);

	bool IsAggregated() { return b_aggregated;}
	void SetAggregation( bool b ) { b_aggregated = b;}
	bool Disaggregate();

	bool ParameterCombinationFeasible();
	bool SolveMIPWithOriginalMP();
	void StoreUB(){	originalUB = UB();}
	double GetStoredUB(){ return originalUB;}

	bool GreedyRepair();
	bool IsRouteFeasible(vector<int>& route, int day);

	// Parameter
	c_DoubleParameter BranchVal;
	c_IntParameter NgNeighborhoodSize;
	c_BoolParameter UseDynamicNgNeighborhood;
	c_IntParameter DynamicNgNeighborhoodUB;
	c_IntListParameter HeuristicPricersNetworkSizes;
	c_IntParameter Solver;
	c_BoolParameter UseSubsetRowCuts;
	c_DoubleParameter CutTolerance; 
	c_IntParameter MaxNumberSRCutsPerIteration; 
	c_IntParameter MaxSRCutsPerCustomerPerIteration;
	c_IntParameter MaxCutNodeLevel;
	c_BoolParameter NetworkOfTasks;
	c_BoolParameter NetworkOfScheduleParts;
	c_BoolParameter RegularVisits;
	c_BoolParameter MinimumTourDuration;
	c_BoolParameter UseCapacityCuts;
	c_IntParameter MaxNumberCapCutsPerIteration;
	c_BoolParameter UseTwoPathCuts;
	c_IntParameter KPathMaxSizeOfSets;	
	c_IntParameter KPathMaxInclusion;
	c_IntParameter KPathRestarts;
	c_BoolParameter AddColumnsOnOtherDays;
	c_BoolParameter BreakSymmetry;
	c_BoolParameter StartWithAggregation;
	c_BoolParameter SeparateAggregatedSRCuts;
	c_BoolParameter UseHeuristicDominance;
	c_BoolParameter UseRyanFosterBranching;
	c_BoolParameter UseMIPSolverAtTheEnd;
	c_IntParameter BranchingCustomerDayCandidates;
	c_BoolParameter RestrictVerticesWithDominanceCheck;
	c_IntParameter BranchingTieBreakerRule;

	c_BoolParameter PricingHierarchyStandardOrder;

	int GetStartHierarchy() { return i_startHierarchyId; }
	void SetStartHierarchy(int hierarchyId) { i_startHierarchyId = hierarchyId; }

	int GetNumFailures(int pricingLevel) { return v_numFailures_hierarchy[pricingLevel]; }
	void IncreaseNumFailures(int pricingLevel) { v_numFailures_hierarchy[pricingLevel]++; }
	void ResetNumFailures() {
		for (int i = 0; i < v_numFailures_hierarchy.size(); i++)
			v_numFailures_hierarchy[i] = 0;
	}

	// setter to overwrite pricing method
	void Set_number_of_solved_pricing_problems(int new_number) { info_number_of_solved_pricing_problems = new_number; }
	void Set_number_of_generated_columns(int new_number) { info_number_of_generated_columns = new_number; }
	void Set_number_of_rmp_iterations(int new_number) { info_number_of_rmp_iterations = new_number; }
	void Set_timer_pricing_update(double new_number) { info_timer_PricingUpdate = new_number; }

	c_IntParameter MaxNumSubsetRow;
	c_BoolParameter BranchVehicleFirst;

	
};


class c_BaseColumnCVRPTW : public c_Column {
protected:
	double d_cost;
public:
	c_BaseColumnCVRPTW( c_Controller* controller, double cost ) : c_Column( controller ), d_cost (cost) {}
	virtual ~c_BaseColumnCVRPTW() {};

	typedef enum type { eDUMMY, eTOUR, eNUM_VEH, eTOTAL_NUM_VEH, eSCHEDULE };
	// getter/setter
	virtual double c() const {return d_cost;}
	double GetCPXObj()	{ return c(); }
	// these methods must be provided by derived class
	virtual type Type() const = 0; 
};


class c_DummyColumnCVRPTW : public c_BaseColumnCVRPTW {
	int i_node;
public:
	c_DummyColumnCVRPTW( c_Controller* controller, int node );
	virtual ~c_DummyColumnCVRPTW() {}

	virtual double c() const { return d_cost; }
	const int Node() const {return i_node;}
	virtual double DefaultLowerBound() { return	0.0; }
	virtual double DefaultUpperBound() { return	CPX_INFBOUND; }
	// these methods must be provided by derived class
	virtual type Type() const { return eDUMMY; }
	virtual bool IsTransferableToPool() const { return false; }
	virtual int GetCPXColumn( double& cost, int* ind, double* val, double& lb, double& ub, char* name );
	virtual void OutputInOStream( ostream& s ) const; 
};


class c_NumVehicleColumnCVRPTW : public c_BaseColumnCVRPTW{
	int i_MaxNumVehicles;
	int i_day;
public:
	c_NumVehicleColumnCVRPTW( c_Controller* controller, int MaxNumVehicles, int day );
	virtual ~c_NumVehicleColumnCVRPTW() {}

	const int Day() const { return i_day; }
	virtual double c() const { return d_cost; }
	virtual double DefaultLowerBound() { return	0.0; }
	virtual double DefaultUpperBound();// { return ((c_ControllerCVRPTW*)Controller())->IsAggregated()? (i_MaxNumVehicles * ((c_ControllerCVRPTW*)Controller())->PlanningHorizon()) : i_MaxNumVehicles; } //TODO: Accepted in model?
	// these methods must be provided by derived class
	virtual type Type() const { return eNUM_VEH; }
	virtual bool IsTransferableToPool() const { return false; }
	virtual int GetCPXColumn( double& cost, int* ind, double* val, double& lb, double& ub, char* name );
	virtual void OutputInOStream( ostream& s ) const; 
};

class c_TotalNumVehicleColumnCVRPTW : public c_BaseColumnCVRPTW{
	int i_MaxNumVehicles;
public:
	c_TotalNumVehicleColumnCVRPTW( c_Controller* controller, int MaxNumVehicles);
	virtual ~c_TotalNumVehicleColumnCVRPTW() {}

	virtual double c() const { return d_cost; }
	virtual double DefaultLowerBound() { return	0.0; }
	virtual double DefaultUpperBound() { return i_MaxNumVehicles; }
	// these methods must be provided by derived class
	virtual type Type() const { return eTOTAL_NUM_VEH; }
	virtual bool IsTransferableToPool() const { return false; }
	virtual int GetCPXColumn( double& cost, int* ind, double* val, double& lb, double& ub, char* name );
	virtual void OutputInOStream( ostream& s ) const; 
};


class c_TourColumnCVRPTW : public c_BaseColumnCVRPTW {
	std::vector<int> v_route;
	//std::vector<c_REF_CVRPTW*> v_REFs; //sequence of refs
	int i_day;
	int i_duration;
	vector<int> v_routeOfSchedulePartIds; 
	vector<int> v_routeOfTasks;
	vector<int> v_routeNodes;
public:
	c_TourColumnCVRPTW( c_Controller* controller, const std::vector<int>& route, double cost, int day, int duration = 0, bool CustomersInsteadOfVertices= false );
	virtual ~c_TourColumnCVRPTW() {}

	const std::vector<int>& Route() const { return v_route; }
	const std::vector<int>& RouteOfScheduleParts() const { return v_routeOfSchedulePartIds; }
	const std::vector<int>& RouteOfTasks() const { return v_routeOfTasks; }
	const std::vector<int>& RouteOfNodes() const { return v_routeNodes; }
	const int Day() const { return i_day; }
	const int Duration() const { return i_duration;}
	bool visits(int customer);
	bool IsFeasible() const;
	virtual double DefaultLowerBound() { return	0.0; }
	virtual double DefaultUpperBound() { return	CPX_INFBOUND; }
	// these methods must be provided by derived class
	virtual type Type() const { return eTOUR; }
	virtual bool IsTransferableToPool() const { return true; }
	virtual int GetCPXColumn( double& cost, int* ind, double* val, double& lb, double& ub, char* name );
	virtual void OutputInOStream( ostream& s ) const;
	int countVisits(int task);
};


class c_ScheduleColumnCVRPTW : public c_BaseColumnCVRPTW {
	int customer;
	int sId;
	vector<bool> v_days;
public:
	c_ScheduleColumnCVRPTW( c_Controller* controller, int customer, int scheduleId, const std::vector<bool>& days );
	virtual ~c_ScheduleColumnCVRPTW() {}

	const int Customer() const { return customer; }
	const std::vector<bool>& Schedule() const { return v_days; }
	virtual double DefaultLowerBound() { return	0.0; }
	virtual double DefaultUpperBound() { return	CPX_INFBOUND; }
	// these methods must be provided by derived class
	virtual type Type() const { return eSCHEDULE; }
	virtual bool IsTransferableToPool() const { return ((c_ControllerCVRPTW*)Controller())->IsAggregated()? true : false; }
	virtual int GetCPXColumn( double& cost, int* ind, double* val, double& lb, double& ub, char* name );
	virtual void OutputInOStream( ostream& s ) const;
};


class c_DualVariablesCVRPTW : public c_DualVariables
{
public:
	c_DualVariablesCVRPTW( c_Controller* controller );
	virtual ~c_DualVariablesCVRPTW() {};

	double pi(int node) const; // covering of schedule
	double rho(int spId) const; // linking
	double tau(int node) const; // aggregated constraint: ensures required frequency
	double mu(int day) const; // convexity constraint 
	double sigma(int cutId) const; //Dualvariable SRI-Cuts 
	double phi(int cutId) const; //Dualvariable Capacity-Cuts 
	double alpha( int cutId ) const; //Dualvariable ArcSet-Cut (2Path)
	virtual void OutputInOStream( ostream& s ) const; 
};


class c_SupportCVRPTW {
	c_Controller* _controller;
	c_DoubleMatrix m_flow;
	c_DoubleMatrix m_vertexFlow;
	c_DoubleMatrix m_flowPerDay;
	c_DoubleMatrix m_flowPerDayFor2PathCut;
	c_DoubleMatrix m_together;
	bool b_is_updated;
	bool b_is_feasible;
	bool b_is_integral;	
	vector<pair<vector<int>, int> > v_solution;
	vector<vector<bool> > v_schedules;
	c_DoubleMatrix m_schedules;
protected:
	void SetUpdateRequired() { b_is_updated = false; }
public:
	c_SupportCVRPTW( c_Controller* controller );
	~c_SupportCVRPTW() {};

	c_Controller* Controller() const { return _controller; }
	void GraphMLOutput( const char* filename ) const;
	// Getter
	bool IsFeasible() const;
	bool IsIntegral() const;
	const double Flow( int tail, int head ) const;
	const double FlowPerDay( int tail, int head, int day ) const;
	const double FlowPerDayFor2PathCut( int tail, int head, int day ) const;
	const double VertexFlow( int tailVertex, int headVertex ) const {return m_vertexFlow(tailVertex, headVertex);}
	const double Together(int node1, int node2) { return m_together(node1, node2);}
	const double NumVehicles() const;
	const double NumVehicles(int day) const;
	const double NumSchedules(int customer, int day) const;
	double Solution( vector<pair<vector<int>,int> >& solution, vector<double>& costs, vector<vector<bool> > & schedules ) const;
	// Update
	void UpdateFlows();
	void UpdateFlows() const;
};


class c_RMP_CVRPTW : public c_RMP, public c_SupportCVRPTW {
	c_Controller* _controller;
public:
	c_RMP_CVRPTW( c_Controller* controller );
	virtual ~c_RMP_CVRPTW() {};

	typedef enum { eCOVERING=0, eLINKING = 1, eCONVEXITY=2, eNUMVEH = 3, eSubsetRowCut=4, eCapacityCut=5, eARCSET=6, eVisitFrequency = 7, eNgIncreasementCut = 8, eAggregatedSubsetRowCut=9 };
	// these methods can be modified
	virtual double RoundLBTo( double lb ) { return lb; } //No rounding for costs in double precision
	virtual void Optimize( e_solver solver );
	virtual void OptimizeMIP( e_solver solver );
	// these methods must be provided by derived class
	virtual void InitializeFirstRMP( );
	virtual void OutputInOStream( ostream& s ) const;
	virtual	bool IsIntegral() const;
	virtual	bool IsFeasible() const;

	//virtual void Update( c_BranchAndBoundNode* node );
	void RemoveAggregatedConstraints(list<c_BranchAndBoundConstraint*>& constraint_list);
};

class c_SolutionCVRPTW : public c_Solution {
	vector<pair<vector<int>, int> > v_solution;
	vector<vector<bool>> v_schedules;
	vector<double> v_costs;
public:
	c_SolutionCVRPTW( c_Controller*, c_RMP*, c_DualVariables* );
	virtual ~c_SolutionCVRPTW() {}

	double VehicleNumber() const;
	double Cost() const;
	double getCost( int i ) const { return v_costs[i];}
	const vector<pair<vector<int>,int> >& getRoutes() const { return v_solution;} 
	const vector<vector<bool>>& getSchedules() const { return v_schedules;}

	// these methods can be modified 
	virtual void OutputInOStream( ostream& s ) const; 
};


class c_BranchAndBoundNodeCVRPTW : public c_BranchAndBoundNode {
public:
	c_BranchAndBoundNodeCVRPTW( c_Controller* controller );
	c_BranchAndBoundNodeCVRPTW( c_BranchAndBoundNode* p_father, list<c_BranchAndBoundConstraint*>& add_constraints );
	virtual ~c_BranchAndBoundNodeCVRPTW() {};

	// these methods can be modified
	// these methods must be provided by derived class
	virtual c_BranchAndBoundNode* CreateNewNode( list<c_BranchAndBoundConstraint*>& new_constraint_list ); 
	virtual void GetBranchAndBoundConstraints( list< list<c_BranchAndBoundConstraint*> >& con_list );
	virtual void Get_k_th_best_BranchAndBoundContraints( int k, list< list<c_BranchAndBoundConstraint*> >& con_list );

	virtual void GetConstraints( list<c_BranchAndBoundConstraint*>& ) const; 
};



class c_Constraint_PVRPTW : public c_Constraint
{
	int i_type;
public:
	c_Constraint_PVRPTW(c_Controller* controller, char sense, double rhs, double estimator_dual, int con_type, int n1=0, int n2=0, int n3=0 );
	virtual ~c_Constraint_PVRPTW() {};
	virtual void OutputInOStream(ostream& s) const; // = 0;
	//virtual int Type() = 0;
	
};


class c_GlobalValidConstraint_PVRPTW : public c_Constraint_PVRPTW{
public: 
	c_GlobalValidConstraint_PVRPTW(c_Controller* controller, char sense, double rhs, double estimator_dual, int type, int n1=0, int n2=0, int n3=0 )
		: c_Constraint_PVRPTW( controller, sense, rhs, estimator_dual, type, n1, n2, n3) {};
	virtual ~c_GlobalValidConstraint_PVRPTW() {};
	virtual bool IsGloballyValid() const { return true;}
};



class c_BranchAndBoundConstraintCVRPTW : public c_BranchAndBoundConstraint {
public:
	c_BranchAndBoundConstraintCVRPTW( c_Controller* controller, list<c_Constraint*>& rmp_constraints )
		: c_BranchAndBoundConstraint( controller, rmp_constraints ) {};

	typedef enum type { eArc, eNumVehicles, eTotalNumVehicles, eCustomerDay, eCustomerPairDay, eBreakSymmetry, eCustsTogether };
	// these methods must be provided by derived class
	virtual type Type() const = 0;
	virtual void Serialize( ostream& ) {}
	virtual void Serialize( istream& ) {}
};


class c_BranchAndBoundConstraintBreakSymmetry: public c_BranchAndBoundConstraintCVRPTW{
	int i_cust;
public:
	c_BranchAndBoundConstraintBreakSymmetry( c_Controller* controller, int cust);

	virtual type Type() const { return eBreakSymmetry; }
	int Customer() const{ return i_cust; }

	virtual void ComputeBounds( c_Column* col, double& lb, double& ub );
	virtual void OutputInOStream( ostream& s ) const; 
	virtual int GnuPlotLineStyle() { return 0; }
	virtual string TextInBranching() const ;
};


class c_BranchAndBoundConstraintTotalNumVehicles: public c_BranchAndBoundConstraintCVRPTW{
	int i_number;
	bool b_greater_eq;
public:
	c_BranchAndBoundConstraintTotalNumVehicles( c_Controller* controller, int quantity, bool bigger);

	virtual type Type() const { return eTotalNumVehicles; }
	int Number() const{ return i_number; }
	bool GreaterEq() const{ return b_greater_eq; }

	virtual void ComputeBounds( c_Column* col, double& lb, double& ub );
	virtual void OutputInOStream( ostream& s ) const; 
	virtual int GnuPlotLineStyle() { return 1; }
	virtual string TextInBranching() const ;
};

class c_BranchAndBoundConstraintNumVehicles: public c_BranchAndBoundConstraintCVRPTW{
	int i_number;
	bool b_greater_eq;
	int i_day;
public:
	c_BranchAndBoundConstraintNumVehicles( c_Controller* controller, int quantity, bool bigger, int day);

	virtual type Type() const { return eNumVehicles; }
	int Number() const{return i_number;}
	bool GreaterEq() const{return b_greater_eq;}
	const int Day() const {return i_day;}

	virtual void ComputeBounds( c_Column* col, double& lb, double& ub );
	virtual void OutputInOStream( ostream& s ) const; 
	virtual int GnuPlotLineStyle() { return 2; }
	virtual string TextInBranching() const ;
};


class c_BranchAndBoundConstraintCustsTogether: public c_BranchAndBoundConstraintCVRPTW{
	int i_cust1;
	int i_cust2;
	bool b_together;
public:
	c_BranchAndBoundConstraintCustsTogether( c_Controller* controller, int customer1, int customer2, bool together);

	virtual type Type() const { return eCustsTogether; }
	const int Customer1() const {return i_cust1;}
	const int Customer2() const {return i_cust2;}
	const bool Together() const {return b_together;}

	virtual void ComputeBounds( c_Column* col, double& lb, double& ub );
	virtual void OutputInOStream( ostream& s ) const; 
	virtual int GnuPlotLineStyle() { return 3; }
	virtual string TextInBranching() const ;
};

class c_BranchAndBoundConstraintCustomerDay: public c_BranchAndBoundConstraintCVRPTW{
	bool b_active;
	int i_customer;
	int i_day;
public:
	c_BranchAndBoundConstraintCustomerDay( c_Controller* controller, int customer, int day, bool active);

	virtual type Type() const { return eCustomerDay; }
	const int Customer() const {return i_customer;}
	const int Day() const {return i_day;}
	const bool Active() const {return b_active;}

	virtual void ComputeBounds( c_Column* col, double& lb, double& ub );
	virtual void OutputInOStream( ostream& s ) const; 
	virtual int GnuPlotLineStyle() { return 3; }
	virtual string TextInBranching() const ;
};

class c_BranchAndBoundConstraintNodeArc : public c_BranchAndBoundConstraintCVRPTW{
	int i_tail;
	int i_head;
	int i_day;
	bool b_enforced;
public:
	c_BranchAndBoundConstraintNodeArc(c_Controller* controller, int tail, int head, int day, bool val);

	virtual type Type() const { return eCustomerPairDay; }
	int Tail() const {return i_tail;}
	int Head() const{return i_head;}
	int Day() const{return i_day;}
	bool Enforced() const{return b_enforced;}

	virtual void ComputeBounds( c_Column* col, double& lb, double& ub );
	virtual void OutputInOStream( ostream& s ) const; 
	virtual int GnuPlotLineStyle() { return 4; }
	virtual string TextInBranching() const;
};

class c_BranchAndBoundConstraintArc : public c_BranchAndBoundConstraintCVRPTW{
	int i_tail;
	int i_head;
	bool b_enforced;
public:
	c_BranchAndBoundConstraintArc(c_Controller* controller, int tail, int head, bool val);

	virtual type Type() const { return eArc; }
	int Tail() const {return i_tail;}
	int Head() const{return i_head;}
	bool Enforced() const{return b_enforced;}

	virtual void ComputeBounds( c_Column* col, double& lb, double& ub );
	virtual void OutputInOStream( ostream& s ) const; 
	virtual int GnuPlotLineStyle() { return 5; }
	virtual string TextInBranching() const;
};

//typedef c_Bucket_Manager_UsingVector<c_LabelCVRPTW_Fw , true> BManagerFW;
typedef c_Extension_Manager<c_LabelCVRPTW_Fw, true> ExtManagerFW;
typedef c_Dominance_Manager<c_LabelCVRPTW_Fw> DManagerFW;
//typedef c_Bucket_Manager_UsingVector<c_LabelCVRPTW_Bw, false> BManagerBW;
typedef c_Extension_Manager<c_LabelCVRPTW_Bw, false> ExtManagerBW;
typedef c_Dominance_Manager<c_LabelCVRPTW_Bw> DManagerBW;

class c_MergeManagerCVRPTW {
	int i_capacity;
	int i_startTime;
	int i_endTime;
	int i_halfway_point;
	int i_halfway_point_fw;
	int i_halfway_point_bw;
	c_ControllerCVRPTW* _controller;
public:
	typedef c_LabelCVRPTW_Fw LabelFW;
	typedef c_LabelCVRPTW_Bw LabelBW;
public:
	c_MergeManagerCVRPTW( int capacity, int startTime, int endTime, c_ControllerCVRPTW* ctlr, int halfway_point = -1 );
	bool MergeableFW( const LabelFW& label_fw ) const;
	bool MergeableBW( const LabelBW& label_bw ) const;
	bool Mergeable( const LabelFW& label_fw, const LabelBW& label_bw, double& rdc ) const;
	bool BeforeHalfWayPointFW( const LabelFW& label_fw ) const;
	bool BeforeHalfWayPointBW( const LabelBW& label_bw ) const;
	// dynamic half-way point
	int HalfwayPoint() const { return i_halfway_point; }
	int HalfwayPointFW() const { return i_halfway_point_fw; }
	int HalfwayPointBW() const { return i_halfway_point_bw; }
	void SetHalfwayPointFW( int value ) { i_halfway_point = i_halfway_point_fw = value; }
	void SetHalfwayPointBW( int value ) { i_halfway_point = i_halfway_point_bw = value; }
};

class c_PricingSolverCVRPTW : public c_PricingProblem {
	enum { FW=0, BW=1, BIDIR_DYN=2, BIDIR=3 };
	int i_direction; 
	int i_networkSize;
	int i_day;
	bool b_exactDom;
	c_MergeManagerCVRPTW o_merger;
	c_SPPRC_Solver<c_REF_CVRPTW, ExtManagerFW, DManagerFW, ExtManagerBW, DManagerBW, c_MergeManagerCVRPTW> o_esppc_solver;
public:
	typedef c_LabelCVRPTW_Fw tLabel_Fw;
	typedef c_LabelCVRPTW_Bw tLabel_Bw;
public:
	c_PricingSolverCVRPTW( c_Controller* controller, int networkSize, int day, bool exactDom);
	virtual ~c_PricingSolverCVRPTW() {}

	double RouteRDC_FW( const tLabel_Fw* label, vector<int>& route ) const;
	double RouteRDC_BW( const tLabel_Bw* label, vector<int>& route ) const;
	double RouteRDC_BIDIR( const pair<tLabel_Fw*,tLabel_Bw*> labels, vector<int>& route ) const;
	// these methods can be modified
	virtual void Update( c_BranchAndBoundNode* node );
	virtual void Update( c_DualVariables* dual ); 
	virtual int  Go( c_DualVariables* dual, list<c_ColumnPricePair>& cols_and_prices, double& best_val );
	virtual double RHSofConvexityConstraint();
};


class c_PricingSolverHierarchyCVRPTW : public c_PricingProblemHierarchy {
	int i_day;	
public:
	c_PricingSolverHierarchyCVRPTW( c_Controller* controller, int max_num_failures, int min_num_columns, int day );

	const int Day(){return i_day;}
	void SetBestReducedCost(double val) { d_best_reduced_costs = val; }
	virtual void Update( c_BranchAndBoundNode* node );
	virtual void Update( c_DualVariables* dual );
	int GetNumPricingLevels() const { return (int)v_hierarchy.size(); }
	int GetPricingStartLevel() const { return i_pricing_start_level; }
	void SetPricingStartLevel(int val) { i_pricing_start_level = val; }
	const int GetMinNumCols() const { return i_min_num_columns; }

	//overwrite
	virtual int Go(c_DualVariables* dual, list<c_ColumnPricePair>& cols_and_prices, int NumPricersUsed = MaxNumPricers);

};


class c_SeparationProblemCVRPTW : public c_SeparationProblem {
protected:
	c_DoubleMatrix lhsPerSubset;
	//for 2 Path Cuts
	/*c_TwoPathCutTester o_tester;
	c_TwoPathCut_DAGHeuristic<pvrptw_bitset> o_generator_DAGHeuristic;
	c_TwoPathCutSeparator_PVRPTW_DAGHeuristic o_separator_DAGHeuristic;*/
	//c_TwoPathCut_RopkeCordeauHeuristic<pvrptw_bitset> o_generator_RopkeCordeauHeuristic;
	//c_TwoPathCutSeparator_PVRPTW_RopkeCordeauHeuristic o_separator_RopkeCordeauHeuristic;
	//c_Random random_generator;
public: 
	c_SeparationProblemCVRPTW (c_Controller* controller, c_CVRPTW_Instance_With_RDC& inst);
	virtual ~c_SeparationProblemCVRPTW() {}

	// these methods must be provided by derived class
	int Go( c_RMP* rmp, list<c_Constraint*>& cuts, int node_level );  
	int SeparateSubSetRowCuts( c_RMP* rmp, list<c_Constraint*>& cuts );
	int SeparateCapacityCuts( c_RMP* rmp, list<c_Constraint*>& cuts );
	//int Separate2PathCuts(c_RMP* rmp, list<c_Constraint*>& cuts);

	/*int SeparateExtendNG(c_RMP* rmp, list<c_Constraint*>& cuts);
	int SeparateSubSetRowCuts( c_RMP* rmp, list<c_Constraint*>& cuts );
	int Separate2PathCuts(c_RMP* rmp, list<c_Constraint*>& cuts);
	int Separate2PathCutsOwnCode(c_RMP* rmp, list<c_Constraint*>& cuts);
	bool CapacityCutAlreadyGenerated(const vector<pair<double, pair<PVRPTW_bitset, int>>>& violatingSubsets, const PVRPTW_bitset& subset);
	int SeparateCapacityCuts(c_RMP* rmp, list<c_Constraint*>& cuts);*/


	double GetFlowIntoSubset(vector<int> & subset, int c);
	double GetNeededVehicles(vector<int>& subset);

	int EliminateCyclesByExtendingNeighborhood(list<c_Constraint*>& cuts);

	void OutputInOStream( ostream& s ) const;
};


class c_SubsetRowCutConstraintCVRPTW : public c_GlobalValidConstraint_PVRPTW { //changed from c_Constraint_PVRPTW
	c_Controller* _controller;
protected:
	int i_cutNumber;
	vector<int> v_Subset;
public:
	c_SubsetRowCutConstraintCVRPTW( c_Controller* controller, vector<int> customers);

	const vector<int>& GetSubset(){return v_Subset;} 
	int getCutId() const {return i_cutNumber;}
	virtual void OutputInOStream( ostream& s ) const;
	int Type() const { return c_RMP_CVRPTW::eSubsetRowCut; }
};


class c_AggregatedSubsetRowCutConstraintCVRPTW : public c_GlobalValidConstraint_PVRPTW{
	c_Controller* _controller;
protected:
	int i_cutNumber;
	vector<int> v_Subset;
	vector<vector<int>> v_SubsetsPerDay;
public:
	c_AggregatedSubsetRowCutConstraintCVRPTW( c_Controller* controller, vector<int> customers);

	const vector<int>& GetSubset(int day) const { return v_SubsetsPerDay[day];}
	int getCutId() const {return i_cutNumber;}
	virtual void OutputInOStream( ostream& s ) const;
	int Type() const { return c_RMP_CVRPTW::eAggregatedSubsetRowCut; }
};

class c_NgIncreasementCVRPTW : public c_GlobalValidConstraint_PVRPTW{
	c_Controller* _controller;
protected:
	int i_cutNumber;
	vector<int> v_changedLocations;
public:
	c_NgIncreasementCVRPTW( c_Controller* controller, vector<int> locations);

	const vector<int>& GetLocations(){return v_changedLocations;} 
	int getCutId() const {return i_cutNumber;}
	virtual void OutputInOStream( ostream& s ) const;
	int Type() const { return c_RMP_CVRPTW::eNgIncreasementCut; }
};




class c_CapacityCutConstraintCVRPTW : public c_Constraint_PVRPTW{
	c_Controller* _controller;
protected:
	int i_cutNumber;
	vector<int> v_customerSubset;
	int i_rhs;
public:
	c_CapacityCutConstraintCVRPTW( c_Controller* controller, vector<int> customers, int rhs);

	const vector<int>& GetCustomerSubset(){return v_customerSubset;} 
	int GetRHS(){return i_rhs;}
	int getCutId() const {return i_cutNumber;}
	virtual void OutputInOStream( ostream& s ) const;
	int Type() const { return c_RMP_CVRPTW::eCapacityCut; }
};


class c_ArcSetConstraint : public c_Constraint_PVRPTW {
	c_Controller* _controller;
protected:
	int i_day;
	c_IntMatrix v_arc_set; //based on nodes, used in combination with i_day
public:
	c_ArcSetConstraint(c_Controller* controller, char sense, double rhs, int day);
	virtual ~c_ArcSetConstraint() {};

	virtual int Coefficient(int tail, int head) const { return v_arc_set(tail, head); }
	const int Day() {return i_day;}
	// moeglicherweise zu spezialisieren
	virtual bool IsGloballyValid() const { return true; }
	virtual void OutputInOStream(ostream& s) const = 0;
	int Type() const { return c_RMP_CVRPTW::eARCSET; }
};

class c_TwoPathConstraint : public c_ArcSetConstraint {
	bitset<pvrptw_bs_max_size>  s_nodes;
public:
	c_TwoPathConstraint(c_Controller* controller, const bitset<pvrptw_bs_max_size>& nodes, int day);
	virtual ~c_TwoPathConstraint() {};

	const bitset<pvrptw_bs_max_size>& Nodes() { return s_nodes; }

	void OutputInOStream(ostream& s) const;
};


#endif // BAP_CVRP_h
