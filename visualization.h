#ifndef VISUALIZATION_H
#define VISUALIZATION_H

#include <string>
#include <vector>
using namespace std;

class c_TikzVisualization {
	const char* s_filename;
	int i_numNodes;
	vector<double> v_xCoords;
	vector<double> v_yCoords;
	vector<double> v_demands;
	vector<int> v_startTimes;
	vector<int> v_endTimes;

	double planningHorizon;
	static const int planningHorizonInPt = 10;
	static const int halfPH = planningHorizonInPt /2;
	static const int ptAbove = 3;

public:
	c_TikzVisualization(const char* filename, int numNodes, vector<double> xCoord, vector<double> yCoord, vector<double> demands, vector<int> startTimes, vector<int> endTimes, vector<int> nodeTypes = vector<int>() );

	void addRoutes(vector<vector<int>> routes, vector<vector<int>> visitTimes);
};

#endif // of VISUALIZATION_H