#ifndef CVRPTW_DATA_H
#define CVRPTW_DATA_H

//#include "cvrptw_instance.h"

class c_CVRPTW_Instance;

class c_SchedulePart {
	int i_id;
	int i_customer;
	int i_startDay;
	int i_endDay;
	int i_demand;
	int i_additionalServiceTime;
public:
	c_SchedulePart(int id, int customer, int startDay, int endDay, int demand, int addServiceTime );

	const int getId() const {return i_id;}
	const int getCustomer() const {return i_customer;}
	const int getStartDay() const { return i_startDay;}
	const int getEndDay() const { return i_endDay;}
	const int getDemand() const { return i_demand;}
	const int getAdditionalServiceTime() const { return i_additionalServiceTime;}

	//customer, start and end period define a schedule part uniquely
	bool operator==(const c_SchedulePart &other) const {
		if( i_customer == other.getCustomer() && i_startDay == other.getStartDay() && i_endDay == other.getEndDay() ){
			return true;
		} else{
			return false;
		}
	}

	bool operator!=(const c_SchedulePart &other) const {
		return !(*this == other);
	}
};

#endif // of CVRPTW_DATA_H