#include <algorithm>
#include <vector>

#include "bcp_cvrptw.h"
#include "CVRPSEP\CAPSEP.H"
#include "CVRPSEP\cnstrmgr.h"



using namespace std;
bool minTourDuration;


static list<int> empty_int_list;

bool exactDom; 
bool ryanFosterActive;

c_ControllerCVRPTW::c_ControllerCVRPTW(std::string instanceFile, string scheduleFile, std::string settings_file/*= "settings.txt" */)
:	c_Controller( settings_file ),
	c_CVRPTW_Instance_With_RDC( instanceFile, scheduleFile ),
	i_numSubsetRowCuts (0),
	i_numNgIncreasements (0),
	i_numCapacityCuts (0),
	i_numArcSetCuts (0),
	fixedCustomer(-1),
	b_daySpecificBranching(false),
	counterCutGeneration(0),
	BranchVal("Branchval", 0.5, false),
	NgNeighborhoodSize("NgNeighborhoodSize", 10, false),
	UseDynamicNgNeighborhood ("UseDynamicNgNeighborhood", false, false),
	DynamicNgNeighborhoodUB ("DynamicNgNeighborhoodUB", 18, false),
	CutTolerance("CutTolerance",0.1, false), 
	MaxCutNodeLevel("MaxCutNodeLevel", 3, false),
	HeuristicPricersNetworkSizes( "HeuristicPricersNetworkSizes", empty_int_list, false ),
	Solver ("Solver", 3, false ),
	MaxNumberSRCutsPerIteration ("MaxNumberSRCutsPerIteration", 10, false), 
	UseSubsetRowCuts ("UseSubsetRowCuts", false, false),
	MaxSRCutsPerCustomerPerIteration("MaxSRCutsPerCustomerPerIteration", (int) INFTY, false),
	UseCapacityCuts("UseCapacityCuts", false, false),
	MaxNumberCapCutsPerIteration ("MaxNumberCapCutsPerIteration", 10, false),
	NetworkOfTasks ("NetworkOfTasks", true, false),
	NetworkOfScheduleParts ("NetworkOfScheduleParts", false, false),
	RegularVisits ("RegularVisits", false, false),
	MinimumTourDuration("MinimumTourDuration", false, false),
	UseTwoPathCuts( "UseTwoPathCuts", false, false),
	KPathMaxSizeOfSets("KPathMaxSizeOfSets", 10, false),	
	KPathMaxInclusion ("KPathMaxInclusion", 3, false),
	KPathRestarts ("KPathRestarts", 5, false),
	AddColumnsOnOtherDays("AddColumnsOnOtherDays", false, false),
	BreakSymmetry("BreakSymmetry", false, false),
	StartWithAggregation("StartWithAggregation", false, false),
	SeparateAggregatedSRCuts("SeparateAggregatedSRCuts", false, false),
	UseHeuristicDominance("UseHeuristicDominance", false, true),
	UseRyanFosterBranching("UseRyanFosterBranching", false, false),
	UseMIPSolverAtTheEnd("UseMIPSolverAtTheEnd", false, false),
	BranchingCustomerDayCandidates("BranchingCustomerDayCandidates", 1, false),
	RestrictVerticesWithDominanceCheck("RestrictVerticesWithDominanceCheck", false, false),
	BranchingTieBreakerRule("BranchingTieBreakerRule", 1, false),
	cnt_extendedFWLabels(0),
	cnt_extendedBWLabels(0),
	i_numAddedColumns(0),
	PricingHierarchyStandardOrder("PricingHierarchyStandardOrder",true,true),
	i_startHierarchyId(0),
	MaxNumSubsetRow("MaxNumSubsetRow",300,true),
	BranchVehicleFirst("BranchVehicleFirst",true,true)
{	
	// Parameters
	AddParameter( &BranchVal );
	AddParameter( &NgNeighborhoodSize );
	AddParameter( &UseDynamicNgNeighborhood );
	AddParameter( &DynamicNgNeighborhoodUB );
	AddParameter( &HeuristicPricersNetworkSizes );
	AddParameter( &Solver );
	AddParameter( &UseSubsetRowCuts );
	AddParameter( &MaxNumberSRCutsPerIteration );
	AddParameter( &MaxSRCutsPerCustomerPerIteration );
	AddParameter( &UseCapacityCuts );
	AddParameter( &CutTolerance );
	AddParameter( &MaxNumberCapCutsPerIteration );
	AddParameter( &MaxCutNodeLevel );
	AddParameter( &NetworkOfTasks );
	AddParameter( &NetworkOfScheduleParts );
	AddParameter( &RegularVisits );
	AddParameter( &MinimumTourDuration );
	AddParameter( &UseTwoPathCuts );
	AddParameter( &KPathMaxSizeOfSets );	
	AddParameter( &KPathMaxInclusion );
	AddParameter( &KPathRestarts );
	AddParameter( &AddColumnsOnOtherDays );
	AddParameter( &BreakSymmetry );
	AddParameter( &StartWithAggregation );
	AddParameter( &SeparateAggregatedSRCuts );
	AddParameter( &UseHeuristicDominance );
	AddParameter( &UseRyanFosterBranching );
	AddParameter( &UseMIPSolverAtTheEnd );
	AddParameter( &BranchingCustomerDayCandidates );
	AddParameter( &RestrictVerticesWithDominanceCheck );
	AddParameter( &BranchingTieBreakerRule );
	AddParameter(& PricingHierarchyStandardOrder);
	AddParameter(&MaxNumSubsetRow);
	AddParameter(&BranchVehicleFirst);
}


c_ControllerCVRPTW::~c_ControllerCVRPTW()
{
	int v = NumVertices();
	for (int i=0; i<v; i++) 
		for (int j=0; j<v; j++)
			for( int d= 0; d < PlanningHorizon(); d++)
				if ( ArcExistsOnDay(i,j,d) )
					delete m_REFs(i,j,d);
}

void c_ControllerCVRPTW::Go( void )
{

	if (MaxNumSubsetRow() > 300)
	{
		cout << "Maximum number of SR cuts is " << 300  << " atm, if you want more change it in cvrptw_ref.h!" << endl;
		MaxNumSubsetRow.SetValue(300);
	}
	taskNetwork = NetworkOfTasks();
	schedulePartNetwork = NetworkOfScheduleParts();

	if( !ParameterCombinationFeasible() ){
		cerr << "Invalid combination of setting parameters. Error. Check method ParameterCombinationFeasible!" << endl;
		throw;
	}

	if( !StartWithAggregation() && BreakSymmetry() ){
		BreakSymmetryByScheduleSelection();
	}

	minTourDuration = MinimumTourDuration();
	createScheduleParts();
	storeVertexInfos();
	InitializeRDCMatrix();
	createREFs();	

	/*static const int arr1[] = {0,81,82,83,78,75,94,22,79,97};
	vector<int> route1 (arr1, arr1 + sizeof(arr1) / sizeof(arr1[0]) );
	for ( int p= 0; p < route1.size()-1; p++ ){
		cout << route1[p] << " - " << route1[p+1] << ": " 
			<< GetNode(route1[p]) << " - " << GetNode(route1[p+1]) << ": "
			<< Time(GetNode(route1[p]), GetNode(route1[p+1])) 
			<< " , Start: " << StartTime(GetNode(route1[p+1])) 
			<< " , End: " << EndTime(GetNode(route1[p+1])) 
			<< endl;
	}*/

	int bits = 8*sizeof (long);
	if ( NgNeighborhoodSize() > bits )
	{
		cout << "NG neighborhoods must not exceed " << bits << "." << endl;
		throw;
	}
	// create NG-neighborhoods first
	int n = NumNodes();
	int ng_sz = NgNeighborhoodSize();
	for ( int i=0; i<n; i++ )
	{
		vector<pair<double,int> > sorter;
		for ( int j=0; j<n; j++ )
			if ( ArcExists(i,j) && ArcExists(j,i) )
				sorter.push_back( make_pair( Cost(i,j), j ) );
		sort( sorter.begin(), sorter.end() );
		vector<int> ng_vect;
		ng_vect.push_back(i); // make sure that i \in NG(i) and at position 0
		for ( int j=0; j<min(ng_sz, (int)sorter.size() ); j++ )
			ng_vect.push_back( sorter[j].second );
		SetNGNeighborhood( i, ng_vect );
	}

	// cout << "#Arcs= " << getNumArcs() << endl;
	// Default behavior...
	c_Controller::Go();
}

void c_ControllerCVRPTW::ActivateREF( int i, int j, int day, bool flag/*=true */ )
{
	m_REFs(i,j,day)->Activate(flag);
}

void c_ControllerCVRPTW::WriteSolutionInFile( string FileName )
{
	using namespace formatted_output;
	//instance
	c_HostnameField host( 100, "Computer" );
	c_TimeStampField timestamp( 200, "Date" );
	c_StringField problem_type( 300, "problem type|Instance", "CVRPTW" );
	c_StringField inst_name( 450, "instance name|Instance", this->InstanceName() );
	c_StringField schedule_name( 460, "schedule name|Instance", this->ScheduleName() );
	c_IntegerField num_nodes( 500, "num Customers|Instance", this->NumCustomers() );
	c_IntegerField capacity( 505, "capacity|Instance", this->Capacity() );
	c_IntegerField planningHorizon( 510, "planningHorizon|Instance", this->PlanningHorizon() );

	c_IntegerField minColsPerPricer( 580, "minColsPerPricer|Settings", this->PricingHierarchyMinNumColsToGenerate());
	c_IntegerField maxFailuresPerPricer( 590, "maxFailuresPerPricer|Settings", this->PricingHierarchyMaxNumFailuresBeforeSwitchOff());
	c_BoolField hierarchyOrder( 591, "StandardHierarchyOrder|Settings", this->PricingHierarchyStandardOrder() );

	c_IntegerField solver( 600, "Solver|Settings", this->Solver());
	c_IntegerField ngSize( 602, "NgSizeInit|Settings", this->NgNeighborhoodSize());
	c_BoolField ngSizeIncreasement( 603, "DynamicNgSize|Settings", this->UseDynamicNgNeighborhood());
	c_IntegerField maxNgSize( 604, "MaxNgSize|Settings", this->DynamicNgNeighborhoodUB());
	c_IntegerField numSRCutPerItPerDay( 608, "#SRCutsPerItPerDay|Settings", this->MaxNumberSRCutsPerIteration());
	c_IntegerField maxCutNodeLevel( 609, "maxCutLevel|Settings", this->MaxCutNodeLevel());

	c_StringField network( 610, "Network|Settings", this->NetworkOfTasks()? "tasks": "scheduleParts");
	stringstream network_sizes;
	for ( auto nwSize= HeuristicPricersNetworkSizes().begin(); nwSize != HeuristicPricersNetworkSizes().end(); nwSize++ )
		network_sizes << *nwSize << "_";
	c_StringField networkSizes( 611, "networkSizes|Settings", network_sizes.str() );

	c_BoolField regularVisits ( 614, "RegularVisits|Settings", this->RegularVisits() );
	c_BoolField addColOtherDays( 613, "AddColsOtherDays|Settings", this->AddColumnsOnOtherDays() );
	c_BoolField breakSym (614, "BreakSymmetry|Settings", this->BreakSymmetry() );
	c_BoolField aggregation (616, "Aggregation|Settings", this->StartWithAggregation() );
	c_BoolField heuDom( 617, "HeuristicDom|Settings", this->UseHeuristicDominance() );

	c_IntegerField branchingRule (650, "BranchingTieBreakerRule|Settings", this->BranchingTieBreakerRule() );
	c_IntegerField numBranCustDayCandidates (652, "#BranchCustDayCandidates|Settings", this->BranchingCustomerDayCandidates() );
	c_BoolField strongBranching (658, "StrongBranching|Settings", this->StrongBranching() );
	c_IntegerField numBranchingCandidates (659, "#BranchingCandidates|Settings", this->StrongBranchingNumCandidates() );
	//c_BoolField sBexactLB (660, "sBexactLB|Settings", this->StrongBranchingSolveExact() );

	c_BoolField mipSolverEnd (680, "MIPSolverAtTheEnd|Settings", this->UseMIPSolverAtTheEnd() );
	c_IntegerField mipTime(681, "MIPMaxTime|Settings", this->MIPMaxSolutionTimeSec() );

	c_IntegerField numVertices ( 700, "#Vertices|Instance", this->NumVertices() );
	c_IntegerField numArcs ( 701, "#Arcs|Instance", this->getNumArcs() );
	// solution
	bool is_opt = !TimeOut();
	double solutionTime = SolutionTimeSec();
	c_BoolField is_optimal( 1400, "opt?|Solution", is_opt );

	c_DoubleField info_number_of_solved_nodes( 1405, "number Solved Nodes|Solution", InfoNumberOfSolvedNodes()  );
	c_IntegerField info_number_cut_generations( 1406, "number Cut Generations|Solution", counterCutGeneration );

	c_DoubleField info_number_of_generated_cuts( 1407, "number Generated Cuts|Solution", InfoNumberOfGeneratedCuts() );
	
	c_DoubleField ub( 1410, "UB |Solution", GetStoredUB() );
	c_DoubleField ubImproved( 1411, "Improved UB |Solution", UB() );

	//c_DoubleField ub( 1410, "UB |Solution", UB() );
	c_DoubleField lb( 1420, "LB |Solution", LB() );
	c_DoubleField lb_root( 1422, "LB Root |Solution", LB1() );
	c_DoubleField seconds( 1430, "time [s]|Solution", solutionTime );
	c_DoubleField secondsRoot( 1431, "time root [s]|Solution", InfoTimeRootLB1() );
	c_DoubleField secondsRootCuts( 1432, "time ext. root [s]|Solution", InfoTimeRootLB2() );

	c_DoubleField secondsPricing( 1433, "time pricing [s]|Solution", InfoTimePricing() );
	c_DoubleField secondsSeparation( 1434, "time separation [s]|Solution", InfoTimeSeparation() );
	c_DoubleField secondsBranching( 1435, "time branching [s]|Solution", InfoTimeBranching() );
	c_DoubleField secondsReopt( 1435, "time reopt [s]|Solution", InfoTimeReoptimization() );

	double time_limit = MaxSolutionTimeSec();
	c_DoubleField tl( 1437, "time limit [s]|Solution", time_limit );

	c_IntegerField noFWLabels( 1438, "#FW Labels|Pricing", (int)GetNumFWLabels());
	c_IntegerField noBWLabels( 1439, "#BW Labels|Pricing", (int)GetNumBWLabels());
	c_IntegerField numAddedCols (1440, "NumAddedCols|Pricing", NumAddedColumns() );

	c_BoolField srCuts( 1442, "SubsetRow Cuts|Solution", UseSubsetRowCuts() );
	c_IntegerField numSrCuts( 1443, "#SR Cuts|Solution", NumSubsetRowCuts() );
	c_BoolField capCuts( 1444, "Cap Cuts|Solution", UseCapacityCuts() );
	c_IntegerField numCapCuts( 1445, "#Cap Cuts|Solution", NumCapacityCuts() );
	c_BoolField twoPathCuts( 1446, "2Path Cuts|Solution", UseTwoPathCuts() );
	c_IntegerField numTwoPathCuts( 1447, "#2Path Cuts|Solution", NumArcSetConstraints() );
	
	double numVehicles = -1;
	std::ostringstream ossRoutes;
	std::ostringstream ossSchedules;
	c_SolutionCVRPTW* sol = (c_SolutionCVRPTW*) BestSolution();
	if(sol != NULL){
		numVehicles = sol->VehicleNumber();
	
		for ( int i=0; i<(int) sol->getRoutes().size(); i++ )
		{
			ossRoutes << i+1 << ": Day " << sol->getRoutes()[i].second << " [";
			const vector<int>& route = sol->getRoutes()[i].first;

			ossRoutes << "0,";
			if( RegularVisits() ){ //order of customers gives complete information
				for ( int t=0; t<(int)route.size(); t++ )
					ossRoutes << GetSchedulePart(route[t])->getCustomer() << ","; 
			} else{
				//write order of customers and their visit end day
				for ( int t=0; t<(int)route.size(); t++ )
					ossRoutes << GetSchedulePart(route[t])->getCustomer() << "_" << GetSchedulePart(route[t])->getEndDay() << ","; 
			}
			ossRoutes << DestDepot() << ",";


			/*for ( int idx=0; idx<(int)route.size(); idx++ ){
			if( NetworkOfTasks ()){
			ossRoutes << route[idx] << ",";
			} else if ( NetworkOfScheduleParts() ){
			ossRoutes << GetSchedulePart(route[idx])->getCustomer() << "_" << GetSchedulePart(route[idx])->getEndDay() << ",";
			}
			}
			ossRoutes << "] , Day " << sol->getRoutes()[i].second << ", "; */
			ossRoutes << "] , " << sol->getCost(i) << ", ";
		}

		for ( int i=1; i<(int) sol->getSchedules().size()-1; i++ )
		{
			ossSchedules << i << ": [";
			const vector<bool>& schedule = sol->getSchedules()[i];
			if( schedule.size() == PlanningHorizon() ){
				for ( int idx = 0; idx < PlanningHorizon(); idx++ )
					ossSchedules << schedule[idx] << ",";
			}
			ossSchedules << "] ,"; 
		}
	}
	c_DoubleField numVehicle( 1460, "num vehicles|Solution", numVehicles);
	c_StringField routes( 1480, "routes|Solution", ossRoutes.str() );
	c_StringField schedules( 1490, "schedules|Solution", ossSchedules.str() );
	host.OutputToFile( FileName );
}

c_RMP* c_ControllerCVRPTW::CreateNewRMP()
{
	return new c_RMP_CVRPTW( this );
}

c_BranchAndBoundNode* c_ControllerCVRPTW::CreateRootNode()
{
	return new c_BranchAndBoundNodeCVRPTW( this );
}

c_DualVariables* c_ControllerCVRPTW::CreateDualVariables()
{
	return new c_DualVariablesCVRPTW( this );
}

c_Solution* c_ControllerCVRPTW::CreateSolution( c_RMP* rmp , c_DualVariables* dual )
{
	return new c_SolutionCVRPTW( this, rmp, dual );
}

void c_ControllerCVRPTW::AddPricingProblems()
{
	int max_num_failures     = PricingHierarchyMaxNumFailuresBeforeSwitchOff();
	int min_num_cols		 = PricingHierarchyMinNumColsToGenerate();

	//int easierPricingProblems = 0;
	//one hierarchy for each day, each beginning with heuristic pricers until the exact pricer with maximal networksize
	vector< c_PricingSolverHierarchyCVRPTW* > hierarchies;
	int maxNumPricingLevels = 0;
	for ( int d = 0; d < PlanningHorizon(); d++){
		/*if( !GetVehicleClass(vcId)->hasTrailer() )
		easierPricingProblems++;*/
		hierarchies.push_back( new c_PricingSolverHierarchyCVRPTW( this, max_num_failures, min_num_cols, d ) );

		if (UseHeuristicDominance()){
			//Add pricers with every second network size with heuristic dominance
			int cnt = 0;
			for  (auto i = HeuristicPricersNetworkSizes().begin(); i != HeuristicPricersNetworkSizes().end();i++ ) {
				if( cnt%2 == 0 ){
					hierarchies[d]->Add( new c_PricingSolverCVRPTW( this, *i, d, false ) );
					if ( *i > NumNodes() ){
						break;
					}
				}
				cnt++;
			}
			//Add pricers with all network sizes with exact dominance
			for (auto i = HeuristicPricersNetworkSizes().begin(); i != HeuristicPricersNetworkSizes().end(); i++) {
				if ( *i > 0.5 * NumNodes() ){ //Add only reduced networks with at most half of all nodes
					break;
				}
				hierarchies[d]->Add( new c_PricingSolverCVRPTW( this, *i, d, true ) );
			}
			//Finally add an exact dominance PricingSolver with all nodes
			hierarchies[d]->Add( new c_PricingSolverCVRPTW( this, 10000, d, true ) );
			
		} else{
			for  (auto i = HeuristicPricersNetworkSizes().begin(); i != HeuristicPricersNetworkSizes().end();i++ ) {
				if ( *i > 0.5 * NumNodes() ){ //Add only reduced networks with at most half of all nodes
					break;
				}
				hierarchies[d]->Add( new c_PricingSolverCVRPTW( this, *i, d, true ) );
			}
			//Finally add an exact dominance PricingSolver with all nodes
			hierarchies[d]->Add( new c_PricingSolverCVRPTW( this, 10000, d, true ) );
			//Former version: All network sizes
			//Add pricers with all network sizes with exact dominance
			/*for each( int i in HeuristicPricersNetworkSizes() ) {
			hierarchies[d]->Add( new c_PricingSolverCVRPTW( this, i, d, true ) );
			if ( i > NumNodes() ){
			break;
			}
			}*/
		}
		if( hierarchies[d]->GetNumPricingLevels() > maxNumPricingLevels)
			maxNumPricingLevels = hierarchies[d]->GetNumPricingLevels();
		AddPricingProblemHierarchy( hierarchies[d] ); 
		v_hierarchy_vec.push_back(hierarchies[d]);
	}
	v_numFailures_hierarchy = vector<int>(maxNumPricingLevels, 0);
	//setNumEasierPricingProblems(easierPricingProblems);
}

void c_ControllerCVRPTW::AddSeparationProblems()
{
	AddSeparationProblem(new c_SeparationProblemCVRPTW(this, *this));
}

void c_ControllerCVRPTW::Pricing(int iteration, c_DualVariables* dual, list<c_Column*>& col_list, int NumPricersUsed)
{
	// Initialisierung
	list<c_ColumnPricePair> column_price_list;
	list<c_ColumnPricePair> to_remove_list;

	if (InfoLevel() >= 3)
		Info3() << "Pricing " << flush;

	c_TimeInfo pricing_time_info;
	pricing_time_info.Start();
	int generated_cols = 0;
 

	if (PricingHierarchyStandardOrder()) {
		for (auto hh = v_hierarchy_vec.begin(); hh != v_hierarchy_vec.end(); ++hh)
		{
			if (TimeOut())
				return;
			c_PricingProblemHierarchy* hierarchy = *hh;
			hierarchy->Update(dual);
			generated_cols += hierarchy->Go(dual, column_price_list);
			//info_number_of_solved_pricing_problems++;
		}
	}
	else {
		//NowTODO
		int startHierarchy = GetStartHierarchy();
		int numHierarchies = (int)v_hierarchy_vec.size();
		int maxPricingLevel = v_hierarchy_vec[0]->GetNumPricingLevels() - 1;
		for (auto hh = v_hierarchy_vec.begin(); hh != v_hierarchy_vec.end(); hh++) {
			c_PricingSolverHierarchyCVRPTW* hierarchy = (c_PricingSolverHierarchyCVRPTW * ) *hh;
			hierarchy->SetBestReducedCost(-10 * INFTY); //Initialize reduced cost to -infinity, so that the lagrangean lower bound is also valid, if not all pricing hierarchies are started
		}

		bool continuePricing = true;
		vector<bool> v_failed = vector<bool>(numHierarchies, false);
		//vector<bool> v_failed = vector<bool>(numHierarchies, false);
		do {
			if (TimeOut())
				return;
			if(InfoLevel() >= 3)
				Info3() << "Hierarchy " << startHierarchy << ": ";
			c_PricingSolverHierarchyCVRPTW* hierarchy = v_hierarchy_vec[startHierarchy];
			hierarchy->Update(dual);

			if (all_of(v_failed.begin(), v_failed.end(), [](bool b) {return b; })) {
				IncreaseNumFailures(min(hierarchy->GetPricingStartLevel(), maxPricingLevel));
				if (hierarchy->GetPricingStartLevel() >= maxPricingLevel) //no more improving columns for this node
					break; //Other behaviour when MinNumCols > 0?
				else {
					//increase pricing level of all hierarchies (at least temporarily; if maxNumFailures is not reached, it is decreased again)
					int minPricingStartLevel = maxPricingLevel;
					for (int p = 0; p < numHierarchies; p++) {
						if (v_hierarchy_vec[p]->GetPricingStartLevel() < minPricingStartLevel)
							minPricingStartLevel = v_hierarchy_vec[p]->GetPricingStartLevel();
					}
					for (int p = 0; p < v_failed.size(); p++) {
						v_hierarchy_vec[p]->SetPricingStartLevel(minPricingStartLevel + 1); //hierarchies[p]->GetPricingStartLevel() +1 );
						v_failed[p] = false;
					}
				}
			}

			int added_cols = hierarchy->Go(dual, column_price_list);
			generated_cols += added_cols;

			if (added_cols >= hierarchy->GetMinNumCols()) { //stay at the same hierarchy until no more columns for it can be found
				continuePricing = false;
				SetStartHierarchy(startHierarchy);
			}
			else {
				v_failed[startHierarchy] = true;
				startHierarchy = (startHierarchy + 1) % numHierarchies;
			}

		} while (continuePricing);

		////New test: always start with pricing hierarchy which has the minimal pricingStartLevel
		//int maxPricingLevel = hierarchies[0]->GetNumPricingLevels();
		//int minPricingStartLevel = maxPricingLevel+1;
		//int bestH = -1;
		//int numHierarchies = (int)hierarchies.size();
		//for ( int h = 0; h < numHierarchies; h++ )	{
		//	hierarchies[h]->SetBestReducedCost(-10*INFTY); //Initialize reduced cost to -infinity, so that the lagrangean lower bound is also valid, if not all pricing hierarchies are started
		//	if( pricing_levels_in_hierarchies[h] < minPricingStartLevel ){
		//		minPricingStartLevel = hierarchies[h]->GetPricingStartLevel();
		//		bestH = h;
		//	}
		//}

		////stay at one pricing hierarchy until no more columns can be found. Then switch first to other hierarchies with same pricing level.
		//int hNum = bestH;
		//bool continuePricing = true;
		//vector<bool> hierarchyRunWithMaxPricingLevel = vector<bool>(numHierarchies);
		//for( int p = 0; p < numHierarchies; p++)
		//	hierarchyRunWithMaxPricingLevel[p] = false;
		//bool pricingLevelIncreased = false;
		//do{
		//	if( TimeOut() ) 
		//		return;
		//	c_PricingProblemHierarchy* hierarchy = hierarchies[hNum];
		//	hierarchy->Update( dual );
		//	hierarchyRunWithMaxPricingLevel[hNum] = (pricing_levels_in_hierarchies[hNum] >= maxPricingLevel-1)? true : false;
		//	generated_cols += hierarchy->Go( dual, column_price_list );
		//	int curPricingLevel = hierarchy->GetPricingStartLevel();
		//	if( curPricingLevel > pricing_levels_in_hierarchies[hNum] )
		//		pricingLevelIncreased = true;
		//	if( generated_cols > hierarchies[hNum]->GetMinNumCols() ){ //stay at the same hierarchy until no more columns for it can be found
		//		//if the maximal number of failures is not reached and the pricing level below, go back
		//		if( curPricingLevel > 0 && hierarchy->GetNumFailures(curPricingLevel-1) < hierarchy->GetMaxNumFailures() ){
		//			hierarchy->SetPricingStartLevel( curPricingLevel-1 );
		//		}
		//		pricing_levels_in_hierarchies[hNum] = hierarchy->GetPricingStartLevel();
		//		break;
		//	}
		//	pricing_levels_in_hierarchies[hNum] = curPricingLevel;
		//	hNum= hNum + 1;
		//	hNum = hNum % numHierarchies;

		//	//if maxNumFailures is greater 1, ensure that the pricing level is although increased if no hierarchy found new columns and the same pricing problem would be resolved
		//	if( hNum == bestH && generated_cols == 0 && !pricingLevelIncreased){ //GetMinPricingStartLevel() != maxPricingLevel
		//		//all hierarchies were tested and found no column, but maximal pricing level not reached, such that pricing will continue:
		//		//increase all minimal pricing levels (by ignoring a maxNumFailures > 1), since nothing has changed
		//		for ( int h = 0; h < numHierarchies; h++ )	{
		//			if( hierarchies[h]->GetPricingStartLevel() == GetMinPricingStartLevel() ){
		//				hierarchies[h]->SetPricingStartLevel(pricing_levels_in_hierarchies[h]+1);
		//				pricing_levels_in_hierarchies[h] += 1;
		//			}
		//		}
		//	}
		//	continuePricing = ( find(begin(hierarchyRunWithMaxPricingLevel), end(hierarchyRunWithMaxPricingLevel), false) != end(hierarchyRunWithMaxPricingLevel) ); // not all true
		//} while( continuePricing ); //hNum != bestH || GetMinPricingStartLevel() != maxPricingLevel
	}

	// Select an appropriate subset of columns to include into the
	// RMP. Especially in situations, where the number of generated columns is
	// large this method should take care of a good choice of columns.
	int size_bofore_sel = (int)column_price_list.size();
	if (PricingWithColumnSelection())
	{
		ColumnSelectionInPricing(column_price_list, to_remove_list);
		list<c_ColumnPricePair>::iterator col_price;
		for (col_price = to_remove_list.begin(); col_price != to_remove_list.end(); ++col_price)
			delete (*col_price).first;
	}
	pricing_time_info.Stop();

	//info_time_pricing += pricing_time_info.Seconds();
	info_number_of_generated_columns += generated_cols;
	info_number_of_rmp_iterations++;

	if (generated_cols && (InfoLevel() >= 3))
	{
		Info3() << " " << generated_cols << " col's generated" << flush;
		if (size_bofore_sel != column_price_list.size())
			Info3() << ", " << (int)column_price_list.size() << " col's selected" << flush;
	}
	if (InfoLevel() >= 3)
		Info3() << " (" << pricing_time_info.MilliSeconds() << "ms)" << flush;

	// check, if too many columns generated
	double threshold = INFTY * INFTY;
	if ((int)column_price_list.size() >= MaxColumnsToAddInPricing())
	{
		// take only the best MaxColumnsToAddInPricing() of
		c_FindKthElement k_of_n_alg((int)column_price_list.size());

		list<c_ColumnPricePair>::iterator col_price;
		int idx = 0;
		for (col_price = column_price_list.begin(); col_price != column_price_list.end(); ++col_price)
			k_of_n_alg.SetElement(idx++, (*col_price).second);
		threshold = k_of_n_alg.FindKthElementOfN(MaxColumnsToAddInPricing() - 1, (int)column_price_list.size());
		if (InfoLevel() >= 3)
		{
			int to_add = 0;
			for (col_price = column_price_list.begin(); col_price != column_price_list.end(); ++col_price)
				if ((*col_price).second <= threshold)
					to_add++;
			Info3() << ", only " << to_add << " added.\n";
		}
	}
	else
		if (InfoLevel() >= 3)
			Info3() << ".\n";

	// Transfer Columns from pairs to single columns
	col_list.clear();

	list<c_ColumnPricePair>::iterator col_price;
	for (col_price = column_price_list.begin();
		col_price != column_price_list.end();
		++col_price)
		if ((*col_price).second <= threshold)
			col_list.push_back((*col_price).first);
		else
			delete (*col_price).first;
	column_price_list.clear();
}
void c_ControllerCVRPTW::createREFs()
{
	
	m_REFs = c_Matrix<c_REF_CVRPTW*>(NumVertices(), NumVertices(), PlanningHorizon());
	m_REFs.Initialisierung(NULL);

	//if( NetworkOfTasks() ){
		//NetworkOfTasks(): one vertex for every customer and day -> demand-delivery-task
		//NetworkOfScheduleParts(): one vertex for every customer and schedule part -> allowed demand fulfillment together
		int v = NumVertices();
		for (int i=0; i<v; i++) 
			for (int j=0; j<v; j++)
				for( int d = 0; d < PlanningHorizon(); d++)
					if ( ArcExistsOnDay(i,j,d) )
						m_REFs(i,j,d) = new c_REF_CVRPTW( i, j, d, *this );
	//}
	//else if( NetworkOfScheduleParts() ){
	//	//one vertex for every schedule part
	//	int n = NumNodes();
	//	for (int i=0; i<n; i++){ 
	//		for (int j=0; j<n; j++){
	//			if( ArcExists(i,j) ){ //a.o. different nodes
	//				for( int d = 0; d < PlanningHorizon(); d++){
	//					for each( int sPartI in GetAllowedScheduleParts(i) ){
	//						if( GetSchedulePart(sPartI)->getStartDay() == d ){
	//							for each( int sPartJ in GetAllowedScheduleParts(j) ){
	//								if( GetSchedulePart(sPartJ)->getStartDay() == d ){
	//									//same day is ensured, make sure that different customers
	//									//create REF, start and end vertex have to get id
	//									m_REFs(sPartI, sPartJ, d) = new c_REF_CVRPTW( sPartI, sPartJ, d, *this );
	//								}
	//							}
	//						}
	//					}
	//				}
	//			}
	//		}
	//	}
	//}
}

double c_ControllerCVRPTW::calculateCostOfRoute(const vector<int>& route)
{
	double cost = 0;
	//Attention: GetNode is incorrect for the taskNetwork, since route is here in schedulePartIds
	//route contains not start and end depot, thus add costs of these two arcs
	cost += Cost( OrigDepot(), GetSchedulePart(route[0])->getCustomer() );
	cost += Cost( GetSchedulePart(route[route.size() -1])->getCustomer(), DestDepot() );
	for ( int i = 0; i < (int)route.size()-1; i++){
		cost += Cost( GetSchedulePart(route[i])->getCustomer(), GetSchedulePart(route[i+1])->getCustomer() );
	}
	return cost;
}

int c_ControllerCVRPTW::getNumArcs()
{
	int cnt_arcs = 0;
	int v = NumVertices();
	for (int i=0; i<v; i++) 
		for (int j=0; j<v; j++)
			for( int d = 0; d < PlanningHorizon(); d++)
				if ( ArcExistsOnDay(i,j,d) )
					cnt_arcs++;
	return cnt_arcs;
}

void c_ControllerCVRPTW::BreakSymmetryByScheduleSelection()
{
	int minNumVisits = PlanningHorizon();
	//int maxFrequence = 0;
	//double maxDemand = 0;
	double maxDistToDepot = 0;
	int cust = 0;
	for( int i = 1; i <= NumCustomers(); i++){
		/*if( GetAllowedSchedules(i).size() > maxFrequence ){
		maxFrequence = (int) GetAllowedSchedules(i).size();
		maxDemand = DemandPerDay(i);
		cust = i;

		}
		if( GetAllowedSchedules(i).size() == maxFrequence && DemandPerDay(i) > maxDemand ){
		maxFrequence = (int) GetAllowedSchedules(i).size();
		maxDemand = DemandPerDay(i);
		cust = i;
		}*/
		if( Frequency(i) < minNumVisits ){
			minNumVisits = Frequency(i);
			maxDistToDepot = DemandPerDay(i);
			cust = i;

		}
		/*if( Frequency(i) == minNumVisits && DemandPerDay(i) > maxDemand ){
		minNumVisits = Frequency(i);
		maxDemand = DemandPerDay(i);
		cust = i;
		}*/
		if( Frequency(i) == minNumVisits && getDistToDepot(i) > maxDistToDepot ){
			minNumVisits = Frequency(i);
			maxDistToDepot = getDistToDepot(i);
			cust = i;
		}
	}
	FixCustToFirstDay(cust);
	fixedCustomer = cust;
	if( InfoLevel() >= 1 ){
		cout << "Customer " << cust << " was assigned to a fixed schedule to break the symmetry." << endl;
	}
}

vector<int> c_ControllerCVRPTW::SwitchRouteToOtherDay(const vector<int>& tour, int numDaysToShift)
{
	vector<int> shiftedTour = vector<int>(tour.size());
	for( int i = 0; i < (int)tour.size(); i++){
		if( GetNode(tour[i]) == GetFixedCustomer() ){
			//route which includes fixed customer cannot be shifted
			shiftedTour.clear();
			return shiftedTour;
		}

		if( taskNetwork ){
			shiftedTour[i] = GetVertex( GetNode(tour[i]), (GetDay(tour[i]) + numDaysToShift)%PlanningHorizon());
		} else if (schedulePartNetwork ){
			shiftedTour[i] = GetVertex( GetNode(tour[i]), (GetDay(tour[i]) + numDaysToShift)%PlanningHorizon(), ( GetSchedulePart(tour[i])->getEndDay() + numDaysToShift)%PlanningHorizon() );
		}
	}
	return shiftedTour;
}

vector<int> c_ControllerCVRPTW::SwitchSubsetToOtherDay(const vector<int>& subset, int numDaysToShift)
{
	vector<int> shiftedSubset = vector<int>(subset.size());
	for( int i = 0; i < (int)subset.size(); i++){
		shiftedSubset[i] = GetTask( GetNodeOfTask(subset[i]), GetDayOfTask(subset[i]) + numDaysToShift)%PlanningHorizon();
	}
	return shiftedSubset;
}

int c_ControllerCVRPTW::GetSchedulePartShiftedToFirstPeriod(int spId)
{
	int shiftedSPId = GetSchedulePartId(GetSchedulePart(spId)->getCustomer(), 0, GetNumTasksCoveredBySP(spId)-1);
	return shiftedSPId;
}

bool c_ControllerCVRPTW::Disaggregate()
{
	c_RMP_CVRPTW* rmp = (c_RMP_CVRPTW*)RMP();
	double objBeforeDisaggregation = rmp->ActualObjectiveValue();
	cout << "Objective function value before disaggregation: " << objBeforeDisaggregation << endl;

	if( InfoLevel() >= 2 )
		rmp->write("AggregatedModel.lp", "LP");

	//copy active tour columns to other days (for accessing variable values, this must be done before modifying the model)
	list<c_Column*> col_list_toAdd;
	double x;
	for ( int i=0; i<rmp->numcols(); i++ ){
		rmp->getX( &x, i, i );
		if ( x > CG_EPS*CG_EPS ){
			c_BaseColumnCVRPTW* col = (c_BaseColumnCVRPTW*) ColumnInRMP( i );
			if ( !col->IsSystemColumn() ){
				if ( col->Type() == c_BaseColumnCVRPTW::eTOUR ){
					c_TourColumnCVRPTW* col_tour = (c_TourColumnCVRPTW*) col;
					vector<int> route = col_tour->Route();
					//if( !BreakSymmetry() || !v_contains(route, GetFixedCustomer() )){ //not valid to break symmetry during aggregation
						for( int d = 1; d < PlanningHorizon(); d++){ 
							vector<int> shiftedRoute = SwitchRouteToOtherDay(route, d);
							c_TourColumnCVRPTW* new_col = new c_TourColumnCVRPTW(this, shiftedRoute, col_tour->c(), d, col_tour->Duration() );
							col_list_toAdd.push_back(new_col);
						}
					//}
				}
			}
		}
	}

	if( !RegularVisits() ){
		//TODO: Schedule columns need to reset their coefficents, so delete them and add them again (there is no easier way for update)
		//needs to be done before aggregaion-boolean is set to false
		list<c_Column*> col_list_toDelete;
		for ( int i=0; i<rmp->numcols(); i++ ){
			c_BaseColumnCVRPTW* col = (c_BaseColumnCVRPTW*) ColumnInRMP( i );
			if ( !col->IsSystemColumn() ){
				if ( col->Type() == c_BaseColumnCVRPTW::eSCHEDULE ){
					col_list_toDelete.push_back(col);
				}
			}
		}
		TransferColumnsToPool(col_list_toDelete);
	}

	SetAggregation(false);

	//Delete Aggregated SR Cuts
	list<c_Constraint*> cut_list_toRemove;
	for (int i = 0; i < rmp->Constraints().size(); i++) {
		c_Constraint_PVRPTW* con_pvrptw = (c_Constraint_PVRPTW*) (rmp->Constraints()[i]);
		if( con_pvrptw->Type() == c_RMP_CVRPTW::eAggregatedSubsetRowCut )
			cut_list_toRemove.push_back(con_pvrptw);
	}
	rmp->RemoveConstraints( cut_list_toRemove );
	int oldNumSRCuts = NumSubsetRowCuts();
	ResetNumSubsetRowCuts();
	//rmp->write("DisaggregatedModel2.lp", "LP");
	
	if( RegularVisits() ){
		//Remove aggegrated constraints
		list<c_Constraint*> con_list_toRemove;
		for (int i = 0; i < rmp->Constraints().size(); i++) {
			c_Constraint_PVRPTW* con_pvrptw = (c_Constraint_PVRPTW*)(rmp->Constraints()[i]);
			if( con_pvrptw->Type() == c_RMP_CVRPTW::eVisitFrequency )
				con_list_toRemove.push_back(con_pvrptw);
		}
		rmp->RemoveConstraints( con_list_toRemove );
	
	//rmp->write("DisaggregatedModel1.lp", "LP");

		//Add day specific constraints
		list<c_Constraint*> con_list_toAdd;
		for ( int i=0; i<NumNodes(); i++ )
			if ( IsCustomerNode(i) )
				con_list_toAdd.push_back( new c_GlobalValidConstraint_PVRPTW( this, 'G', 1.0, 0.0, c_RMP_CVRPTW::eCOVERING, i ) );
		for ( int i=0; i<NumNodes(); i++ )
			if ( IsCustomerNode(i) )
				for ( auto sPart=GetAllowedScheduleParts(i).begin();sPart!= GetAllowedScheduleParts(i).end();sPart++)
					con_list_toAdd.push_back( new c_GlobalValidConstraint_PVRPTW( this, 'G', 0.0, 0.0, c_RMP_CVRPTW::eLINKING, *sPart) );
		rmp->AddConstraints(con_list_toAdd);
		
		
		//rmp->write("DisaggregatedModel3.lp", "LP");
	} 
	list<c_Constraint*> cut_list_toAdd;
	//Add shifted subset-row cuts (of active cuts) to obtain same objective function value
	for( int cutId = 0; cutId < oldNumSRCuts * PlanningHorizon(); cutId++ ){
		vector<int> subset = getSRIs()[cutId];
		cut_list_toAdd.push_back( new c_SubsetRowCutConstraintCVRPTW(this, subset ) );
	}
	rmp->AddConstraints(cut_list_toAdd);

	//Add Schedule columns
	for ( int i=0; i<NumNodes(); i++ ){
		if ( IsCustomerNode(i) ){
			int scheduleId = 0;
			for (auto schedule = GetAllowedSchedules(i).begin(); schedule != GetAllowedSchedules(i).end(); schedule++) {
				col_list_toAdd.push_back( new c_ScheduleColumnCVRPTW( this, i, scheduleId, *schedule) );
				scheduleId++;
			}
		}
	}

	//Change upper bound of numVehicleColumn for the first day (before it was TotalNumVehicles)
	//this column is the first after the dummy columns, thus index is #customer
	int* indices = new int[1];
	indices[0] = NumCustomers();
	char* boundNames = new char[1];
	boundNames[0] = 'U';
	double* bounds = new double[1];
	bounds[0] = NumVehiclesPerDay();
	rmp->chgbds(1, indices, boundNames, bounds );
	//rmp->write("DisaggregatedModel4.lp", "LP");

	AddColumns(col_list_toAdd);
	//rmp->write("DisaggregatedModel5.lp", "LP");

	//reoptimize
	rmp->Optimize( c_RMP::eDUALOPT );

	if( InfoLevel() >= 1 )
		cout << "Objective function value after disaggregation: " << rmp->ActualObjectiveValue() << endl;
	if( InfoLevel() >= 2 ){
		rmp->write("DisaggregatedModel.lp", "LP");
		cout << *rmp << endl;
	}

	if( abs(objBeforeDisaggregation - rmp->ActualObjectiveValue()) > 0.001 ){
		cout << "Error. Objective function value changed through disaggregation." << endl;
		throw;
	}

	rmp->UpdateFlows();
	//if solution is integral, no branching is necessary. Set new optimal solution!
	if( rmp->IsFeasible() && rmp->IsIntegral() ){
		double result = rmp->ActualObjectiveValue();
		cout << "Solution after disaggregation is feasible. Optimal solution found with objective function value " << result << "." << endl;
		if ( ( result - UB() ) < CG_EPS ){ //  new best solution found
			// Store  integral Solution
			SetBestSolution( CreateSolution( rmp, NULL /* no dual info available */ ) );
			if ( InfoLevel() >= 1 ){
				if ( BestSolution() && InfoPrintBestSolution() )
					Info1() << BestSolution() << flush;
			}
		}
		return true;
	}
	return false;
}

bool c_ControllerCVRPTW::ParameterCombinationFeasible()
{
	if( StartWithAggregation() ){
		if( UseMIPSolverForUBs() ){
			return false;
		}
	}
	//vollst�ndiges Set unregelm��iger Schedules ist auch symmetrisch, so BreakSymmetry and AddColumnsOnOtherDay does not contradict with !RegularVisits
	return true;
}



bool c_ControllerCVRPTW::SolveMIPWithOriginalMP()
{
	if( UseMIPSolverAtTheEnd() == 0 ){
		return false;
	}
	//Reset RMP, keep all generated paths, to start mip-solver
	cout << "Starting MIP-Solver to search for better upper bound..." << endl;
	c_RMP_CVRPTW* rmp = (c_RMP_CVRPTW*) RMP();

	if(InfoLevel() > 1){
		rmp->write("ActualModel.lp", "LP");
	}

	if( IsAggregated() ){
		Disaggregate();
	}

	//delete branch-and-bound constraints (SumNumTrains & BranchOnEdge-Enforced)
	int cntAlreadyDeleted = 0; 
	for (auto con = rmp->Constraints().begin(); con != rmp->Constraints().end();con++) {
		int index = (*con)->Index();
		if(!(*con)->IsGloballyValid()){
			rmp->delrows(index-cntAlreadyDeleted, index-cntAlreadyDeleted); 
			cntAlreadyDeleted++;
		}
	}

	//Reset lower and upper bounds of all variables
	int numCols = rmp->NumCols();
	int* indices = new int[numCols];
	char* lu = new char[numCols];
	double* val = new double[numCols];

	//Setze lb wieder auf 0
	for(int i = 0; i < numCols; i++){
		c_Column* col = ColumnInRMP(i);
		indices[i] = i;
		lu[i] = 'L';
		val[i] = col->DefaultLowerBound();
	}
	rmp->chgbds( numCols, indices, lu, val);
	//Setze ub wieder auf Default-UpperBound
	for(int i = 0; i < numCols; i++){
		c_Column* col = ColumnInRMP(i);
		lu[i] = 'U';
		val[i] = col->DefaultUpperBound();
	}
	rmp->chgbds( numCols, indices, lu, val);
	delete[] indices;
	delete[] lu;
	delete[] val;


	//set time limit
	CPXENVptr env = (CPXENVptr) CPLEXEnv()->getEnv();
	//CPXsetintparam(env, CPX_PARAM_MIPEMPHASIS, CPX_MIPEMPHASIS_FEASIBILITY);
	cout << "Maximal time for mip-solving: " << MIPMaxSolutionTimeSec() << endl;
	CPXsetdblparam(env, CPX_PARAM_TILIM, (double)MIPMaxSolutionTimeSec()); 
	CPXsetintparam(env, CPX_PARAM_THREADS, 1);
	CPXsetintparam(env, CPX_PARAM_SCRIND, CPX_ON);

	if(InfoLevel() > 1){
		rmp->write("MIPModel.lp", "LP");
	}
	clock_t zeit1= clock();
	//optimize
	rmp->OptimizeMIP(c_RMP::ePRIMOPT);

	double result = rmp->mipobjval();
	clock_t zeit2= clock();
	cout << "MIP-Solver at the end has run for " << (zeit2-zeit1)/1000 << " seconds and found this UB:" << result << endl;
	// quality?
	if ( ( result - UB() ) < -CG_EPS ){ //  new best solution found
		// Store MIP-Solution
		SetBestSolution( CreateSolution( rmp, NULL /* no dual info available */ ) );
		return true;
	} else {
		cout << "MIP-Solver found no improving upper bound." << endl;
		return false;
	}
}


bool c_ControllerCVRPTW::GreedyRepair()
{
	//start from best found solution, especially if it includes dummies
	c_SolutionCVRPTW* sol = (c_SolutionCVRPTW*) BestSolution();

	vector<int> dummies;
	vector<vector<int>> spIdsPerCustomer = vector<vector<int>>( NumNodes() );
	vector<vector<bool>> spIdsPerCustomerCovered = vector<vector<bool>>( NumNodes() );
	//determine the chosen schedule parts per customer and realize which customers are missing
	for ( int i=1; i<(int) sol->getSchedules().size()-1; i++ )
	{
		const vector<bool>& schedule = sol->getSchedules()[i];
		if( schedule.size() == 0 ){
			dummies.push_back(i);
		} else{
			for( int p = 0; p < schedule.size(); p++){
				if( schedule[p] == true ){
					int endDay = p;
					int day = p;
					day = (day+1)%PlanningHorizon();
					while( schedule[day] == false ){
						endDay = day;
						day = (day+1)%PlanningHorizon();
					}
					spIdsPerCustomer[i].push_back( GetSchedulePartId( i, p, endDay) );
					spIdsPerCustomerCovered[i].push_back(false);
				}
			}
		}
	}

	//routes of schedule parts
	vector<vector<int>> routes = vector<vector<int>>(sol->getRoutes().size());
	vector<int> dayOfRoutes; // = vector<int>(sol->getRoutes().size());
	for ( int i=0; i<(int) sol->getRoutes().size(); i++ ){
		routes[i] = sol->getRoutes()[i].first;
	}


	double cost = 0.0;
	for (auto route = routes.begin(); route != routes.end();route++) {
		dayOfRoutes.push_back( GetSchedulePart((*route)[0])->getStartDay() );
		cost += calculateCostOfRoute( *route );
	}
	cout << "Actual routing costs: " << cost << endl;

	//delete all unnecessary customer visits from the routes
	//for the moment: accept the first covering of each chosen schedule part and delete further occurances
	//for each( vector<int>& route in routes ){
	for( int r = 0; r < routes.size(); r++){
		vector<int> route = routes[r];
		for( int t = 0; t < route.size(); t++ ){
			int cust = GetSchedulePart( route[t] )->getCustomer();
			//find the schedule part of the route -> if it is not already covered, remember this now, otherwise delete this schedule part from the route
			bool schedulePartBelongsToSchedule = false;
			for( int p = 0; p < spIdsPerCustomer[cust].size(); p++ ){
				if( spIdsPerCustomer[cust][p] == route[t] ){
					schedulePartBelongsToSchedule = true;
					if( spIdsPerCustomerCovered[cust][p] ){
						route.erase( route.begin() + t );
						routes[r] = route;
					} else{
						spIdsPerCustomerCovered[cust][p] = true;
					}
				}
			}
			if( !schedulePartBelongsToSchedule ){
				route.erase( route.begin() + t );
				routes[r] = route;
			}
		}
	}
	
	cost = 0.0;
	for (auto route = routes.begin(); route != routes.end();route++ ) {
		cost += calculateCostOfRoute( *route );
	}
	cout << "Routing costs after removal of duplicates: " << cost << endl;

	//try to insert the missing dummy customer visits in one of the routes (etwas umst�ndlich um scheduleIds zu bekommen ->sollte abgespeichert werden)
	vector<vector<int>> new_routes;
	vector<int> new_route;
	vector<int> indicesOfModifiedRoutes;
	int successfullyInserted = 0;
	for ( int i=0; i< NumNodes(); i++ ){
		if ( IsCustomerNode(i) ){
			int scheduleId = 0;
			for (auto schedule = GetAllowedSchedules(i).begin(); schedule != GetAllowedSchedules(i).end();schedule++) {
				if( v_contains(dummies, i) ){
					bool schedulePartInserted;
					//try every possible schedule
					for (auto spId = GetScheduleParts(i, scheduleId).begin(); spId != GetScheduleParts(i, scheduleId).end();spId++) {
						schedulePartInserted = false;
						for (int r = 0; r < routes.size(); r++ ){
							if( dayOfRoutes[r] == GetSchedulePart(*spId)->getStartDay() ){
								//try insertion at every place
								for( int p = 0; p < routes[r].size(); p++ ){
									new_route.clear();
									//insert spId before position p in the current route
									for( int p1 = 0; p1 < p; p1++ ){
										new_route.push_back( routes[r][p1] );
									}
									new_route.push_back(*spId);
									for( int p2 = p; p2 < routes[r].size(); p2++ ){
										new_route.push_back( routes[r][p2] );
									}
									if( IsRouteFeasible(new_route, dayOfRoutes[r]) ){
										schedulePartInserted = true;
										indicesOfModifiedRoutes.push_back(r);
										new_routes.push_back( new_route );
										break;
									}
								}
							}
						}
						if( schedulePartInserted == false ){
							//this schedule does not work, don't try the other schedule parts belonging to this schedule
							break;
						}
					}
					if( schedulePartInserted == true ){
						//all schedule parts of this schedule could be inserted
						successfullyInserted++;
						for( int k = 0; k < indicesOfModifiedRoutes.size(); k++ ){
							routes[ indicesOfModifiedRoutes[k] ] = new_route;
						}
						break;
					}

				}
				scheduleId++;
			}
		}
	}

	cost = 0.0;
	for (auto route = routes.begin();route!=routes.end();route++){
		cost += calculateCostOfRoute( *route );
	}

	if( successfullyInserted == dummies.size() ){
		cout << "Feasible solution found!" << endl;
		cout << "Final routing costs: " << cost << endl;
		return true;
	} else{
		cout << "No feasible solution found." << endl;
		return false;
	}
}

bool c_ControllerCVRPTW::IsRouteFeasible(vector<int>& route, int day)
{
	double i_load = 0;
	int i_time = 0; //earliest possible visit time
	int i_dur = 0; //minimum tour duration
	int i_help = 0;
	route.push_back( DestDepotVertex( day ) );
	int tailSPId = OrigDepotVertex( day );
	int tailCust = 0;
	for( int t = 0; t < route.size() ; t++ ){
		int headCust = GetSchedulePart( route[t] )->getCustomer();
		i_load = i_load + DemandSchedulePart( route[t] );
		//check for capacity
		if ( i_load > Capacity() + CGBase::CG_EPS ) 
			return false;
		i_time = max( StartTime( headCust ), i_time + Time( tailCust, headCust ) + AdditionalServiceTimeSchedulePart( tailSPId ));
		//check for time window feasibility
		if( i_time > EndTime( headCust ) )
			return false;
	
		//check for route duration
		if( minTourDuration ){ //Attention: Assumes that there is no additional service time for an increased demand fulfillment
			int old_i_dur = i_dur;
			i_dur = max( i_dur + Time( tailCust, headCust ), i_help + StartTime( headCust ) );
			if( i_dur > MaxTourDuration() )
				return false;
			i_help = max( old_i_dur + Time( tailCust, headCust ) - EndTime(headCust), i_help );
			if ( i_help < - EndTime( OrigDepot() ) )
				return false;
		} 
		tailCust = headCust;
		tailSPId = route[t];
	}
	return true;
}

/////////////////////////////////////////////////////////////////////////////
//   c_DummyColumnCVRP
/////////////////////////////////////////////////////////////////////////////

c_DummyColumnCVRPTW::c_DummyColumnCVRPTW( c_Controller* controller, int node )
:	c_BaseColumnCVRPTW( controller, INFTY ),
	i_node(node)
{
	c_ControllerCVRPTW* ctlr = (c_ControllerCVRPTW*) controller;
	d_cost = INFTY;
}


int c_DummyColumnCVRPTW::GetCPXColumn( double& cost, int* ind, double* val, double& lb, double& ub, char* name )
{
	c_ControllerCVRPTW* ctlr = (c_ControllerCVRPTW*) Controller();
	cost = c();
	int idx = 0;

	if( ctlr->IsAggregated() && ctlr->RegularVisits() ) {
		ind[idx] = ctlr->RMP()->ConstraintIndex( c_RMP_CVRPTW::eVisitFrequency, i_node );
		val[idx++] = ctlr->PlanningHorizon(); // ctlr->Frequency(i_node);
	} else{
		ind[idx] = ctlr->RMP()->ConstraintIndex( c_RMP_CVRPTW::eCOVERING, i_node );
		val[idx++] = 1.0;
	}

	lb = 0.0;
	ub = CPX_INFBOUND;
	return idx;
}


void c_DummyColumnCVRPTW::OutputInOStream( ostream& s ) const
{
	s << "Dummy( " << i_node << ") ";
}


/////////////////////////////////////////////////////////////////////////////
//   c_TourColumnCVRP
/////////////////////////////////////////////////////////////////////////////


c_TourColumnCVRPTW::c_TourColumnCVRPTW(c_Controller* controller, const vector<int>& route, double cost, int day, int duration /*= 0*/, bool CustomersInsteadOfVertices /*= false*/ )
:	c_BaseColumnCVRPTW( controller,cost ),
	v_route( route ),
	i_day(day),
	i_duration( duration )
{
	c_ControllerCVRPTW* ctlr = (c_ControllerCVRPTW*) Controller();
	if( CustomersInsteadOfVertices ){
		for ( int t=1; t<(int)v_route.size()-1; t++ ){
			v_routeOfSchedulePartIds.push_back( ctlr->GetSchedulePartId(v_route[t], i_day, -1));
		}
		if( ctlr->NetworkOfTasks() ){
			v_route.clear();
			v_route.push_back( ctlr->OrigDepotVertex() );
			for ( int t=0; t<(int)v_routeOfSchedulePartIds.size(); t++ ){
				int day = ctlr->GetSchedulePart(v_routeOfSchedulePartIds[t])->getStartDay();
				int customer = ctlr->GetSchedulePart(v_routeOfSchedulePartIds[t])->getCustomer();
				v_route.push_back( ctlr->GetVertex( customer, day) );
				while( day !=  ctlr->GetSchedulePart(v_routeOfSchedulePartIds[t])->getEndDay() ){
					day = (day +1)%ctlr->PlanningHorizon();
					v_route.push_back( ctlr->GetVertex( customer, day) );
				}
			}
			v_route.push_back( ctlr->DestDepotVertex() );
		}
	} else if( ctlr->NetworkOfTasks() ){
		int customer = -1;
		for ( int t=1; t<(int)v_route.size()-1; t++ ){
		//for ( int t=0; t<(int)v_route.size(); t++ ){
			if( ctlr->GetDay(v_route[t]) == i_day ){
				//start of customer visit found
				customer = ctlr->GetNode(v_route[t]);
				//determine end period of demand fulfillment
				while( ctlr->GetNode(v_route[t+1]) == customer )
					t++;

				v_routeOfSchedulePartIds.push_back( ctlr->GetSchedulePartId(customer, i_day, ctlr->GetDay(v_route[t])) );
			}
		}
		v_routeOfTasks = v_route;
	} else if( ctlr->NetworkOfScheduleParts() ){
		for ( int t=1; t<(int)v_route.size()-1; t++ ){
		//for ( int t=0; t<(int)v_route.size(); t++ ){
			v_routeOfSchedulePartIds.push_back( v_route[t] );
		}
		for (auto spId = v_route.begin(); spId != route.end();spId++ ) {
			int cust = ctlr->GetSchedulePart( *spId )->getCustomer();
			int d = ctlr->GetSchedulePart( *spId )->getStartDay();
			v_routeOfTasks.push_back( ctlr->GetTask(cust, d) );
			int endDay =  ctlr->GetSchedulePart( *spId )->getEndDay();
			while ( d != endDay) {
				 d = (d+1)%ctlr->PlanningHorizon();
				 v_routeOfTasks.push_back( ctlr->GetTask(cust, d) );
			} 
		}
	}
	v_routeNodes = vector<int>(v_routeOfSchedulePartIds.size() + 2 ); //start and end depot additionally
	v_routeNodes[0] = ctlr->OrigDepot();
	for ( int p = 0; p < v_routeOfSchedulePartIds.size(); p++ ){
		v_routeNodes[p+1] = ctlr->GetSchedulePart(v_routeOfSchedulePartIds[p])->getCustomer();
	}
	v_routeNodes[v_routeNodes.size()-1] = ctlr->DestDepot();
}

bool c_TourColumnCVRPTW::visits(int customer)
{
	if (find (v_route.begin(),v_route.end(),customer)==v_route.end()){
		return false;}
	else { 
		return true;
	}
}

bool c_TourColumnCVRPTW::IsFeasible() const
{
	return true;
}

// ind = indices of non zeros
// val = values at these positions
int c_TourColumnCVRPTW::GetCPXColumn( double& cost, int* ind, double* val, double& lb, double& ub, char* name )
{
	c_ControllerCVRPTW* ctlr = (c_ControllerCVRPTW*) Controller();
	cost = c();
	int idx = 0;
	
	if( ctlr->IsAggregated() && ctlr->RegularVisits() ){
		//create map based on customers instead of schedule parts because with NG, a customer can for non-standard instances be visited several times with different schedule parts 
		//and cplex does not allow two coefficients for the same row
		map<int,int> cnt_visits; //customer is key, number of visits is value
		for (auto SPId = v_routeOfSchedulePartIds.begin(); SPId != v_routeOfSchedulePartIds.end();SPId++) {
			cnt_visits[ctlr->GetSchedulePart(*SPId)->getCustomer()] += ctlr->GetNumTasksCoveredBySP(*SPId);
		}
		for ( auto ii=cnt_visits.begin(); ii!=cnt_visits.end(); ++ii ){
			//consider this tour for visit frequency requirement
			ind[idx] = ctlr->RMP()->ConstraintIndex( c_RMP_CVRPTW::eVisitFrequency, ii->first );
			val[idx++] = ii->second;
		}
	} else{
		//If the model is aggregated, only routes for day 0 exists, so only schedule parts starting at day 0 are active. The schedule columns have to accept schedule parts that cover the correct number of tasks
		// determine entries for covering constraints
		map<int,int> cnt_visits; //schedulePartId is key, number of visits is value
		for (auto SPId = v_routeOfSchedulePartIds.begin(); SPId != v_routeOfSchedulePartIds.end(); SPId++) {
			cnt_visits[*SPId]++;
		}
		for ( auto ii=cnt_visits.begin(); ii!=cnt_visits.end(); ++ii ){
			////link tour columns with scheduleParts.
			ind[idx] = ctlr->RMP()->ConstraintIndex( c_RMP_CVRPTW::eLINKING, ii->first );
			val[idx++] = ii->second;
		}
	}
	// one convexity constraint
	ind[idx] = ctlr->RMP()->ConstraintIndex( c_RMP_CVRPTW::eCONVEXITY, i_day );
	val[idx++] = +1.0;

	//Set coefficients in SR cuts
	if( ctlr->UseSubsetRowCuts() ){
		//if( ctlr->NetworkOfTasks() ){
		if ( ctlr->IsAggregated() ){
			for ( int cutId = 0; cutId < ctlr->NumSubsetRowCuts(); cutId++ ){ 
				if( ctlr->RMP()->ConstraintIndexExists( c_RMP_CVRPTW::eAggregatedSubsetRowCut, cutId ) ){
					c_AggregatedSubsetRowCutConstraintCVRPTW* cut = (c_AggregatedSubsetRowCutConstraintCVRPTW*) ctlr->RMP()->Constraint( c_RMP_CVRPTW::eAggregatedSubsetRowCut, cutId );
					//count visits of this tour column of customer-vertices in cut subset 	
					vector<double> alphasum = vector<double>(ctlr->PlanningHorizon()); 	
					int coef = 0;
					for( int d = 0; d <ctlr->PlanningHorizon(); d++){
						alphasum[d] = 0;
						for (auto cust = cut->GetSubset(d).begin(); cust != cut->GetSubset(d).end();cust++) {
							alphasum[d] += countVisits(*cust);
						}
						coef += floor(alphasum[d]/2);
					}					

					if( coef >= 1 ){
						ind[idx] = ctlr->RMP()->ConstraintIndex( c_RMP_CVRPTW::eAggregatedSubsetRowCut, cutId );
						val[idx++] = coef; //calculate coefficient by dividing by 2 and flooring
					}
				}				
			}
		} else{
			for ( int cutId = 0; cutId < ctlr->NumSubsetRowCuts(); cutId++ ){ 
				if( ctlr->RMP()->ConstraintIndexExists( c_RMP_CVRPTW::eSubsetRowCut, cutId ) ){
					c_SubsetRowCutConstraintCVRPTW* cut = (c_SubsetRowCutConstraintCVRPTW*) ctlr->RMP()->Constraint( c_RMP_CVRPTW::eSubsetRowCut, cutId );
					//count visits of this tour column of customer-vertices in cut subset 										
					double alphasum = 0; 			
					for (auto cust = cut->GetSubset().begin(); cust != cut->GetSubset().end();cust++)
						alphasum += countVisits(*cust);
	
					if( alphasum >= 2 ){
						ind[idx] = ctlr->RMP()->ConstraintIndex( c_RMP_CVRPTW::eSubsetRowCut, cutId );
						val[idx++] = floor(alphasum/2); //calculate coefficient by dividing by 2 and flooring
					}
				}				
			}
		}
		/*} else if( ctlr->NetworkOfScheduleParts() ){
			cerr << "SR-Cuts don't work with NetworkOfScheduleParts so far." << endl;
			throw;
		} */
	}

	//Set coefficients in Capacity Cuts
	if( ctlr->NumCapacityCuts() > 0 ){
		for (int cutId = 0; cutId < ctlr->NumCapacityCuts(); cutId++){
			if( ctlr->RMP()->ConstraintIndexExists( c_RMP_CVRPTW::eCapacityCut, cutId ) ){
				c_CapacityCutConstraintCVRPTW* capCut = (c_CapacityCutConstraintCVRPTW*) ctlr->RMP()->Constraint(c_RMP_CVRPTW::eCapacityCut, cutId);
				int numSubsetLeavings = 0;
				for( int p = 0; p < v_route.size(); p++ ){
					if( v_contains(capCut->GetCustomerSubset(), v_route[p]) && !v_contains(capCut->GetCustomerSubset(), v_route[p+1]) )
						numSubsetLeavings++;
				}
				if( numSubsetLeavings > 0 ){
					ind[idx] = ctlr->RMP()->ConstraintIndex( c_RMP_CVRPTW::eCapacityCut, cutId );
					val[idx++] = numSubsetLeavings;
				}
			}
		}
	}

	//Arc Set constraints
	for (int i = 0; i < ctlr->NumArcSetConstraints(); i++) {
		if (ctlr->RMP()->ConstraintIndexExists(c_RMP_CVRPTW::eARCSET, i)) {
			c_ArcSetConstraint* constraint = (c_ArcSetConstraint*)ctlr->RMP()->Constraint(c_RMP_CVRPTW::eARCSET, i);
			if( constraint->Day() == i_day ){
				int coeff = 0;
				for (int t = 1; t < (int)v_route.size(); t++)
					coeff += constraint->Coefficient(ctlr->GetNode(v_route[t - 1]), ctlr->GetNode(v_route[t]));
				if (coeff != 0) {
					ind[idx] = ctlr->RMP()->ConstraintIndex(c_RMP_CVRPTW::eARCSET, i);
					val[idx++] = coeff;
				}
			}
		}
	}


	// bounds
	lb = 0.0;
	ub = CPX_INFBOUND;
	return idx;
}


void c_TourColumnCVRPTW::OutputInOStream( ostream& s ) const
{
	c_ControllerCVRPTW* ctlr = (c_ControllerCVRPTW*) Controller();
	s << "Route on day =" << i_day << ", [ ";
	if( ctlr->RegularVisits() ){ //order of customers gives complete information

		////TODO: Just for testing task network
		//for ( int t=0; t<(int)v_route.size(); t++ ){
		//	s << v_route[t] << ","; 
		//}

		s << "0,";
		for ( int t=0; t<(int)v_routeOfSchedulePartIds.size(); t++ ){
			s << ctlr->GetSchedulePart(v_routeOfSchedulePartIds[t])->getCustomer() << ","; 
		}
		s << ctlr->DestDepot() << ",";
	} else{
		//write order of customers and their visit end day
		s << "0,";
		for ( int t=0; t<(int)v_routeOfSchedulePartIds.size(); t++ ){
			s << ctlr->GetSchedulePart(v_routeOfSchedulePartIds[t])->getCustomer() << "_" << ctlr->GetSchedulePart(v_routeOfSchedulePartIds[t])->getEndDay() << ","; 
		}
		s << ctlr->DestDepot() << ",";
	}
	s << "], with cost =" << c() << ", duration =" << i_duration << ",";
}


int c_TourColumnCVRPTW::countVisits(int task)
{
	return (int) count( v_routeOfTasks.begin(), v_routeOfTasks.end(), task);
	/*
	c_ControllerCVRPTW* ctlr = (c_ControllerCVRPTW*) Controller();
	if( taskNetwork ){
	int task = ctlr->GetVertex(task, day);
	return (int) count(this->v_route.begin(), this->v_route.end(), task);
	} else if( schedulePartNetwork ){
	int counter = 0;
	for each( int spId in v_route ){
	if ( ctlr->GetSchedulePart(spId)->getCustomer() == task ){
	if( ctlr->GetSchedulePart(spId)->containsDay( day ) )
	counter++;
	}
	}
	return counter;
	throw;
	} else{
	cout << "Error. countVisits not implemented for neither task nor schedule part network" << endl;
	throw;
	}*/
}

/////////////////////////////////////////////////////////////////////////////
//   c_NumVehicleColumnCVRP
/////////////////////////////////////////////////////////////////////////////

c_NumVehicleColumnCVRPTW::c_NumVehicleColumnCVRPTW(c_Controller* controller, int MaxNumVehicles, int day)
	:	c_BaseColumnCVRPTW( controller, 0.0 ),
	i_MaxNumVehicles(MaxNumVehicles),
	i_day( day )
{}


double c_NumVehicleColumnCVRPTW::DefaultUpperBound()
{
	//Attention: Column is not re-checked after disaggregation such that upper bound of first day vehicle column stays too high -> thus, an extra constraint is added? pay attention on dual price //TODO! or chgbds (int cnt, int indices[], char lu[], double bd[] ); ?
	if ( ((c_ControllerCVRPTW*)Controller())->IsAggregated() && i_day == 0){
		return i_MaxNumVehicles * ((c_ControllerCVRPTW*)Controller())->PlanningHorizon();
	} else{
		return i_MaxNumVehicles;
	}
}

int c_NumVehicleColumnCVRPTW::GetCPXColumn(double& cost, int* ind, double* val, double& lb, double& ub, char* name)
{
	c_ControllerCVRPTW* ctlr = (c_ControllerCVRPTW*) Controller();
	cost = c();
	int idx = 0;

	ind[idx] = ctlr->RMP()->ConstraintIndex( c_RMP_CVRPTW::eCONVEXITY, i_day);
	val[idx++] = -1.0;

	ind[idx] = ctlr->RMP()->ConstraintIndex( c_RMP_CVRPTW::eNUMVEH);
	val[idx++] = 1.0;

	lb = 0.0;
	ub = ctlr->IsAggregated()? i_MaxNumVehicles * ctlr->PlanningHorizon() : i_MaxNumVehicles; //TODO: check if changement is realised!
	return idx;
}

void c_NumVehicleColumnCVRPTW::OutputInOStream(ostream& s) const
{
	s << "NumVeh(" << i_day << "): " ;
}


/////////////////////////////////////////////////////////////////////////////
//   c_TotalNumVehicleColumnCVRP
/////////////////////////////////////////////////////////////////////////////

c_TotalNumVehicleColumnCVRPTW::c_TotalNumVehicleColumnCVRPTW(c_Controller* controller, int MaxNumVehicles)
	:	c_BaseColumnCVRPTW( controller, 0.0 ),
	i_MaxNumVehicles(MaxNumVehicles)
{}


int c_TotalNumVehicleColumnCVRPTW::GetCPXColumn(double& cost, int* ind, double* val, double& lb, double& ub, char* name)
{
	c_ControllerCVRPTW* ctlr = (c_ControllerCVRPTW*) Controller();
	cost = c();
	int idx = 0;

	ind[idx] = ctlr->RMP()->ConstraintIndex( c_RMP_CVRPTW::eNUMVEH);
	val[idx++] = -1.0;
	lb = 0.0;
	ub = i_MaxNumVehicles;
	return idx;
}

void c_TotalNumVehicleColumnCVRPTW::OutputInOStream(ostream& s) const
{
	s << "TotalNumVeh: " ;
}

//Periodic VRP
/////////////////////////////////////////////////////////////////////////////
//   c_ScheduleColumnCVRPTW
/////////////////////////////////////////////////////////////////////////////
c_ScheduleColumnCVRPTW::c_ScheduleColumnCVRPTW(c_Controller* controller, int customer, int scheduleId, const std::vector<bool>& days)
	:	c_BaseColumnCVRPTW( controller, 0.0 ),
	v_days(days),
	customer(customer),
	sId(scheduleId)
	{}

int c_ScheduleColumnCVRPTW::GetCPXColumn(double& cost, int* ind, double* val, double& lb, double& ub, char* name)
{
	c_ControllerCVRPTW* ctlr = (c_ControllerCVRPTW*) Controller();
	cost = c();
	int idx = 0;

	ind[idx] = ctlr->RMP()->ConstraintIndex( c_RMP_CVRPTW::eCOVERING, customer);
	val[idx++] = 1.0;

	if( !ctlr->IsAggregated() ){
		for (auto sPart = ctlr->GetScheduleParts(customer, sId).begin(); sPart != ctlr->GetScheduleParts(customer, sId).end();sPart++) {
			/*ind[idx] = ctlr->RMP()->ConstraintIndex( c_RMP_CVRPTW::eLINKING, customer, ctlr->GetSchedulePart(sPart)->getStartDay(), ctlr->GetSchedulePart(sPart)->getEndDay() );*/
			ind[idx] = ctlr->RMP()->ConstraintIndex( c_RMP_CVRPTW::eLINKING, *sPart );
			val[idx++] = -1.0;
		}
	} else{
		//in case of regular schedules, the schedule columns are not necessary (not existing) while the model is aggregated (thus no error occurs)
		//every schedule part is shifted to its aquivalent on day 0
		map<int,int> cnt_visits; //schedulePartId is key, number of schedule parts with this length in the schedule is its value
		for (auto sPart = ctlr->GetScheduleParts(customer, sId).begin(); sPart != ctlr->GetScheduleParts(customer, sId).end();sPart++) {
			int sPartShiftedToFirstPeriod = ctlr->GetSchedulePartShiftedToFirstPeriod(*sPart);
			cnt_visits[sPartShiftedToFirstPeriod]++;
		}

		for ( auto ii=cnt_visits.begin(); ii!=cnt_visits.end(); ++ii ){
			////link tour columns with scheduleParts.
			ind[idx] = ctlr->RMP()->ConstraintIndex( c_RMP_CVRPTW::eLINKING, ii->first );
			val[idx++] = -ii->second;
		}

		//vector<int> v_alreadyResepectedScheduleParts = vector<int>();
		//
		//for each( int sPart in ctlr->GetScheduleParts(customer, sId) ){
		//	int sPartShiftedToFirstPeriod = ctlr->GetSchedulePartShiftedToFirstPeriod(sPart);
		//	if( !v_contains(v_alreadyResepectedScheduleParts, sPartShiftedToFirstPeriod) ){
		//		v_alreadyResepectedScheduleParts.push_back(sPartShiftedToFirstPeriod);
		//		/*ind[idx] = ctlr->RMP()->ConstraintIndex( c_RMP_CVRPTW::eLINKING, customer, ctlr->GetSchedulePart(sPart)->getStartDay(), ctlr->GetSchedulePart(sPart)->getEndDay() );*/
		//		ind[idx] = ctlr->RMP()->ConstraintIndex( c_RMP_CVRPTW::eLINKING, sPartShiftedToFirstPeriod );
		//		val[idx++] = -1.0;
		//	}
		//}
	}

	lb = 0.0;
	ub = CPX_INFBOUND;
	return idx;
}

void c_ScheduleColumnCVRPTW::OutputInOStream(ostream& s) const
{
	s << "Cust: " << customer << " Schedule: [";
	for (auto d = v_days.begin(); d != v_days.end();d++ ) {
		s << *d << ", ";
	}
	s << "]";
}


/////////////////////////////////////////////////////////////////////////////
//   c_DualVariablesMASS
/////////////////////////////////////////////////////////////////////////////

c_DualVariablesCVRPTW::c_DualVariablesCVRPTW( c_Controller* controller )
	:	c_DualVariables( controller, controller->RMP()->NumConstraints() ) {}

double c_DualVariablesCVRPTW::pi( int node ) const
{
	c_ControllerCVRPTW* ctlr = (c_ControllerCVRPTW*) Controller();
	c_DualVariablesCVRPTW* my_dual = (c_DualVariablesCVRPTW*) this;
	return my_dual->CPXDual()[ ctlr->RMP()->ConstraintIndex( c_RMP_CVRPTW::eCOVERING, node ) ];
}

double c_DualVariablesCVRPTW::rho(int spId) const
{
	c_ControllerCVRPTW* ctlr = (c_ControllerCVRPTW*) Controller();
	c_DualVariablesCVRPTW* my_dual = (c_DualVariablesCVRPTW*) this;
	return my_dual->CPXDual()[ ctlr->RMP()->ConstraintIndex( c_RMP_CVRPTW::eLINKING, spId ) ];
}

double c_DualVariablesCVRPTW::tau(int node) const
{
	c_ControllerCVRPTW* ctlr = (c_ControllerCVRPTW*) Controller();
	c_DualVariablesCVRPTW* my_dual = (c_DualVariablesCVRPTW*) this;
	return my_dual->CPXDual()[ ctlr->RMP()->ConstraintIndex( c_RMP_CVRPTW::eVisitFrequency, node ) ];
}

double c_DualVariablesCVRPTW::mu(int day) const 
{
	c_ControllerCVRPTW* ctlr = (c_ControllerCVRPTW*) Controller();
	c_DualVariablesCVRPTW* my_dual = (c_DualVariablesCVRPTW*) this;
	return my_dual->CPXDual()[ ctlr->RMP()->ConstraintIndex( c_RMP_CVRPTW::eCONVEXITY, day ) ];
}


double c_DualVariablesCVRPTW::sigma( int cutId) const 
{																
	c_ControllerCVRPTW* ctlr = (c_ControllerCVRPTW*) Controller();
	c_DualVariablesCVRPTW* my_dual = (c_DualVariablesCVRPTW*) this;
	if( ctlr->IsAggregated() ){
		return my_dual->CPXDual()[ ctlr->RMP()->ConstraintIndex( c_RMP_CVRPTW::eAggregatedSubsetRowCut, cutId ) ];
	} else{
		return my_dual->CPXDual()[ ctlr->RMP()->ConstraintIndex( c_RMP_CVRPTW::eSubsetRowCut, cutId ) ];
	}
}

double c_DualVariablesCVRPTW::phi(int cutId) const
{
	c_ControllerCVRPTW* ctlr = (c_ControllerCVRPTW*) Controller();
	c_DualVariablesCVRPTW* my_dual = (c_DualVariablesCVRPTW*) this;
	return my_dual->CPXDual()[ ctlr->RMP()->ConstraintIndex( c_RMP_CVRPTW::eCapacityCut, cutId ) ];
}

double c_DualVariablesCVRPTW::alpha(int cutId) const
{
	c_ControllerCVRPTW* ctlr = (c_ControllerCVRPTW*) Controller();
	c_DualVariablesCVRPTW* my_dual = (c_DualVariablesCVRPTW*) this;
	return my_dual->CPXDual()[ ctlr->RMP()->ConstraintIndex( c_RMP_CVRPTW::eARCSET, cutId ) ];
}

void c_DualVariablesCVRPTW::OutputInOStream( ostream& s ) const
{
	c_ControllerCVRPTW* ctlr = (c_ControllerCVRPTW*)Controller();
	s << "Dual Variables:" << endl;
	for( int d = 0; d < ctlr->PlanningHorizon(); d++){
		s << "Mu(" << d << ")= " << mu(d) << endl;
	}

	if( ctlr->IsAggregated() && ctlr->RegularVisits())
	{
		for ( int i = 0; i<ctlr->NumNodes(); i++ )
			if ( ctlr->IsCustomerNode(i) )
				s << "Tau( " << i << ")=" << tau(i)  << endl;
	} else
	{
		for ( int i = 0; i<ctlr->NumNodes(); i++ )
			if ( ctlr->IsCustomerNode(i) )
				s << "Pi( " << i << ")=" << pi(i)  << endl;

		for (int i = 0; i < ctlr->NumNodes(); i++)
			if (ctlr->IsCustomerNode(i))
				for (auto sPart = ctlr->GetAllowedScheduleParts(i).begin(); sPart != ctlr->GetAllowedScheduleParts(i).end();sPart++) {
					s << "Rho( " << *sPart << "=["<<  i << "," << ctlr->GetSchedulePart(*sPart)->getStartDay() << "," << ctlr->GetSchedulePart(*sPart)->getEndDay() << "])=";
					s << rho(*sPart)  << endl;
				}
	}
	
	if (ctlr->UseSubsetRowCuts() && ctlr->CuttingPlanes())
	{
		s << "SR-cuts: " << endl;
		for (int i = 0; i<ctlr->NumSubsetRowCuts(); i++)
		{
			int cuttype = ctlr->IsAggregated()? c_RMP_CVRPTW::eAggregatedSubsetRowCut : c_RMP_CVRPTW::eSubsetRowCut; 
			if (ctlr->RMP()->ConstraintIndexExists(cuttype, i))
			{
				c_SubsetRowCutConstraintCVRPTW* cut = (c_SubsetRowCutConstraintCVRPTW*)ctlr->RMP()->Constraint(cuttype, i);
				const vector<int>& vertices = cut->GetSubset();
				s << "Sigma( ";
				for (int a = 0; a < (int)vertices.size(); ++a)
					s << vertices[a] << ",";
				s << ") = " << sigma(i) << endl;
			}
		}
	}
}


/////////////////////////////////////////////////////////////////////////////
//   c_RMP_CVRP
/////////////////////////////////////////////////////////////////////////////

c_RMP_CVRPTW::c_RMP_CVRPTW( c_Controller* controller )
:	c_RMP( controller ),
	c_SupportCVRPTW( controller )
{}


void c_RMP_CVRPTW::Optimize( e_solver solver )
{
	c_RMP::Optimize( solver );
	SetUpdateRequired();
}


void c_RMP_CVRPTW::OptimizeMIP( e_solver solver )
{
	c_RMP::OptimizeMIP( solver );
	SetUpdateRequired();
}


void c_RMP_CVRPTW::InitializeFirstRMP() // Startmodell wird hier erstellt
{
	c_ControllerCVRPTW* ctlr = (c_ControllerCVRPTW*) c_RMP::Controller();
	// create constraints first

	if( !ctlr->StartWithAggregation() || !ctlr->RegularVisits()){
		//In case of aggregation with irregular schedules, the linking with schedule variables remains necessary, but not the days, but only the lengths of the schedule parts have to fit to a schedule
		ctlr->SetAggregation(false);
		for ( int i=0; i<ctlr->NumNodes(); i++ )
			if ( ctlr->IsCustomerNode(i) )
				AddConstraint( new c_GlobalValidConstraint_PVRPTW( ctlr, 'G', 1.0, 0.0, c_RMP_CVRPTW::eCOVERING, i ) );

		for ( int i=0; i<ctlr->NumNodes(); i++ )
			if ( ctlr->IsCustomerNode(i) )
				for  ( auto sPart = ctlr->GetAllowedScheduleParts(i).begin(); sPart != ctlr->GetAllowedScheduleParts(i).end();sPart++)
					AddConstraint( new c_GlobalValidConstraint_PVRPTW( ctlr, 'G', 0.0, 0.0, c_RMP_CVRPTW::eLINKING, *sPart)); //i, ctlr->GetSchedulePart(sPart)->getStartDay() , ctlr->GetSchedulePart(sPart)->getEndDay() ) );

	}
	for ( int d = 0; d < ctlr->PlanningHorizon(); d++ ){
		AddConstraint( new c_GlobalValidConstraint_PVRPTW( ctlr, 'E', 0.0, 0.0, c_RMP_CVRPTW::eCONVEXITY, d ) );
	}

	//Total number of vehicles
	AddConstraint( new c_GlobalValidConstraint_PVRPTW( ctlr, 'E', 0.0, 0.0, c_RMP_CVRPTW::eNUMVEH) );

	if( ctlr->StartWithAggregation() ){
		ctlr->SetAggregation(true);
		if( ctlr->RegularVisits() ){
			//No need for schedule variables, numTask-Covering is enough
			for ( int i=0; i<ctlr->NumNodes(); i++ )
				if ( ctlr->IsCustomerNode(i) )
					AddConstraint( new c_Constraint_PVRPTW( ctlr, 'G', ctlr->PlanningHorizon(), 0.0, c_RMP_CVRPTW::eVisitFrequency, i ) ); //ctlr->Frequency(i)
		} //otherwise: 
			//Schedule variables necessary, otherwise it is possible that in Lp relaxation only one schedule part of a irregular schedules is used and the remaining does not exist
			//the case is covered in the regular code		
	} 

	// CPLEX 
	ConstructEmptyRMP( CPLEXProblem::MINIMIZE );
	// dummy columns for covering	
	list<c_Column*> col_list;
	for ( int i=0; i<ctlr->NumNodes(); i++ )
		if ( ctlr->IsCustomerNode(i) )
			col_list.push_back( new c_DummyColumnCVRPTW( ctlr, i) );
	if( !ctlr->StartWithAggregation() || !ctlr->RegularVisits() ){
		// schedule columns
		for ( int i=0; i<ctlr->NumNodes(); i++ ){
			if ( ctlr->IsCustomerNode(i) ){
				int scheduleId = 0;
				for (auto schedule = ctlr->GetAllowedSchedules(i).begin(); schedule != ctlr->GetAllowedSchedules(i).end();schedule++) {
					col_list.push_back( new c_ScheduleColumnCVRPTW( ctlr, i, scheduleId, *schedule) );
					scheduleId++;
				}
			}
		}
	}
	// column for vehicle number
	for ( int d = 0; d < ctlr->PlanningHorizon(); d++ ){
		c_NumVehicleColumnCVRPTW* vehcol = new c_NumVehicleColumnCVRPTW( ctlr, ctlr->NumVehiclesPerDay(), d ); 
		col_list.push_back( vehcol );
	}

	c_TotalNumVehicleColumnCVRPTW* totalvehcol = new c_TotalNumVehicleColumnCVRPTW( ctlr, ctlr->NumberOfVehicles() );
	col_list.push_back( totalvehcol );
	////TODO: remove opt solution of C101 with schedule_12
	/*static const int arr1[] = {0,5,3,7,8,10,6,4,2,1,26 };
	vector<int> route1 (arr1, arr1 + sizeof(arr1) / sizeof(arr1[0]) );
	col_list.push_back( new c_TourColumnCVRPTW( ctlr, route1, 567, 1 ) );

	static const int arr2[] = {0,13,17,18,19,16,14,12,26 };
	vector<int> route2 (arr2, arr2 + sizeof(arr2) / sizeof(arr2[0]) );
	col_list.push_back( new c_TourColumnCVRPTW( ctlr, route2, 928, 1 ) );

	static const int arr3[] = {0,20,24,25,23,22,21,26 };
	vector<int> route3 (arr3, arr3 + sizeof(arr3) / sizeof(arr3[0]) );
	col_list.push_back( new c_TourColumnCVRPTW( ctlr, route3, 363, 1 ) );

	static const int arr4[] = {0,8,10,11,9,6,4,2,26 }; //2,1,26 gleich weit wie 2,26
	vector<int> route4 (arr4, arr4 + sizeof(arr4) / sizeof(arr4[0]) );
	col_list.push_back( new c_TourColumnCVRPTW( ctlr, route4, ctlr->calculateCostOfRoute(route4), 0 ) );

	static const int arr5[] = {0,20,24,18,15,16,14,12,22,26 }; // 22,21,26 gleich weit wie 22,26
	vector<int> route5 (arr5, arr5 + sizeof(arr5) / sizeof(arr5[0]) );
	col_list.push_back( new c_TourColumnCVRPTW( ctlr, route5, ctlr->calculateCostOfRoute(route5), 0  ) );*/

	// add all new dummy columns
	ctlr->AddColumns( col_list );

	/*write("InitModel.lp",  "LP");*/
}


void c_RMP_CVRPTW::OutputInOStream( ostream& s ) const
{
	c_ControllerCVRPTW* ctlr = (c_ControllerCVRPTW*) c_RMP::Controller();
	// c_RMP::OutputInOStream(s);
	for ( int i=0; i<numcols(); i++ )
	{
		double x;
		getX( &x, i, i );
		if ( x > CG_EPS )
		{
			c_BaseColumnCVRPTW* col = (c_BaseColumnCVRPTW*) ctlr->ColumnInRMP( i );
			if ( !col->IsSystemColumn() )
			{
				s << "__" << *col << " = " << x << "\n";
				if ( col->Type() == c_BaseColumnCVRPTW::eTOUR )
				{
					c_TourColumnCVRPTW* col_bin = (c_TourColumnCVRPTW*) col;
					if ( !col_bin->IsFeasible() )
					{
						cout << "Infeasible shift column. Serious error." << endl;
						throw;
					}
				}
				// other types
			}
		}
	}
	// Handle GraphML output
	if ( ctlr->InfoGraphMLOutput() )
		GraphMLOutput( "output/support_graph.graphml" );
}


bool c_RMP_CVRPTW::IsIntegral() const
{
	return c_SupportCVRPTW::IsIntegral();
}


bool c_RMP_CVRPTW::IsFeasible() const
{	
	return c_SupportCVRPTW::IsFeasible();
}


//void c_RMP_CVRPTW::Update(c_BranchAndBoundNode* node)
//{
//	c_ControllerCVRPTW* ctlr = (c_ControllerCVRPTW*) c_RMP::Controller();
//	// Info
//	actual_iteration = -1;
//
//	// remove all constraints
//	RemoveAllBranchAndBoundConstraints();
//
//	// loop over all constraints and eliminate those that are not valid here
//	list<c_Constraint*> non_active;
//	vector<c_Constraint*>::iterator con;
//	for ( con=constraints.begin(); con!=constraints.end(); ++con )
//		if ( !(*con)->IsActiveAtNode( node ) )
//			non_active.push_back( *con );
//	if ( !non_active.empty() )
//		RemoveConstraints( non_active );
//
//	num_removed_constraints += (int)non_active.size();
//
//	list<c_BranchAndBoundConstraint*> constraint_list;
//	node->GetConstraints( constraint_list );
//	//if( !ctlr->IsAggregated() ) //TODO: Arbeite an effektiverer Variante um die AggregatedSRCuts nicht jedes Mal extra rauszusuchen um sie zu l�schen
//	//	RemoveAggregatedConstraints( constraint_list );
//	AddBranchAndBoundConstraints( constraint_list );
//
//	num_added_constraints += (int)constraint_list.size();
//}

void c_RMP_CVRPTW::RemoveAggregatedConstraints(list<c_BranchAndBoundConstraint*>& constraint_list)
{
	list<c_BranchAndBoundConstraint*> con_list_to_delete;
	for (auto con = constraint_list.begin(); con != constraint_list.end();con++) {
		bool isAggregatedCut = false;
		if (dynamic_cast<c_BranchAndBoundConstraintCVRPTW*>(*con) == 0){
			for (auto cut = (*con)->RMPConstraints().begin(); cut != (*con)->RMPConstraints().end();cut++) {
				c_Constraint_PVRPTW* pvrptw_cut = (c_Constraint_PVRPTW*) *cut;
				if( pvrptw_cut->Type() == c_RMP_CVRPTW::eAggregatedSubsetRowCut ){
					isAggregatedCut = true;
					break;
				}
			}
			if( isAggregatedCut )
				con_list_to_delete.push_back(*con);
		}
	}
	for (auto con = con_list_to_delete.begin(); con != con_list_to_delete.end();con++) {
		constraint_list.remove(*con);
	}
	//throw std::logic_error("The method or operation is not implemented.");
}

/////////////////////////////////////////////////////////////////////////////
//   c_SupportCVRP
/////////////////////////////////////////////////////////////////////////////

c_SupportCVRPTW::c_SupportCVRPTW( c_Controller* controller )
:	_controller( controller ),
	m_flow( ((c_ControllerCVRPTW*) controller)->NumNodes(), ((c_ControllerCVRPTW*) controller)->NumNodes() ),
	m_flowPerDay( ((c_ControllerCVRPTW*) controller)->NumNodes(), ((c_ControllerCVRPTW*) controller)->NumNodes(), ((c_ControllerCVRPTW*) controller)->PlanningHorizon() ),
	m_flowPerDayFor2PathCut( ((c_ControllerCVRPTW*) controller)->NumNodes(), ((c_ControllerCVRPTW*) controller)->NumNodes(), ((c_ControllerCVRPTW*) controller)->PlanningHorizon() ),
	m_vertexFlow( ((c_ControllerCVRPTW*) controller)->NumVertices(), ((c_ControllerCVRPTW*) controller)->NumVertices() ),
	b_is_updated( false ),
	b_is_integral( false ),
	b_is_feasible( true ),
	v_schedules(((c_ControllerCVRPTW*) controller)->NumNodes()),
	m_schedules( ((c_ControllerCVRPTW*) controller)->NumNodes(), ((c_ControllerCVRPTW*) controller)->PlanningHorizon() ),
	m_together( ((c_ControllerCVRPTW*) controller)->NumNodes(), ((c_ControllerCVRPTW*) controller)->NumNodes() )
{
	m_flow.Initialisierung(-1);
	m_flowPerDay.Initialisierung(-1);
	m_flowPerDayFor2PathCut.Initialisierung(-1);
	m_vertexFlow.Initialisierung(-1);
	m_schedules.Initialisierung(-1);
	m_together.Initialisierung(-1);
}

void c_SupportCVRPTW::UpdateFlows() const
{	
	// dirty tricks...
	const_cast<c_SupportCVRPTW*>(this)->UpdateFlows();	
}

void c_SupportCVRPTW::UpdateFlows()
{
	if ( !b_is_updated )
	{
		c_ControllerCVRPTW* ctlr = (c_ControllerCVRPTW*)Controller();	
		m_flow.Initialisierung(0.0);
		m_flowPerDay.Initialisierung(0.0);
		m_flowPerDayFor2PathCut.Initialisierung(0.0);
		m_schedules.Initialisierung(0.0);
		m_vertexFlow.Initialisierung(0.0);
		if( ctlr->UseRyanFosterBranching() ){
			m_together.Initialisierung(0.0);
		}
		b_is_feasible = true;
		b_is_integral = true;
		v_solution.clear();
		v_schedules.clear();
		v_schedules = vector<vector<bool>>( ctlr->NumNodes() );
		for ( int i=0; i<ctlr->RMP()->numcols(); i++ )
		{
			double x;
			ctlr->RMP()->getX( &x, i, i );
			if ( x > CG_EPS*CG_EPS )
			{
				c_BaseColumnCVRPTW* col = (c_BaseColumnCVRPTW*) ctlr->ColumnInRMP( i );
				if ( !col->IsSystemColumn() )
				{
					if ( col->Type() == c_BaseColumnCVRPTW::eDUMMY )
						b_is_feasible = false;

					if ( col->Type() == c_BaseColumnCVRPTW::eTOUR )
					{
						c_TourColumnCVRPTW* col_tour = (c_TourColumnCVRPTW*) col;

						const vector<int>& route = col_tour->Route();
						int pred = -1;
						for (int i=0; i<(int)route.size()-1; i++ ){
							m_vertexFlow( route[i], route[i+1] ) += x;
							m_flow( ctlr->GetNode(route[i]),  ctlr->GetNode(route[i+1]) ) += x;
							m_flowPerDay( ctlr->GetNode(route[i]),  ctlr->GetNode(route[i+1]), col_tour->Day() ) += x;
						}
						//const vector<int>& routeSPs = col_tour->RouteOfScheduleParts();
						//const c_SchedulePart* spHead = ctlr->GetSchedulePart(col_tour->Day()); //start depot
						//for (int i=0; i<=(int)routeSPs.size(); i++ ){
						//	const c_SchedulePart* spTail = spHead; //ctlr->GetSchedulePart(routeSPs[i]);
						//	spHead = (i == routeSPs.size())? ctlr->GetSchedulePart(col_tour->Day()) : ctlr->GetSchedulePart(routeSPs[i]);
						//
						//	//startDay of both schedule parts has to be col_tour->Day(), add flow on all days until max of tail- and head-SchedulePart-End (modulo PlanningHorizon)
						//	int d = spTail->getStartDay();
						//	bool reachedTailEnd = false;
						//	bool reachedHeadEnd = false;
						//	do{
						//		int headNode = (i == routeSPs.size())? ctlr->DestDepot() : spHead->getCustomer();
						//		m_flowPerDayFor2PathCut( spTail->getCustomer(), headNode, d ) += x;
						//		if( d == spTail->getEndDay() )
						//			reachedTailEnd = true;
						//		if( d == spHead->getEndDay() )
						//			reachedHeadEnd = true;
						//		d = (d+1)%ctlr->PlanningHorizon();
						//	} while( !reachedTailEnd || !reachedHeadEnd );
						//}

						if( ctlr->UseRyanFosterBranching() ){
							//store for every pair of customer demand satisfaction of day 0 how often they are visited during the same tour
							//only used in branching for customers with visit frequency of 1, for whom it is not relevant which task is considered (appear only all together)
							const vector<int>& routeSPs = col_tour->RouteOfScheduleParts();
							for (int i=0; i<(int)routeSPs.size()-1; i++ ){
								for (int j=i+1; j<(int)routeSPs.size(); j++ ){
									m_together( ctlr->GetSchedulePart(routeSPs[i])->getCustomer(), ctlr->GetSchedulePart(routeSPs[j])->getCustomer() ) += x;
								}
							}
						}
						if ( x > 0.001 && x < 0.999 )
							b_is_integral = false;
						if ( x >= 0.999 )
							v_solution.push_back( make_pair(col_tour->RouteOfScheduleParts(), col_tour->Day()) );
					}
					if ( col->Type() == c_BaseColumnCVRPTW::eSCHEDULE )
					{
						c_ScheduleColumnCVRPTW* col_schedule = (c_ScheduleColumnCVRPTW*) col;
						for (int d=0; d<ctlr->PlanningHorizon(); d++ ){
							if( col_schedule->Schedule()[d] ){
								m_schedules(col_schedule->Customer(), d) += x; //count for each customer and day number of chosen schedules visiting then
							}
						}
						if ( x > 0.001 && x < 0.999 )
							b_is_integral = false;
						if ( x >= 0.999 )
							v_schedules[col_schedule->Customer()] =  col_schedule->Schedule(); //in integer solution, every customer has exactly one schedule, before: at least one schedule
					}
				}
			}
		}
		sort( v_solution.begin(), v_solution.end() );
		if( ctlr->IsAggregated() )
			b_is_integral = false;
		b_is_updated = true;
	}
}


bool c_SupportCVRPTW::IsFeasible() const 
{ 
	UpdateFlows();
	return b_is_feasible; 
}

bool c_SupportCVRPTW::IsIntegral() const
{
	UpdateFlows();
	return b_is_feasible && b_is_integral; 
}

void c_SupportCVRPTW::GraphMLOutput( const char* filename ) const
{
	using namespace graphml;
	UpdateFlows();
	c_ControllerCVRPTW* ctlr = (c_ControllerCVRPTW*) Controller();
	int num_nodes = ctlr->NumNodes();
	c_GraphML g( filename );
	for ( int i = 0; i < num_nodes; ++i )
	{
		nodeproperties np;
		np.bordercolor = Color( "Blue" );
		np.coord_x = (int) (20 * ctlr->XCoord(i));
		np.coord_y = (int) (20 * ctlr->YCoord(i));
		if ( i == 0 || i == num_nodes-1 )
		{
			np.shape = nodeproperties::roundrectangle;
			np.fillcolor2 = Color( "Red" );
		}
		g.AddNode( i, np );
	}

	for ( int i = 0; i < num_nodes; i++)
		for (int j=0; j < num_nodes; j++)
		{
			double weight = m_flow(i, j);
			if ( weight > 0.01 )
			{
				if( i == j )
					continue;
				bool is_fract = ( min( ceil(weight)-weight, weight-floor(weight) ) > 0.05 );
				linkproperties lp;
				lp.linewidth = floor( 1.0 + 2.0*weight );
				stringstream ss; 
				if ( weight > 0.05 )
					ss << "x=" << setprecision(2) << setw(3) << weight << " ";
				lp.text = ss.str();
				if ( is_fract )
					lp.linecolor = Color("Dark Slate Blue");
				if ( weight <= 0.05 )
				{
					lp.linecolor = Color("Light Grey");
					if ( is_fract )
						lp.linecolor = Color("Light Blue");
				}
				g.AddArc( i, j, lp );
			}
		}
}


const double c_SupportCVRPTW::Flow(int tail, int head) const
{
	UpdateFlows();
	return m_flow( tail, head );
}

const double c_SupportCVRPTW::FlowPerDay(int tail, int head, int day) const
{
	UpdateFlows();
	return tail == head? 0 : m_flowPerDay( tail, head, day );
}

const double c_SupportCVRPTW::FlowPerDayFor2PathCut(int tail, int head, int day) const
{
	UpdateFlows();
	return tail == head? 0 : m_flowPerDayFor2PathCut( tail, head, day );
}

const double c_SupportCVRPTW::NumVehicles() const
{
	UpdateFlows();
	c_ControllerCVRPTW* ctlr = (c_ControllerCVRPTW*) _controller;
	double result = 0.0;
	for ( int i = 0; i < m_flow.Dimension1(); ++i ){
		result += m_flow( ctlr->OrigDepot(), i );
		/*if( ctlr->NetworkOfTasks() ){
		result += m_flow( ctlr->OrigDepotVertex(), i );
		} else if ( ctlr->NetworkOfScheduleParts() ){
		for ( int d = 0; d < ctlr->PlanningHorizon() ; d++){
		result += m_flow( ctlr->OrigDepotVertex(d), i );
		}
		}*/
	}
	return result;
}

const double c_SupportCVRPTW::NumVehicles(int day) const
{
	UpdateFlows();
	c_ControllerCVRPTW* ctlr = (c_ControllerCVRPTW*) _controller;
	double result = 0.0;
	for ( int i = 0; i < m_flowPerDay.Dimension1(); ++i ){
		result += m_flowPerDay( ctlr->OrigDepot(), i, day);
	}
	return result;
}

const double c_SupportCVRPTW::NumSchedules(int customer, int day) const
{
	UpdateFlows();
	return m_schedules(customer, day);
}

double c_SupportCVRPTW::Solution( vector<pair<vector<int>,int> >& solution, vector<double>& costs, vector<vector<bool>>& schedules ) const
{
	UpdateFlows();
	c_ControllerCVRPTW* ctlr = (c_ControllerCVRPTW*) _controller;
	solution = v_solution;
	schedules = v_schedules;
	costs.clear();
	double cost = 0.0;
	for ( int i=0; i<(int)v_solution.size(); i++ )
	{
		vector<int> route = v_solution[i].first;
		double routeCost = ctlr->calculateCostOfRoute(route);
		costs.push_back(routeCost);
		cost += routeCost;
	}
	if( b_is_feasible == false )
		cost = 100000000;
	return cost;
}


/////////////////////////////////////////////////////////////////////////////
//   c_SolutionCVRP
/////////////////////////////////////////////////////////////////////////////

c_SolutionCVRPTW::c_SolutionCVRPTW( c_Controller* controller, c_RMP* rmp, c_DualVariables* dual )
	:	c_Solution( controller, rmp, dual )	
{
	c_ControllerCVRPTW* ctlr = (c_ControllerCVRPTW*) Controller();
	c_RMP_CVRPTW* myrmp = (c_RMP_CVRPTW*) rmp;
	if ( !myrmp->IsIntegral() )
	{
		cout << "Solution is fractional. Cannot construct integer solution. Serious error. " << endl;
		cout << *myrmp << endl;
		//throw; //TODO: at least do not throw if solution is from MIPSolverAtTheEnd
	}	
	obj = myrmp->Solution( v_solution, v_costs, v_schedules );
}

void c_SolutionCVRPTW::OutputInOStream( ostream& s) const
{
	c_ControllerCVRPTW* ctlr = (c_ControllerCVRPTW*) Controller();	
	for ( int i=0; i<(int)v_solution.size(); i++ )
	{
		s << "  Day "  << v_solution[i].second << " Route " << i+1 << ": [";
		for ( int idx=0; idx<(int)v_solution[i].first.size(); idx++ ){
			s << (v_solution[i].first)[idx] << ",";
		}
		s << "] : " << ctlr->calculateCostOfRoute(v_solution[i].first) << endl;
	}
	s << "Overall cost: " << obj << endl;
}

double c_SolutionCVRPTW::VehicleNumber() const
{
	return (int)v_solution.size();
}


/////////////////////////////////////////////////////////////////////////////
//   c_BranchAndBoundNodeCVRP
/////////////////////////////////////////////////////////////////////////////

static list<c_Constraint*> emptyconslist;

c_BranchAndBoundNodeCVRPTW::c_BranchAndBoundNodeCVRPTW( c_Controller* controller )
	: c_BranchAndBoundNode( controller )
{}

c_BranchAndBoundNodeCVRPTW::c_BranchAndBoundNodeCVRPTW( c_BranchAndBoundNode* p_father, list<c_BranchAndBoundConstraint*>& add_constraints )
	: c_BranchAndBoundNode( p_father, add_constraints )
{}

c_BranchAndBoundNode* c_BranchAndBoundNodeCVRPTW::CreateNewNode( list<c_BranchAndBoundConstraint*>& new_constraint_list )
{
	return new c_BranchAndBoundNodeCVRPTW( this, new_constraint_list );
}

void c_BranchAndBoundNodeCVRPTW::GetBranchAndBoundConstraints( list< list<c_BranchAndBoundConstraint*> >& con_list )
{
	c_ControllerCVRPTW* ctlr = (c_ControllerCVRPTW*) Controller();
	c_RMP_CVRPTW* support= (c_RMP_CVRPTW*) ctlr->RMP();

	//When we start to make branching decisions (per day), we break the symmetry and have to disaggregate and add the mirrored solution columns of the other days
	if( ctlr->StartWithAggregation() && ctlr->IsAggregated() ){
		bool feasible = ctlr->Disaggregate();
		if( feasible )
			return;
	}

	if( ctlr->BreakSymmetry() && ctlr->GetFixedCustomer() == -1 && !ctlr->IsAggregated() ){
		//time for symmetry breaking by fixing the schedule of one customer if not already done
		ctlr->BreakSymmetryByScheduleSelection();

		c_BranchAndBoundConstraintBreakSymmetry* con_fixCust = new c_BranchAndBoundConstraintBreakSymmetry(ctlr, ctlr->GetFixedCustomer());
		list<c_BranchAndBoundConstraint*> cons;
		cons.push_back(con_fixCust);
		con_list.push_back(cons);
		
		return;
	}

	// branching on total number of vehicles
	if ( IsFractional( support->NumVehicles() ) )
	{
		int val = (int) floor(support->NumVehicles());

		//at least uprounded total num vehicles
		c_BranchAndBoundConstraintTotalNumVehicles* con_GE = new c_BranchAndBoundConstraintTotalNumVehicles(ctlr,val+1,true);	
		list<c_BranchAndBoundConstraint*> cons_GE;
		cons_GE.push_back(con_GE);

		//at most downrounded total num vehicles
		c_BranchAndBoundConstraintTotalNumVehicles* con_LE = new c_BranchAndBoundConstraintTotalNumVehicles(ctlr,val,false);	
		list<c_BranchAndBoundConstraint*> cons_LE;
		cons_LE.push_back(con_LE);

		con_list.push_back(cons_GE);
		con_list.push_back(cons_LE);	
		return;
	} else {
		
		// branching on number of vehicles per day
		for( int d = 0; d < ctlr->PlanningHorizon(); d++ ){
			if ( IsFractional( support->NumVehicles(d) ) ){
				int val = (int) floor( support->NumVehicles(d) );

				//at least uprounded num vehicles on day d
				c_BranchAndBoundConstraintNumVehicles* con_GE = new c_BranchAndBoundConstraintNumVehicles(ctlr,val+1,true,d);	
				list<c_BranchAndBoundConstraint*> cons_GE;
				cons_GE.push_back(con_GE);

				//at most downrounded num vehicles on day d
				c_BranchAndBoundConstraintNumVehicles* con_LE = new c_BranchAndBoundConstraintNumVehicles(ctlr,val,false,d);	
				list<c_BranchAndBoundConstraint*> cons_LE;
				cons_LE.push_back(con_LE);

				con_list.push_back(cons_GE);
				con_list.push_back(cons_LE);
				return;
			}
		}


		if( ctlr->UseRyanFosterBranching() ){
			cerr << "Ryan-Foster-Branching not longer supported. Increases pricing time too much." << endl;
			throw;
			//for ( int i=0; i<ctlr->NumNodes(); i++ ){
			//	if( ctlr->Frequency(i) == 1 ){
			//		for ( int j=0; j<ctlr->NumNodes(); j++ ){
			//			if( (i != j ) && ctlr->Frequency(j) == 1 ){
			//				if ( IsFractional( support->Together(i,j) )){
			//					if( ctlr->GetEnforcedPartners(i).size() > 0 || ctlr->GetEnforcedPartners(j).size() > 0 ){
			//						continue; //Todo: At the moment it is not allowed for a node to be included in more than one enforced-pair
			//					}
			//					//enforce the two customers to be included in the same tour
			//					c_BranchAndBoundConstraintCustsTogether* con_together = new c_BranchAndBoundConstraintCustsTogether(ctlr, i, j, true);	
			//					list<c_BranchAndBoundConstraint*> cons_together;
			//					cons_together.push_back(con_together);

			//					//forbid the two customers to be included in the same tour
			//					c_BranchAndBoundConstraintCustsTogether* con_separated = new c_BranchAndBoundConstraintCustsTogether(ctlr, i, j, false);	
			//					list<c_BranchAndBoundConstraint*> cons_separated;
			//					cons_separated.push_back(con_separated);

			//					con_list.push_back(cons_together);
			//					con_list.push_back(cons_separated);
			//					return;
			//				}
			//			}
			//		}
			//	}
			//}
		}


		//branching on days for customers -> deactivate some schedules per customer
		//customers are sorted from input such that their number of possible visit combinations increases. 

		//choose among first 5 customers with fractional schedule the one with the maximal fractionality (the highest number of active schedules and the closest value to 0.5)
		const int numTestCustomerDayPairs = ctlr->BranchingCustomerDayCandidates();//ctlr->PlanningHorizon() * 5;
		
		int bestCust = -1;
		int bestDay = ctlr->PlanningHorizon(); //preferrably 0
		int bestDemand = 0; //preferrably high
		int bestNumActiveSchedulesPerCust = 0;
		int lowestNumPossibleSchedules = 1000; //preferrably 2;
		int bestFrequency = 0;
		double lowestDistFromHalf = 0.5; //preferrably 0
		//init criteria
		//if( ctlr->BranchingTieBreakerRule() == 1 ){
		//	bestDay = ctlr->PlanningHorizon(); //preferrably 0
		//	bestNumActiveSchedulesPerCust = 0;
		//} else if( ctlr->BranchingTieBreakerRule() == 2 || ctlr->BranchingTieBreakerRule() == 3){
		//	bestFrequency = 0;
		//}

		//Decide customer and day to branch on
		for ( int i=0; i<ctlr->NumNodes(); i++ ){
			if ( ctlr->IsCustomerNode(i) ){
				if( ctlr->BranchingTieBreakerRule() == 1 ){
					//1. Highest number of active schedules, 2. lowestDistFromHalf, day= lowestFractionalDay of this customer
					int numActiveSchedulesPerCust = 0;
					double lowestDistFromHalfPerCust = 0.5;
					int firstFractionalDayPerCust = ctlr->PlanningHorizon();
					for( int d = 0; d < ctlr->PlanningHorizon(); d++ ){
						if ( IsFractional( support->NumSchedules(i, d) ) ){
							numActiveSchedulesPerCust++;
							if( abs(support->NumSchedules(i,d) - 0.5) < lowestDistFromHalfPerCust )
								lowestDistFromHalfPerCust  = abs(support->NumSchedules(i,d) - 0.5);
							if( d < firstFractionalDayPerCust )
								firstFractionalDayPerCust = d; //choose the smallest possible day per customer for uniformity
						}
					}
					//cout <<" Cust " << i << " #activeSchedules= " << numActiveSchedulesPerCust << endl;
					for( int d = 0; d < ctlr->PlanningHorizon(); d++ ){
						if ( IsFractional( support->NumSchedules(i, d) ) ){
							if( numActiveSchedulesPerCust > bestNumActiveSchedulesPerCust || 
								( numActiveSchedulesPerCust == bestNumActiveSchedulesPerCust && lowestDistFromHalfPerCust < lowestDistFromHalf ) ) {
									bestDay = firstFractionalDayPerCust;
									bestCust = i;
									bestNumActiveSchedulesPerCust = numActiveSchedulesPerCust;
									//lowestNumPossibleSchedules = ctlr->Frequency(i);
									lowestDistFromHalf = lowestDistFromHalfPerCust;
							}
						}
					}
				} else if( ctlr->BranchingTieBreakerRule() == 2 ){
					//1. H�chste Frequenz, 2. H�chster Demand, 3. H�chste Distanz von 0.5
					if( ctlr->Frequency(i) < bestFrequency ) //ctlr->Frequency(i) == ctlr->PlanningHorizon() || 
						continue;
					for( int d = 0; d < ctlr->PlanningHorizon(); d++ ){
						if ( IsFractional( support->NumSchedules(i, d) ) ){
							if ( ctlr->Frequency(i) > bestFrequency ){
								bestFrequency = ctlr->Frequency(i);
								bestDemand = ctlr->DemandPerDay(i);
								lowestDistFromHalf  = abs(support->NumSchedules(i,d) - 0.5);
								bestCust = i;
								bestDay = d;
							} else if ( ctlr->Frequency(i) == bestFrequency && ctlr->DemandPerDay(i) > bestDemand){
								bestDemand = ctlr->DemandPerDay(i);
								lowestDistFromHalf  = abs(support->NumSchedules(i,d) - 0.5);
								bestCust = i;
								bestDay = d;
							} else if ( ctlr->Frequency(i) == bestFrequency && ctlr->DemandPerDay(i) == bestDemand && abs(support->NumSchedules(i,d) - 0.5) < lowestDistFromHalf ){
								lowestDistFromHalf  = abs(support->NumSchedules(i,d) - 0.5);
								bestCust = i;
								bestDay = d;
							}
						}
					}
				} else if( ctlr->BranchingTieBreakerRule() == 3 ){
					//1. H�chste Frequenz, 2. H�chste Distanz von 0.5, 3. H�chster Demand
					if( ctlr->Frequency(i) == ctlr->PlanningHorizon() || ctlr->Frequency(i) < bestFrequency )
						continue;
					for( int d = 0; d < ctlr->PlanningHorizon(); d++ ){
						if ( IsFractional( support->NumSchedules(i, d) ) ){
							if ( ctlr->Frequency(i) > bestFrequency){
								bestFrequency = ctlr->Frequency(i);
								lowestDistFromHalf  = abs(support->NumSchedules(i,d) - 0.5);
								bestDemand = ctlr->DemandPerDay(i);
								bestCust = i;
								bestDay = d;
							} else if ( ctlr->Frequency(i) == bestFrequency && abs(support->NumSchedules(i,d) - 0.5) < lowestDistFromHalf){
								lowestDistFromHalf  = abs(support->NumSchedules(i,d) - 0.5);
								bestDemand = ctlr->DemandPerDay(i);
								bestCust = i;
								bestDay = d;
							} else if ( ctlr->Frequency(i) == bestFrequency && abs(support->NumSchedules(i,d) - 0.5) < lowestDistFromHalf && ctlr->DemandPerDay(i) > bestDemand ){
								bestDemand = ctlr->DemandPerDay(i);
								bestCust = i;
								bestDay = d;
							}
						}
					}
				} else if( ctlr->BranchingTieBreakerRule() == 4 ){
					//1. H�chste Frequenz, 2. lowestFractionalDay, 3. H�chster Demand, 4. H�chste Distanz von 0.5
					if( ctlr->Frequency(i) == ctlr->PlanningHorizon() || ctlr->Frequency(i) < bestFrequency )
						continue;
					for( int d = 0; d < ctlr->PlanningHorizon(); d++ ){
						if ( IsFractional( support->NumSchedules(i, d) ) ){
							if ( ctlr->Frequency(i) > bestFrequency){
								bestFrequency = ctlr->Frequency(i);
								lowestDistFromHalf  = abs(support->NumSchedules(i,d) - 0.5);
								bestDemand = ctlr->DemandPerDay(i);
								bestCust = i;
								bestDay = d;
							} else if ( ctlr->Frequency(i) == bestFrequency && d < bestDay ){
								lowestDistFromHalf  = abs(support->NumSchedules(i,d) - 0.5);
								bestDemand = ctlr->DemandPerDay(i);
								bestCust = i;
								bestDay = d;
							} else if ( ctlr->Frequency(i) == bestFrequency && d == bestDay && ctlr->DemandPerDay(i) > bestDemand){
								lowestDistFromHalf  = abs(support->NumSchedules(i,d) - 0.5);
								bestDemand = ctlr->DemandPerDay(i);
								bestCust = i;
								bestDay = d;
							} else if ( ctlr->Frequency(i) == bestFrequency && d == bestDay && ctlr->DemandPerDay(i) == bestDemand && abs(support->NumSchedules(i,d) - 0.5) < lowestDistFromHalf ){
								lowestDistFromHalf  = abs(support->NumSchedules(i,d) - 0.5);
								bestCust = i;
								bestDay = d;
							}
						}
					}
				}

				
						//if( abs(support->NumSchedules(i,d) - 0.5) < lowestDistFromHalfPerCust )
						//	lowestDistFromHalfPerCust  = abs(support->NumSchedules(i,d) - 0.5); // store the 
						//if( d < firstFractionalDayPerCust )
						//	firstFractionalDayPerCust = d; //choose the smallest possible day per customer for uniformity
						//if( (ctlr->Frequency(i) <= lowestNumPossibleSchedules && numActiveSchedulesPerCust > bestNumActiveSchedulesPerCust) ||
						//	( (ctlr->Frequency(i) <= lowestNumPossibleSchedules) && (numActiveSchedulesPerCust == bestNumActiveSchedulesPerCust) && (lowestDistFromHalfPerCust < lowestDistFromHalf) )  ){
						//		bestDay = firstFractionalDayPerCust;
						//		lowestDistFromHalf = abs(support->NumSchedules(i,d) - 0.5);
						//		bestCust = i;
						//}
						//TODO: Try branching orders: 
						//01: 1. Fr�hester Tag, 2. Kleinste Frequenz (init lowestNumPossibleSchedules = ctlr->PlanningHorizon()), 3. H�chster Demand
						/*if ( d < bestDay ){
						bestDay = d;
						bestCust = i;
						}
						if ( d== bestDay && ctlr->Frequency(i) < lowestNumPossibleSchedules ){
						lowestNumPossibleSchedules = ctlr->Frequency(i);
						bestCust = i;
						}
						if ( d== bestDay && ctlr->Frequency(i) <= lowestNumPossibleSchedules && ctlr->DemandPerDay(i) > bestDemand){
						bestDemand = ctlr->DemandPerDay(i);
						bestCust = i;
						}*/
						//02: 1. Fr�hester Tag, 2. H�chste Frequenz, 3. H�chster Demand
						/*if ( d < bestDay ){
						bestDay = d;
						bestCust = i;
						}
						if ( d== bestDay && ctlr->Frequency(i) > lowestNumPossibleSchedules ){
						lowestNumPossibleSchedules = ctlr->Frequency(i);
						bestCust = i;
						}
						if ( d== bestDay && ctlr->Frequency(i) >= lowestNumPossibleSchedules && ctlr->DemandPerDay(i) > bestDemand){
						bestDemand = ctlr->DemandPerDay(i);
						bestCust = i;
						}*/
						
						//03: 1. Kleinste Frequenz, 2. H�chster Demand, 3. H�chste Distanz von 0.5
						/*if ( ctlr->Frequency(i) < lowestNumPossibleSchedules){
						lowestNumPossibleSchedules = ctlr->Frequency(i);
						bestCust = i;
						bestDay = d;
						}
						if ( ctlr->Frequency(i) == lowestNumPossibleSchedules && ctlr->DemandPerDay(i) > bestDemand){
						bestDemand = ctlr->DemandPerDay(i);
						bestCust = i;
						bestDay = d;
						}
						if ( ctlr->Frequency(i) == lowestNumPossibleSchedules && ctlr->DemandPerDay(i) == bestDemand && abs(support->NumSchedules(i,d) - 0.5) < lowestDistFromHalf ){
						lowestDistFromHalf  = abs(support->NumSchedules(i,d) - 0.5);
						bestCust = i;
						bestDay = d;
						}*/
						//04: 1. H�chste Frequenz, 2. H�chster Demand, 3. H�chste Distanz von 0.5
						/*if ( ctlr->Frequency(i) > lowestNumPossibleSchedules){
						lowestNumPossibleSchedules = ctlr->Frequency(i);
						bestCust = i;
						bestDay = d;
						}
						if ( ctlr->Frequency(i) == lowestNumPossibleSchedules && ctlr->DemandPerDay(i) > bestDemand){
						bestDemand = ctlr->DemandPerDay(i);
						bestCust = i;
						bestDay = d;
						}
						if ( ctlr->Frequency(i) == lowestNumPossibleSchedules && ctlr->DemandPerDay(i) == bestDemand && abs(support->NumSchedules(i,d) - 0.5) < lowestDistFromHalf ){
						lowestDistFromHalf  = abs(support->NumSchedules(i,d) - 0.5);
						bestCust = i;
						bestDay = d;
						}*/
						

						//if( cnt_candidates >= numTestCustomerDayPairs ){
						//	//allow only schedules of customer i that are ACTIVE on day d
						//	c_BranchAndBoundConstraintCustomerDay* con_active = new c_BranchAndBoundConstraintCustomerDay(ctlr,bestCust,correspondingDay, true);	
						//	list<c_BranchAndBoundConstraint*> cons_active;
						//	cons_active.push_back(con_active);

						//	//allow only schedules of customer i that are INACTIVE on day d
						//	c_BranchAndBoundConstraintCustomerDay* con_inactive = new c_BranchAndBoundConstraintCustomerDay(ctlr,bestCust,correspondingDay, false);	
						//	list<c_BranchAndBoundConstraint*> cons_inactive;
						//	cons_inactive.push_back(con_inactive);

						//	con_list.push_back(cons_active);
						//	con_list.push_back(cons_inactive);
						//	return;
						//}
					//}
				//}
			}
		}
		if( ctlr->InfoLevel() >= 2 ){
			cout << "Branch on customer " << bestCust << " on day " << bestDay << " with daily demand " << ctlr->DemandPerDay(bestCust) << endl;
		}

		if( bestCust != -1 ){ //if not enough (numTestCustomerDayPairs) fractional values were found, but at least one, branch on this decision
			//allow only schedules of customer i that are ACTIVE on day d
			c_BranchAndBoundConstraintCustomerDay* con_active = new c_BranchAndBoundConstraintCustomerDay(ctlr,bestCust,bestDay, true);	
			list<c_BranchAndBoundConstraint*> cons_active;
			cons_active.push_back(con_active);

			//allow only schedules of customer i that are INACTIVE on day d
			c_BranchAndBoundConstraintCustomerDay* con_inactive = new c_BranchAndBoundConstraintCustomerDay(ctlr,bestCust,bestDay, false);	
			list<c_BranchAndBoundConstraint*> cons_inactive;
			cons_inactive.push_back(con_inactive);

			con_list.push_back(cons_active);
			con_list.push_back(cons_inactive);
			return;
		}


		//branching on successive customers per day (not vertices)
		double near_value = 0.5;
		double min = 1.0;
		int tailNode = -1;
		int headNode = -1;
		int dayOfFlow = -1;
		// find most fractional flow between customers on one day
		for( int d = 0; d < ctlr->PlanningHorizon(); d++ ){
			for (int i=0;i<ctlr->NumNodes();i++){
				if( i != ctlr->OrigDepot() && i != ctlr->DestDepot() ){
					for (int j=0;j<ctlr->NumNodes();j++){
						if( j != ctlr->OrigDepot() && j != ctlr->DestDepot() && i != j ){

							double flow = support->FlowPerDay(i,j,d);
							if ( IsFractional(flow) && fabs( flow - near_value ) < min ) {
								min = fabs( flow - near_value );
								tailNode = i;
								headNode = j;
								dayOfFlow = d;
							}
						}
					}
				}
			}
		}
		if (tailNode == -1)
		{
			cout << "No fractional flow found. This should not happen. Serious error. "<< endl;
			throw;
		}
		c_BranchAndBoundConstraintNodeArc* con_EnforceArc = new c_BranchAndBoundConstraintNodeArc( ctlr, tailNode, headNode, dayOfFlow, true );	
		list<c_BranchAndBoundConstraint*> cons_Enforcing;
		cons_Enforcing.push_back(con_EnforceArc);

		c_BranchAndBoundConstraintNodeArc* con_ForbidArc = new c_BranchAndBoundConstraintNodeArc( ctlr, tailNode, headNode, dayOfFlow, false );	
		list<c_BranchAndBoundConstraint*> cons_Forbidding;
		cons_Forbidding.push_back(con_ForbidArc);

		con_list.push_back(cons_Enforcing);
		con_list.push_back(cons_Forbidding);


		//// branching on vertices-arc
		//double near_value = 0.5;
		//double min = 1.0;
		//int tailVertex=-1;
		//int headVertex=-1;
		//// find most fractional flow over arc
		//for (int i=0;i<ctlr->NumVertices();i++){
		//	if( i != ctlr->OrigDepotVertex() && i != ctlr->DestDepotVertex() ){
		//		for (int j=0;j<ctlr->NumVertices();j++){
		//			if( j != ctlr->OrigDepotVertex() && j != ctlr->DestDepotVertex() ){
		//				double flow = support->Flow(i,j);
		//				if ( IsFractional(flow) && fabs( flow - near_value ) < min ) {
		//					min = fabs( flow - near_value );
		//					tailVertex = i;
		//					headVertex = j;
		//				}
		//			}
		//		}
		//	}
		//}
		//if (tailVertex == -1)
		//{
		//	cout << "No fractional flow found. This should not happen. Serious error. "<< endl;
		//	throw;
		//}
		//c_BranchAndBoundConstraintArc* con_EnforceArc = new c_BranchAndBoundConstraintArc( ctlr, tailVertex, headVertex, true );	
		//list<c_BranchAndBoundConstraint*> cons_Enforcing;
		//cons_Enforcing.push_back(con_EnforceArc);

		//c_BranchAndBoundConstraintArc* con_ForbidArc = new c_BranchAndBoundConstraintArc( ctlr, tailVertex, headVertex, false );	
		//list<c_BranchAndBoundConstraint*> cons_Forbidding;
		//cons_Forbidding.push_back(con_ForbidArc);

		//con_list.push_back(cons_Enforcing);
		//con_list.push_back(cons_Forbidding);
			
	}
}

void c_BranchAndBoundNodeCVRPTW::Get_k_th_best_BranchAndBoundContraints(int k, list< list<c_BranchAndBoundConstraint*> >& con_list)
{
	c_ControllerCVRPTW* ctlr = (c_ControllerCVRPTW*) Controller();
	c_RMP_CVRPTW* support= (c_RMP_CVRPTW*) ctlr->RMP();

	if( ctlr->BreakSymmetry() && ctlr->StartWithAggregation() && ctlr->GetFixedCustomer() == -1){
		if( k > 0 ){
			return; //no strong branching choice when problem is still symmetric
		}
		//time for symmetry breaking by fixing the schedule of one customer if not already done
		ctlr->BreakSymmetryByScheduleSelection();

		c_BranchAndBoundConstraintBreakSymmetry* con_fixCust = new c_BranchAndBoundConstraintBreakSymmetry(ctlr, ctlr->GetFixedCustomer());
		list<c_BranchAndBoundConstraint*> cons;
		cons.push_back(con_fixCust);
		con_list.push_back(cons);
		
		return;
	}

	// branching on total number of vehicles
	if ( IsFractional( support->NumVehicles() ) )
	{
		if( k > 0 ){
			return; //no strong branching choice when total number of vehicles is still fractional
		}

		int val = (int) floor(support->NumVehicles());

		//at least uprounded total num vehicles
		c_BranchAndBoundConstraintTotalNumVehicles* con_GE = new c_BranchAndBoundConstraintTotalNumVehicles(ctlr,val+1,true);	
		list<c_BranchAndBoundConstraint*> cons_GE;
		cons_GE.push_back(con_GE);

		//at most downrounded total num vehicles
		c_BranchAndBoundConstraintTotalNumVehicles* con_LE = new c_BranchAndBoundConstraintTotalNumVehicles(ctlr,val,false);	
		list<c_BranchAndBoundConstraint*> cons_LE;
		cons_LE.push_back(con_LE);

		con_list.push_back(cons_GE);
		con_list.push_back(cons_LE);	
		return;
	} else {

		//When we start to make branching decisions per day, we break the symmetry and have to disaggregate and add the mirrored solution columns of the other days
		if( ctlr->StartWithAggregation() && ctlr->IsAggregated() ){
			bool feasible = ctlr->Disaggregate();
			if( feasible )
				return;
		}
		
		int cnt_branchingPossibility = 0;
		// branching on number of vehicles per day
		for( int d = 0; d < ctlr->PlanningHorizon(); d++ ){
			if ( IsFractional( support->NumVehicles(d) ) ){
				
				if( cnt_branchingPossibility == k ){
					int val = (int) floor( support->NumVehicles(d) );
				
					//at least uprounded num vehicles on day d
					c_BranchAndBoundConstraintNumVehicles* con_GE = new c_BranchAndBoundConstraintNumVehicles(ctlr,val+1,true,d);	
					list<c_BranchAndBoundConstraint*> cons_GE;
					cons_GE.push_back(con_GE);

					//at most downrounded num vehicles on day d
					c_BranchAndBoundConstraintNumVehicles* con_LE = new c_BranchAndBoundConstraintNumVehicles(ctlr,val,false,d);	
					list<c_BranchAndBoundConstraint*> cons_LE;
					cons_LE.push_back(con_LE);

					con_list.push_back(cons_GE);
					con_list.push_back(cons_LE);
				
					return;
				}
				cnt_branchingPossibility++;
			}
		}

		//branching on days for customers -> deactivate some schedules per customer
		//customers are sorted from input such that their number of possible visit combinations increases. 
		//To decide as much as possible, start the loop at the last customer
		//for ( int i=ctlr->NumNodes()-1; i>0; i-- ){
		for ( int i=0; i<ctlr->NumNodes(); i++ ){
			if ( ctlr->IsCustomerNode(i) ){
				for( int d = 0; d < ctlr->PlanningHorizon(); d++ ){
					if ( IsFractional( support->NumSchedules(i, d) ) ){
						if( cnt_branchingPossibility == k ){
							if( cnt_branchingPossibility > 0 && d != 0 ){
								continue; //only choose those of first day if branchable. Otherwise all possible branching decisions are nearly identical
							}
							//allow only schedules of customer i that are ACTIVE on day d
							c_BranchAndBoundConstraintCustomerDay* con_active = new c_BranchAndBoundConstraintCustomerDay(ctlr,i,d, true);	
							list<c_BranchAndBoundConstraint*> cons_active;
							cons_active.push_back(con_active);

							//allow only schedules of customer i that are INACTIVE on day d
							c_BranchAndBoundConstraintCustomerDay* con_inactive = new c_BranchAndBoundConstraintCustomerDay(ctlr,i,d, false);	
							list<c_BranchAndBoundConstraint*> cons_inactive;
							cons_inactive.push_back(con_inactive);

							con_list.push_back(cons_active);
							con_list.push_back(cons_inactive);
							return;
						} 
						cnt_branchingPossibility++;
					}
				}
			}
		}


		//branching on successive customers per day (not vertices)
		double near_value = 0.5;
		double min = 1.0;
		int tailNode = -1;
		int headNode = -1;
		int dayOfFlow = -1;
		// find most fractional flow between customers on one day
		for( int d = 0; d < ctlr->PlanningHorizon(); d++ ){
			for (int i=0;i<ctlr->NumNodes();i++){
				if( i != ctlr->OrigDepot() && i != ctlr->DestDepot() ){
					for (int j=0;j<ctlr->NumNodes();j++){
						if( j != ctlr->OrigDepot() && j != ctlr->DestDepot() && i != j ){

							double flow = support->FlowPerDay(i,j,d);
							if ( IsFractional(flow) && fabs( flow - near_value ) < min ) {
								min = fabs( flow - near_value );
								tailNode = i;
								headNode = j;
								dayOfFlow = d;
							}
						}
					}
				}
			}
		}
		if (tailNode == -1)
		{
			cout << "No fractional flow found. This should not happen. Serious error. "<< endl;
			throw;
		}
		if( cnt_branchingPossibility < k ){
			return;
		}
		c_BranchAndBoundConstraintNodeArc* con_EnforceArc = new c_BranchAndBoundConstraintNodeArc( ctlr, tailNode, headNode, dayOfFlow, true );	
		list<c_BranchAndBoundConstraint*> cons_Enforcing;
		cons_Enforcing.push_back(con_EnforceArc);

		c_BranchAndBoundConstraintNodeArc* con_ForbidArc = new c_BranchAndBoundConstraintNodeArc( ctlr, tailNode, headNode, dayOfFlow, false );	
		list<c_BranchAndBoundConstraint*> cons_Forbidding;
		cons_Forbidding.push_back(con_ForbidArc);

		con_list.push_back(cons_Enforcing);
		con_list.push_back(cons_Forbidding);
	}

}

void c_BranchAndBoundNodeCVRPTW::GetConstraints(list<c_BranchAndBoundConstraint*>& con_list) const
{
	//TODO: this method should not add GlobalValidConstraints!, they are included anyway (or in case of AggregatedSrCuts are removed on purpose)
	c_ControllerCVRPTW* ctlr = (c_ControllerCVRPTW*) Controller();
 	//con_list = AddedConstraints(); //don't add the just added constraints immediately, check first whether they are globally valid 
	//(in which case they are added anyway, or in case of AggregatedSrCuts are removed on purpose)
	list<c_BranchAndBoundConstraint*>::const_iterator iter;
	for(iter=AddedConstraints().begin(); iter!=AddedConstraints().end(); ++iter ){
			bool addBaBConstraint = true; //SR-Cuts are globally valid, don't add them again
			if( ctlr->StartWithAggregation() && !ctlr->IsAggregated() ){
				//Aggregated SR Cuts no longer valid, don't add them
				for (auto cut = (*iter)->RMPConstraints().begin(); cut != (*iter)->RMPConstraints().end();cut++) {
					if( (*cut)->IsGloballyValid() ){
						addBaBConstraint = false;
						break;
					}
				}
			}
			if( addBaBConstraint)
				con_list.push_back( *iter );
	}

	c_BranchAndBoundNode* p_father = Father();
	while ( p_father )
	{
		list<c_BranchAndBoundConstraint*>::const_iterator iter;
		const list<c_BranchAndBoundConstraint*>& added_list = p_father->AddedConstraints();
		for(iter=added_list.begin(); iter!=added_list.end(); ++iter ){
			bool addBaBConstraint = true; //SR-Cuts are globally valid, don't add them again
			if( ctlr->StartWithAggregation() && !ctlr->IsAggregated() ){
				//Aggregated SR Cuts no longer valid, don't add them
				for (auto cut = (*iter)->RMPConstraints().begin(); cut != (*iter)->RMPConstraints().end();cut++) {
					if( (*cut)->IsGloballyValid() ){
						/*c_Constraint_PVRPTW* pvrptw_cut = (c_Constraint_PVRPTW*) cut;
						if( pvrptw_cut->Type() == c_RMP_CVRPTW::eAggregatedSubsetRowCut ){*/
						addBaBConstraint = false;
						break;
					}
				}
			}
			if( addBaBConstraint)
				con_list.push_back( *iter );
		}
		p_father = p_father->Father();
	}
}

/////////////////////////////////////////////////////////////////////////////
//   c_BranchAndBoundConstrainBreakSymmetryCVRP
/////////////////////////////////////////////////////////////////////////////
c_BranchAndBoundConstraintBreakSymmetry::c_BranchAndBoundConstraintBreakSymmetry(c_Controller* controller,int cust)
	:c_BranchAndBoundConstraintCVRPTW(controller,emptyconslist),
	i_cust(cust)
{
}

void c_BranchAndBoundConstraintBreakSymmetry::ComputeBounds(c_Column* col, double& lb, double& ub)
{
	if (col->IsSystemColumn())
		return;
	c_ControllerCVRPTW* ctlr = (c_ControllerCVRPTW*) Controller();
	c_BaseColumnCVRPTW* CVRPTW_col= (c_BaseColumnCVRPTW*) col;
	if (CVRPTW_col->Type()!= c_BaseColumnCVRPTW::eSCHEDULE)
		return; //not relevant for not-schedule columns
	c_ScheduleColumnCVRPTW* schedule_col = (c_ScheduleColumnCVRPTW*) CVRPTW_col;

	//i_cust fixed to first day, thus all schedule columns belonging to the fixed customer but not including visit on the first day are forbidden
	if ( schedule_col->Customer() == i_cust ) 
		if( schedule_col->Schedule()[0] != true )
			ub = 0;
}

void c_BranchAndBoundConstraintBreakSymmetry::OutputInOStream(ostream& s) const
{
	s << "Customer " << i_cust << " fixed.";
}

std::string c_BranchAndBoundConstraintBreakSymmetry::TextInBranching() const
{
	stringstream ss;
	ss << "BreakSymmetry_" << i_cust;
	return ss.str();
}



/////////////////////////////////////////////////////////////////////////////
//   c_BranchAndBoundConstrainTotalNumVehCVRP
/////////////////////////////////////////////////////////////////////////////
c_BranchAndBoundConstraintTotalNumVehicles::c_BranchAndBoundConstraintTotalNumVehicles(c_Controller* controller,int quantity, bool bigger)
	:c_BranchAndBoundConstraintCVRPTW(controller,emptyconslist),
	i_number(quantity),
	b_greater_eq(bigger)
{
}

void c_BranchAndBoundConstraintTotalNumVehicles::ComputeBounds(c_Column* col, double& lb, double& ub)
{
	if (col->IsSystemColumn())
		return;
	c_ControllerCVRPTW* ctlr = (c_ControllerCVRPTW*) Controller();
	c_BaseColumnCVRPTW* CVRPTW_col= (c_BaseColumnCVRPTW*) col;
	if (CVRPTW_col->Type()!= c_BaseColumnCVRPTW::eTOTAL_NUM_VEH){
		//if( CVRPTW_col->Type() == c_BaseColumnCVRPTW::eNUM_VEH ){
		//	if( b_greater_eq && i_number == ctlr->NumberOfVehicles() ){
		//		//If total number of vehicles is maximal, all vehicles on all days have to be used (implicitely required anyway)
		//		lb = ctlr->NumVehiclesPerDay();
		//	}
		//	return;
		//} else{
			return;
		//}
	}
	if ( b_greater_eq ) 
		lb = i_number;
	else
		ub = i_number;
}

void c_BranchAndBoundConstraintTotalNumVehicles::OutputInOStream(ostream& s) const
{
	s << "Total Num Vehicles " << (b_greater_eq? ">=" : "<=" ) << i_number;
}

std::string c_BranchAndBoundConstraintTotalNumVehicles::TextInBranching() const
{
	stringstream ss;
	ss << "Total Num Vehicles";
	return ss.str();
}

/////////////////////////////////////////////////////////////////////////////
//   c_BranchAndBoundConstrainNumVehCVRP
/////////////////////////////////////////////////////////////////////////////
c_BranchAndBoundConstraintNumVehicles::c_BranchAndBoundConstraintNumVehicles(c_Controller* controller,int quantity, bool bigger, int day)
	:c_BranchAndBoundConstraintCVRPTW(controller,emptyconslist),
	i_number(quantity),
	b_greater_eq(bigger),
	i_day(day)
{
}

void c_BranchAndBoundConstraintNumVehicles::ComputeBounds(c_Column* col, double& lb, double& ub)
{
	if (col->IsSystemColumn())
		return;
	c_ControllerCVRPTW* ctlr = (c_ControllerCVRPTW*) Controller();
	c_BaseColumnCVRPTW* CVRPTW_col= (c_BaseColumnCVRPTW*) col;
	if (CVRPTW_col->Type()!= c_BaseColumnCVRPTW::eNUM_VEH)
		return;
	c_NumVehicleColumnCVRPTW* numVehicle_col = (c_NumVehicleColumnCVRPTW*) CVRPTW_col;
	if( numVehicle_col->Day() != i_day )
		return;

	if ( b_greater_eq ) 
		lb = i_number;
	else
		ub = i_number;
}


void c_BranchAndBoundConstraintNumVehicles::OutputInOStream(ostream& s) const
{
	s << "Num Vehicles(" << i_day << ") " << (b_greater_eq? ">=" : "<=" ) << i_number;
}


std::string c_BranchAndBoundConstraintNumVehicles::TextInBranching() const
{
	stringstream ss;
	ss << "Num Vehicles(" << i_day << ")";
	return ss.str();
}


/////////////////////////////////////////////////////////////////////////////
//   c_BranchAndBoundConstrainCustomerDayCVRP
/////////////////////////////////////////////////////////////////////////////
c_BranchAndBoundConstraintCustsTogether::c_BranchAndBoundConstraintCustsTogether(c_Controller* controller, int customer1, int customer2, bool together)
	:c_BranchAndBoundConstraintCVRPTW(controller,emptyconslist),
	i_cust1(customer1),
	i_cust2(customer2),
	b_together(together)
{}

void c_BranchAndBoundConstraintCustsTogether::ComputeBounds(c_Column* col, double& lb, double& ub)
{
	if (col->IsSystemColumn())
		return;
	c_ControllerCVRPTW* ctlr = (c_ControllerCVRPTW*) Controller();
	c_BaseColumnCVRPTW* CVRPTW_col= (c_BaseColumnCVRPTW*) col;
	if (CVRPTW_col->Type()!= c_BaseColumnCVRPTW::eTOUR)
		return;
	c_TourColumnCVRPTW* tour_col = (c_TourColumnCVRPTW*) CVRPTW_col;
	if( Together() ){
		//forbid tours that include only one of the two customers
		if( taskNetwork ){
			if( v_contains(tour_col->Route(), Customer1()) ){
				if( !v_contains(tour_col->Route(), Customer2())){
					ub = 0;
				}
			} else{
				if( v_contains(tour_col->Route(), Customer2()) ){
					ub = 0;
				}
			}	
		} else{
			cerr << "Ryan-Foster-branching not yet implemented for schedulePartNetwork." << endl;
			throw;
		}
	} else{ //customers have to be visited by different routes
		//forbid tours that contain both customers
		if( taskNetwork ){
			if( v_contains(tour_col->Route(), Customer1()) && v_contains(tour_col->Route(), Customer2()) ){
				ub = 0;
			}
		} else{
			cerr << "Ryan-Foster-branching not yet implemented for schedulePartNetwork." << endl;
			throw;
		}
	}
}

void c_BranchAndBoundConstraintCustsTogether::OutputInOStream(ostream& s) const
{
	s << "Custs(" << i_cust1 << ", " << i_cust2 << ")_" << "b_" << Together();
}

std::string c_BranchAndBoundConstraintCustsTogether::TextInBranching() const
{
	stringstream ss;
	ss << "Custs(" << i_cust1 << ", " << i_cust2 << ")";
	return ss.str();
}

/////////////////////////////////////////////////////////////////////////////
//   c_BranchAndBoundConstrainCustomerDayCVRP
/////////////////////////////////////////////////////////////////////////////
c_BranchAndBoundConstraintCustomerDay::c_BranchAndBoundConstraintCustomerDay(c_Controller* controller, int customer, int day, bool active)
	:c_BranchAndBoundConstraintCVRPTW(controller,emptyconslist),
	i_customer(customer),
	i_day(day),
	b_active(active)
{}

void c_BranchAndBoundConstraintCustomerDay::ComputeBounds(c_Column* col, double& lb, double& ub)
{
	if (col->IsSystemColumn())
		return;
	c_ControllerCVRPTW* ctlr = (c_ControllerCVRPTW*) Controller();
	c_BaseColumnCVRPTW* CVRPTW_col= (c_BaseColumnCVRPTW*) col;
	if (CVRPTW_col->Type()!= c_BaseColumnCVRPTW::eSCHEDULE)
		return;
	c_ScheduleColumnCVRPTW* schedule_col = (c_ScheduleColumnCVRPTW*) CVRPTW_col;
	//forbid schedule columns of i_customer, if their schedule is not b_active at i_day
	if( schedule_col->Customer() != i_customer)
		return;

	if ( b_active != schedule_col->Schedule()[i_day] ) 
		ub = 0;
}

void c_BranchAndBoundConstraintCustomerDay::OutputInOStream(ostream& s) const
{
	s << "Cust(" << i_customer << ")_Day(" << i_day << ")_" << b_active;
}

std::string c_BranchAndBoundConstraintCustomerDay::TextInBranching() const
{
	stringstream ss;
	ss << "Cust(" << i_customer << ")_Day(" << i_day << ")";
	return ss.str();
}


/////////////////////////////////////////////////////////////////////////////
//   c_BranchAndBoundConstrainNodeArcCVRP
/////////////////////////////////////////////////////////////////////////////
c_BranchAndBoundConstraintNodeArc::c_BranchAndBoundConstraintNodeArc(c_Controller* controller, int tail, int head, int day, bool val)
	:	c_BranchAndBoundConstraintCVRPTW(controller,emptyconslist),
	b_enforced(val),
	i_tail(tail),
	i_head(head),
	i_day(day)
{}

void c_BranchAndBoundConstraintNodeArc::ComputeBounds(c_Column* col, double& lb, double& ub)
{
	if (col->IsSystemColumn())
		return;
	c_ControllerCVRPTW* ctlr = (c_ControllerCVRPTW*) Controller();
	c_BaseColumnCVRPTW* col2= (c_BaseColumnCVRPTW*) col;
	if (col2->Type()!= c_BaseColumnCVRPTW::eTOUR)
		return;
	c_TourColumnCVRPTW* col3 = (c_TourColumnCVRPTW*) col2;
	if ( col3->Day() != i_day ) //only columns on i_day are concerned from this branching decision
		return;
	const vector<int>& route = col3->Route();
	if ( !Enforced() ) // If REF is in Tour => UB=0;
	{
		for (int i=0;i<(int)route.size()-1;i++)
			if ( ctlr->GetNode(route[i]) == Tail() && ctlr->GetNode(route[i+1]) == Head() )
			{
				ub=0.0;
				return;
			}
	}
	else
	{	
		for (int i=0;i<(int)route.size()-1;i++)
			if ( ( ctlr->GetNode(route[i]) == Tail() && ctlr->GetNode(route[i+1]) != Head() )
				|| ( ctlr->GetNode(route[i]) != Tail() && ctlr->GetNode(route[i+1]) == Head() ) )
			{
				ub=0.0;
				return;
			}
	}
}

void c_BranchAndBoundConstraintNodeArc::OutputInOStream(ostream& s) const
{
	s << "nodeArc(" <<  Tail() << ","<< Head() << ","<< Day() << ")=" << ( Enforced() ? "1" : "0");
}

std::string c_BranchAndBoundConstraintNodeArc::TextInBranching() const
{
	stringstream ss;
	ss << "NodeArc(" <<  Tail() << ","<< Head() << ","<< Day() << ")"; 
	return ss.str();
}


/////////////////////////////////////////////////////////////////////////////
//   c_BranchAndBoundConstrainArcCVRP
/////////////////////////////////////////////////////////////////////////////
c_BranchAndBoundConstraintArc::c_BranchAndBoundConstraintArc(c_Controller* controller, int tail, int head, bool val)
	:	c_BranchAndBoundConstraintCVRPTW(controller,emptyconslist),
	b_enforced(val),
	i_tail(tail),
	i_head(head)
{}


void c_BranchAndBoundConstraintArc::ComputeBounds(c_Column* col, double& lb, double& ub)
{
	if (col->IsSystemColumn())
		return;
	c_ControllerCVRPTW* ctlr = (c_ControllerCVRPTW*) Controller();
	c_BaseColumnCVRPTW* col2= (c_BaseColumnCVRPTW*) col;
	if (col2->Type()!= c_BaseColumnCVRPTW::eTOUR)
		return;
	c_TourColumnCVRPTW* col3 = (c_TourColumnCVRPTW*) col2;
	const vector<int>& route = col3->Route();
	if ( !Enforced() ) // If REF is in Tour => UB=0;
	{
		for (int i=0;i<(int)route.size()-1;i++)
			if ( route[i] == Tail() && route[i+1] == Head() )
			{
				ub=0.0;
				return;
			}
	}
	else
	{	
		for (int i=0;i<(int)route.size()-1;i++)
			if ( ( route[i] == Tail() && route[i+1] != Head() )
				|| ( route[i] != Tail() && route[i+1] == Head() ) )
			{
				ub=0.0;
				return;
			}
	}
}

void c_BranchAndBoundConstraintArc::OutputInOStream(ostream& s) const
{
	s << "arc(" <<  Tail() << ","<< Head() << ")=" << ( Enforced() ? "1" : "0");
}

std::string c_BranchAndBoundConstraintArc::TextInBranching() const
{
	stringstream ss;
	ss << "Arc(" <<  Tail() << ","<< Head() << ")"; 
	return ss.str();
}


/////////////////////////////////////////////////////////////////////////////
//   c_PricingBaseSolverCVRP
/////////////////////////////////////////////////////////////////////////////
c_PricingSolverCVRPTW::c_PricingSolverCVRPTW( c_Controller* controller, int networkSize, int day, bool exactDom )
:	c_PricingProblem( controller ),
	o_esppc_solver( ((c_ControllerCVRPTW*) controller)->NumVertices() ),
	i_direction( ((c_ControllerCVRPTW*) controller)->Solver() ), //Changable between: FW(=0), BW(=1), BIDIR(=3)
	o_merger( ((c_ControllerCVRPTW*) controller)->Capacity(), ((c_ControllerCVRPTW*) controller)->StartTime( ((c_ControllerCVRPTW*) controller)->OrigDepot()), ((c_ControllerCVRPTW*) controller)->EndTime( ((c_ControllerCVRPTW*) controller)->DestDepot()),  ((c_ControllerCVRPTW*) controller) ),
	i_networkSize(networkSize),
	i_day(day),
	b_exactDom(exactDom)
{
	c_ControllerCVRPTW* ctlr = (c_ControllerCVRPTW*) Controller();	
	
	////11 can be visited only once
	//assert( ctlr->ArcExistsOnDay(36,11,0) == false);
	//assert( ctlr->ArcExistsOnDay(11, 36, 0) == true );
	//assert( ctlr->ArcExistsOnDay(11, 33, 0) == false );
	//assert( ctlr->ArcExistsOnDay(11, 8, 0) == false );
	//assert( ctlr->ArcExistsOnDay(36, 33, 0) == false );
	//assert( ctlr->ArcExistsOnDay(36, 8, 0) == true );

	////10 has to be visited twice
	//assert( ctlr->ArcExistsOnDay(10, 35, 0) == false );
	//assert( ctlr->ArcExistsOnDay(10, 8, 0) == true );
	//assert( ctlr->ArcExistsOnDay(10, 33, 0) == false );
	//assert( ctlr->ArcExistsOnDay(35, 8, 0) == false );
	//assert( ctlr->ArcExistsOnDay(35, 33, 0) == false );
	
	int n = ctlr->NumNodes();
	//Nur die Network-size n�chsten Nachbarn von jedem Knoten aus als REF anlegen
	for ( int i=1; i<n-1; i++ )//von jedem Knoten ausgehend
	{
		vector<pair<double,int> > sorter;
		for ( int j=1; j<n-1; j++ ){ // zu allen anderen Knoten
			if ( ctlr->ArcExists(i,j) ){
				sorter.push_back( make_pair( ctlr->Cost(i,j), j ) ); //f�ge Bogen zum Sorter dazu
			}
		}
		sort( sorter.begin(), sorter.end() ); //sortiere nach aufsteigenden Kosten

		if( ctlr->NetworkOfTasks() ){
			for ( int j=0; j<min(networkSize, (int)sorter.size()); j++ ){
				int headVertex = ctlr->GetVertex(sorter[j].second, i_day);
				for ( int d = 0; d < ctlr->PlanningHorizon(); d++ ){
					if( ctlr->ArcExistsOnDay(ctlr->GetVertex(i, d), headVertex, i_day))
						o_esppc_solver.Add( ctlr->REF(ctlr->GetVertex(i, d), headVertex, i_day) );
				}
			}
			//add also all allowed arcs between vertices of same node
			for ( int d = 0; d < ctlr->PlanningHorizon(); d++ ){
				if( ctlr->ArcExistsOnDay(ctlr->GetVertex(i, d), ctlr->GetVertex(i, d+1), i_day))
					o_esppc_solver.Add( ctlr->REF(ctlr->GetVertex(i, d), ctlr->GetVertex(i, d+1), i_day) );
			}
		} else if ( ctlr->NetworkOfScheduleParts() ){
			for ( int j=0; j<min(networkSize, (int)sorter.size()); j++ ){
				for (auto spID_i = ctlr->GetAllowedScheduleParts(i).begin(); spID_i != ctlr->GetAllowedScheduleParts(i).end();spID_i++) {
					for (auto spID_j = ctlr->GetAllowedScheduleParts(sorter[j].second).begin(); spID_j != ctlr->GetAllowedScheduleParts(sorter[j].second).end();spID_j) {
						if( ctlr->ArcExistsOnDay(*spID_i, *spID_j, i_day)) //especially if both scheduleParts belong to i_day
							o_esppc_solver.Add( ctlr->REF(*spID_i, *spID_j, i_day) );
					}
				}
			}
		}
	}

	//Add all from/to the depot
	if( ctlr->NetworkOfTasks() ){
		for ( int j=0; j<n; j++ ){
			if ( ctlr->ArcExistsOnDay(0,ctlr->GetVertex(j, i_day), i_day) )
				o_esppc_solver.Add( ctlr->REF(0, ctlr->GetVertex(j, i_day), i_day) );
			if ( ctlr->ArcExists(j,n-1) ){
				for ( int d = 0; d < ctlr->PlanningHorizon(); d++ ){
					if( ctlr->ArcExistsOnDay(ctlr->GetVertex(j, d), ctlr->DestDepotVertex(), i_day))
						o_esppc_solver.Add( ctlr->REF(ctlr->GetVertex(j, d), ctlr->DestDepotVertex(), i_day) );
				}
			}	
		}
	} else if ( ctlr->NetworkOfScheduleParts() ){
		for ( int j=0; j<n; j++ ){
			for (auto spID = ctlr->GetAllowedScheduleParts(j).begin(); spID != ctlr->GetAllowedScheduleParts(j).end();spID++) {
				if ( ctlr->ArcExistsOnDay( ctlr->OrigDepotVertex(i_day), *spID, i_day) ){
					o_esppc_solver.Add( ctlr->REF(ctlr->OrigDepotVertex(i_day), *spID, i_day) );
				}
				if ( ctlr->ArcExistsOnDay( *spID, ctlr->DestDepotVertex(i_day), i_day) ){
					o_esppc_solver.Add( ctlr->REF(*spID, ctlr->DestDepotVertex(i_day), i_day) );
				}
			}
		}
	}

	vector<bool> dominanceNodes_Fw = vector<bool>(ctlr->NumVertices());
	//vector<bool> dominanceNodes_Bw = vector<bool>(ctlr->NumVertices());

	//-->CT:New version of solver does not have  setDominanceNodes_FW
	//if( taskNetwork && ctlr->RestrictVerticesWithDominanceCheck() ){
	//	dominanceNodes_Fw[0] = true;
	//	//dominanceNodes_Bw[0] = true;
	//	for( int i = 1; i < ctlr->NumVertices()-1; i++ ){
	//		//Fw take only the vertex of each customer which belongs to the current day
	//		dominanceNodes_Fw[i] = ctlr->GetDay(i) == i_day ? true : false;
	//		//Bw: there may be several entering vertices of a customer in a specific day network, check if arc from vertex to dest depot exists
	//		/*if( ctlr->ArcExistsOnDay( i, ctlr->DestDepotVertex(), i_day)){
	//		dominanceNodes_Bw[i] = true;
	//		} else{
	//		dominanceNodes_Bw[i] = false;
	//		}*/
	//	}
	//	dominanceNodes_Fw[ctlr->NumVertices()-1] = true;
	//	//dominanceNodes_Bw[ctlr->NumVertices()-1] = true;

	//	o_esppc_solver.setDominanceNodes_FW(dominanceNodes_Fw);
	//	//o_esppc_solver.setDominanceNodes_BW(dominanceNodes_Bw);
	//} else{
	//	for( int i = 0; i < ctlr->NumVertices(); i++ ){
	//		dominanceNodes_Fw[i] = true;
	//		//dominanceNodes_Bw[i] = true;
	//	}
	//	o_esppc_solver.setDominanceNodes_FW(dominanceNodes_Fw);
	//	//o_esppc_solver.setDominanceNodes_BW(dominanceNodes_Bw);
	//}
	//<-
}

void c_PricingSolverCVRPTW::Update( c_BranchAndBoundNode* node )
{
	//This is done in the Update of the Hierarchy now!!
}

void c_PricingSolverCVRPTW::Update( c_DualVariables* dual )
{
	//This is done in the hierarchy now:
	//Decide which arcs should be in the network is done in constructor of PricingSolver, not depending on duals!

}

typedef c_LabelCVRPTW_Fw tLabel_Fw;
typedef c_LabelCVRPTW_Bw tLabel_Bw;

struct cost_comp_fct_Fw : public binary_function<tLabel_Fw const*,tLabel_Fw const*,bool> { 
	bool operator()( tLabel_Fw const* l1, tLabel_Fw const* l2 ) { return ( l1->Cost() < l2->Cost() ); }
};
struct cost_comp_fct_Bw : public binary_function<tLabel_Bw const*,tLabel_Bw const*,bool> { 
	bool operator()( tLabel_Bw const* l1, tLabel_Bw const* l2 ) { return ( l1->Cost() < l2->Cost() ); }
};

function<bool (tLabel_Fw*,tLabel_Fw*)> 
	sorter_Fw = [](tLabel_Fw* l1,tLabel_Fw* l2) 
		{ return l1->Cost() < l2->Cost(); };

function<bool (tLabel_Bw*,tLabel_Bw*)> 
	sorter_Bw = [](tLabel_Bw* l1,tLabel_Bw* l2) 
{ return l1->Cost() < l2->Cost(); };

function<bool (pair<double,pair<tLabel_Fw*,tLabel_Bw*> >,pair<double, pair<tLabel_Fw*,tLabel_Bw*> >)> 
	sorter_bi = [](pair<double, pair<tLabel_Fw*,tLabel_Bw*> > p1,pair<double,pair<tLabel_Fw*,tLabel_Bw*> > p2) 
		{ return p1.first < p2.first; };

int c_PricingSolverCVRPTW::Go( c_DualVariables* dual, list<c_ColumnPricePair>& cols_and_prices, double& best_val )
{
	c_ControllerCVRPTW* ctlr = (c_ControllerCVRPTW*) Controller();
	
	if( i_day != 0 && ctlr->IsAggregated() ){
		return 0; //don't search for new columns on days unequal the first day as long as the constraints are aggregated over the days
	}
	int numColsBefore = (int) cols_and_prices.size();
	exactDom = b_exactDom;
	// Debugging ->
	// c_RMP_CVRP* myrmp = (c_RMP_CVRP*) ctlr->RMP();
	// myrmp->write( "RMP.lp" );
	// <- Debugging

	c_TimeInfo timer;
	timer.Reset();
	timer.Start();

	c_ResVectorCVRPTW_Fw init_res_fw;
	c_ResVectorCVRPTW_Bw init_res_bw;
	init_res_bw.setEndTime(ctlr->EndTime(ctlr->DestDepot())); //Workaround...

	if( minTourDuration ){ 
		init_res_fw.setHelp( -ctlr->EndTime(ctlr->OrigDepot()) );
		init_res_bw.setDuration( ctlr->MaxTourDuration() );
		init_res_bw.setHelp( ctlr->MaxTourDuration() - ctlr->StartTime(ctlr->DestDepot()) );
	}

	int d = ctlr->NetworkOfScheduleParts() ? i_day : -1;
	int source = ctlr->OrigDepotVertex(d);
	int sink = ctlr->DestDepotVertex(d);
	// 3 cases
	vector<tLabel_Fw*> routes_Fw; 
	vector<tLabel_Bw*> routes_Bw; 
	vector<pair<double,pair<tLabel_Fw*,tLabel_Bw*> > > routes_BIDIR; 
	const int max_num_return_paths = 3* ctlr->NumNodes(); // TODO z.b. 4x der Wert  (anzahl der spalten pro pricing)

	if ( i_direction == FW )
	{
		//if( i_day == 3 && i_networkSize > ctlr->NumNodes()){ //i_networkSize > ctlr->NumNodes()
		//	static const int arr1[] = {0,81,82,83,78,75,94,22,79,97}; //0,93,25,55,72,87,20,46,175,46,158,66,45,96,58,161,56,159,207
		//	vector<int> route1 (arr1, arr1 + sizeof(arr1) / sizeof(arr1[0]) );
		//	o_esppc_solver.SolveFw( source, sink, std::move(init_res_fw), route1, max_num_return_paths);
		//} else{
			o_esppc_solver.SolveFw( source, sink, std::move(init_res_fw), max_num_return_paths );
		//}
		o_esppc_solver.GetPathsFW( sink, routes_Fw, &sorter_Fw, max_num_return_paths );
	}
	if ( i_direction == BW )
	{
		o_esppc_solver.SolveBw( source, sink, std::move(init_res_bw), max_num_return_paths );
		o_esppc_solver.GetPathsBW( source, routes_Bw, &sorter_Bw, max_num_return_paths );
	}
	if ( i_direction == BIDIR ) 
	{
		o_esppc_solver.SolveBidir( o_merger, source, sink, std::move(init_res_fw), std::move(init_res_bw), max_num_return_paths );
		o_esppc_solver.GetPathsBidir( sink, routes_BIDIR, &sorter_bi, max_num_return_paths );

	}		
	if ( i_direction == BIDIR_DYN )
	{
		//if( ctlr->InfoNumberOfSolvedNodes() >= 0 && i_networkSize >= 1000){ //i_networkSize > ctlr->NumNodes()
		//	static const int arr1[] = {0,51,86,134,182,38,69,117,59,92,140,188,44,72,120,53,193};
		//	vector<int> route1 (arr1, arr1 + sizeof(arr1) / sizeof(arr1[0]) );
		//	o_esppc_solver.SolveBidir_DynamicHalfway( o_merger, source, sink, std::move(init_res_fw), std::move(init_res_bw), route1, max_num_return_paths);
		//} else{
			o_esppc_solver.SolveBidir_DynamicHalfway( o_merger, source, sink, std::move(init_res_fw), std::move(init_res_bw), max_num_return_paths );
		//}
		o_esppc_solver.GetPathsBidir( sink, routes_BIDIR, &sorter_bi, max_num_return_paths );
	}
	best_val = 0.0;
	for ( int i=0; i<(int) max (max(routes_Fw.size(), routes_Bw.size()), routes_BIDIR.size()); i++ )
	{
		vector<int> tour;
		double rdc = 0.0;
		if ( i_direction == FW )
			rdc = RouteRDC_FW( routes_Fw[i], tour );
		if ( i_direction == BW )
			rdc = RouteRDC_BW( routes_Bw[i], tour );
		if ( i_direction == BIDIR || i_direction == BIDIR_DYN ){
			rdc = RouteRDC_BIDIR( routes_BIDIR[i].second, tour );
			rdc = routes_BIDIR[i].first;
		}
		if (rdc<-0.00001){
			if ( rdc < best_val )
				best_val = rdc;
			double cost = 0.0;
			for ( int p=1; p<(int)tour.size(); p++ )
				cost += ctlr->Cost( ctlr->GetNode(tour[p-1]), ctlr->GetNode(tour[p]) );
			int duration = 0;
			if( minTourDuration ){
				if ( i_direction == FW )
					duration = routes_Fw[i]->Duration();
				if ( i_direction == BW )
					duration = ctlr->MaxTourDuration() - routes_Bw[i]->Duration();
				if ( i_direction == BIDIR || i_direction == BIDIR_DYN )
					duration = ctlr->MaxTourDuration() 
					- min(routes_BIDIR[i].second.second->Duration() - routes_BIDIR[i].second.first->Duration(), 
							routes_BIDIR[i].second.second->Help() - routes_BIDIR[i].second.first->Help() );
			}
			c_TourColumnCVRPTW* col = new c_TourColumnCVRPTW( ctlr, tour, cost, i_day, duration );
			if ( ctlr->InfoLevel() >= 4 )
				ctlr->Info3() << *col << ": " << rdc << endl;
		
			cols_and_prices.push_back( c_ColumnPricePair( col, rdc ) );
			ctlr->IncreaseColumnCounter();

			if( ctlr->AddColumnsOnOtherDays() && !ctlr->DaySpecificBranching() && !ctlr->IsAggregated() ){
				//if( !v_contains(tour, ctlr->GetVertex(ctlr->GetFixedCustomer(), i_day) ) ){ //incorrect to require the day
					//copy column for the other days too
					int d = (i_day +1)%ctlr->PlanningHorizon();
					int numDaysToShift = 1;
					do {
						vector<int> shiftedTour = ctlr->SwitchRouteToOtherDay( tour, numDaysToShift );
						//if route was successfully shifted (for all routes that do not contain the fixed customer)
						if( shiftedTour.size() != 0 ){
							c_TourColumnCVRPTW* col = new c_TourColumnCVRPTW(ctlr, shiftedTour, cost, d, duration );
							if( col->ReducedCosts( *dual ) < -0.00001 ){ //don't add all shifted columns but all with negative rdc
								cols_and_prices.push_back( c_ColumnPricePair( col, rdc ) );
								ctlr->IncreaseColumnCounter();
							}
						}
						d = (d + 1)%ctlr->PlanningHorizon();
						numDaysToShift++;
					} while ( d != i_day );
				//}
			}
		
			double rdc2 = col->ReducedCosts( *dual );
			if ( abs(rdc - rdc2)  > 0.0001 )
			{
				cout << "Wrong rdc: col-rdc=" << rdc2 << " vs. pricing-rdc=" << rdc << endl;
				cout << "Diff: " << rdc2 - rdc << endl;
				cout << "Route: ";
				for ( int p=0; p<(int)tour.size(); p++ )
					cout << tour[p] << ",";
				cout << endl;
				cout << *dual << endl;
				double test = col->ReducedCosts( *dual );
			}
		}
	}

	//if(ctlr->InfoNumberOfSolvedNodes() == 18  && i_networkSize == 1000 &&  cols_and_prices.size() == 0){
	//	cout << "PricingProblemsSolved: " << ctlr->InfoNumberOfSolvedPricingProblems() << ": " ;
	//	cout << ", Generated cols: " << ctlr->InfoNumberOfGeneratedColumns() << endl;

	//	static const int arr1[] = {0,27,30,33,9,35,34,24,51};
	//	vector<int> route1 (arr1, arr1 + sizeof(arr1) / sizeof(arr1[0]) );
	//	c_TourColumnCVRP* col1 = new c_TourColumnCVRP( ctlr, route1, 1185 );
	//	cout << "rdc1 =" <<  col1->ReducedCosts( *dual ) << endl;


	//	static const int arr2[] = {0,28,12,29,3,50,51};
	//	vector<int> route2 (arr2, arr2 + sizeof(arr2) / sizeof(arr2[0]) );
	//	c_TourColumnCVRP* col2 = new c_TourColumnCVRP( ctlr, route2, 656 );
	//	cout << "rdc2 =" <<  col2->ReducedCosts( *dual ) << endl;

	//	static const int arr3[] = {0,31,19,47,49,36,46,48,17,51};
	//	vector<int> route3 (arr3, arr3 + sizeof(arr3) / sizeof(arr3[0]) );
	//	c_TourColumnCVRP* col3 = new c_TourColumnCVRP( ctlr, route3, 1417 );
	//	cout << "rdc3 =" <<  col3->ReducedCosts( *dual ) << endl;

	//	static const int arr4[] = {0,2,15,41,22,40,26,51};
	//	vector<int> route4 (arr4, arr4 + sizeof(arr4) / sizeof(arr4[0]) );
	//	c_TourColumnCVRP* col4 = new c_TourColumnCVRP( ctlr, route4, 812 );
	//	cout << "rdc4 =" <<  col4->ReducedCosts( *dual ) << endl;

	//	static const int arr5[] = {0,5,45,8,18,6,51};
	//	vector<int> route5 (arr5, arr5 + sizeof(arr5) / sizeof(arr5[0]) );
	//	c_TourColumnCVRP* col5 = new c_TourColumnCVRP( ctlr, route5, 716 );
	//	cout << "rdc5 =" <<  col5->ReducedCosts( *dual ) << endl;


	//	static const int arr6[] = {0,7,11,10,32,20,1,51};
	//	vector<int> route6 (arr6, arr6 + sizeof(arr6) / sizeof(arr6[0]) );
	//	c_TourColumnCVRP* col6 = new c_TourColumnCVRP( ctlr, route6, 998);
	//	cout << "rdc6 =" <<  col6->ReducedCosts( *dual ) << endl;

	//	static const int arr7[] = {0,21,23,39,25,4,51};
	//	vector<int> route7 (arr7, arr7 + sizeof(arr7) / sizeof(arr7[0]) );
	//	c_TourColumnCVRP* col7 = new c_TourColumnCVRP( ctlr, route7, 890 );
	//	cout << "rdc7 =" <<  col7->ReducedCosts( *dual ) << endl;


	//	static const int arr8[] = {0,42,16,44,38,14,43,37,13,51};
	//	vector<int> route8 (arr8, arr8 + sizeof(arr8) / sizeof(arr8[0]) );
	//	c_TourColumnCVRP* col8 = new c_TourColumnCVRP( ctlr, route8, 1194 );
	//	cout << "rdc8 =" <<  col8->ReducedCosts( *dual ) << endl;
	//}

	//if( ctlr->InfoNumberOfSolvedNodes() >= 9 && cols_and_prices.size() == 0 && i_day == 3 && i_networkSize >= 1000 ){ //
	//	cout << "NumberOfSolvedNodes= " << ctlr->InfoNumberOfSolvedNodes() << endl;
	//	ifstream file("mod0_pr04_UB.txt", ios_base::in );
	//	string line;
	//	//ignore first line (num vehicles)
	//	//getline(file,line);
	//	int lineCounter = 1;
	//	cout << *dual << endl;
	//	while(getline(file,line))
	//	{
	//		std::stringstream linestream(line);
	//		string my_str;
	//		vector<int> route;
	//		
	//		//read vcId
	//		linestream.ignore(1000, '=');  
	//		getline(linestream,my_str,',');
	//		int day = stoi( my_str);

	//		linestream.ignore(1000, '[');
	//		while(getline(linestream,my_str,','))
	//		{
	//			if( my_str == "]" ){
	//				break;
	//			}
	//			route.push_back( stoi( my_str) );
	//		}
	//	
	//		//read cost
	//		linestream.ignore(1000, '=');
	//		getline(linestream,my_str,',');
	//		double cost = stod( my_str);

	//		c_TourColumnCVRPTW* col = new c_TourColumnCVRPTW(ctlr, route, cost, day, 0, true);


	//		//TODO: remove! only for testing: add them only once
	//		/*if( ctlr->InfoNumberOfSolvedNodes() == 9 )
	//		cols_and_prices.push_back( c_ColumnPricePair( col, 0 ) );*/


	//		cout << "rdc =" <<  col->ReducedCosts( *dual ) << endl;
	//		if( col->ReducedCosts( *dual )  < -CG_EPS){
	//			cout << "Route " << lineCounter << " has negative rdc! Error." << endl;
	//			cout << *dual << endl;
	//			//throw;
	//		}
	//		lineCounter++;
	//	}
	//}

	ctlr->AddToExtendedFW(o_esppc_solver.InfoExtendedFW());
	ctlr->AddToExtendedBW(o_esppc_solver.InfoExtendedBW());

	timer.Stop();

	if( ctlr->InfoLevel() >= 4){
		cout << setw(8) << timer.MilliSeconds() << "ms: " << (b_exactDom? "Exact" : "Heuristic") <<  " Pricer with nwSize = " << i_networkSize << " for day " << i_day << " has generated " << ((int) cols_and_prices.size() - numColsBefore) << " column(s)." << endl;
	}

	if ( i_networkSize < ctlr->NumNodes() )
		best_val = -10 * INFTY;


	/*ofstream addedCols( "AddedCols.txt");
	for each ( c_ColumnPricePair colprice in cols_and_prices ){
	c_TourColumnCVRPTW* tour_col = (c_TourColumnCVRPTW*) colprice.first;
	addedCols << tour_col->GetCPXObj() << " " << *tour_col << endl;
	}
	addedCols.close();*/

	return (int)cols_and_prices.size();
}

double c_PricingSolverCVRPTW::RHSofConvexityConstraint()
{
	c_ControllerCVRPTW* ctlr = (c_ControllerCVRPTW*) Controller();
	if( ctlr->IsAggregated() )
		return (double)ctlr->NumberOfVehicles() * ctlr->PlanningHorizon(); //total num vehicles * max frequency
	else
		return (double)ctlr->NumVehiclesPerDay();
}

double c_PricingSolverCVRPTW::RouteRDC_FW( const tLabel_Fw* label, vector<int>& route ) const
{
	route.clear();
	double rdc = label->Cost();
	if ( label->generator_REF() )
		route.push_back( label->generator_REF()->Head() );
	while ( label->generator_REF() )
	{
		route.push_back( label->generator_REF()->Tail() );
		label = label->predecessor();
	}
	reverse( route.begin(), route.end() );
	return rdc;
}

double c_PricingSolverCVRPTW::RouteRDC_BW( const tLabel_Bw* label, vector<int>& route ) const
{
	route.clear();
	double rdc = label->Cost();
	if ( label->generator_REF() )
		route.push_back( label->generator_REF()->Tail() );
	while ( label->generator_REF() )
	{
		route.push_back( label->generator_REF()->Head() );
		label = label->predecessor();
	}
	return rdc;
}

double c_PricingSolverCVRPTW::RouteRDC_BIDIR( const pair<tLabel_Fw*,tLabel_Bw*> labels, vector<int>& route ) const
{
	route.clear();
	const tLabel_Fw* label_FW = labels.first;	
	const tLabel_Bw* label_BW = labels.second;
	// Merge node and bitsets cutCounters of generating REFs
	int mergenode = 0;
	if ( label_FW->generator_REF() ){
		route.push_back( label_FW->generator_REF()->Head() );
		mergenode = label_FW->generator_REF()->Head();	
	}
	else{
		route.push_back( label_BW->generator_REF()->Tail() );
		mergenode = label_BW->generator_REF()->Tail();
	}
	
	// fw part
	while ( label_FW->generator_REF() )
	{
		route.push_back( label_FW->generator_REF()->Tail() );
		label_FW = label_FW->predecessor();
	}
	reverse( route.begin(), route.end() );
	// bw part
	while ( label_BW->generator_REF() )
	{
		route.push_back( label_BW->generator_REF()->Head() );
		label_BW = label_BW->predecessor();
	}
	return 0; //not used anymore
}


/////////////////////////////////////////////////////////////////////////////
//   c_PricingSolverHierarchyCVRP
/////////////////////////////////////////////////////////////////////////////

c_PricingSolverHierarchyCVRPTW::c_PricingSolverHierarchyCVRPTW( c_Controller* controller, int max_num_failures, int min_num_columns, int day )
	: c_PricingProblemHierarchy( controller, max_num_failures, min_num_columns ),
		i_day(day)
{
	//c_ControllerCVRPTW* ctlr = (c_ControllerCVRPTW*) Controller();
	//int v = ctlr->NumVertices();
	////Alle REFs anlegen, im Update des Solvers wird dann aussortiert je nach NetworkSize
	//for (int i = 0; i<v; i++)
	//	for (int j = 0; j<v; j++)
	//		if (ctlr->ArcExistsOnDay(i, j, i_day))
	//			o_esppc_solver.Add(ctlr->REF(i, j, i_day));

}


void c_PricingSolverHierarchyCVRPTW::Update(c_BranchAndBoundNode* node)
{
	c_ControllerCVRPTW* ctlr = (c_ControllerCVRPTW*) Controller();
	ctlr->SetDaySpecificBranching(false);
	/*ctlr->ResetTogetherness();
	ryanFosterActive = false;*/
	int v = ctlr->NumVertices();
	// deactivate REFs that are forbidden
	for ( int i=0; i<v; i++ )
		for ( int j=0; j<v; j++)
			if ( ctlr->REF(i,j,i_day) != NULL ) //zuvor ArcExists( i, j )
			{
				ctlr->REF(i,j,i_day)->ResetIncrementCutCounter_Fw();
				ctlr->REF(i,j,i_day)->ResetIncrementCutCounter_Bw();
				bool active = true;
				c_BranchAndBoundNode* my_node = node;
				while( my_node )
				{
					const list<c_BranchAndBoundConstraint*>& con_list = my_node->AddedConstraints();

					for ( auto ii=con_list.begin(); ii!=con_list.end(); ++ii )
					{
						if (dynamic_cast<c_BranchAndBoundConstraintCVRPTW*>(*ii) !=0){
							//REFs an Branching-Decisions anpassen, d.h. gewisse Arcs verbieten
							c_BranchAndBoundConstraintCVRPTW* help_con = (c_BranchAndBoundConstraintCVRPTW*) (*ii);
							if (help_con->Type() == c_BranchAndBoundConstraintCVRPTW::eCustomerPairDay )
							{
								ctlr->SetDaySpecificBranching(true);
								c_BranchAndBoundConstraintNodeArc* con = (c_BranchAndBoundConstraintNodeArc*) (*ii);
								if( con->Day() == i_day ){
									int tailNode = con->Tail();
									int headNode = con->Head();
								
									if ( con->Enforced() ) // (tail,head) = 1
									{
										if ( ( ctlr->GetNode(i) == tailNode && ctlr->GetNode(j) != headNode ) 
											|| ( ctlr->GetNode(i) != tailNode && ctlr->GetNode(j) == headNode ) )
											active = false;
									}
									else // (tail,head) = 0
									{
										if ( ctlr->GetNode(i) == tailNode && ctlr->GetNode(j) == headNode )
											active = false;
									}
								}
							} else if( help_con->Type() == c_BranchAndBoundConstraintCVRPTW::eCustomerDay ){
								ctlr->SetDaySpecificBranching(true);
							}
							// else if( help_con->Type() == c_BranchAndBoundConstraintCVRPTW::eCustsTogether ){ //Ryan-Foster-Branching
							//	c_BranchAndBoundConstraintCustsTogether* con = (c_BranchAndBoundConstraintCustsTogether*) (*ii);
							//	ctlr->SetTogetherness(con->Customer1(), con->Customer2(), con->Together());
							//	ryanFosterActive = true;
							//}
						} //else Cut, but that are globally valid (so use all, not only those of father, done below)
					}
					my_node = my_node->Father();
				}

				//consider all subset row cuts (not only those found by going to the fathers), also easier for change from aggregated SR cuts to single SR cuts
				//REF-Bitsets aufgrund der Cuts setzen
				int numSR = ctlr->NumSubsetRowCuts();
				for( int cutId = 0; cutId < ctlr->NumSubsetRowCuts(); cutId++ ){
					if( ctlr->RMP()->ConstraintIndexExists( c_RMP_CVRPTW::eAggregatedSubsetRowCut, cutId ) ){
						c_AggregatedSubsetRowCutConstraintCVRPTW*  srCut = (c_AggregatedSubsetRowCutConstraintCVRPTW*)ctlr->RMP()->Constraint(c_RMP_CVRPTW::eAggregatedSubsetRowCut, cutId);
						for( int d= 0; d < ctlr->PlanningHorizon(); d++){
							vector<int> subset = srCut->GetSubset(d);
							if( taskNetwork ){
								if( v_contains( subset, j) ){
									ctlr->REF( i, j, i_day )->SetCutIncrement_Fw( srCut->getCutId()+d*numSR );
								}
								if( v_contains( subset, i) ){
									ctlr->REF( i, j, i_day )->SetCutIncrement_Bw( srCut->getCutId()+d*numSR );
								}
							} else if( schedulePartNetwork) {
								if( ctlr->TaskOfSubsetContainedInSP(subset, j) ){
									ctlr->REF( i, j, i_day )->SetCutIncrement_Fw( srCut->getCutId()+d*numSR );
								}
								if( ctlr->TaskOfSubsetContainedInSP(subset, i) ){
									ctlr->REF( i, j, i_day )->SetCutIncrement_Bw( srCut->getCutId()+d*numSR );
								}
							}
						}
					} else if( ctlr->RMP()->ConstraintIndexExists( c_RMP_CVRPTW::eSubsetRowCut, cutId ) ) {
						c_SubsetRowCutConstraintCVRPTW*  srCut = (c_SubsetRowCutConstraintCVRPTW*)ctlr->RMP()->Constraint(c_RMP_CVRPTW::eSubsetRowCut, cutId);
						if( taskNetwork ){
							if( v_contains( srCut->GetSubset(), j) ){
								ctlr->REF( i, j, i_day )->SetCutIncrement_Fw( srCut->getCutId() );
							}
							if( v_contains( srCut->GetSubset(), i) ){
								ctlr->REF( i, j, i_day )->SetCutIncrement_Bw( srCut->getCutId() );
							}
						} else if( schedulePartNetwork) {
							if( ctlr->TaskOfSubsetContainedInSP(srCut->GetSubset(), j) ){
								ctlr->REF( i, j, i_day )->SetCutIncrement_Fw( srCut->getCutId() );
							}
							if( ctlr->TaskOfSubsetContainedInSP(srCut->GetSubset(), i) ){
								ctlr->REF( i, j, i_day )->SetCutIncrement_Bw( srCut->getCutId() );
							}
						}
					}
				}
				ctlr->ActivateREF( i, j, i_day, active );
			}

	c_PricingProblemHierarchy::Update(node);
}

void c_PricingSolverHierarchyCVRPTW::Update(c_DualVariables* dual)
{
	c_ControllerCVRPTW* ctlr = (c_ControllerCVRPTW*) Controller();
	c_DualVariablesCVRPTW* my_dual = (c_DualVariablesCVRPTW*) dual;

	if( ctlr->IsAggregated() ){
		int numPeriods = ctlr->PlanningHorizon(); 
		vector<double> sriDuals(numPeriods * ctlr->NumSubsetRowCuts()); 
		vector<vector<int> > sriSubsets(numPeriods * ctlr->NumSubsetRowCuts());
		vector<int> activeCutIds;

		int numSR = ctlr->NumSubsetRowCuts();
		for( int cutId = 0; cutId < ctlr->NumSubsetRowCuts(); cutId++ ){
			if( ctlr->RMP()->ConstraintIndexExists( c_RMP_CVRPTW::eAggregatedSubsetRowCut, cutId ) ){
				for( int d = 0; d< numPeriods; d++ ){
					sriDuals[cutId + numSR * d] = my_dual->sigma(cutId); 
					sriSubsets[cutId + numSR *d] = (((c_AggregatedSubsetRowCutConstraintCVRPTW*) ctlr->RMP()->Constraint(c_RMP_CVRPTW::eAggregatedSubsetRowCut, cutId))->GetSubset(d));
					if (my_dual->sigma(cutId) < -CG_EPS * CG_EPS){
						activeCutIds.push_back(cutId + numSR *d);
					}
				}
			}
		}
		ctlr->SetDualsToSRIs(sriDuals);
		ctlr->SetSRIs(sriSubsets);
		ctlr->SetActiveCutIds(activeCutIds);
	}else{
		vector<double> sriDuals(ctlr->NumSubsetRowCuts()); 
		vector<vector<int> > sriSubsets(ctlr->NumSubsetRowCuts());
		vector<int> activeCutIds;

		for( int cutId = 0; cutId < ctlr->NumSubsetRowCuts(); cutId++ ){
			if( ctlr->RMP()->ConstraintIndexExists( c_RMP_CVRPTW::eSubsetRowCut, cutId ) ){
				sriDuals[cutId] = my_dual->sigma(cutId); 
				sriSubsets[cutId] = (((c_SubsetRowCutConstraintCVRPTW*) ctlr->RMP()->Constraint(c_RMP_CVRPTW::eSubsetRowCut, cutId))->GetSubset());
				if (my_dual->sigma(cutId) < -CG_EPS * CG_EPS){
					activeCutIds.push_back(cutId);
				}
			}
		}
		ctlr->SetDualsToSRIs(sriDuals);
		ctlr->SetSRIs(sriSubsets);
		ctlr->SetActiveCutIds(activeCutIds);
	}

	//TODO: Ineffizient, lieber nur �ber alle Knoten die beiden For-Schleifen und nur bei Rho und SetRdc nach Tag differenzieren
	int v = ctlr->NumVertices();
	for ( int i=0; i<v; i++ ){
		for ( int j=0; j<v; j++){
			if ( ctlr->REF(i,j,i_day) != NULL ) //ctlr->ArcExists(i,j)
			{
				int nodeI = ctlr->GetNode(i);
				int nodeJ = ctlr->GetNode(j);
				double cost = ctlr->Cost( nodeI, nodeJ ); 
				if ( nodeI != ctlr->OrigDepot() ){
					//dual price of belonging to a schedule part is substracted only when leaving a customer (only then the number of fulfilled demand periods is known)
					int spId = -1;
					if( ctlr->NetworkOfTasks() ){
						if( nodeI != nodeJ ){
							//cost -= my_dual->rho( nodeI, i_day, ctlr->GetDay(i) );
							if( ctlr->IsAggregated() && ctlr->RegularVisits() ){
								//new version: coefficent is number of covered tasks
								cost -= ctlr->GetNumTasksCovered( i_day, ctlr->GetDay(i) ) * my_dual->tau( nodeI );

								//old version: coefficent is number of visits
								//cost -= my_dual->tau( nodeI );	
							} else{
								int spId = ctlr->GetSchedulePartId(nodeI, i_day, ctlr->GetDay(i) );
								cost -= my_dual->rho( spId );
							}
						}
					} else if ( ctlr->NetworkOfScheduleParts() ){
						if( ctlr->IsAggregated() && ctlr->RegularVisits() ){
							//new version: coefficent is number of covered tasks
							cost -= ctlr->GetNumTasksCoveredBySP(i) * my_dual->tau( nodeI );

							//old version: coefficent is number of visits
							//cost -= my_dual->tau( nodeI );
						} else{
							cost -= my_dual->rho( i );
						}
					}

					//if a vertex is left, that does not belong to the network day, no dual price is substracted
					//if( ctlr->GetDay(i) == i_day )
					//cost -= my_dual->pi( ctlr->GetNode(i), ctlr->GetDay(i) ); //i, i_day
				}else{
					cost -= my_dual->mu(i_day);
				}

				//Incorporate capacity cuts -> Attention: Forward on outgoing arcs, backward on ingoing arcs
				if( ctlr->NumCapacityCuts() > 0 ){
					for (int cutId = 0; cutId < ctlr->NumCapacityCuts(); cutId++){
						if( ctlr->RMP()->ConstraintIndexExists( c_RMP_CVRPTW::eCapacityCut, cutId ) ){
							c_CapacityCutConstraintCVRPTW* capCut = (c_CapacityCutConstraintCVRPTW*) ctlr->RMP()->Constraint(c_RMP_CVRPTW::eCapacityCut, cutId);
							if( v_contains(capCut->GetCustomerSubset(), i) && !v_contains(capCut->GetCustomerSubset(), j) ){
								cost -= my_dual->phi(cutId);//TODO: store info more efficient in capCut * capCut->Coefficient(i,j); 
							}
						}
					}
				}


				for (int cutId = 0; cutId <ctlr->NumArcSetConstraints(); cutId++) {
					if (ctlr->RMP()->ConstraintIndexExists(c_RMP_CVRPTW::eARCSET, cutId)){
						c_ArcSetConstraint* cut = (c_ArcSetConstraint*)ctlr->RMP()->Constraint(c_RMP_CVRPTW::eARCSET, cutId);
						if( cut->Day() == i_day )
							cost -= (my_dual->alpha(cutId) * cut->Coefficient(ctlr->GetNode(i), ctlr->GetNode(j)));
						//costBW -= (my_dual->alpha(t) * cut->Coefficient(i, j));
					}
				}

				ctlr->SetRDC( i, j, i_day, cost ); 
			}
		}
	}
}

int c_PricingSolverHierarchyCVRPTW::Go(c_DualVariables* dual, list<c_ColumnPricePair>& cols_and_prices, int NumPricersUsed)
{
	c_ControllerCVRPTW* ctlr = (c_ControllerCVRPTW*)Controller();
	//NowTODO
	int num_generated_cols = 0;
	d_best_reduced_costs = -10 * INFTY;
	int start_level = min(i_pricing_start_level, (int)v_hierarchy.size() - 1);
	for (int pricing_level = start_level; pricing_level < (int)v_hierarchy.size(); pricing_level++)
	{
		if (ctlr->TimeOut())
			return 0;

		// call pricer
		c_PricingProblem* pricer = v_hierarchy[pricing_level];
		pricer->Update(dual);
		double value = 0.0;
		int cols_before = (int)cols_and_prices.size();
		if (ctlr->InfoLevel() >= 3)
			ctlr->Info3() << "[" << pricing_level << "] ";
		int pricer_return_val = pricer->Go(dual, cols_and_prices, value);
		num_generated_cols += ((int)cols_and_prices.size() - cols_before);
		ctlr->Set_number_of_solved_pricing_problems(ctlr->InfoNumberOfSolvedPricingProblems() + 1);
		// update d_best_reduced_costs
		if (pricing_level == (int)v_hierarchy.size() - 1)
		{
			d_best_reduced_costs = value;
			if (num_generated_cols == 0)
				d_best_reduced_costs = 0.0;
		}

		// update failures and start level
		v_num_failures[pricing_level] = (num_generated_cols < i_min_num_columns) ? v_num_failures[pricing_level] + 1 : v_num_failures[pricing_level]; //else 0 if consecutive failures are counted, otherwise v_num_failures[pricing_level]

		if (!ctlr->PricingHierarchyStandardOrder()) { //pricing_level > p_controller->GetMinPricingStartLevel()

			//if not all failed attempts of lower level are used, go back
			if (num_generated_cols >= i_min_num_columns && pricing_level > 0 && ctlr->GetNumFailures(pricing_level - 1) < i_max_num_failures) {
				i_pricing_start_level = pricing_level - 1;
			}

			return num_generated_cols;
		}

		if (v_num_failures[pricing_level] >= i_max_num_failures) { //in not standard order, the pricing level is increased in function Pricing()
			i_pricing_start_level = pricing_level + 1;
		}

		// sufficiently many cols generated?
		if (num_generated_cols >= i_min_num_columns)
			break;

	}
	//// update failures and start level
	//for( int i = 0; i < (int)v_hierarchy.size()-1; i++){
	//	if( v_num_failures[i] >= i_max_num_failures ){
	//		i_pricing_start_level = i+1;
	//	}
	//}
	return num_generated_cols;
}

///////////////////////////////////////////////////////////////////////////////
// c_MergeManagerCVRP
///////////////////////////////////////////////////////////////////////////////

c_MergeManagerCVRPTW::c_MergeManagerCVRPTW( int capacity, int startTime, int endTime, c_ControllerCVRPTW* ctlr, int halfway_point )
:	i_capacity( capacity ),
	i_startTime ( startTime),
	i_endTime ( endTime ),
	_controller( ctlr ),
	i_halfway_point( halfway_point<0 ? (int) ceil((startTime + endTime)/2.0) : halfway_point )
{
	i_halfway_point_fw = i_halfway_point_bw = i_halfway_point;
}



bool c_MergeManagerCVRPTW::MergeableFW( const LabelFW& label_fw ) const
{
	// no merge at origin node
	return ( label_fw.generator_REF() != nullptr );
}


bool c_MergeManagerCVRPTW::MergeableBW( const LabelBW& label_bw ) const
{
	return true;
}

bool c_MergeManagerCVRPTW::Mergeable( const LabelFW& label_fw, const LabelBW& label_bw, double& rdc ) const
{
	// exclude duplicate solutions
	if ( ( label_fw.Time() <= i_halfway_point ) && label_bw.generator_REF() )
		return false;
	// exclude infeasible solutions
	if ( ( label_fw.l_visit & label_bw.l_visit ) > 1 )
		return false;
	double load = label_fw.predecessor() ? label_fw.predecessor()->Load() + label_bw.Load() : label_fw.Load() + label_bw.predecessor()->Load();
	if ( load > i_capacity )
		return false;
	if( label_fw.Time() > label_bw.Time() )
		return false;

	if( minTourDuration ){
		if( label_fw.Duration() > label_bw.Duration() || label_fw.Help() > label_bw.Help() )
			return false;
	}

	//if( ryanFosterActive ){
	//	//TODO: many routes will be discarded here
	//	throw;
	//}

	int mergeVertex = ( label_fw.generator_REF() ? label_fw.generator_REF()->Head() : label_bw.generator_REF()->Tail() );
	rdc = 0.0; 
	rdc = label_fw.Cost() + label_bw.Cost();  

	for (auto cutId = _controller->getActiveCutIds().begin(); cutId != _controller->getActiveCutIds().end();cutId++) {
		bool singleSRCutExists = !_controller->IsAggregated() &&  _controller->RMP()->ConstraintIndexExists(c_RMP_CVRPTW::eSubsetRowCut,*cutId);
		bool aggregatedSRCutExists = _controller->IsAggregated() && _controller->RMP()->ConstraintIndexExists(c_RMP_CVRPTW::eAggregatedSubsetRowCut, ((*cutId)%_controller->NumSubsetRowCuts()) );
		if(singleSRCutExists || aggregatedSRCutExists ){
			//if mergeVertex is not in cut subset, but cut is active forward and backward: punish one more time by this cut
			if( taskNetwork ){
				if (!v_contains(_controller->getSRIs()[*cutId], mergeVertex)){	
					if (label_fw.Bs_SRI(*cutId) && label_bw.Bs_SRI(*cutId) ){
						rdc -=  _controller->getSRIDual(*cutId); //increase rdc (sigma is negative)
					}
				}
				//if mergeVertex is in cut subset and cut is not-active forward and backward: redo one punishment
				if (v_contains(_controller->getSRIs()[*cutId], mergeVertex)){	
					if ( !label_fw.Bs_SRI(*cutId) && !label_bw.Bs_SRI(*cutId) ){ 
						rdc +=  _controller->getSRIDual(*cutId); 
					}
				}
			} else if( schedulePartNetwork ){
				if ( !_controller->TaskOfSubsetContainedInSP( _controller->getSRIs()[*cutId], mergeVertex) ){	 
					if (label_fw.Bs_SRI(*cutId) && label_bw.Bs_SRI(*cutId) ){
						rdc -=  _controller->getSRIDual(*cutId); //increase rdc (sigma is negative)
					}
				}
				//if mergeVertex is in cut subset and cut is not-active forward and backward: redo one punishment
				if ( _controller->TaskOfSubsetContainedInSP( _controller->getSRIs()[*cutId], mergeVertex) ){	
					if ( !label_fw.Bs_SRI(*cutId) && !label_bw.Bs_SRI(*cutId) ){ 
						rdc +=  _controller->getSRIDual(*cutId); 
					}
				}
			}			
		}
	}
	return ( rdc <= -0.001 );
}

bool c_MergeManagerCVRPTW::BeforeHalfWayPointFW( const LabelFW& label_fw ) const
{
	return ( label_fw() <= i_halfway_point_fw );
}

bool c_MergeManagerCVRPTW::BeforeHalfWayPointBW( const LabelBW& label_bw ) const
{
	return ( label_bw() > i_halfway_point_bw ); // time decreases in bw-solver
}




///////////////////////////////////////////////////////////////////////////////
// Separation Problem
///////////////////////////////////////////////////////////////////////////////


c_SeparationProblemCVRPTW::c_SeparationProblemCVRPTW(c_Controller* controller, c_CVRPTW_Instance_With_RDC& inst)
	:	c_SeparationProblem( controller )
	//o_tester(inst),
	//o_generator_DAGHeuristic( ((c_ControllerCVRPTW*)controller)->KPathMaxSizeOfSets(), (((c_ControllerCVRPTW*)controller)->KPathMaxInclusion()) ), 
	//o_separator_DAGHeuristic(o_tester, o_generator_DAGHeuristic),
	//o_generator_RopkeCordeauHeuristic(10,10), //((c_ControllerCVRPTW*)controller)->KPathMaxSizeOfSets(), ((c_ControllerCVRPTW*)controller)->KPathRestarts()),
	//o_separator_RopkeCordeauHeuristic(o_tester, o_generator_RopkeCordeauHeuristic),
	//random_generator(-1)
{
	c_ControllerCVRPTW* ctlr = (c_ControllerCVRPTW*) Controller();
	//lhsPerSubset = c_DoubleMatrix(ctlr->NumVertices()-1, ctlr->NumVertices()-1, ctlr->NumVertices()-1); //only for customer nodes relevant values, but for simpler accessibility also index for origDepot
	lhsPerSubset = c_DoubleMatrix(ctlr->NumNodes()-1, ctlr->NumNodes()-1, ctlr->NumNodes()-1); //only for customer nodes relevant values, but for simpler accessibility also index for origDepot
}


int c_SeparationProblemCVRPTW::Go(c_RMP* rmp, list<c_Constraint*>& cuts, int node_level)
{
	c_ControllerCVRPTW* ctlr = (c_ControllerCVRPTW*) Controller();
	c_RMP_CVRPTW* my_rmp = (c_RMP_CVRPTW*) rmp;
	my_rmp->UpdateFlows();


	if(IsFractional(my_rmp->NumVehicles()) && ctlr->BranchVehicleFirst())
		return 0;

	if( node_level <= ctlr->MaxCutNodeLevel() ){
		if( ctlr->UseDynamicNgNeighborhood() ){
			EliminateCyclesByExtendingNeighborhood( cuts );
		}
	}

	if( ctlr->SeparateAggregatedSRCuts() ){
		if( ctlr->StartWithAggregation() && !ctlr->IsAggregated() ){
		//after disaggregation, let BranchAndBoundConstraint break the symmetry	(no more SR cuts)
		return 0;
		//generate SR Cuts after disaggregation (after disaggregation, let first the branching break the symmetry; when this is done, separate SR cuts again)
		/*if( ctlr->BreakSymmetry() && ctlr->GetFixedCustomer() == -1 ){
		return 0;
		}*/
		}
	} else{
		//Variant without aggregated SR-Cuts: don't separate cuts as long as model is aggregated
		if( ctlr->IsAggregated() ){
			if( cuts.size() == 0 ){
				ctlr->Disaggregate();
				if( ctlr->BreakSymmetry() ){
					return 0;
					//let BranchAndBoundConstraint break the symmetry
				}
			} else{
				//do not disaggregate if cycles were eliminated
				return cuts.size();
			}
		}
	}

	if( ctlr->UseSubsetRowCuts() ){
		int factorNumSRCuts = ctlr->IsAggregated()? ctlr->PlanningHorizon() : 1;
		if( node_level <= ctlr->MaxCutNodeLevel() && ctlr->NumSubsetRowCuts() * factorNumSRCuts  < ctlr->MaxNumSubsetRow()){
			SeparateSubSetRowCuts(rmp, cuts);
		}
	} 
	if( ctlr->UseCapacityCuts()  ){
		cout << "Capacity Cuts not allowed. Error." << endl;
		throw;
		//SeparateCapacityCuts(rmp, cuts);
	}
	if( ctlr->UseTwoPathCuts()  ){
		cout << "TwoPath-Cuts not allowed. Error." << endl;
		throw;
		//Separate2PathCuts(rmp, cuts);
	}

	return (int)cuts.size();
}

int c_SeparationProblemCVRPTW::SeparateSubSetRowCuts(c_RMP* rmp, list<c_Constraint*>& cuts)   
{		
	//cout << "Start separation of subset-row cuts..." << endl;
	c_ControllerCVRPTW* ctlr = (c_ControllerCVRPTW*) Controller();
	c_RMP_CVRPTW* my_rmp = (c_RMP_CVRPTW*) rmp;

	if( ctlr->InfoLevel() >= 2 ){
		cout << "Start separation of SR cuts." << endl;
	}

	//Overall number of subset-row cuts is limited
	if(ctlr->NumSubsetRowCuts() > ctlr->MaxNumSubsetRow()){
		return 0;
	}		
	
	int k = 2; 
	double eps = ctlr->CutTolerance();
	double val;

	int startVertex;
	int endVertex;
	if( taskNetwork ){
		startVertex = 1;
		endVertex = ctlr->NumVertices()-1;
	} else if( schedulePartNetwork ){
		startVertex = ctlr->PlanningHorizon(); //number of vertices for startDepot
		endVertex = ctlr->NumVertices() - ctlr->PlanningHorizon();
	}
	c_TimeInfo o_separationTimer;
	o_separationTimer.Start();
	
	for( int d = 0; d < ctlr->PlanningHorizon(); d++ ){

		if( ctlr->IsAggregated() && d > 0){
			return (int)cuts.size();
		}
		
		lhsPerSubset.Initialisierung(0.0);
		//iterate over all chosen tour columns
		for (int i=0;i<ctlr->RMP()->NumCols();i++){ 
			c_BaseColumnCVRPTW* col = (c_BaseColumnCVRPTW*) ctlr->ColumnInRMP(i);						
			if(col->Type() == c_BaseColumnCVRPTW::eTOUR){
				ctlr->RMP()->getX(&val,i,i);					
				if( val > CG_EPS ){	
					c_TourColumnCVRPTW* tourCol = (c_TourColumnCVRPTW*) col;
					//add for all subsets of 3 tasks its contribution to the left hand side of the sr cut
					for( int s1 = 1; s1 < ctlr->NumCustomers()-2; s1++ ){
						for( int s2 = s1 + 1; s2 < ctlr->NumCustomers()-1; s2++ ){ 
							for( int s3 = s2 + 1; s3 < ctlr->NumCustomers() ; s3++){ //s1 < s2 < s3
								lhsPerSubset( s1, s2, s3 ) += 
									val * ((tourCol->countVisits( ctlr->GetTask(s1, d) ) + tourCol->countVisits( ctlr->GetTask(s2, d) ) + tourCol->countVisits( ctlr->GetTask(s3, d) )) / k) ; //used that division by Integer cuts off decimal places
								if( ctlr->IsAggregated() ){
									for( int d2 = 1; d2 < ctlr->PlanningHorizon(); d2++ ){
										lhsPerSubset( s1, s2, s3 ) += 
											val * ((tourCol->countVisits( ctlr->GetTask(s1, d2) ) + tourCol->countVisits( ctlr->GetTask(s2, d2) ) + tourCol->countVisits( ctlr->GetTask(s3, d2) )) / k) ; //used that division by Integer cuts off decimal places
									}
								}	
							}
						}
					}
				}
			}
		}

		//iterate through all subsets of 3 vertices. Store those that belong to violated SR cuts.
		vector<pair<double, vector<int> >> violatingSubsets; 
		int cnt_FoundViolatedCuts = 0;
		for( int s1 = 1; s1 < ctlr->NumCustomers()-2; s1++ ){
			for( int s2 = s1 + 1; s2 < ctlr->NumCustomers()-1; s2++ ){ 
				for( int s3 = s2 + 1; s3 < ctlr->NumCustomers() ; s3++){ //s1 < s2 < s3
					//if lhs of subsets violates cut by more than eps
					int rhs = ctlr->IsAggregated()? ctlr->PlanningHorizon() : 1;
					if( lhsPerSubset( s1, s2, s3 ) > rhs + eps  ){  
						vector<int> subset;
						subset.push_back( ctlr->GetTask(s1, d) );
						subset.push_back( ctlr->GetTask(s2, d) );
						subset.push_back( ctlr->GetTask(s3, d) );
						violatingSubsets.push_back( make_pair(lhsPerSubset( s1, s2, s3 ), subset));	
						cnt_FoundViolatedCuts += 1; 
					}
				}
			}
		}

		int added_cuts = 0;
		sort(violatingSubsets.begin(), violatingSubsets.end());
		vector<int> v_addedSRCutsPerCustomer = vector<int>( ctlr->NumTasks(), 0 );
		int factorNumSRCuts = ctlr->IsAggregated()? ctlr->PlanningHorizon() : 1;
		for (int i = 0; (ctlr->NumSubsetRowCuts()+1) * factorNumSRCuts < ctlr->MaxNumSubsetRow() && i < (int)violatingSubsets.size() && added_cuts < ctlr->MaxNumberSRCutsPerIteration(); ++i)
		{
			bool to_add = true;
			for (int j = 0; j<(int)violatingSubsets[i].second.size(); j++)
			{
				int cust = violatingSubsets[i].second[j];
				if (v_addedSRCutsPerCustomer[cust] >= ctlr->MaxSRCutsPerCustomerPerIteration())
					to_add = false;
			}
			if (to_add)
			{
				if( ctlr->IsAggregated() ){
					cuts.push_back( new c_AggregatedSubsetRowCutConstraintCVRPTW(ctlr, violatingSubsets[i].second ) );
				} else{
					cuts.push_back( new c_SubsetRowCutConstraintCVRPTW(ctlr, violatingSubsets[i].second ) );
				}
				//cuts.push_back(new c_Constraint_SubSetRow(ctlr, violatingSubsets[i].second, ctlr->NumSubsetRowCuts()));
				for (int j = 0; j<(int)violatingSubsets[i].second.size(); j++)
				{
					int cust = violatingSubsets[i].second[j];
					v_addedSRCutsPerCustomer[cust]++;
				}
				added_cuts++;
			}
		}

		////if more than the allowed number of violated cuts are found, add the most violating ones by sorting first
		//if ( cnt_FoundViolatedCuts > ctlr->MaxNumberSRCutsPerIteration() ){ 		
		//	sort (violatingSubsets.begin(),violatingSubsets.end()); //sort by increasing lhs
		//	for (int i = (int)violatingSubsets.size()-1;  i >= (int)violatingSubsets.size() - ctlr->MaxNumberSRCutsPerIteration(); i--){	
		//		if (ctlr->NumSubsetRowCuts()<maxCutNumber){
		//			if( ctlr->IsAggregated() ){
		//				cuts.push_back( new c_AggregatedSubsetRowCutConstraintCVRPTW(ctlr, violatingSubsets[i].second ) );
		//			} else{
		//				cuts.push_back( new c_SubsetRowCutConstraintCVRPTW(ctlr, violatingSubsets[i].second ) );
		//			}
		//		}
		//	}
		//} else{
		//	//add all found violated sr cuts
		//	for (int i = 0; i<(int)violatingSubsets.size();i++){	
		//		if (ctlr->NumSubsetRowCuts() < maxCutNumber){
		//			if( ctlr->IsAggregated() ){
		//				cuts.push_back( new c_AggregatedSubsetRowCutConstraintCVRPTW(ctlr, violatingSubsets[i].second ) );
		//			} else{
		//				cuts.push_back( new c_SubsetRowCutConstraintCVRPTW(ctlr, violatingSubsets[i].second ) );
		//			}	
		//		}
		//	}
		//}
		
	}
	o_separationTimer.Stop();

	if(ctlr->InfoLevel() >= 2){
		cout << "Separation time: " << o_separationTimer.Seconds() << "sec." << endl; 
		cout << cuts.size() << " cut(s) generated. There are " << ctlr->NumSubsetRowCuts() << " cuts in total."  << endl;
	}
	if(cuts.size() > 0){
		ctlr->IncreaseCutGenerationCounter(); 
	}
	return (int)cuts.size();
}	

int c_SeparationProblemCVRPTW::SeparateCapacityCuts(c_RMP* rmp, list<c_Constraint*>& cuts)
{
	c_ControllerCVRPTW* ctlr = (c_ControllerCVRPTW*) Controller();
	c_RMP_CVRPTW* my_rmp = (c_RMP_CVRPTW*) rmp;

	if( ctlr->InfoLevel() >= 2 ){
		cout << "Start separation of Capacity cuts." << endl;
	}

	/* Initialize something */
	int Dim;
	CnstrMgrPointer MyCutsCMP;
	Dim = 100;
	CMGR_CreateCMgr(&MyCutsCMP,Dim);

	CnstrMgrPointer MyExistingCutsCMP;
	int DimExisting = 0;
	CMGR_CreateCMgr(&MyExistingCutsCMP,DimExisting);

	vector<int> usedArcsTails;
	vector<int> usedArcsHeads;
	vector<double> usedArcsFlows;
	usedArcsTails.push_back(0);
	usedArcsHeads.push_back(0);
	usedArcsFlows.push_back(0);
	//Attention: Depot has to be n+1 (start and end depot) and customers have to be 1...n
	//TODO: CVRPSEP will Kanten, nicht B�gen. Eventuell kann man bei j<i den Fluss einfach auf vertauschtes i und j dazuaddieren (oder anlegen)
	my_rmp->UpdateFlows();
	for( int i = 0; i < ctlr->NumVertices(); i++){
		for( int j = i+1; j < ctlr->NumVertices(); j++){ //start at i+1 to get only arcs with i < j
			//if( i < j ){			
				if( my_rmp->VertexFlow(i,j) > 0.001 ||  my_rmp->VertexFlow(j,i) > 0.001 ){
					usedArcsTails.push_back(ctlr->GetSepVertexNumber(i));
					usedArcsHeads.push_back(ctlr->GetSepVertexNumber(j));
					usedArcsFlows.push_back(my_rmp->VertexFlow(i,j) + my_rmp->VertexFlow(j,i)); //TODO: check second term
				}
			//}
		}
	}

	char IntegerAndFeasible;
	double MaxViolation =0.0;
	vector<int> demandsOfVertices = ctlr->GetDemandsForVertices();
	//CnstrMgrPointer CutsCMP;
	/* Now call the separation routine */
	CAPSEP_SeparateCapCuts(ctlr->NumCustomerVertices(), 
		&demandsOfVertices[0], 
		ctlr->Capacity(),
		(int)(usedArcsTails.size())-1,
		&usedArcsTails[0],
		&usedArcsHeads[0],
		&usedArcsFlows[0],
		MyExistingCutsCMP,
		ctlr->MaxNumberCapCutsPerIteration(),
		ctlr->CutTolerance(),
		&IntegerAndFeasible,
		&MaxViolation,
		MyCutsCMP);

	/* Get separated cuts */
	vector<int> customerList;
	for (int i=0; i<MyCutsCMP->Size; i++) {
		customerList.clear();
		//Listsize=0;
		for (int j=1; j<=MyCutsCMP->CPL[i]->IntListSize; j++) {
			customerList.push_back(MyCutsCMP->CPL[i]->IntList[j]);
			//List[++Listsize] = MyCutsCMP->CPL[i]->IntList[j];
		}
		/* Now List contains the customer numbers defining the cut. */
		/* The right-hand side of the cut, */
		/* in the form x(S:S) <= |S| - k(S), is RHS. */
		double RHS = customerList.size() - MyCutsCMP->CPL[i]->RHS;
		/* Add the cut to the LP. */
		cuts.push_back( new c_CapacityCutConstraintCVRPTW(ctlr, customerList, (int) ceil(RHS) ) );
	}

	return (int)cuts.size();
}


int c_SeparationProblemCVRPTW::EliminateCyclesByExtendingNeighborhood( list<c_Constraint*>& cuts )
{
	int numTotalChanges = 0;
	int numIncreasedNeighborhoods = 0;
	bool checkAgainForEliminatableCycles = true;
	while (checkAgainForEliminatableCycles ){
		numIncreasedNeighborhoods = 0;
		c_ControllerCVRPTW* ctlr = (c_ControllerCVRPTW*) Controller();
		vector<int> locationsWithChange;
		bool cycleWithThisNode = false;
		vector<int> nodesInBetween;
		for ( int i=0; i<ctlr->RMP()->numcols(); i++ ){
			double x;
			ctlr->RMP()->getX( &x, i, i );
			if ( x > CG_EPS && x < (1- CG_EPS) ){//only columns with fractional value are interesting
				c_BaseColumnCVRPTW* col = (c_BaseColumnCVRPTW*) ctlr->ColumnInRMP( i );
				if ( col->Type() == c_BaseColumnCVRPTW::eTOUR ){
					c_TourColumnCVRPTW* col_tour = (c_TourColumnCVRPTW*) col;
					const vector<int>& route = col_tour->RouteOfNodes();
					
					for (vector<int>::const_iterator it_n = route.begin(); it_n != route.end(); it_n++){
						cycleWithThisNode = false;
						//from the first occurence to the second (if exists)
						for (vector<int>::const_iterator it = find(it_n, route.end(), *it_n) + 1 ; it != route.end(); it++){
							if( *it == *it_n ){
								cycleWithThisNode = true;
								break;
							}
							nodesInBetween.push_back(*it);
						}
						if( cycleWithThisNode ){
							locationsWithChange.push_back(*it_n);
							if( nodesInBetween.size() < ctlr->DynamicNgNeighborhoodUB() ){
								for (auto v = nodesInBetween.begin(); v != nodesInBetween.end();v++) {
									//the neighborhood of the nodes in between must be extended (not the other way around!)
									if( ctlr->AddToNeighborhood(*v, *it_n, ctlr->DynamicNgNeighborhoodUB()) ){ // of course some of the locations can be inserted already before, but not all
										numIncreasedNeighborhoods++;
									}
								}
							}
						}
						nodesInBetween.clear();
					}
				}
			}
		}

		numTotalChanges += locationsWithChange.size();
		if( numIncreasedNeighborhoods > 0 ){
			checkAgainForEliminatableCycles = true;
			list<c_Column*> col_list_toDelete; //Delete cols that are now infeasible and Reoptimize
			for( int i = 0; i < ctlr->RMP()->numcols(); i++){
				c_Column* col = ctlr->ColumnInRMP(i);
				c_BaseColumnCVRPTW* col_base = (c_BaseColumnCVRPTW*) col;
				if ( col_base->Type() == c_BaseColumnCVRPTW::eTOUR ){
					//check if tour is infeasible according to new extended neighborhood
					c_TourColumnCVRPTW* col_tour = (c_TourColumnCVRPTW*) col;
					const vector<int>& route = col_tour->RouteOfNodes();
					long l_visit = 0;
					long l_visit_new;
					for ( int n = 0; n < route.size() - 1; n++ ){
						int idx = ctlr->NGIndex(route[n], route[n+1]);
						if ( idx >= 0 && (l_visit & (1<<idx)) ){
							//route is infeasible corresponding to the new extended neighborhood
							col_list_toDelete.push_back( col );
							break;
						}
						//update l_visit
						l_visit_new = 0;
						int ng_size = ctlr->NGNeighborhoodSize(route[n]);
						for ( int idx=0; idx<ng_size; idx++ )
							if (l_visit & (1<<idx) ) {
								int node = ctlr->NGNeighbor( route[n], idx );
								int idx_new = ctlr->NGIndex( route[n+1], node );
								if ( idx_new >= 0 )
									l_visit_new |= (1<<idx_new);
							}
						l_visit_new |= (1<<0);
						l_visit = l_visit_new;		
					}
				}
			}
			if( numTotalChanges > 0 ){
				cuts.push_back( new c_NgIncreasementCVRPTW( ctlr, locationsWithChange ) );
			}
			if( ctlr->InfoLevel() >= 2 ){
				cout << "NG increased because of " << locationsWithChange.size() << " cycles. " << col_list_toDelete.size() << " column(s) removed." << endl; 
			}
			//ctlr->RMP()->write("ModelBeforeNgIncreasement.lp", "LP");
			ctlr->TransferColumnsToPool( col_list_toDelete );
			//ctlr->RMP()->write("ModelAfterNgIncreasement.lp", "LP");
			ctlr->RMP()->Optimize(c_RMP::eDUALOPT);// // ePRIMOPT);
		} else{
			checkAgainForEliminatableCycles = false;
		}
	}
	
	return numTotalChanges;
}

/*int c_SeparationProblemCVRPTW::Separate2PathCuts(c_RMP* rmp, list<c_Constraint*>& cuts)
{
	c_ControllerCVRPTW* ctlr = (c_ControllerCVRPTW*)Controller();
	c_RMP_CVRPTW* my_rmp = (c_RMP_CVRPTW*)rmp;

	int old_size = (int)cuts.size();
	int n = ctlr->NumNodes();
	int v = ctlr->NumVertices();

	//separate 2 path cuts for every day independently -> bitset has size NumNodes only
	//taskNetwork: flows between vertices of same customer are ignored
	for( int d = 0; d < ctlr->PlanningHorizon(); d++ ){
		vector<pair<double, pair<pvrptw_bitset, int>>> violatingSubsets;
		t_Star fw_star;

		//TODO: Wie flowPerDay z�hlen? Nur im Netzwerk des Tages d ist zu wenig. Alles, was Vertex vom Tag d erreicht ist m�glicherweise zu viel
		for (int i = 0; i<n; ++i){
			vector<pair<int, double> > star;
			for (int j = 0; j<n; ++j) {
				if ( i != j ){
					double flow = my_rmp->FlowPerDayFor2PathCut(i, j, d);
					if (flow>0)
						star.push_back(make_pair(j, flow));
				}
			}
			fw_star.push_back(star);
		}
		int max_cuts_to_add = (int)INFTY;

		o_separator_DAGHeuristic.Separate(fw_star, ctlr->CutTolerance(), max_cuts_to_add, violatingSubsets);

		for (int i = 0; i< (int)violatingSubsets.size(); i++)
		{
			cuts.push_back(new c_TwoPathConstraint(Controller(), violatingSubsets[i].second.first, d));
			//ctlr->IncreaseNumTwoPathConstraints();
		}
	}

	return (int)cuts.size() - old_size;
}
*/

void c_SeparationProblemCVRPTW::OutputInOStream(ostream& s ) const
{
	//give statistic info about separation
	c_ControllerCVRPTW* ctlr = (c_ControllerCVRPTW*) Controller();
	s << ctlr->NumSubsetRowCuts() << " SR cut(s) generated." << endl;
}


/////////////////////////////////////////////////////////////////////////////
//   c_Constraint_PVRPTW
/////////////////////////////////////////////////////////////////////////////

c_Constraint_PVRPTW::c_Constraint_PVRPTW(c_Controller* controller, char sense, double rhs, double estimator_dual, int con_type, int n1/*=0*/, int n2/*=0*/, int n3/*=0*/)
	:c_Constraint(controller, sense, rhs, estimator_dual, con_type, n1, n2, n3),
	i_type(con_type)
{
}




void c_Constraint_PVRPTW::OutputInOStream(ostream& s) const
{
	s << "Type = " << Type() << endl;
}

c_SubsetRowCutConstraintCVRPTW::c_SubsetRowCutConstraintCVRPTW(c_Controller* controller, vector<int> customers) 
	: c_GlobalValidConstraint_PVRPTW(controller, 'L', 1.0, 0.0, c_RMP_CVRPTW::eSubsetRowCut, ((c_ControllerCVRPTW*)controller)->NumSubsetRowCuts()), //(controller, 'L', 1, 0.0, c_RMP_CVRPTW::eSubsetRowCut, ),
	v_Subset(customers)
{
	c_ControllerCVRPTW* ctlr = (c_ControllerCVRPTW*) Controller();
	i_cutNumber = ctlr->NumSubsetRowCuts();    
	ctlr->IncreaseNumSubsetRowCuts(); 
}


void c_SubsetRowCutConstraintCVRPTW::OutputInOStream(ostream& s) const
{
	s << "Subset(";
	for (auto c = v_Subset.begin(); c != v_Subset.end();c++) {
		s << *c << ",";
	}
	s << ")";
}



c_AggregatedSubsetRowCutConstraintCVRPTW::c_AggregatedSubsetRowCutConstraintCVRPTW(c_Controller* controller, vector<int> customers) 
	: c_GlobalValidConstraint_PVRPTW(controller, 'L', ((c_ControllerCVRPTW*)controller)->PlanningHorizon(), 0.0, c_RMP_CVRPTW::eAggregatedSubsetRowCut, ((c_ControllerCVRPTW*)controller)->NumSubsetRowCuts()), //(controller, 'L', 1, 0.0, c_RMP_CVRPTW::eSubsetRowCut, ),
	v_Subset(customers)
{
	c_ControllerCVRPTW* ctlr = (c_ControllerCVRPTW*) Controller();
	int numPeriods = ctlr->PlanningHorizon();
	v_SubsetsPerDay = vector<vector<int>>(numPeriods);
	for( int d = 0; d < numPeriods; d++){
		vector<int> subset = vector<int>(3);
		for( int p = 0; p < 3; p++){
			subset[p] = ctlr->GetTask( ctlr->GetNodeOfTask(v_Subset[p]), d);
		}
		v_SubsetsPerDay[d] = subset;
	}

	i_cutNumber = ctlr->NumSubsetRowCuts();    
	ctlr->IncreaseNumSubsetRowCuts(); 
}

void c_AggregatedSubsetRowCutConstraintCVRPTW::OutputInOStream(ostream& s) const
{
	s << "Agg-Subset(";
	for (auto c = v_Subset.begin(); c != v_Subset.end(); c++) {
		s << *c << ",";
	}
	s << ")";
}



//Artificial constraint such that base recognizes that a cut has been added. In reality, the cut is realized by increasement of the ng vector in InstanceWithRdc
c_NgIncreasementCVRPTW::c_NgIncreasementCVRPTW(c_Controller* controller, vector<int> locations) 
	: c_GlobalValidConstraint_PVRPTW(controller, 'L', 0.0, 0.0, c_RMP_CVRPTW::eNgIncreasementCut, ((c_ControllerCVRPTW*)controller)->NumNgIncreasements()), //(controller, 'L', 1, 0.0, c_RMP_CVRPTW::eSubsetRowCut, ),
	v_changedLocations(locations)
{
	c_ControllerCVRPTW* ctlr = (c_ControllerCVRPTW*) Controller();
	i_cutNumber = ctlr->NumNgIncreasements();    
	ctlr->IncreaseNumNgIncreasements(); 
}


void c_NgIncreasementCVRPTW::OutputInOStream(ostream& s) const
{
	s << "Locations(";
	for (auto c = v_changedLocations.begin(); c != v_changedLocations.end();c++) {
		s << *c << ",";
	}
	s << ")";
}


//Capacity cut
c_CapacityCutConstraintCVRPTW::c_CapacityCutConstraintCVRPTW(c_Controller* controller, vector<int> customers, int rhs) 
	: c_Constraint_PVRPTW(controller, 'G', rhs, 0.0, c_RMP_CVRPTW::eCapacityCut, ((c_ControllerCVRPTW*)controller)->NumCapacityCuts()),
	//: c_Constraint(controller, 'G', rhs, 0.0, c_RMP_CVRPTW::eCapacityCut, ((c_ControllerCVRPTW*)controller)->NumCapacityCuts()),
	v_customerSubset(customers),
	i_rhs(rhs)
{
	c_ControllerCVRPTW* ctlr = (c_ControllerCVRPTW*) Controller();
	i_cutNumber = ctlr->NumCapacityCuts();    
	ctlr->IncreaseNumCapacityCuts(); 
}


void c_CapacityCutConstraintCVRPTW::OutputInOStream(ostream& s) const
{
	s << "Subset(";
	for (auto c = v_customerSubset.begin(); c != v_customerSubset.end();c++) {
		s << *c << ",";
	}
	s << ")";
}


/////////////////////////////////////////////////////////////////////////////
//  c_ArcSetConstraint
/////////////////////////////////////////////////////////////////////////////

c_ArcSetConstraint::c_ArcSetConstraint(c_Controller* controller, char sense, double rhs, int day)
	: c_Constraint_PVRPTW(controller, sense, rhs, 0.0, c_RMP_CVRPTW::eARCSET, ((c_ControllerCVRPTW*)controller)->NumArcSetConstraints()),
	//: c_Constraint(controller, Type(), ((c_ControllerCVRPTW*)controller)->NumArcSetConstraints(), sense, rhs),
	_controller(controller),
	v_arc_set(((c_ControllerCVRPTW*)controller)->NumNodes(), ((c_ControllerCVRPTW*)controller)->NumNodes()),
	i_day(day)
{
	((c_ControllerCVRPTW*)controller)->IncreaseNumArcSetConstraints();
}


/////////////////////////////////////////////////////////////////////////////
//  c_TwoPathConstraint
/////////////////////////////////////////////////////////////////////////////
c_TwoPathConstraint::c_TwoPathConstraint(c_Controller* controller, const bitset<pvrptw_bs_max_size>& nodes, int day)
	: c_ArcSetConstraint(controller, 'G', 2, day),
	s_nodes(nodes)
{
	c_ControllerCVRPTW* ctlr = (c_ControllerCVRPTW*)Controller();
	for (int i = 0; i < ctlr->NumNodes(); ++i) {
		for (int j = 0; j < ctlr->NumNodes(); ++j) {
			if (ctlr->ArcExists(i, j) && s_nodes[i] && !s_nodes[j])
				v_arc_set(i, j) = 1;
			else
				v_arc_set(i, j) = 0;
		}
	}
}

void c_TwoPathConstraint::OutputInOStream(ostream& s) const
{
	c_ControllerCVRPTW* ctlr = (c_ControllerCVRPTW*)Controller();
	s << "(Two Path) Flow out of S={ ";
	for (int i = 0; i < ctlr->NumNodes(); ++i)
		if (s_nodes[i])
			s << i << ",";
	s << "} on day " << i_day << " >= 2";
}