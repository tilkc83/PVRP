#include "base.h"
#include "base.h"
/******************** Include eigenes Modul ********************************/

#include "base.h"

/******************** Include fremde Module ********************************/

#include <math.h>

#include <fstream>
#include <iostream>
#include <set>
#include <algorithm>
#include <iomanip>
#include <cassert>
#include <limits>

#include "cplexcpp.h"

using namespace std;

namespace CGBase
{

/******************** Konstanten *******************************************/

#ifndef TRUE
#define TRUE 1
#endif
#ifndef FALSE
#define FALSE 0
#endif


/******************** Implementation ***************************************/


/****************************************************************************

c_Controller

****************************************************************************/

c_Controller::c_Controller( string settings_file )
:   c_SettingsReader( settings_file ),
	env(nullptr),
	p_pool(nullptr),	
	p_red_costs_fixing(nullptr),
	p_currentNode(nullptr), 
	p_conv_info(nullptr),
	next_node_id(0),
	ub(100.0 * INFTY),
	aspiration_lb(INFTY),
	aspiration_ub(-INFTY),
	last_node_terminated(false),
	last_node_integer(false),
	// protected members
	p_root_node(nullptr), 
	p_rmp(nullptr),
	p_best_solution(nullptr),

	info_number_of_rmp_iterations( 0 ),
	info_number_of_generated_columns( 0 ),
	info_number_of_solved_pricing_problems( 0 ),
	info_number_of_generated_cuts( 0 ),
	info_number_of_solved_nodes( 0 ),

	first_integer_solution(INFTY),
	info_time_first_integer_solution( INFTY ),
	info_time_best_integer_solution( INFTY ),

	 info_time_overall(0.0),
	 info_time_pricing(0.0),
	 info_time_separation(0.0),
	 info_time_reoptimization(0.0),
	 info_time_root_lb1(0.0),
	 info_time_root_lb2(0.0),
	 info_time_root_branching(0.0),
	 info_time_AddColumn(0.0),
	 info_time_Init(0.0),
	 info_time_NodeTerm(0.0),
	 info_time_NodeRun(0.0),
	 info_time_NodeUpdateUB(0.0),
	 info_time_MIPForUB(0.0),
	 info_timer_nodeSolve(0.0),
	 info_timer_ContUpdate(0.0),
	 info_timer_CreateDual(0.0),
	 info_timer_OrgPool(0.0),
	 info_timer_GetDual(0.0),
	 info_timer_RMPUpdate(0.0),
	 info_timer_PricingUpdate(0.0),
	 info_timer_StrongBranching(0.0),
	 info_timer_last_PP_root(0.0),

	info_1( &cout ),
	info_2( &cout ),
	info_3( &cout ),
	info_err( &cout ),
	info_statistics( &cout ),
	// Parameter
	DEBUG( "Debug", false, false ),
	UseLPOptimizer( "UseLPOptimizer", "dual", false ),
	ReloadSettings( "ReloadSettings", false, false ),
	Stabilization( "Stabilization", false, false ),

	InfoLevel( "InfoLevel", 1, false ),
	InfoStatisticsLevel( "InfoStatisticsLevel", 1, false),
	InfoDottyOutput( "InfoDottyOutput", false, false ),
	InfoGnuplotOutput( "InfoGnuplotOutput", false, false ),
	InfoGraphMLOutput( "InfoGraphMLOutput", false, false ),
	InfoTikzOutput("InfoTikzOutput", false, false),
	InfoDottySuppressIntermediateNodes( "InfoDottySuppressIntermediateNodes", false, false ),
	InfoInstance("InfoInstance", "Any Instance", false),
	InfoPrintOverallBestSolution( "InfoPrintOverallBestSolution", true, false ),
	InfoPrintBestSolution( "InfoPrintBestSolution", true, false ),
	InfoOutputFileName( "InfoOutputFileName", "Solutions.csv", false ),
	InfoConvergency( "InfoConvergency", false, false ),
	InfoStatisticsColumns( "InfoStatisticsColumns", false, false ),
	
	MaxSolutionTimeSec( "MaxSolutionTimeSec", INFTY, false ),
	MaxColumnsToAddInPricing( "MaxColumnsToAddInPricing", 500, false ),
	PricingWithColumnSelection( "PricingWithColumnSelection", true, false ),
	PricingHierarchyMaxNumFailuresBeforeSwitchOff( "PricingHierarchyMaxNumFailuresBeforeSwitchOff", 999999, false ),
	PricingHierarchyMinNumColsToGenerate( "PricingHierarchyMinNumColsToGenerate", 1, false ),
	MaxRatioColsToRows( "MaxRatioColsToRows", 5.0, false ),
	MaxRatioColsToRowsMIP( "MaxRatioColsToRowsMIP", 5.0, false ),
	Branching( "Branching", true, false ),
	BranchingNodeSelection( "BranchingNodeSelection", "BestFirst", false ),
	EarlyBranching( "EarlyBranching", false, false ),
	StrongBranching( "StrongBranching", false, false ),
	StrongBranchingUptoLevel( "StrongBranchingUptoLevel", 8, false ),
	StrongBranchingNumCandidates( "StrongBranchingNumCandidates", 10 , false ),
	StrongBranchingMinNumCandidates( "StrongBranchingMinNumCandidates", 5, false ),
	StrongBranchingNumCandidatesDecreasePerLevel( "StrongBranchingNumCandidatesDecreasePerLevel", 0, false ),

	CuttingPlanes( "CuttingPlanes", true, false ),
	
	UseMIPSolverForUBs( "UseMIPSolverForUBs", false, false ),
	UseMIPSolverUptoLevel( "UseMIPSolverUptoLevel", 0, false ),
	UseMIPSolverUpFromLevel("UseMIPSolverUpFromLevel", 0, false),
	MIPMaxSolutionTimeSec( "MIPMaxSolutionTimeSec", 60, false ),
	MIPScreenOutput("MIPScreenOutput", false, false),
	MIPAccelerate("MIPAccelerate", false, false),
	SolveAsMipAfterTimeOut("SolveAsMipAfterTimeOut", false, false),

	UseLagrangeanLB( "UseLagrangeanLB", false, false ),
	ReducedCostVariableFixing( "ReducedCostVariableFixing", true, false ),
	StabFrequence("StabFrequence",5,false),
	FeasyCutFrequence ("FeasyCutFrequence",0,false),
	FeasyCutGap("FeasyCutGap",1.00,false),
	NumThreadsRMP("NumThreadsRMP",1,false),
	NameDottyOutput("NameDottyOutput", "bbtree.dty", false),
	CheckForInftyAndLBError("CheckForInftyAndLBError",false,false),

	StrongBranchingRule("StrongBranchingRule", "BestSmall", false),
	StrongBranchingMu("StrongBranchingMu", 0.0, false),
	PricersUsedForStrongBranching("PricersUsedForStrongBranching", MaxNumPricers, false),
	LocalCG_EPS("LocalCG_EPS", CG_EPS, false),
	LocalInfty("LocalInfty", INFTY, false)
{
	env = new CPLEXEnvironment();
	p_pool = new c_ColumnPool( this );

	// Parameter
	AddParameter( &DEBUG );
	AddParameter( &UseLPOptimizer );
	AddParameter( &ReloadSettings );
	AddParameter( &Stabilization );
	AddParameter( &InfoLevel );
	AddParameter( &InfoStatisticsLevel );
	AddParameter( &InfoDottyOutput );
	AddParameter( &InfoGnuplotOutput );
	AddParameter( &InfoGraphMLOutput );
	AddParameter(&InfoTikzOutput);
	AddParameter( &InfoDottySuppressIntermediateNodes );
	AddParameter( &InfoPrintOverallBestSolution );
	AddParameter( &InfoPrintBestSolution );
	AddParameter( &InfoOutputFileName );
	AddParameter( &InfoConvergency );
	AddParameter( &InfoStatisticsColumns );
	AddParameter( &MaxSolutionTimeSec );
	AddParameter( &MaxColumnsToAddInPricing );
	AddParameter( &PricingWithColumnSelection );
	AddParameter( &PricingHierarchyMaxNumFailuresBeforeSwitchOff );
	AddParameter( &PricingHierarchyMinNumColsToGenerate );
	AddParameter( &MaxRatioColsToRows );
	AddParameter( &MaxRatioColsToRowsMIP );
	AddParameter( &Branching );
	AddParameter( &BranchingNodeSelection );
	AddParameter( &EarlyBranching );
	AddParameter( &StrongBranching );
	AddParameter( &StrongBranchingUptoLevel );
	AddParameter( &StrongBranchingNumCandidates );
	AddParameter( &StrongBranchingMinNumCandidates );
	AddParameter( &StrongBranchingNumCandidatesDecreasePerLevel );
	AddParameter( &CuttingPlanes );
	AddParameter( &InfoInstance );
	AddParameter( &UseMIPSolverForUBs );
	AddParameter( &UseMIPSolverUptoLevel );
	AddParameter( &MIPMaxSolutionTimeSec );
	AddParameter( &UseLagrangeanLB );
	AddParameter( &ReducedCostVariableFixing );
	AddParameter( &StabFrequence);
	AddParameter( &FeasyCutFrequence);
	AddParameter( &FeasyCutGap);
	AddParameter( &NumThreadsRMP);
	AddParameter( &NameDottyOutput);
	AddParameter( &PricersUsedForStrongBranching);
	AddParameter( &StrongBranchingRule);
	AddParameter( &StrongBranchingMu);
	AddParameter( &LocalInfty);
	AddParameter( &LocalCG_EPS); 
	AddParameter( &SolveAsMipAfterTimeOut);
	AddParameter(&CheckForInftyAndLBError);
	AddParameter(&UseMIPSolverUpFromLevel);
	AddParameter(&MIPScreenOutput);
	AddParameter(&MIPAccelerate);
	// AddParameter(  );
}


c_Controller::~c_Controller()
{
	// clean memory at the end
	for ( auto hierarchy : pricing_problem_hierarchy )
		delete hierarchy;
	pricing_problem_hierarchy.clear();
	// colums
	for ( int i=0; i<(int)cols_in_rmp.size(); i++ )
		delete cols_in_rmp[i];
	// separation problems ...
	for ( auto sep_prob : separation_problems )
		delete sep_prob;
	separation_problems.clear();
	// nodes of the B&B-Tree
	DeleteTree();
	// constraints
	for ( auto con_iter : l_constraints )
		delete con_iter;
	// basic objects
	// NOT: "delete p_root_node"
	delete p_rmp;
	delete p_pool;
	delete p_best_solution;
	delete p_currentNode;
	env->terminate();
	delete env;
}

void c_Controller::DeleteTree()
{
	// remove existing tree
	for ( auto node : active_nodes )
		delete node;
	active_nodes.clear();
	for ( auto node : inactive_nodes )
		delete node;
	inactive_nodes.clear();
	p_root_node = nullptr;
	next_node_id = 0;
	// reset bounds
	ub = 100.0 * INFTY;
	// remove solution information
	delete p_best_solution;
	p_best_solution = nullptr;
	p_currentNode = nullptr;
	first_integer_solution = INFTY;
	// reset infos about the solution process
	last_node_terminated = false;
	last_node_integer = false;
	info_number_of_rmp_iterations = 0 ;
	info_number_of_generated_columns = 0 ;
	info_number_of_solved_pricing_problems = 0 ;
	info_number_of_generated_cuts = 0 ;
	info_number_of_solved_nodes = 0 ;
	info_time_first_integer_solution = INFTY ;
	info_time_best_integer_solution = INFTY ;
	info_time_pricing = 0.0 ;
	info_time_separation = 0.0 ;
	info_time_reoptimization = 0.0 ;
	info_time_root_branching = 0.0 ;
	for ( int i=0; i<(int)v_info_time_pricing_level.size(); i++ )
		v_info_time_pricing_level[i] = 0;
	for (int i = 0; i<(int)v_info_calls_pricing_level.size(); i++)
		v_info_calls_pricing_level[i] = 0;
	for (int i = 0; i<(int)v_info_success_pricing_level.size(); i++)
		v_info_success_pricing_level[i] = 0;
	info_time_Init = 0.0;
	info_time_AddColumn = 0.0;
	info_time_NodeTerm = 0.0;
	info_time_NodeUpdateUB = 0.0;
	info_timer_nodeSolve = 0.0;
	info_timer_ContUpdate = 0.0;
	info_timer_CreateDual = 0.0;
	info_timer_OrgPool = 0.0;
	info_timer_GetDual = 0.0;
	info_timer_RMPUpdate = 0.0;
	info_timer_PricingUpdate = 0.0;
	info_time_MIPForUB = 0.0;
	timer.Reset();
	timer.Start();
	GetBranchingInfo.Reset();
	GetBranchingInfo.Start();
}

int c_Controller::InfoNumberOfSimplexIterations() const
{
	return p_rmp->InfoNumberOfSimplexIterations();
}


c_Controller::e_branching c_Controller::BranchingRule() const
{
	// Best First
	if ( BranchingNodeSelection().compare("BestFirst" ) == 0 )
		return best_first;

	// Depth First
	if ( BranchingNodeSelection().compare("DepthFirst" ) == 0 )
		return depth_first;

	// Mixed Depth and Best First
	if ( BranchingNodeSelection().compare("MixedDepthBestFirst" ) == 0 )
	{
		if ( LastNodeTerminated() )
			return best_first;
		else
			return depth_first; // best_first;
	}
	// no valid parameter found
	cout << "#ERROR: unknown branching rule! I assume best-first branching ";
	return best_first;
}

void c_Controller::TerminateNode( c_BranchAndBoundNode* node )
{
	for ( auto tester=active_nodes.begin(); tester!=active_nodes.end(); ++tester )
		if ( (*tester) == node )
		{
			active_nodes.erase( tester );
			inactive_nodes.insert( inactive_nodes.end(), node );
			break;
		}
}

void c_Controller::AddPricingProblemHierarchy( c_PricingProblemHierarchy* hierarchy )
{
	pricing_problem_hierarchy.push_back( hierarchy );
}

void c_Controller::AddPricingProblem( c_PricingProblem* prob )
{
	int max_num_failures = PricingHierarchyMaxNumFailuresBeforeSwitchOff();
	int min_num_cols =     PricingHierarchyMinNumColsToGenerate();
	c_PricingProblemHierarchy* hierarchy = new c_PricingProblemHierarchy( this, max_num_failures, min_num_cols );
	hierarchy->Add( prob );
	AddPricingProblemHierarchy( hierarchy );
}

void c_Controller::FixPartOfSolution()
{}

void c_Controller::UnfixSolution()
{}

class c_GnuPlotNodeInfo {
public:
	double from_bound;
	double from_pos;
	double pos;
	double bound;
	int line_style;
};

void GnuPlotTree( double from_bound, double from_pos, c_BranchAndBoundNode* node, double interval_lhs, double interval_rhs,
	vector<c_GnuPlotNodeInfo>& result )
{
	if ( !node )
		return;
	c_GnuPlotNodeInfo gnu_node;
	gnu_node.from_bound = from_bound;
	gnu_node.from_pos = from_pos;
	gnu_node.bound = node->LB();
	gnu_node.pos = ( interval_lhs + interval_rhs ) / 2.0;
	gnu_node.line_style = 0;
	if ( !node->AddedConstraints().empty() )
		gnu_node.line_style = node->AddedConstraints().front()->GnuPlotLineStyle();
	result.push_back( gnu_node );

	int num_sons = node->NumSons();
	double offset = ( interval_rhs - interval_lhs ) / double( num_sons );
	double startpos = interval_lhs;
	for ( auto son : node->Sons())
	{
		GnuPlotTree( gnu_node.bound, gnu_node.pos, son, startpos, startpos+offset, result );
		startpos += offset;
	}
}

void c_Controller::UpdateAllNodeInformations( c_BranchAndBoundNode* selected_node )
{
	///////////////////
	// G N U P L O T
	///////////////////
	if ( InfoGnuplotOutput() )
	{
		double lb = this->LB();
		double ub = this->UB();
		c_BranchAndBoundNode* root = p_root_node;
		double root_lb = root->LB();
		vector<c_GnuPlotNodeInfo> result;
		double pos = .5;
		GnuPlotTree( root_lb, pos, root, 0.0, 1.0, result );
		// find out
		// 1) different line styles
		set<int> line_styles;
		for ( int i=0; i<(int)result.size(); i++ )
			line_styles.insert( result[i].line_style );
		// 2) optimum known
		bool optimum_known = lb < ub - 0.1;

		// B&B search tree as .gnu file
		ofstream gnu( "gnuplot/bbtree.gnu" );
		gnu << "reset                                                                 " << endl;
		gnu << "unset xtics                                                           " << endl;
		if ( optimum_known )
			gnu << "set ytics add (\"LB\" " << lb << ",\"UB\" " << ub << ")             " << endl;
		else
			gnu << "set ytics add (\"opt\" " << lb << ")                                 " << endl;
		int as = 1;
		for ( auto ii : line_styles )
		{
			gnu << "set style arrow " << as++ << " nohead nofilled front lt " << ii << " lw 1 " << endl;
		}
		gnu << "set style rect back fc rgb \"white\" fs solid 1.0 noborder            " << endl;
		gnu << "set object 1 rect from -0.02," << lb << " to 1.02," << ub << " fc rgb \"gray\"     " << endl;
		gnu << "plot [-.02:1.02] [" << root_lb-0.1*(ub-root_lb) << ":" << ub+0.3*(ub-root_lb) << "] ";
		as = 1;
		for ( int as=0; as<(int)line_styles.size(); as++ )
		{
			gnu << "  \"-\" using 2:1:($4-$2):($3-$1) with vectors as " << (as+1) << " notitle, \\" << endl;
		}
		gnu << "  \"-\" using 4:3 with points pt 13 ps .5 notitle, \\" << endl;
		gnu << "  \"-\" using 4:3 with points pt 10 ps .5 notitle  " << endl;

		// now the data
		for ( const auto ii : line_styles )
		{
			for ( const auto& res : result )
				if ( res.line_style == ii )
					gnu << res.from_bound
					<< " " << res.from_pos
					<< " " << res.bound
					<< " " << res.pos
					<< endl;
			gnu << "e" << endl;
		}
		for ( const auto& res : result )
			if ( res.bound < ub )
				gnu << res.from_bound
				<< " " << res.from_pos
				<< " " << res.bound
				<< " " << res.pos
				<< endl;
		gnu << "e" << endl;
		for ( auto res : result )
			if ( res.bound >= ub )
				gnu << res.from_bound
				<< " " << res.from_pos
				<< " " << res.bound
				<< " " << res.pos
				<< endl;
		gnu << "e" << endl;
	}
	//////////////
	// D O T T Y
	//////////////
	if ( InfoDottyOutput() )
	{
		// Get all nodes...
		list<c_BranchAndBoundNode*> all_nodes;
		for ( auto node : active_nodes )
			all_nodes.push_back( node );
		for ( auto node : inactive_nodes )
			all_nodes.push_back( node );

		// Hier auch die Ausgabe des B&B Suchbaums als .dty file
		ofstream bbout( NameDottyOutput());
		bbout << "digraph bbtree { " << endl;
		if ( InfoInstance().size() != 0 )
			bbout << "title = \"" << InfoInstance() << "\"\n";
		// alle Knoten beschriften
		for ( auto node : all_nodes )
		{
			ostringstream buffer;
			// alle Knoten beschriften; Fallunterscheidung
			bool inactive= true;
			for ( auto act : active_nodes )
				if ( node == act )
				{
					inactive = false;
					break;
				}
			// do not display intermediate nodes
			if ( InfoDottySuppressIntermediateNodes() && node->NumSons() == 1 )
				continue;
			if ( node->NodeNumber() < 0 )
				continue;
			// determine (proper) father
			c_BranchAndBoundNode* son = node;
			c_BranchAndBoundNode* father = node->Father();
			// do not display intermediate nodes
			while( InfoDottySuppressIntermediateNodes() && father && father->Level() == node->Level() )
			{
				son = father;
				father = father->Father();
			}
			if ( node == selected_node )
			{
				// current node
				buffer << "node" << node->NodeNumber()
					<< " [color=green, shape=record, label= \"{num=" << node->NodeNumber()
					<< "|}|{LB=" << node->LB()
					<< "|*current*}\"];";
			}
			else
			{
				// already solved or terminated?
				if ( inactive )
				{
					string text = "";
					string color = "black";
					ostringstream sol_text2;
					sol_text2 << "sol=" << node->NodeNumberSolved();
					string sol_text = sol_text2.str();
					if ( node->Terminated() )
					{
						text = "terminated";
						sol_text = "";
					}
					else
					{
						if ( node->IsFeasible() )
						{
							if (node->IsIntegral())
							{
								if (node->LB() == UB())
								{
									text = "best integer";
									color = "blue";
								}
								else
								{
									text = "integer";
									color = "lightblue";
								}
							}
						}
						else
						{
							text = "infeasible";
							color = "yellow";
						}
					}

					if (node->LB() > son->LB() + LocalCG_EPS())
						buffer << "node" << node->NodeNumber()
						<< " [color=" << color
						<< ", shape=record, label= \"{num="<< node->NodeNumber()
						<< "|" << sol_text
						<< "}|{LB=" << son->LB()
						<< "\\n" << node->LB() 
						<< "|" << text
						<< "}\"];";
					else
						buffer << "node" << node->NodeNumber()
						<< " [color=" << color
						<< ", shape=record, label= \"{num=" << node->NodeNumber()
						<< "|" << sol_text
						<< "}|{LB=" << node->LB()
						<< "|" << text
						<< "}\"];";
				}
				// active node
				if ( !inactive )
					buffer << "node" << node->NodeNumber() << " [color=red, shape=circle, label=\"active\"];";
			}
			bbout << buffer.str() << endl;

			// arcs
			// connection to father node
			if ( father )
			{
				ostringstream buffer;
				buffer << "node" << father->NodeNumber()
					<< " -> node" << node->NodeNumber() << "[label=\"";
				bbout << buffer.str();
				bool first = true;
				for ( auto con : son->AddedConstraints())
				{
					if ( !first )
						bbout << "\\n";
					bbout << *con;
					first = false;
				}
				bbout << "\"]; " << endl;
			}
		}
		bbout << "}" << endl;
	}
}

void c_Controller::SetBestSolution( c_Solution* sol )
{
	// Store Solution
	delete p_best_solution;
	p_best_solution = sol;
	if (sol->Objective() < UB())
	{
		SetUB(sol->Objective());
		//new set first int
		if (info_time_first_integer_solution > timer.Seconds())
		{
			info_time_first_integer_solution = timer.Seconds();
			first_integer_solution = sol->Objective();
		}
		//end new first int
	}
	// Best Integer Solution
	// and First Integer Solution
	info_time_best_integer_solution = timer.Seconds();
}

void c_Controller::Go( void )
{
	// initialization
	//timer.Start();
	DeleteTree();
	c_TimeInfo timer_init;
	timer_init.Restart();
	// creation of pricers and separators
	i_phase_in_bap = initialization;
	if ( pricing_problem_hierarchy.empty() )
		AddPricingProblems();
	if ( separation_problems.empty() )
		AddSeparationProblems();
	// create RMP 
	if ( !p_rmp )
	{
		p_rmp = CreateNewRMP();
		p_rmp->InitializeFirstRMP();
	}
	else
		p_rmp->Reset();

	//CPXENVptr env = (CPXENVptr) this->CPLEXEnv()->getEnv();
	//useful debugging parameter to check input to cplex more carefully and show problems in the structure of the model immediately, leads to earlier CplexExceptions
	//CPXsetintparam(env, CPX_PARAM_DATACHECK, CPX_ON); 
	//prints cplex logging in the console
	//CPXsetintparam(env, CPX_PARAM_SCRIND, CPX_ON);
	////to write cplex messages in file "CplexOutput.txt"
	////get pointers of type CPXCHANNELptr to hold the address of the channel corresponding cpxresults, cpxwarning, cpxerror and cpxlog. May be NULL.
	//CPXCHANNELptr cpxresults = NULL;
	//CPXCHANNELptr cpxwarning =NULL;
	//CPXCHANNELptr cpxerror = NULL;
	//CPXCHANNELptr cpxlog = NULL;
	//CPXgetchannels (env, &cpxresults, &cpxwarning, &cpxerror, &cpxlog);
	//CPXFILEptr output_p = CPXfopen("CplexOutput.txt", "w");
	//CPXaddfpdest (env, cpxwarning, output_p);
	//CPXaddfpdest (env, cpxerror, output_p);
	//CPXaddfpdest (env, cpxlog, output_p);
	//CPXaddfpdest (env, cpxresults, output_p);

	// create statistics about convergence
	if ( InfoConvergency() ) 
	{
		delete p_conv_info;
		p_conv_info = new c_ConvergencyInfo();
	}
	// create procedures for reduced costs fixing
	p_red_costs_fixing = NULL;
	if ( ReducedCostVariableFixing() && !p_red_costs_fixing )
		p_red_costs_fixing = CreateReducedCostsFixing();

	// create root node of b&b-tree
	i_phase_in_bap = root_node;
	p_root_node = CreateRootNode();
	AddNode( p_root_node );
	timer_init.Stop();
	info_time_Init += timer_init.Seconds();

	c_BranchAndBoundNode* akt_node = NULL;
	while   ( ( akt_node = SelectNextNode() )
		&& !TimeOut() )
	{
		p_currentNode = akt_node;

		if ( akt_node->Father() )
			i_phase_in_bap = tree;

		if ( ReloadSettings() )
			ReadSettings();

		c_TimeInfo timer_Node;
		timer_Node.Restart();
		// check if node can be terminated
		if (p_rmp->RoundLBTo(akt_node->LB()) >= ub - LocalCG_EPS()
			|| (p_root_node->SubtreeLB() >= AspirationLB() - LocalCG_EPS())
			|| (UB() < AspirationUB() + LocalCG_EPS()))
		{
			// yes, terminate...
			if ( InfoLevel() >= 2 )
			{
				if ( akt_node->LB() >= ub )
					Info2() << "\nNew Node: Termination (LB[" << akt_node->LB() << "] >= UB[" << ub << "]).\n" << flush;
				else
					Info2() << "\nNew Node: Termination because of aspiration level\n" << flush;
			}
			akt_node->Terminate();
			timer_Node.Stop();
			info_time_NodeTerm += timer_Node.Seconds();
		}
		else
		{
			UpdateAllNodeInformations( akt_node );
			// do CG for this node
			c_TimeInfo node_time;
			node_time.Restart();
			if ( InfoLevel() >= 2 )
			{
				Info2() << "\nLB = " << p_root_node->SubtreeLB()
					<< ", UB = " << ub
					<< ", open nodes: " << NumOpenBranchAndBoundNodes() << "\n" << flush;
			}
			// here comes the code for solving
			c_DualVariables* p_dual = akt_node->Go();
			node_time.Stop();
			info_timer_nodeSolve += node_time.Seconds();

			if ( !TimeOut() )
				akt_node->SetNodeNumberSolved( ++info_number_of_solved_nodes );

			//
			if ( InfoLevel() >= 2 )
			{
				Info2() << "Computation Time for Node " << akt_node->NodeNumber() << ": "
					<< node_time.MilliSeconds() << "ms\n" << flush;
			}

			// Update UB
			c_TimeInfo timer_updateUB;
			timer_updateUB.Restart();
			bool is_integral = akt_node->IsIntegral();
			bool branching_or_cutting = false;
			if ( is_integral )
			{
				// solution is integral
				if ( akt_node->RMPValue() && info_time_first_integer_solution > timer.Seconds() )
				{
					info_time_first_integer_solution = timer.Seconds();
					first_integer_solution = akt_node->RMPValue();
					if (akt_node->Level() == 0)
						info_time_root_lb2 = timer.Seconds();
				}

				// Update UB
				if ( ub > akt_node->RMPValue()
					&& akt_node->IsFeasible() )
				{
					SetUB( akt_node->RMPValue() );
					if ( InfoLevel() >= 2 )
					{
						Info2() << "Integral solution found is best solution with UB = "
							<< ub
							<< "\n"
							<< flush;
					}

					// new best solution found
					SetBestSolution( CreateSolution( p_rmp, p_dual ) );

					// reduced cost fixing - fixing
					if ( ReducedCostsFixing() )
						ReducedCostsFixing()->CheckAgainstUB( ub );
				}
				last_node_terminated = true;
				last_node_integer = true;
			}
			else
			{
				// solution is not integral
				if ((p_rmp->RoundLBTo(akt_node->LB()) < ub - LocalCG_EPS())
					&& ( akt_node->IsFeasible() ) )
				{
					if ( !TimeOut() )
					{
						// reduced cost fixing - computing bounds
						if ( ReducedCostsFixing() && akt_node->Level() == 0 )
							ReducedCostsFixing()->ComputeLowerBounds( p_rmp, p_dual, akt_node->RMPValue(), true );

						// branching
						if (Branching() || CuttingPlanes())
						{
							PerformBranching(akt_node);
							branching_or_cutting = true;
						}
						last_node_terminated = false;
					}
				}
				else
				{
					// no branching necessary as node can be terminated
					if ( InfoLevel() >= 2 )
						Info2() << "No branching, node can be terminated.\n" << flush;
					last_node_terminated = true;
				}
				last_node_integer = false;

				// Heuristic, try to solve as MIP
				if ( ( UseMIPSolverForUBs()
					&& akt_node->Level() <= UseMIPSolverUptoLevel() && akt_node->Level() >= UseMIPSolverUpFromLevel()
					&& !TimeOut() && akt_node->IsFeasible())
					)
				{
					akt_node->SolveAsMIP( MIPMaxSolutionTimeSec() );
					if ( ReducedCostsFixing() )
						ReducedCostsFixing()->CheckAgainstUB( UB() );
				}
			}
			timer_updateUB.Stop();
			info_time_NodeUpdateUB += timer_updateUB.Seconds();

			info_time_NodeRun += timer_Node.Seconds();
			// statistics
			if ( InfoStatisticsLevel()>0 )
			{
				if ( !akt_node->Father() )
				{
					InfoStatistics() << "-------------------------------------------------------------------------------------------------------" << endl;
					InfoStatistics() << "Time........\t"
						<< "Node no(level)\t"
						<< "Nodes(open/term/all)\t"
						<< "......LB......UB.curr.LB"
						<< " additional info"
						<< endl;
					InfoStatistics() << "-------------------------------------------------------------------------------------------------------" << endl;
				}
				InfoStatistics() << timer.FormattedTime() << "\t";
				InfoStatistics() << "Node " << setw(5) << akt_node->NodeNumber()
					<< "(" << setw(2) << akt_node->Level() << ")\t";
				InfoStatistics() << "("
					<< setw(5) << NumOpenBranchAndBoundNodes() << "/"
					<< setw(5) << (int)inactive_nodes.size() << "/"
					<< setw(5) << NumBranchAndBoundNodes() << ")\t";
				InfoStatistics()
					<< setw(8) << p_root_node->SubtreeLB()
					<< setw(8) << ub
					<< setw(8) << akt_node->LB();

				if ( akt_node->IsIntegral() )
				{
					InfoStatistics() << "	int";
					if ( ub == akt_node->LB() )
					{
						InfoStatistics() << ": currently best solution ";
						if ( BestSolution() && InfoPrintBestSolution() )
							InfoStatistics() << endl << *BestSolution() << flush;
					}
				}
				else
				{
					if (!akt_node->IsFeasible())
						InfoStatistics() << "	infeasible";
					else
					{
						if (branching_or_cutting)
						{
							if (akt_node->NumSons() == 1 
								&& !(akt_node->Sons().front()->AddedConstraints().size() ==1 && akt_node->Sons().front()->AddedConstraints().front()->IsStrongBranchingCandidates()))
							{
								InfoStatistics() << "	cutting: " << akt_node->Sons().front()->AddedConstraints().front()->TextInBranching();
							} 
							else
							{
								InfoStatistics() << "	branching: ";
								if (akt_node->NumSons() > 0)
									InfoStatistics() << akt_node->Sons().front()->AddedConstraints().front()->TextInBranching();
								else
									InfoStatistics() << "strong br";
							}
						}
						else
							InfoStatistics() << "	bounding";
					}
				}
				/*if ( !akt_node->Sons().empty() )
					InfoStatistics() << " #" << akt_node->Sons().size()
					  << ":" << akt_node->Sons().front()->AddedConstraints().front()->TextInBranching();*/
				
				InfoStatistics() << endl << flush;
			}
			delete p_dual;
		}
	}
	// post processing
	if ((TimeOut() && SolveAsMipAfterTimeOut()))
	{
		p_rmp->Update(p_root_node);
		p_root_node->SolveAsMIP(MIPMaxSolutionTimeSec());
	}
	timer.Stop();
	UpdateAllNodeInformations( NULL );
	if ( InfoLevel() >= 1 )
	{
		double base_time = timer.Seconds();
		Info1() << "\n"
			<< "Overall computation time: " << timer.Seconds() << "s\n";
		if ( TimeOut() )
			Info1() << "  Timeout after " << MaxSolutionTimeSec() << "s\n";
		Info1() << "Computation Time for first integer solution: " << InfoTimeFirstIntegerSolution() << "s\n"
			<< "Computation Time for best integer solution: " << InfoTimeBestIntegerSolution() << "s\n"
			<< "Computation Time for separation: " << info_time_separation
			<< " (" << int(info_time_separation/base_time*100) << "%)" << "\n"
			<< "Computation Time for pricing: " << info_time_pricing
			<< " (" << int(info_time_pricing/base_time*100) << "%)" << "\n"
			<< "Computation Time for reoptimization: " << info_time_reoptimization
			<< " (" << int(info_time_reoptimization/base_time*100) << "%)" << "\n"
			<< "Number of Simplex iterations: " << InfoNumberOfSimplexIterations() << endl
			<< "Number of RMP iterations: " << InfoNumberOfRMPIterations() << endl
			<< "Number of columns generated: " << info_number_of_generated_columns << "\n"
			<< "Number of pricing problems solved: " << info_number_of_solved_pricing_problems << "\n"
			<< "Number of cuts generated: " << info_number_of_generated_cuts << "\n"
			<< "Number of B&B-Nodes solved/generated: " << info_number_of_solved_nodes << "/" << NumBranchAndBoundNodes() << "\n"
			<< flush;
		// branching statistics
		map<string,int> counter_for_branching;
		StatisticsBranching( counter_for_branching );
		map<string,int>::const_iterator ii;
		int num_branchings = 0;
		for ( auto ii : counter_for_branching )
			num_branchings += ii.second;
		for ( auto ii : counter_for_branching )
			Info1() << "      | "
			        << setw(5) << ii.second << " branchings on " << setw(10) << ii.first << " ("  
							<< int( double(ii.second)/double(num_branchings)*100.0 +.5 )
			        << "%)" << endl;
		// cutting statistics
		for ( auto prob : SeparationProblems() )
			prob->OutputInOStream(cout);
		// overall best solution
		if ( p_best_solution && InfoPrintOverallBestSolution() )
			Info1() << "Best integer solution: \n"
			<< *p_best_solution
			<< "\n" << flush;
	}
	// column statistics
	PrintColumnStatistics();
	// clean memory
	delete p_red_costs_fixing;
}

void c_Controller::PrintColumnStatistics()
{
	if ( InfoStatisticsColumns() )
	{
		// compute aggregated statistics (like in pivot tables)
		map<list<int>,int> agg_statistics_columns;
		for ( auto ii : o_info_statistics_columns )
		{
			list<int> col_info = ii.first;
			while ( col_info.size() > 2 )
			{
				col_info.pop_back();
				agg_statistics_columns[ col_info ] += ii.second;
			}
		}
		for ( auto ii : agg_statistics_columns )
			o_info_statistics_columns[ ii.first ] += ii.second;
		// output
		if ( InfoLevel() >= 1 )
		{
			Info1() << "Columns Statistics: " << endl;
			for (int i : {0, 1, 2}) // init, root, tree
			{
				if ( i==0 )
					Info1() << "  initialization: " << endl;
				else if ( i==1 )
					Info1() << "  linear relax.:  " << endl;
				else if ( i==2 )
					Info1() << "  search tree:    " << endl;
				for ( auto ii : o_info_statistics_columns )
					if ( ii.first.front() == i )
					{
						Info1() << "    ";
						list<int> col_info = ii.first;

						Info1() << "#";
						for ( auto jj : col_info )
							Info1() << jj << ".";

						col_info.pop_front();
						int col_type = col_info.empty() ? -1 : col_info.front();
						col_info.pop_front();
						if ( s_info_column_type_names[col_type] != "" )
							Info1() << s_info_column_type_names[col_type];
						else
							Info1() << col_type;
						for ( auto jj : col_info )
							Info1() << "~" << jj;
						Info1() << ": " << ii.second << endl;
					}
			}
		}
	}
}

bool pairsorterNodes(const pair<double, list<c_BranchAndBoundNode*> >& i, const pair<double, list<c_BranchAndBoundNode*> >& j)
{
	return i.first < j.first;
}

void c_Controller::PerformBranching( c_BranchAndBoundNode* akt_node )
{
	GetBranchingInfo.Reset();
	GetBranchingInfo.Start();
	// was branching performed earlier?
	if ( akt_node->NumSons() )
		return;
	list< list<c_BranchAndBoundConstraint*> > cons_list;
	// cutting planes
	if ( CuttingPlanes() )
	{
		list<c_BranchAndBoundConstraint*> cons;
		akt_node->GetSeparationConstraints(cons);
		if (!cons.empty())
			cons_list.push_back(cons);
	}
	// time for root node
	if ( !akt_node->Father() )
	{
		info_time_root_lb1 = timer.Seconds();
	}
	if ( akt_node->Level() == 0 )
	{
		info_time_root_lb2 = timer.Seconds();
	}
	if ( Branching() && cons_list.empty() )
	{
		if ( StrongBranching()  )
		{
			// strong branching 
			int max_k = StrongBranchingNumCandidates();
			max_k = max(StrongBranchingMinNumCandidates(), StrongBranchingNumCandidates() - int(StrongBranchingNumCandidatesDecreasePerLevel() * akt_node->Level()));
			if (max_k > 1)
			{
				c_StrongBranchingCandidates* candidates = akt_node->ComputeStrongBranchingCandidates(max_k);
				cons_list.push_back({ candidates });
			}
			else // standard
				akt_node->GetBranchAndBoundConstraints(cons_list);
		}
		else
		{
			// standard branching 
			akt_node->GetBranchAndBoundConstraints( cons_list );
		}
		GetBranchingInfo.Stop();
		info_time_root_branching += GetBranchingInfo.Seconds();
	}
	for ( auto cons : cons_list )
	{
		c_BranchAndBoundNode* new_node = akt_node->CreateNewNode( cons );
		akt_node->AddSon( new_node );
		AddNode( new_node );
	}
}

c_BranchAndBoundNode* c_Controller::SelectNextNode()
{
	c_BranchAndBoundNode* erg = NULL;
	e_branching branching = BranchingRule();
	switch (branching)
	{
		case depth_first:
		{
			// depth-first
			erg = *active_nodes.rbegin();
			list<c_BranchAndBoundNode*>::reverse_iterator brother = active_nodes.rbegin();
			brother++;
			while (brother != active_nodes.rend() && (*brother)->Father() == erg->Father())
			{
				erg = *brother;
				brother++;
			}
			break;
		}
		case best_first:
		{
			// best first, return node with smallest LB
			// break ties by selecting nodes from the tree from left to right...
			double best_lb = LocalInfty() * LocalInfty();
			int best_lvl = LocalInfty() ;
			list<c_BranchAndBoundNode*>::iterator i;
			for (i = active_nodes.begin(); i != active_nodes.end();)
			{
				if (RMP()->RoundLBTo((*i)->LB()) > UB() - (LocalCG_EPS() * LocalCG_EPS())) //pruning
				{
					inactive_nodes.push_back(*i);
					auto new_i = i;
					new_i++;
					active_nodes.erase(i);
					i = new_i;
				}
				else
				{
					if ( (*i)->LB() < best_lb     || (  (int) (100* (*i)->LB() ) == (int)(100* best_lb) && (*i)->GetLevel_new() < best_lvl ) )
					{
						erg = *i;
						best_lb = (*i)->LB();
						best_lvl = (*i)->GetLevel_new();
					}


					/*if ((*i)->LB() < best_lb + (LocalCG_EPS() * LocalCG_EPS()))
					{
						erg = *i;
						best_lb = (*i)->LB();
					}*/
					++i;
				}
			}
			break;
		}
		case fifo:
		{
			erg = *active_nodes.begin();
			break;
		}
	}
	// this should not happen...
	if (erg == NULL)
		return NULL;
	else
	{
		// is this node a strong banching node with candidates not evaluated yet 
		if (erg->AddedConstraints().size() == 1 && erg->AddedConstraints().front()->IsStrongBranchingCandidates())
		{
			erg->DecideOnStrongBranchingCandidate();
		}
		// now we have a node to process
		active_nodes.remove(erg);
		inactive_nodes.push_back(erg);
		return erg;
	}
}

void c_Controller::AddColumns( const list<c_Column*>& col_list )
{
	p_rmp->AddColumns( col_list );
	// store in controller 
	int position = (int)cols_in_rmp.size();
	cols_in_rmp.resize( cols_in_rmp.size() + col_list.size() );
	for ( auto col : col_list )
	{
		cols_in_rmp[position] = col;
		cols_in_rmp[position]->SetNumInRMP( position );
		position++;
	}
}

void c_Controller::AddNode( c_BranchAndBoundNode* node )
{
	node->SetRemoved( false );

	if ( node->IsSolved() )
	{
		inactive_nodes.push_back( node );
		// recursively for the sons...
		for ( auto son_iter : node->Sons() )
			AddNode( son_iter );
		return;
	}
	if ( node->NodeNumber() < 0 )
		node->SetNodeNumber( next_node_id++ );
	active_nodes.push_back( node );

	// globally valid constraints can be added directly to the RMP
	for ( auto bb_con : node->AddedConstraints())
	{
		// loop over all rmp constraints for current B&B constraint
		list<c_Constraint*> constraintsToAdd;
		for ( auto rmp_con : bb_con->RMPConstraints())
			if ( rmp_con->IsActiveAtNode( p_root_node ) && !p_rmp->IsConstraintInRMP( rmp_con ) )
				constraintsToAdd.push_back(rmp_con);
		p_rmp->AddConstraints( constraintsToAdd );
	}
}

void c_Controller::RemoveNode( c_BranchAndBoundNode* node )
{
	node->SetRemoved( true );

	list<c_BranchAndBoundNode*>::iterator i;
	for ( i=active_nodes.begin(); i!=active_nodes.end(); ++i )
		if ( *i == node )
			break;
	if ( i!=active_nodes.end() )
		active_nodes.erase( i );

	for ( i=inactive_nodes.begin(); i!=inactive_nodes.end(); ++i )
		if ( *i == node )
			break;
	if ( i!=inactive_nodes.end() )
		inactive_nodes.erase( i );

	// recursively for the sons...
	for ( auto son_iter : node->Sons() )
		RemoveNode( son_iter );
	if ( InfoLevel() >=3 )
		Info3() << "Removed node " << node->NodeNumber() << endl;
}

void c_Controller::Update( c_BranchAndBoundNode* node )
{
	c_TimeInfo timer_RMPUpdate;
	timer_RMPUpdate.Start();
	// Update of RMP
	RMP()->Update( node );
	timer_RMPUpdate.Stop();
	info_timer_RMPUpdate += timer_RMPUpdate.Seconds();

	c_TimeInfo timer_PricingUpdate;
	timer_PricingUpdate.Restart();
	// loop over all Pricing Problems and update
	vector<c_PricingProblem*>::iterator prob;
	for ( auto hierarchy : pricing_problem_hierarchy )
		hierarchy->Update( node );		
	timer_PricingUpdate.Stop();
	info_timer_PricingUpdate += timer_PricingUpdate.Seconds();
}

void c_Controller::Pricing( int iteration, c_DualVariables* dual, list<c_Column*>& col_list, int NumPricersUsed )
{
	// Initialisierung
	list<c_ColumnPricePair> column_price_list;
	list<c_ColumnPricePair> to_remove_list;

	if ( InfoLevel() >= 3 )
		Info3() << "Pricing " << flush;

	c_TimeInfo pricing_time_info;
	pricing_time_info.Start();
	int generated_cols = 0;
	for ( auto hh : PricingProblemHierarchies() )
	{
		if( TimeOut() ) 
			return;
		hh->Update( dual );
		generated_cols += hh->Go( dual, column_price_list, NumPricersUsed );
		info_number_of_solved_pricing_problems++;
	}

	// Select an appropriate subset of columns to include into the
	// RMP. Especially in situations, where the number of generated columns is
	// large this method should take care of a good choice of columns.
	int size_bofore_sel = (int) column_price_list.size();
	if ( PricingWithColumnSelection() )
	{
		ColumnSelectionInPricing( column_price_list, to_remove_list );
		for ( auto col_price : to_remove_list)
			delete col_price.first;
	}
	pricing_time_info.Stop();

	//info_time_pricing += pricing_time_info.Seconds();
	info_number_of_generated_columns += generated_cols;
	info_number_of_rmp_iterations++;

	if ( generated_cols && ( InfoLevel() >= 3 ) )
	{
		Info3() << " " << generated_cols << " col's generated" << flush;
		if ( size_bofore_sel != column_price_list.size() )
			Info3() << ", " << (int)column_price_list.size() << " col's selected" << flush;
	}
	if ( InfoLevel() >= 3 )
		Info3() << " (" << pricing_time_info.MilliSeconds() << "ms)" << flush;

	// check, if too many columns generated
	int max_cols_to_add = MaxColumnsToAddInPricing();
	double threshold = LocalInfty() * LocalInfty();
	if ( (int)column_price_list.size() >= MaxColumnsToAddInPricing() )
	{
		// take only the best MaxColumnsToAddInPricing() of
		c_FindKthElement k_of_n_alg( (int)column_price_list.size() );

		int idx=0;
		for ( auto col_price : column_price_list )
			k_of_n_alg.SetElement( idx++, col_price.second );
		threshold = k_of_n_alg.FindKthElementOfN(max_cols_to_add - 1, (int)column_price_list.size() );
		if ( InfoLevel() >= 3 )
		{
			int to_add=0;
			for ( auto col_price : column_price_list )
				if ( col_price.second <= threshold && to_add < max_cols_to_add )
					to_add++;
			Info3() << ", only " << to_add << " added.\n";
		}
	}
	else
		if ( InfoLevel() >= 3 )
			Info3() << ".\n";

	// Transfer Columns from pairs to single columns
	col_list.clear();
	for ( auto col_price : column_price_list )
		if ( col_price.second <= threshold && (int)col_list.size() < max_cols_to_add )
			col_list.push_back( col_price.first );
		else
			delete col_price.first;
	column_price_list.clear();
}

void c_Controller::Separation( c_RMP* rmp, list<c_Constraint*>& cuts, int node_level )
{
	auto& separators = SeparationProblems();
	if ( separators.size() == 0 )
		return;
	if ( InfoLevel() >= 3 && separators.size() )
		Info3() << "Separation " << flush;

	// loop over all separation problems
	c_TimeInfo separation_time_info;
	separation_time_info.Start();
	for ( auto prob : separators )
		prob->Go( rmp, cuts, node_level );
	separation_time_info.Stop();

	int num_of_cuts = (int)cuts.size();
	info_number_of_generated_cuts += num_of_cuts;
	if ( num_of_cuts && ( InfoLevel() >= 3 ) )
		Info3() << " " << num_of_cuts << " cuts generated" << flush;
	if ( InfoLevel() >= 3 )
		Info3() << " (" << separation_time_info.MilliSeconds() << "ms)." << flush;
}

void c_Controller::AddSeparationProblem( c_SeparationProblem* prob )
{
	separation_problems.push_back( prob );
}

void c_Controller::AddFeasibilityCuttingProblem( c_SeparationProblem* prob )
{
	feasibility_cutting_problems.push_back( prob );
}

int c_Controller::NumBranchAndBoundNodes() const 
{ 
	return NumOpenBranchAndBoundNodes() + (int)inactive_nodes.size();
}

int c_Controller::NumOpenBranchAndBoundNodes() const
{ 
	return (int)active_nodes.size() + NumOpenStrongBranchingNodes();
}

int c_Controller::NumOpenStrongBranchingNodes() const
{
	int num_strong_branching_nodes = 0;
	for (auto node : active_nodes)
		if (node->AddedConstraints().size() == 1 && node->AddedConstraints().front()->IsStrongBranchingCandidates())
		{
			c_StrongBranchingCandidates* cand = (c_StrongBranchingCandidates*)node->AddedConstraints().front();
			num_strong_branching_nodes += (cand->EstimatedNumBranchAndBoundNodes() - 1);
		}
	return num_strong_branching_nodes;
}

bool c_Controller::FeasibilityCutting()
{
	bool result = false;
	for ( auto iter : feasibility_cutting_problems )
	{
		// call each cutting problem
		list<c_Constraint*> cuts;
		iter->Go( RMP(), cuts, -1 );
		if ( !cuts.empty() )
		{
			RMP()->AddConstraints( cuts );
			result = true;
		}
	}
	return result;
}

double c_Controller::LB() const
{
	if ( p_root_node )
		return p_root_node->SubtreeLB();
	return -LocalInfty();
}

double c_Controller::LB1() const // lower bound at root node
{
	// Get all nodes...
	list<c_BranchAndBoundNode*> all_nodes;
	for ( auto node : active_nodes )
		all_nodes.insert( all_nodes.end(), node );
	for ( auto node : inactive_nodes )
		all_nodes.insert( all_nodes.end(), node );
	for ( auto node : all_nodes )
		if ( !node->Father() )
			return node->LB();
	return -LocalInfty();
}

double c_Controller::LB2() const    // lower bound at root node + cutting planes
{
	// Get all nodes...
	list<c_BranchAndBoundNode*> all_nodes;
	for ( auto node : active_nodes )
		all_nodes.insert( all_nodes.end(), node );
	for ( auto node : inactive_nodes )
		all_nodes.insert( all_nodes.end(), node );
	double result = -LocalInfty();
	for ( auto node : all_nodes )
		if ( node->IsRootNode() && result < node->LB() )
			result = node->LB();
	return result;
}

void c_Controller::OrganizePool()
{
	double max_ratio_cols_rows = MaxRatioColsToRows();
	long max_cols_allowed = (long) ceil( 1.333 * max_ratio_cols_rows
		* double( p_rmp->NumRows() ) );
	long max_cols_allowed2= (long) ceil( max_ratio_cols_rows
		* double( p_rmp->NumRows() ) );
	int n = p_rmp->NumCols();
	if ( n <= max_cols_allowed )
		return;
	else
	{
		if ( InfoLevel() >= 3 )
		{
			Info3() << "\nColumnPool: " << flush;
			Info3() << "sort... " << flush;
		}
		c_TimeInfo pool_time_info;
		pool_time_info.Start();

		double* c_red = new double[n];
		p_rmp->GetReducedCosts( c_red );
		double test_rmp_val = p_rmp->objval();

		list<c_Column*> add_to_pool;
		c_FindKthElement k_of_n_alg( n );
		int i;
		for ( i=0; i<n; i++ )
			if ( ColumnInRMP(i)->IsTransferableToPool() )
				k_of_n_alg.SetElement( i, c_red[i] );
			else
				k_of_n_alg.SetElement(i, -LocalInfty());
		double threshold = k_of_n_alg.FindKthElementOfN( max_cols_allowed2 - 1, n );
		for (i = 0; i < n; i++)
		{
			if (ColumnInRMP(i)->IsTransferableToPool()
				&& (c_red[i] > threshold)
				&& (c_red[i] >= LocalCG_EPS()))
			{
				// test value
				double val;
				p_rmp->getX(&val, i, i);
				if (val == 0.0)
					add_to_pool.insert(add_to_pool.end(), ColumnInRMP(i));
			}
			
		}
		delete[] c_red;

		if (InfoLevel() >= 3)
			Info3() << "transfer... " << flush;
		int pool_plus = (int)add_to_pool.size();
		TransferColumnsToPool(add_to_pool);
		add_to_pool.clear(); // no longer valid

		if (InfoLevel() >= 3)
			Info3() << "reopt... " << flush;
		p_rmp->Optimize(c_RMP::eDUALOPT);

		// Test
		if (fabs(test_rmp_val - p_rmp->objval()) > 100.0 * LocalCG_EPS())
		{
			InfoErr() << "#ERROR: reopt. error after pool transfer" << endl;
			InfoErr() << "#ERROR: " << test_rmp_val << " <> " << p_rmp->objval() << endl;
		}

		pool_time_info.Stop();
		if (InfoLevel() >= 3)
		{
			Info3() << pool_plus
				<< " columns added to pool ("
				<< pool_time_info.MilliSeconds() << " ms).\n" << flush;
		}
	}
}

void c_Controller::TransferColumnsToPool( const list<c_Column*>& col_list )
{
	for ( auto col : col_list )
		if ( col->IsInRMP() && col->IsTransferableToPool() )
		{
			int pos = col->NumInRMP();
			cols_in_rmp[ pos ] = NULL;
		}
		else
			InfoErr() << "#ERROR: column not in RMP or not transferable to pool.\n";

	int i;
	for ( i=(int)cols_in_rmp.size()-1; i>=0; i-- )
		if ( !cols_in_rmp[i] )
			p_rmp->delcols( i, i );

	int pos = 0;
	for (i = 0; i < (int)cols_in_rmp.size(); i++)
	{
		if (cols_in_rmp[i])
		{
			cols_in_rmp[pos] = cols_in_rmp[i];
			cols_in_rmp[pos]->SetNumInRMP(pos);
			pos++;
		}
	}
	cols_in_rmp.resize(pos);
	p_pool->AddColumns(col_list);
}

void c_Controller::OutputInOStream( ostream& s ) const
{
	s << "Controller: \n"
		<< "  Columns in RMP: " << (int)cols_in_rmp.size() << "\n"
		<< "  UB = " << ub << "\n";
}

void c_Controller::StatisticsBranching( map<string,int>& counter_for_branching )
{
	counter_for_branching.clear();
	list<c_BranchAndBoundNode*> all_nodes;
	for ( auto node : active_nodes )
		all_nodes.insert( all_nodes.end(), node );
	for ( auto node : inactive_nodes )
		all_nodes.insert( all_nodes.end(), node );
	for ( auto node : all_nodes )
		if ( !node->Sons().empty() )
		{
			string text = node->Sons().front()->AddedConstraints().front()->TextInBranching();
			counter_for_branching[text]++;
		}
}

void c_Controller::LBUpdateAllert()
{
	if ( p_conv_info )
	{
		double lb = p_root_node->SubtreeLB();
		double ub = UB();
		if ( p_conv_info->NumEntries() == 0 || ub < p_conv_info->BestUB() || lb > p_conv_info->BestLB() )
		{
			long time_ms = long( SolutionTimeSec() * 1000.0 );
			p_conv_info->AddPoint( time_ms, lb, ub );
		}
	}
}

void c_Controller::WriteConvergencyInfo( const char* filename )
{
	if ( p_conv_info )
		p_conv_info->WriteFile( filename );
	else
		cout << "No convergency information recorded.  Turn on parameter <InfoConvergency>." << endl;
}
void c_Controller::RecordStatisticsColumns( const list<int>& col_info_type )
{
	o_info_statistics_columns[ col_info_type ]++;
}

c_Controller::e_phase_in_bap c_Controller::PhaseInBaP() const
{
	return i_phase_in_bap;
}

int c_Controller::StatisticsColumns( const list<int>& col_info ) const
{
	auto finder = o_info_statistics_columns.find( col_info );
	if ( finder != o_info_statistics_columns.end() )
		return finder->second;
	else
		return 0;
}

bool c_Controller::local_IsFractional(double x)
{
	return (((ceil(x) - x > x - floor(x)) ? x - floor(x) : ceil(x) - x) > LocalCG_EPS());
}

c_Solution* c_Controller::CreateSolutionFromMIP(c_RMP* theRMP, vector<pair<int, double> >& columns_in_Mip_sol)
{
	cerr << "Serious Error! If you use SolveAsMip, you have to implement the function virtual c_Solution* CreateSolutionFromMIP(c_RMP* theRMP) in your problem-specific controller" << endl;
	return nullptr;
}


/****************************************************************************

c_ColumnPool

****************************************************************************/

c_ColumnPool::~c_ColumnPool()
{
}

void c_ColumnPool::AddColumns( const list<c_Column*>& col_list )
{
	bool do_nothing = true;
	if ( do_nothing )
	{
		for ( auto col : col_list )
			delete col;
		return;
	}
	else
	{
		// allocate sufficient memory
		int max = Controller()->RMP()->NumConstraints();
		double cost1;
		int nzcnt1;
		int* ind1           = new int[ max ];
		double* val1    = new double[ max] ;
		double lb1;
		double ub1;
		double cost2;
		int nzcnt2;
		int* ind2           = new int[ max ];
		double* val2    = new double[ max ];
		double lb2;
		double ub2;

		// in pool?
		// pairwise comparison
		for ( auto col1 : col_list )
		{
			// nzcnt1 = (*col1)->CPXnzcnt();
			nzcnt1 = col1->GetCPXColumn( cost1, ind1, val1, lb1, ub1, NULL );
			bool vorhanden = false;
			for ( auto col2 : cols_in_pool )
			{
				// Teste *col1 und *col2 auf Gleichheit
				bool equal = true;
				// nzcnt2 = (*col2)->CPXnzcnt();
				nzcnt2 = col2->GetCPXColumn( cost2, ind2, val2, lb2, ub2, NULL );
				if ( nzcnt1!=nzcnt2 || cost1 != cost2 )
					equal = false;
				else
					for (int i=0; i<nzcnt1; i++ )
						if ( ( ind1[i] != ind2[i] ) || ( val1[i] != val2[i] ) )
						{
							equal = false;
							break;
						}

				if ( equal )
				{
					vorhanden = true;
					break;
				}
			}
			if ( !vorhanden )
				cols_in_pool.insert( cols_in_pool.end(), col1 );
		}
		// clean up everything
		delete[] ind1;
		delete[] ind2;
		delete[] val1;
		delete[] val2;
	}
}

void c_ColumnPool::RemoveColumns( c_DualVariables* dual, double threshold )
{
	for ( auto col=cols_in_pool.begin(); col!=cols_in_pool.end();  )
		if ( (*col)->ReducedCosts( *dual ) >= threshold )
		{
			list<c_Column*>::iterator merk = col;
			++merk;
			delete *col;
			cols_in_pool.remove( *col );
			col = merk;
		}
		else
			++col;
}

void c_ColumnPool::ReducePool( c_DualVariables* dual, int max_number_of_columns )
{
	int size = (int)cols_in_pool.size();
	if ( size <= max_number_of_columns )
		return;
	c_FindKthElement find_k_of_n( (int)cols_in_pool.size() );
	int pos = 0;
	for ( auto col : cols_in_pool )
		find_k_of_n.SetElement( pos++, col->ReducedCosts( *dual ) );
	double threshold = find_k_of_n.FindKthElementOfN( max_number_of_columns, size, c_FindKthElement::increasing_order );
	RemoveColumns( dual, threshold );
}

void c_ColumnPool::MoveAllColumnsToRMP()
{
	Controller()->AddColumns( cols_in_pool );
	cols_in_pool.clear();
}

void c_ColumnPool::Pricing( c_DualVariables* dual, list<c_Column*>& col_list, list<double>& prices )
{
}


/****************************************************************************

c_BranchAndBoundNode

****************************************************************************/

c_BranchAndBoundNode::c_BranchAndBoundNode( c_Controller* controller )
:	p_controller( controller ),
	p_father( NULL ),
	i_node_number(-1),
	i_iteration(NO_ITERATION),
	i_node_number_solved(0),
	i_num_iter_wo_feasibility_cut(0),
	b_is_removed( false ),
	b_is_integral( false ),
	b_is_feasible( true ),
	b_terminated( false ),
	d_LB( -numeric_limits<double>::max() ),
	d_RMP_value( -INFTY ),
	i_level(0)
{}

c_BranchAndBoundNode::c_BranchAndBoundNode( c_BranchAndBoundNode* p_father, list<c_BranchAndBoundConstraint*>& add_constraints )
:	p_controller( p_father->Controller() ),
	p_father( p_father ),
	i_node_number(-1), 
	i_iteration( NO_ITERATION ),
	i_num_iter_wo_feasibility_cut(0),
	b_is_removed(false),
	b_is_integral(false),
	b_is_feasible(true),
	b_terminated(false),
	d_LB(p_father->LB()),
	d_RMP_value(-INFTY),
	i_level(1+p_father->GetLevel_new())
{
	// B&B constraints from father
	// add new B&B constraints
	for ( auto con : add_constraints )
	{
		l_added_constraints.push_back( con );
		// set the RMP constraint active
		for ( auto rmp_con : con->RMPConstraints())
		{
			if ( rmp_con->IsGloballyValid() )
				rmp_con->SetActiveAtNode( NULL );
			else
				rmp_con->SetActiveAtNode( this );
		}
	}
}

c_BranchAndBoundNode::~c_BranchAndBoundNode()
{
	for (auto con : l_added_constraints )
		delete con;
}

string c_BranchAndBoundNode::UniquePattern() const
{
	if ( !Father() )
		return string("0");
	string result = Father()->UniquePattern();
	list<c_BranchAndBoundNode*>::const_iterator ii;
	int pos = 0;
	for (ii = Father()->l_sons.begin(); ii != Father()->l_sons.end(); ++ii, pos++)
	{
		if (*ii != this)
		{
			ostringstream buffer;
			buffer << pos;
			result.append(buffer.str());
			break;
		}
	}
	return result;
}

void c_BranchAndBoundNode::AddSon( c_BranchAndBoundNode* son )
{
	/*
	if (_eldestChild == nullptr) 
		_eldestChild = son;
	if (_youngestChild != nullptr) 
		son->setElderSibling(_youngestChild);
	_youngestChild = son;
	*/
	l_sons.insert( l_sons.end(), son );
}

int c_BranchAndBoundNode::Level() const
{
	if ( !p_father )
		return 0;
	if ( p_father->l_sons.size() == 1 )
		return p_father->Level();
	return 1 + p_father->Level();
}

int c_BranchAndBoundNode::DetailedLevel() const
{
	if ( !p_father )
		return 0;
	return 1 + p_father->DetailedLevel();
}

int CGBase::c_BranchAndBoundNode::GetLevel_new() const
{
	return i_level;
}

bool c_BranchAndBoundNode::IsIntegral() const
{
	return b_is_integral;
}

bool c_BranchAndBoundNode::IsFeasible() const
{
	return b_is_feasible;
}

void c_BranchAndBoundNode::SetLB( double val )
{
	d_LB = val;
	p_controller->LBUpdateAllert();
}

//double c_BranchAndBoundNode::SubtreeLB() const
//{
//	// if node is infeasible
//	if (!IsFeasible() && !p_controller->TimeOut())
//		return INFTY;
//
//	double sLB = LB();
//	for (const auto s : sons) // consider all LBs from all sons and determine minimum
//	{
//		if (s->IsFeasible())
//		{
//			double sub_lb = s->SubtreeLB();
//			if (sub_lb < sLB) sLB = sub_lb;
//		}
//	}
//	return sLB;
//}

double c_BranchAndBoundNode::SubtreeLB() const
{
	// if node is infeasible
	if (!IsFeasible() && !p_controller->TimeOut())
		return INFTY;
	// if there are no sons
	if (l_sons.size() == 0)
		return LB();
	// consider all LBs from all sons and determine minimum
	double result = INFTY;
	for (auto son : l_sons)
	{
		if (son->IsFeasible())
		{
			double sub_lb = son->SubtreeLB();
			if (sub_lb < result)
				result = sub_lb;
		}
	}
	return result;
}

double c_BranchAndBoundNode::ComputeLagrangeanLB()
{
	double result = -Controller()->LocalInfty();

	if ( Controller()->UseLagrangeanLB() )
	{
		// determine actual LP lower bound
		result = Controller()->RMP()->ActualObjectiveValue();
		// loop over all pricing problems
		double sum = 0.0;
		for ( auto hier : Controller()->PricingProblemHierarchies())
			sum += hier->BestReducedCosts() * hier->RHSofConvexityConstraint();
		result += sum;
		// Safe new lower bound, if bound is good
		if ( LB() < result)
			SetLB( result );
	}
	return result;
}

/*
void c_BranchAndBoundNode::Get_k_th_best_BranchAndBoundContraints
	( int k, list< list<c_BranchAndBoundConstraint*> >& con_list )
{
	cout << "throw - c_BranchAndBoundNode::Get_k_th_best_BranchAndBoundContraints -> Strong Branching is not yet implemented" << endl;
	throw;
}
*/

void c_BranchAndBoundNode::DecideOnStrongBranchingCandidate()
{
	if (AddedConstraints().size() != 1)
	{
		cerr << "Something wrong with strong branching. (1)\n";
		throw;
	}
	c_BranchAndBoundConstraint* con = AddedConstraints().front();
	if (!con->IsStrongBranchingCandidates())
	{
		cerr << "Something wrong with strong branching. (2)\n";
		throw;
	}
	if (Father()->NumSons() != 1)
	{
		cerr << "Something wrong with strong branching. (3)\n";
		throw;
	}
	if (Controller()->InfoLevel() >= 2)
		Controller()->Info2() << "Strong Branching..." << endl;
	// statistics
	c_TimeInfo Timer;
	Timer.Start();
	// OK, lets evaluate the candidates
	auto& info2(Controller()->Info2());
	c_BranchAndBoundNode* father = Father();
	double best_lb_of_test_sons = LB();
	int idx = 0;
	c_StrongBranchingCandidates* candidates = (c_StrongBranchingCandidates*)con;
	vector< pair<double, list<c_BranchAndBoundNode*> > > nodes(candidates->Candidates().size());
	for (auto cand : candidates->Candidates())
	{	
		nodes[idx].first = 0.0;
		double min_lb_of_test_sons = Controller()->LocalInfty();
		double max_lb_of_test_sons = -Controller()->LocalInfty();
		double product = 1.0;
		for (auto cons_list : cand)
		{
			c_BranchAndBoundNode* node = Father()->CreateNewNode(cons_list);
			nodes[idx].second.push_back(node);
			if (Controller()->InfoLevel() >= 2)
			{
				info2 << "Probing { ";
				for (auto ii : node->AddedConstraints())
					info2 << *ii << "; ";
				info2 << "} ";
			}
			// exact
			int old_info_level = Controller()->InfoLevel();
			Controller()->InfoLevel.SetValue(0);
			c_DualVariables* dual = node->Go(numeric_limits<double>::infinity(), best_lb_of_test_sons, Controller()->PricersUsedForStrongBranching());
			delete dual;
			node->SetUnsolved();
			Controller()->InfoLevel.SetValue(old_info_level);
			double value = (Controller()->PricersUsedForStrongBranching() == MaxNumPricers) ? node->LB() : node->RMPValue();
			if (value < min_lb_of_test_sons)
				min_lb_of_test_sons = value;
			if (value > max_lb_of_test_sons)
				max_lb_of_test_sons = value;
			product *= (std::max(value, Controller()->LocalCG_EPS())); // ??? delta?
			if (min_lb_of_test_sons < best_lb_of_test_sons)
				break; // ???
		}
		// Depending on Strategy: product rule or weighted sum
		if (Controller()->StrongBranchingRule() == "Product")
			nodes[idx].first = -product;
		else // weighted sum
			nodes[idx].first = -(((1 - Controller()->StrongBranchingMu()) * min_lb_of_test_sons) + (Controller()->StrongBranchingMu() * max_lb_of_test_sons));
		if (Controller()->InfoLevel() >= 2)
			info2 << "Pairs MinLB " << min_lb_of_test_sons << " and MaxLB " << max_lb_of_test_sons << endl;
		// increment counter
		idx++;
	}
	// Select the best candidate
	sort(nodes.begin(), nodes.end() );
	bool first = true;
	for (auto node : nodes[0].second)
	{
		if (first)
		{
			l_added_constraints = node->AddedConstraints();
		}
		else
		{
			father->AddSon(node);
			Controller()->AddNode(node);
		}
		first = false;
	}
	// Throw away the other B&B nodes
	for (int idx = 1; idx < (int)nodes.size(); idx++)
		for (auto node : nodes[idx].second)
			delete node; // remark: the B&B constraints are deleted in the destructor of c_BranchAndBoundNode
	// clear memory
	delete candidates;
	// undo changes; very important for a following MIP run
	Controller()->Update(this);
	// statistics
	Timer.Stop();
	Controller()->SetTimeStrongBranching(Timer.Seconds() + Controller()->InfoTimeStrongBranching());
}

c_StrongBranchingCandidates* c_BranchAndBoundNode::ComputeStrongBranchingCandidates(int max_k)
{
	cerr << "Serious error: Please provide member function\n"
		 << "   c_StrongBranchingCandidates* c_BranchAndBoundNode::ComputeStrongBranchingCandidates(int k)\n"
		 << "in the derived class of\n"
		 << "   c_BranchAndBoundNode\n"
		 << "if you intend to use strong branching.\n"; 
	throw;
}

c_DualVariables* c_BranchAndBoundNode::Go( double termination_lb, double termination_ub, int NumPricersUsed )
	// terminate if LB(node) goes above termination_lb
	// or UB(node) falls below termination_ub
{
	// Streams for controlled output
	ostream& info_2     = p_controller->Info2();
	ostream& info_3     = p_controller->Info3();
	ostream& info_err = p_controller->InfoErr();
	int info_level = p_controller->InfoLevel();

	c_TimeInfo timer_contUpdate;
	timer_contUpdate.Restart();

	// update of rmp, cols etc.
	p_controller->Update( this );
	timer_contUpdate.Stop();
	p_controller->SetInfoTimeContUpdate( p_controller->InfoTimeContUpdate() + timer_contUpdate.Seconds() );

	c_TimeInfo timer_dual;
	timer_dual.Restart();
	i_iteration = 0;
	c_DualVariables* dual = p_controller->CreateDualVariables();
	c_RMP* rmp = p_controller->RMP();
	if ( p_controller->Stabilization() )
	{
		rmp->UpdateStabilization( -1, 0, dual );
		//rmp->write( "mylp.lp", "LP"   );
	}
	double old_lb = LB();
	// text
	if ( info_level >= 2 )
	{
		info_2 << "Start with new node: node " << NodeNumber();
		if ( Father() )
			info_2 << ", father-node " << Father()->NodeNumber() << " \n";
		info_2 << "\n";
		info_2 << *this << "\n" << flush;
	}
	
	timer_dual.Stop();
	p_controller->SetInfoTimeCreateDual(p_controller->InfoTimeCreateDual() + timer_dual.Seconds());
	bool end = false;
	bool early_termination = false;
	double LLB = -Controller()->LocalInfty();
		
	while ( !end )
	{
		LLB = -Controller()->LocalInfty();
		// count iterations
		i_iteration++;
		i_num_iter_wo_feasibility_cut++;
		if ( info_level >= 3 )
		{
			info_3 << "Iteration " << i_iteration << ":\n" << flush;
			//rmp->write( "lp", "LP"    );
			info_3 << "Opt[" << p_controller->UseLPOptimizer() << "](cols=" << rmp->NumCols()
				<< "; rows=" << rmp->NumRows()
				<< "): " << flush;
		}

		// solve RMP
		// dualen Simplex-Algorithmus zum Reoptimieren zu Beginn eines B&B-Knotens
		// primalen Simplex-Algorithmus danach, ist meist schneller
		c_TimeInfo reopt_time_info;
		reopt_time_info.Start(); 		
		c_TimeInfo timer_rmp;
		timer_rmp.Restart();
	
		if ( p_controller->UseLPOptimizer().compare("primal" ) == 0 )
			rmp->Optimize( c_RMP::ePRIMOPT );
		else if ( p_controller->UseLPOptimizer().compare("dual" ) == 0 )
			rmp->Optimize( c_RMP::eDUALOPT );
		else if ( p_controller->UseLPOptimizer().compare("barrier" ) == 0 )
			rmp->Optimize( c_RMP::eBAROPT );
		else
		{
			cout << "Unknown parameter for UseLPOptimizer: " << p_controller->UseLPOptimizer()<<"!"<< endl;
			throw;
		}
		reopt_time_info.Stop();
		timer_rmp.Stop();

		p_controller->SetInfoTimeReoptimization( p_controller->InfoTimeReoptimization() + reopt_time_info.Seconds() );
		if ( info_level >= 3 )
			info_3 << "(" << reopt_time_info.MilliSeconds() << "ms) ";
		
		double act_rmp = rmp->ActualObjectiveValue();
		int status = rmp->stat();
#if CPX_VERSION >= 900
		if ( status != CPX_STAT_OPTIMAL && status != CPX_STAT_OPTIMAL_INFEAS )
		{
			if ( info_level >= 0 )
			{
				if ( status == CPX_STAT_INFEASIBLE )
					info_3 << "INFEASIBLE" << endl;
				else if ( status == CPX_STAT_UNBOUNDED )
					info_3 << "UNBOUNDED" << endl;
				else // other unknown error
					info_3 << "UNKNOWN ERROR" << endl;
			}
			
		}
		if ( status == CPX_STAT_OPTIMAL_INFEAS  )
		{
			int helpcounter = 0;
			int i = 0;
			while (status == CPX_STAT_OPTIMAL_INFEAS && helpcounter < 12)
			{
				helpcounter++;
				timer_rmp.Restart();
				if (i == 0)
				rmp->Optimize(c_RMP::ePRIMOPT);
				if (i == 1)
				rmp->Optimize(c_RMP::eDUALOPT);
				if (i == 2)
				rmp->Optimize(c_RMP::eBAROPT);
				timer_rmp.Stop();
				status = rmp->stat();
				++i;
				i = i % 3;
			}
			cout << "Optimal Infeasible: Runs " << helpcounter+1 << endl;
		}
		// rmp->write( "RMP.lp", "LP"   );
#else
		if ( status != CPX_OPTIMAL && status != CPX_OPTIMAL_INFEAS )
		{
			if ( info_level >= 0 )
			{
				if ( status == CPX_INFEASIBLE )
					info_3 << "INFEASIBLE" << endl;
				else if ( status == CPX_UNBOUNDED )
					info_3 << "UNBOUNDED" << endl;
				else // other unknown error
					info_3 << "UNKNOWN ERROR" << endl;
			}
		}
#endif
		if (Controller()->CheckForInftyAndLBError())
		{
			int counter = 0; //10 Iterations
			if (status == CPX_STAT_INFEASIBLE || act_rmp < old_lb - 0.1)
			{
				int i = 0;
				while ((status == CPX_STAT_INFEASIBLE || act_rmp < old_lb - 0.1) && counter < 10)
				{
					bool infeasy = false;
					if (status == CPX_STAT_INFEASIBLE)
						infeasy = true;

					if (infeasy)
						cout << "Reoptimize (infeasible) iteration: " << counter << endl;
					else
						cout << "Reoptimize (LB-Error) iteration: " << counter << endl;

					timer_rmp.Restart();
					if (i == 0)
					{
						Controller()->CPLEXEnv()->setparam(CPX_PARAM_ADVIND, 0);
						rmp->primopt();
						//rmp->Optimize(c_RMP::ePRIMOPT);
					}
					if (i == 1)
					{
						Controller()->CPLEXEnv()->setparam(CPX_PARAM_ADVIND, 0);
						Controller()->CPLEXEnv()->setparam(CPX_PARAM_DPRIIND, CPX_DPRIIND_AUTO);
						rmp->dualopt();
						//rmp->Optimize(c_RMP::eDUALOPT);
					}
					if (i == 2)
					{
						rmp->baropt();
						//	rmp->Optimize(c_RMP::eBAROPT);
					}
					timer_rmp.Stop();
					status = rmp->stat();

					if (status != CPX_STAT_INFEASIBLE)
					{
						rmp->SetObjval(rmp->objval());
						act_rmp = rmp->ActualObjectiveValue();
						cout << "Val-rmp: " << act_rmp << endl;
						//rmp->OutputInOStream(cout);
					}
					++i;
					i = i % 3;
					counter++;
				}
			}
		}
		if (Controller()->InfoNumberOfRMPIterations()>1)
		{
			c_TimeInfo timer_OrgPool;
			timer_OrgPool.Restart();
			p_controller->OrganizePool();
			timer_OrgPool.Stop();
			p_controller->SetInfoTimeOrgPool(p_controller->InfoTimeOrgPool() + timer_OrgPool.Seconds());
		}
		//WriteFile();
		if ( act_rmp < old_lb - 0.1 && !p_controller->Stabilization() )
		{
			// here is something wrong
			info_err << "#ERROR: new LB= " << act_rmp << " is smaller than LB before.\n";
			info_err << "Current Node number is " << i_node_number << " father number is " << p_father->NodeNumber() << "\n";
			info_err << *rmp << "\n" << flush;
			rmp->GetDualVariables(*dual);
			info_err << *dual << "\n" << flush;
			rmp->write("lbsmaller_err.lp");
			WriteFile();
			cin.get();
			//throw;
		}
		// text
		if ( info_level >= 3 )
		{
			info_3 << "RMP=" << act_rmp
				<< ", lb=" << LB()
				<< "\n" << flush;
		}

		c_TimeInfo timer_GetDual;
		timer_GetDual.Restart();
		// retrieve duals
		rmp->GetDualVariables( *dual );
		timer_GetDual.Stop();
		p_controller->SetInfoTimeGetDual(p_controller->InfoTimeGetDual() + timer_GetDual.Seconds());

		// solve all pricing problem hierarchies
		// and add new columns to the LP
		list<c_Column*> new_cols;

		bool last_update = !p_controller->Stabilization();
#if CPX_VERSION >= 900
		if ( status == CPX_STAT_OPTIMAL || status == CPX_STAT_OPTIMAL_INFEAS || status == CPX_STAT_NUM_BEST )
		{
			// Pricing
			c_TimeInfo timer_pricing;
			timer_pricing.Restart();
			bool feasibility_cut_added = false;
			do {
				p_controller->Pricing( i_iteration, dual, new_cols, NumPricersUsed );
				// if there are no more new columns, but the RMP is infeasible because of missing constraints
				feasibility_cut_added = false;
				bool smallGAP=false;
				if (LB()/rmp->ActualObjectiveValue()>Controller()->FeasyCutGap())
					smallGAP=true;
				if ( (new_cols.empty() && NodeNumber() >= 0 ) || i_num_iter_wo_feasibility_cut == p_controller->FeasyCutFrequence() || i_iteration ==1 || smallGAP )
				{
					feasibility_cut_added = p_controller->FeasibilityCutting();
					if ( feasibility_cut_added )
					{
						if ( p_controller->UseLPOptimizer().compare("primal" ) == 0 )
							rmp->Optimize( c_RMP::ePRIMOPT );
						else if ( p_controller->UseLPOptimizer().compare("dual" ) == 0 )
							rmp->Optimize( c_RMP::eDUALOPT );
						else if ( p_controller->UseLPOptimizer().compare("barrier" ) == 0 )
							rmp->Optimize( c_RMP::eBAROPT );
						delete dual;
						dual = p_controller->CreateDualVariables();
						rmp->GetDualVariables( *dual );
					}
					i_num_iter_wo_feasibility_cut=0;
				}
			}
			while ( feasibility_cut_added );
			timer_pricing.Stop();
			if (!p_father && new_cols.empty())
				p_controller->SetInfoTimeLastPPRoot(timer_pricing.Seconds());

			p_controller->SetInfoTimePricing(p_controller->InfoTimePricing() + timer_pricing.Seconds());
			// update stabilization
			if ( p_controller->Stabilization() && ( new_cols.size() ==0 || i_iteration % p_controller->StabFrequence() ==0 ) )
			{			
				last_update = rmp->UpdateStabilization( i_iteration, (int)new_cols.size(), dual );			
			}
			// LLB
			if ( last_update )
			{
				LLB = ComputeLagrangeanLB();
				if ( p_controller->ReducedCostsFixing() && Level() == 0 && LLB > 0 )
					p_controller->ReducedCostsFixing()->ComputeLowerBounds( rmp, dual, LLB, false );
			}
		}
#else
		if ( status == CPX_OPTIMAL || status == CPX_OPTIMAL_INFEAS || status == CPX_STAT_NUM_BEST )
		{
			p_controller->Pricing( iteration, dual, new_cols, NumPricersUsed );

			// update stabilization
			if ( p_controller->Stabilization() && ( new_cols.size() ==0 || iteration % stab_freq ==0 ) )
				last_update = rmp->UpdateStabilization( iteration, new_cols.size(), dual );
			// LLB
			if ( last_update )
			{
				LLB = ComputeLagrangeanLB();
				if ( p_controller->ReducedCostsFixing() && Level() == 0 )
					p_controller->ReducedCostsFixing()->ComputeLowerBounds( rmp, dual, LLB, false );
			}
		}

#endif
		c_TimeInfo timer_AddColumn;
		timer_AddColumn.Restart();
		// complex conditions for termination...
		end = ( last_update && ( new_cols.size() == 0 ) )
			|| ( last_update && ( early_termination = ( Controller()->EarlyBranching() && this->EarlyBranching() && i_num_iter_wo_feasibility_cut == 0) ) )
			|| ( p_controller->TimeOut() )
			|| (rmp->RoundLBTo(LLB) >= Controller()->UB() - Controller()->LocalCG_EPS())
			|| (LB() >= 1 * Controller()->LocalInfty())
			|| ( LLB >= termination_lb )
			|| ( act_rmp <= termination_ub );
		// add cols
		if ( !end )
			p_controller->AddColumns( new_cols );
		else
		{
			for ( auto col : new_cols )
				delete col;
			new_cols.clear();
		}
		timer_AddColumn.Stop();
		p_controller->SetInfoTimeAddColum( p_controller->InfoTimeAddColum() + timer_AddColumn.Seconds() );
		/*ofstream stream("Statistik.csv", ios::app);
		stream << iteration << ";" << (int)new_cols.size() << ";" << LB() << endl;*/
	}
	// store LP value
	SetRMPValue( rmp->ActualObjectiveValue() );
	
	if ( !early_termination && !p_controller->TimeOut() )
	{
		if ( LLB > LB() &&  p_controller->UseLagrangeanLB() )
			SetLB( LLB );
		if ( rmp->ActualObjectiveValue() > LB() 
				&& !p_controller->UseLagrangeanLB() 
				&& termination_lb >= -numeric_limits<double>::infinity() 
				&& termination_ub <= numeric_limits<double>::infinity() )
			SetLB( rmp->ActualObjectiveValue() );
	}
	// store integrality and feasibility of node
	if ( rmp->stat() == 0 ) // this call is necessary if the problem has been manipulated by stabilization
	{
		if ( p_controller->UseLPOptimizer().compare("primal" ) == 0 )
			rmp->Optimize( c_RMP::ePRIMOPT );
		else if ( p_controller->UseLPOptimizer().compare("dual" ) == 0 )
			rmp->Optimize( c_RMP::eDUALOPT );
		else if ( p_controller->UseLPOptimizer().compare("barrier" ) == 0 )
			rmp->Optimize( c_RMP::eBAROPT );
		else
		{
			cout << "Unknown parameter for UseLPOptimizer: " << p_controller->UseLPOptimizer() << endl;
			throw;
		}
	}
	b_is_integral = rmp->IsIntegral() && !p_controller->TimeOut();
	b_is_feasible = rmp->IsFeasible() || p_controller->TimeOut();

	// Debug:
	// rmp->write( "lp", "LP"   );

	// node done; print out results
	if ( info_level >= 2 )
	{
		if ( !p_controller->TimeOut() )
			info_2 << "Solution of node:" << NodeNumber() << "\n";
		else
			info_2 << "Incomplete solution of node:" << NodeNumber() << ", running out of time\n";
		info_2 << "  lower bound:" << LB() << "\n";
		if ( LB() != rmp->ActualObjectiveValue() )
			info_2 << "  RMP-value:  " << rmp->ActualObjectiveValue()
			<< " (gap = " << RelativeDifference( LB(), rmp->ActualObjectiveValue() ) << ")\n";
		info_2 <<   "  iterations: " << i_iteration << "\n";
		if ( b_is_feasible )
			info_2 << "  integral:   " << ( b_is_integral ? "yes" :"no" ) << "\n" << flush;
		else
			info_2 << "  feasible:   no\n" << flush;

		if ( info_level >= 2 )
		{
			info_2 << *rmp << flush;
			info_2 << *dual << flush;
			rmp->write( "mylp.lp", "LP" );
		}
	}
	return dual;
}

void c_BranchAndBoundNode::WriteFile()
{
}

bool c_BranchAndBoundNode::AnySonSolved() const
{
	for ( auto son_iter :l_sons )
		if ( son_iter->IsSolved() )
			return true;
	return false;
}

bool c_BranchAndBoundNode::GetSeparationConstraints( list<c_BranchAndBoundConstraint*>& con_list )
{
	int num_cuts = 0;
	c_RMP* rmp = Controller()->RMP();

	c_TimeInfo sep_time_info;
	sep_time_info.Start();
	list<c_Constraint*> cuts;
	p_controller->Separation( rmp, cuts, Level() );
	sep_time_info.Stop();
	p_controller->SetInfoTimeSeparation(p_controller->InfoTimeSeparation() + sep_time_info.Seconds());
	num_cuts = (int)cuts.size();

	if ( num_cuts )
	{
		c_BranchAndBoundConstraint* constraint = new c_BranchAndBoundConstraintSeparation( Controller(), cuts );
		con_list.push_back ( constraint );
		return true;
	}
	else
		return false;
}

// the only option to hand over information to CPLEX/C is a global variables 
static c_Controller* glob_controller = NULL;
static int mip_time_limit = 0;
static c_TimeInfo mip_timer;

// CPLEX-Callback 
#ifndef  CPX_PROTOTYPE_MIN
static int CPXPUBLIC
	mymipcallback (CPXCENVptr env, void *cbdata, int wherefrom, void *cbhandle)
#else
static int CPXPUBLIC
	mycallback (env, cbdata, wherefrom, cbhandle)
	CPXCENVptr  env;
void       *cbdata;
int        wherefrom;
void       *cbhandle;
#endif
{
	// Check, whether MIP-Solver is responsible
	if ( wherefrom < CPX_CALLBACK_MIP
		|| wherefrom > CPX_CALLBACK_MIP_PROBE )
		return 0;

	// Check MIP for new UB
	int    status = 0;
	double best_integer = INFTY;
	status = CPXgetcallbackinfo
		(env, cbdata, wherefrom, CPX_CALLBACK_INFO_BEST_INTEGER, &best_integer );
	if ( status )
		return status;
	else
		if ( glob_controller->UB() > best_integer )
		{
			// New best Solution found by CPLEX-MIP-Solver
			glob_controller->Info1() << "... New best Solution found by CPLEX-MIP-Solver: " << best_integer <<  endl;
			glob_controller->SetUB( best_integer );
		}
		// Check Timeout
	if ((glob_controller->TimeOut() && !glob_controller->SolveAsMipAfterTimeOut()) || mip_timer.Seconds() >= mip_time_limit)
		{
			glob_controller->Info1() << "... MIP timeout." << endl;
			return (status=1) ;
		}
		return (status);
} /* END mycallback */


// solving the node with a MIP solver is a heuristic (to improve UB)
//double c_BranchAndBoundNode::SolveAsMIP( long time_limit )
//{
//	double result = Controller()->LocalInfty()*Controller()->LocalInfty();
//	c_RMP* rmp = Controller()->RMP();
//	c_Controller* ctlr = Controller();
//	//rmp->write("RootReady.lp", "LP");
//	cout << "Solve as MIP..." << endl;
//	mip_time_limit = time_limit;
//	mip_timer.Start();
//	//ctlr->RMP()->Optimize( c_RMP::eDUALOPT );
//	ctlr->FixPartOfSolution();
//	// set my callback function
//	CPXENVptr env = (CPXENVptr) ctlr->CPLEXEnv()->getEnv();
//	//CPXsetdblparam(env, CPX_PARAM_EPGAP, 0.001);
//	CPXsetintparam(env, CPX_PARAM_THREADS, ctlr->NumThreadsRMP());
//	CPXsetintparam(env, CPX_PARAM_MIPSEARCH, 2);
//	glob_controller = ctlr;
//	//CPXsetmipcallbackfunc( env, mymipcallback, NULL);
//	// remove redundant cols (if any)
//	if ( ctlr->MaxRatioColsToRowsMIP() < ctlr->MaxRatioColsToRows() )
//	{
//		if ( ctlr->InfoLevel() >= 1 )
//		{
//			ctlr->Info1() << "Invoking MIP Solver of cplex... " << endl;
//			ctlr->Info1() << "... Reducing columns for MIP-Solver... " << endl;
//		}
//		double merk = Controller()->MaxRatioColsToRows();
//		ctlr->SetMaxRatioColsToRows( ctlr->MaxRatioColsToRowsMIP() );
//		Controller()->RMP()->Optimize( c_RMP::eDUALOPT );
//		ctlr->OrganizePool();
//		ctlr->SetMaxRatioColsToRows( merk );
//	}
//	// Do optimization
//	double old_UB = ctlr->UB();
//	rmp->OptimizeMIP( c_RMP::ePRIMOPT );
//	result = rmp->mipobjval();
//	mip_timer.Stop();
//	ctlr->SetInfoTimeMIPForUB(ctlr->InfoTimeMIPForUB() + mip_timer.Seconds());
//	// quality?
//	//if (rmp->IsFeasible() && (result - ctlr->UB()) < -ctlr->LocalCG_EPS()) //  new best solution found
//	if (rmp->IsFeasible() && (result - old_UB) < -ctlr->LocalCG_EPS()) //  new best solution found
//	{
//		// Store MIP-Solution
//		ctlr->SetBestSolution(
//			ctlr->CreateSolution( Controller()->RMP(),
//			NULL /* no dual info available */ ) );
//		if ( ctlr->InfoLevel() >= 1 )
//		{
//			ctlr->Info1() << "... MIP time: " << mip_timer.Seconds() << "s" << endl;
//			if ( ctlr->BestSolution() && ctlr->InfoPrintBestSolution() )
//				ctlr->Info1() << *ctlr->BestSolution() << flush;
//		}
//	}
//	ctlr->UnfixSolution();
//	// change back to LP 
//	rmp->chgprobtype( CPLEXProblem::LP );
//	// turn off callback
//	CPXsetmipcallbackfunc( env, NULL, NULL);
//	glob_controller = NULL;
//	return result;
//}

double c_BranchAndBoundNode::SolveAsMIP(long time_limit)
{
	double result = Controller()->LocalInfty() * Controller()->LocalInfty();
	c_RMP* rmp = Controller()->RMP();
	c_Controller* ctlr = Controller();
	//rmp->write("RootReady.lp", "LP");
	cout << "Solve as MIP..." << endl;
	mip_time_limit = time_limit;
	mip_timer.Start();


	// remove redundant cols (if any)
	if (ctlr->MaxRatioColsToRowsMIP() < ctlr->MaxRatioColsToRows())
	{
		if (ctlr->InfoLevel() >= 1)
		{
			ctlr->Info1() << "Invoking MIP Solver of cplex... " << endl;
			ctlr->Info1() << "... Reducing columns for MIP-Solver... " << endl;
		}
		double merk = Controller()->MaxRatioColsToRows();
		ctlr->SetMaxRatioColsToRows(ctlr->MaxRatioColsToRowsMIP());
		Controller()->RMP()->Optimize(c_RMP::eDUALOPT);
		ctlr->OrganizePool();
		ctlr->SetMaxRatioColsToRows(merk);
	}

	// Do optimization
	rmp->OptimizeMIP(c_RMP::ePRIMOPT);
	result = rmp->mipobjval();
	mip_timer.Stop();
	ctlr->SetInfoTimeMIPForUB(ctlr->InfoTimeMIPForUB() + mip_timer.Seconds());


	// change back to LP 
	rmp->chgprobtype(CPLEXProblem::LP);
	// turn off callback
	return result;
}

void c_BranchAndBoundNode::GetConstraints( list<c_BranchAndBoundConstraint*>& con_list ) const
{
	con_list = l_added_constraints;
	c_BranchAndBoundNode* p_father = Father();
	while ( p_father )
	{
		const list<c_BranchAndBoundConstraint*>& added_list = p_father->AddedConstraints();
		for(auto iter : added_list )
			con_list.push_back( iter );
		p_father = p_father->p_father;
	}
}

void c_BranchAndBoundNode::OutputInOStream( ostream& s ) const
{
	s << "node LB = " << d_LB << "\n"
		<< "added node constraints: ";
	if ( !l_added_constraints.empty() )
	{
		bool first = true;
		for (auto con : l_added_constraints)
		{
			if (first)
			{
				s << ", ";
				first = false;
			}
			s << *con;
		}
	}
	else
		s << "(none)";
}

/****************************************************************************

c_Constraint

****************************************************************************/

c_Constraint::c_Constraint( c_Controller* controller, char sense, double rhs, double estimator_dual, int type, int n1, int n2, int n3 )
:	p_controller(controller), 
	_type( type ),
	_n1( n1 ),
	_n2( n2 ),
	_n3( n3 ),
	_sense( sense ),
	_rhs( rhs ),
	_estimator_dual( estimator_dual ),
	col_yps_plus( NULL ),
	col_yps_minus( NULL ),
	active_at_node( NULL )
{
	controller->AddConstraint( this );

}


int c_Constraint::Index() const
{
	c_RMP* rmp = p_controller->RMP();
	if ( !rmp->IsConstraintInRMP( this ) )
	{
		cout << "throw - c_Constraint::Index -> Constraint is not in RMP" << endl;
		throw;
	}
	return rmp->ConstraintIndex( _type, _n1, _n2, _n3 );
}

bool c_Constraint::IsActiveAtNode( c_BranchAndBoundNode* node ) const
{
	// globally active
	if ( !active_at_node )
		return true;
	else
	{
		c_BranchAndBoundNode* p_node = node;
		while ( p_node )
		{
			if ( p_node == active_at_node )
				return true;
			p_node = p_node->Father();
		}
		return false;
	}
}


void c_Constraint::SetActiveAtNode( c_BranchAndBoundNode* node )
{
	active_at_node = node;
}



void c_Constraint::CreateYpsColumns()
{
	col_yps_plus  = CreateYpsPlusColumn();
	col_yps_minus = CreateYpsMinusColumn();
}


void c_Constraint::OutputInOStream( ostream& s ) const
{
	s << "constraint with ";
	if ( _sense == 'E' || _sense == 'e' )
		s << "==";
	else if ( _sense == 'G' || _sense == 'g' )
		s << ">=";
	else if ( _sense == 'L' || _sense == 'l' )
		s << "<=";
	s << _rhs;
}

/****************************************************************************

c_RMP

****************************************************************************/

c_RMP::c_RMP( c_Controller* controller )
	: CPLEXProblem( *controller->CPLEXEnv(), "Column Generation/Branch and Price" ),
	p_controller( controller ),
	actual_iteration( -1 ),
	actual_obj_value( INFTY ),
	info_number_of_simplex_iterations( 0 ),
	num_removed_constraints(0),
	num_added_constraints(0),
	help_mipname("")
{
	Controller()->CPLEXEnv()->setparam( CPX_PARAM_THREADS, Controller()->NumThreadsRMP());
}

c_RMP::~c_RMP()
{
	// do not delete c_Constraint* in constraints
	// they are deleted by the controller
	remove(help_mipname.c_str());
}

void c_RMP::Reset()
{
	actual_iteration = -1;
	actual_obj_value = Controller()->LocalInfty();
	info_number_of_simplex_iterations = 0;
}

void c_RMP::GetDualVariables( c_DualVariables& dual ) const
{
	getPi( dual.CPXDual() );
}

void c_RMP::Optimize( e_solver solver )
{
	ostream& info_err = p_controller->InfoErr();
	actual_iteration++;
	try {
		switch ( solver )
		{
		case ePRIMOPT:
			// primal algorithm
			Controller()->CPLEXEnv()->setparam( CPX_PARAM_ADVIND, 1 );
			primopt();
			break;
		case eDUALOPT:
			// dual algorithm
			Controller()->CPLEXEnv()->setparam( CPX_PARAM_ADVIND, 1 );
			Controller()->CPLEXEnv()->setparam( CPX_PARAM_DPRIIND, CPX_DPRIIND_FULLSTEEP );
			dualopt();
			break;
		case eBAROPT:
			// barrier solver
			Controller()->CPLEXEnv()->setparam(CPX_PARAM_SOLUTIONTYPE, 2);
			// hybbaropt(PRIMOPT);
			if (p_controller->InfoNumberOfRMPIterations()<200)
				Controller()->CPLEXEnv()->setparam(CPX_PARAM_BAREPCOMP, 0.000001);
			else
				Controller()->CPLEXEnv()->setparam(CPX_PARAM_BAREPCOMP, 0.0000001);
			baropt();
			if (stat() == CPX_STAT_NUM_BEST )
			{
				cout << "#ERROR: Optimizer errorcode " << stat() << endl;
				cout << "Reopt with primal" << endl;
				primopt();
			}
			break;
		default:
			// error! no appropriate solver selected
			cout << "throw - c_RMP::Optimize -> no appropriate solver selected" << endl;
			throw;
		}
	}
	catch ( CPLEXException e )
	{
		info_err << "#ERROR" << endl;
		info_err << "#errorcode = " << e.errcode() << endl;
		info_err << "#funname = " << e.funcname() << endl;
		info_err << "#what = " << e.what() << endl;
		info_err << "#writing LP to error.lp" << endl;
		write("error.lp");
	}
	// count simplex iterations
	info_number_of_simplex_iterations += getitcnt();

#if CPX_VERSION >= 900

	if ( stat() == CPX_STAT_OPTIMAL || stat() == CPX_STAT_OPTIMAL_INFEAS )
		actual_obj_value = objval();
	else
	{
		cout << "#ERROR: Optimizer errorcode " << stat() << endl;
		switch ( stat() )
		{
		case CPX_STAT_UNBOUNDED:                actual_obj_value = -Controller()->LocalInfty();                  break;
		case CPX_STAT_INFEASIBLE:               actual_obj_value = Controller()->LocalInfty() * Controller()->LocalInfty();   break;

		case CPX_STAT_NUM_BEST:                 actual_obj_value = -Controller()->LocalInfty();                  break;
			//case CPX_STAT_FEASIBLE_RELAXED:   actual_obj_value = -Controller()->LocalInfty();                  break;
			//case CPX_STAT_OPTIMAL_RELAXED:    actual_obj_value = -Controller()->LocalInfty();                  break;
			// Abbruch
		case CPX_STAT_ABORT_IT_LIM:
		case CPX_STAT_ABORT_TIME_LIM:
		case CPX_STAT_ABORT_OBJ_LIM:
		case CPX_STAT_ABORT_USER: actual_obj_value = Controller()->LocalInfty();                     break;
			// XXX
		case CPX_STAT_INForUNBD:                actual_obj_value = Controller()->LocalInfty();                       break;
		}
		write( "error.lp", "LP" );
	}
#else // prior CPLEX 9
	if ( stat() == CPX_OPTIMAL )
		actual_obj_value = objval();
	else
	{
		cout << "#ERROR: Optimizer errorcode " << stat() << endl;
		switch ( stat() )
		{
		case CPX_INFEASIBLE:            actual_obj_value = Controller()->LocalInfty() * Controller()->LocalInfty();       break;
		case CPX_UNBOUNDED:             actual_obj_value = -Controller()->LocalInfty();                  break;
		case CPX_OBJ_LIM:                   actual_obj_value = Controller()->LocalInfty();                       break;
		case CPX_IT_LIM_FEAS:           actual_obj_value = objval();                break;
		case CPX_IT_LIM_INFEAS:   actual_obj_value = Controller()->LocalInfty();                     break;
		case CPX_TIME_LIM_FEAS:     actual_obj_value = objval();                break;
		case CPX_TIME_LIM_INFEAS: actual_obj_value = Controller()->LocalInfty();                     break;
		case CPX_NUM_BEST_FEAS:     actual_obj_value = objval();                break;
		case CPX_NUM_BEST_INFEAS: actual_obj_value = Controller()->LocalInfty();                     break;
		case CPX_OPTIMAL_INFEAS:    actual_obj_value = objval();                break;
			// Abbruch
		case CPX_ABORT_FEAS:
		case CPX_ABORT_INFEAS:
		case CPX_ABORT_DUAL_INFEAS:
		case CPX_ABORT_PRIM_INFEAS:
		case CPX_ABORT_PRIM_DUAL_INFEAS:
		case CPX_ABORT_PRIM_DUAL_FEAS:
		case CPX_ABORT_CROSSOVER: actual_obj_value = Controller()->LocalInfty();                     break;
			// XXX
		case CPX_INForUNBD:             actual_obj_value = Controller()->LocalInfty();                       break;
		}
		write( "error.lp", "LP" );
	}
#endif
}

void c_RMP::SetRequiredVarsIntegral()
{
	char integral[] = { 'I' };
	for ( int i=0; (i<numcols()); i++ )
		chgctype( 1, &i, integral );
}


void c_RMP::SetConstraintSense(int contraintIndex, char sense) {

	c_Controller* ctlr = Controller();

	char _sense[] = { sense };
	int  _indices[] = { contraintIndex };

	CPXchgsense(ctlr->CPLEXEnv()->getEnv(), (CPXLPptr)this, 1, _indices, _sense);
	//chgsense(1, _indices, _sense); //XXX CT

}


// try to solve the LP as a MIP (->UB)
void c_RMP::OptimizeMIP( e_solver solver )
{
	c_Controller* ctlr = Controller();
	const bool accelerate_MIP_solver = ctlr->MIPAccelerate();

	chgprobtype( CPLEXProblem::MIP );
	char integral[] = { 'I' };
	for ( int i=0; (i<numcols()); i++ )
		chgctype( 1, &i, integral );

	try {
		d_mip_objval = Controller()->LocalInfty() * Controller()->LocalInfty();
		vector<pair<int, double> > columns_in_MIPsol;
		if ( !accelerate_MIP_solver )
		{
		CPXENVptr env = (CPXENVptr) ctlr->CPLEXEnv()->getEnv();
		//set parameters
		CPXsetintparam(env, CPX_PARAM_THREADS, ctlr->NumThreadsRMP());
		if (ctlr->MIPScreenOutput())
			CPXsetintparam(env, CPX_PARAM_SCRIND, CPX_ON);
		CPXsetdblparam(env, CPX_PARAM_TILIM, ctlr->MIPMaxSolutionTimeSec());
		CPXsetdblparam(env, CPX_PARAM_CUTUP, ctlr->UB() + 1);

		// We use the same CPLEX problem
		mipopt();
		//Reset parameters for lp
		CPXsetintparam(env, CPX_PARAM_SCRIND, CPX_OFF);
		CPXsetdblparam(env, CPX_PARAM_TILIM, ctlr->MaxSolutionTimeSec());
		CPXsetdblparam(env, CPX_PARAM_CUTUP, INFTY);
		d_mip_objval = CPLEXProblem::mipobjval();    // Call the function of the base class
		if ((d_mip_objval - ctlr->UB()) < -ctlr->LocalCG_EPS()) //  new best solution found
		{
			for (int i = 0; i < numcols(); i++)
			{
				double val_x;
				getX(&val_x, i, i);
				if (val_x > CG_EPS)
					columns_in_MIPsol.push_back(make_pair(i, val_x));
			}
		}
		}
		else
		{
			// We write out a new problem, read it in, and solve the new problem
			// CPLEX uses all the sophisticated preprocessing etc. here
			//
			// Warning: this causes a segmentation fault on the MOGON cluster for unknown reasons
			//
			if (help_mipname == "")
			{
				stringstream mipname;
				srand((int)time(nullptr));
				int help = rand();
				//mipname << "./tmp/MIP" << (int)(10000 * ctlr->InfoTimeReoptimization()) << (int)(10000 * ctlr->InfoTimePricing()) << help << ".mps";
				mipname << "./tmp/MIP" << ".mps";
				help_mipname = mipname.str().c_str();
			}
			write(help_mipname.c_str());
			CPLEXEnvironment env_mip;
			if (ctlr->MIPScreenOutput())
				env_mip.setparam(CPX_PARAM_SCRIND, CPX_ON);
			env_mip.setparam(CPX_PARAM_THREADS, ctlr->NumThreadsRMP());
			double soltimemip = ctlr->MIPMaxSolutionTimeSec();
			env_mip.setparam(CPX_PARAM_TILIM, soltimemip);
			env_mip.setparam(CPX_PARAM_CUTUP, ctlr->UB());
			CPLEXProblem the_mip(env_mip);
			the_mip.read(help_mipname.c_str());
			//the_mip.write("MIP_write.lp");
			the_mip.mipopt();
			d_mip_objval = the_mip.mipobjval();
			if ((d_mip_objval - ctlr->UB()) < -ctlr->LocalCG_EPS()) //  new best solution found
			{
				for (int i = 0; i < the_mip.numcols(); i++)
				{
					double val_x;
					the_mip.getX(&val_x, i, i);
					if (val_x > CG_EPS)
						columns_in_MIPsol.push_back(make_pair(i, val_x));
				}
			}
			env_mip.terminate();
		}
		if (ctlr->InfoLevel() >= 1)
			ctlr->Info1() << "... MIP time: " << mip_timer.Seconds() << "s" << endl;


		c_Solution* sol = ctlr->CreateSolutionFromMIP(Controller()->RMP(), columns_in_MIPsol);
		if (sol != nullptr) //if no dummys in sol
		{
			if (d_mip_objval < ctlr->UB()) //if new UB
			{
				ctlr->SetBestSolution(sol);
				ctlr->SetUB(d_mip_objval);
				if (ctlr->InfoLevel() >= 1)
					ctlr->Info1() << "... New best solution with value: " << d_mip_objval << endl;
			}	
		}
	}
	catch(CPLEXException e) {
		if(e.errcode() != 1217) //No onscreen Info if no solution exists
			cout << e.what() << endl;
	}
	// LATER:
	// chgprobtype( CPLEXProblem::LP );
}

double c_RMP::mipobjval() const
{
	return d_mip_objval;
}

bool c_RMP::IsFeasible() const
{
	// Falls Optimierung fehlgeschlagen...
#if CPX_VERSION >= 900
	if ( stat() != CPX_STAT_OPTIMAL )
		return false;
#else
	if ( stat() != CPX_OPTIMAL )
		return false;
#endif
	double x;
	double obj;
	for (int i = 0, maxi = numcols(); i < maxi; ++i)
	{
		getX( &x, i, i );
		getobj( &obj, i, i );
		if ((x > Controller()->LocalCG_EPS()) && (obj >= Controller()->LocalInfty()))
			return false;
	}
	return true;
}

bool c_RMP::IsIntegral() const
{
#if CPX_VERSION >= 900
	if ( stat() != CPX_STAT_OPTIMAL )
		return false;
#else
	if ( stat() != CPX_OPTIMAL )
		return false;
#endif
	double x;
	for (int i = 0, maxi = numcols(); i < maxi; ++i)
	{
		getX( &x, i, i );
		if (((x - floor(x)) > Controller()->LocalCG_EPS())
			&& ((ceil(x) - x) > Controller()->LocalCG_EPS()))
			return false;
	}
	return true;
}

void c_RMP::AddColumns( const list<c_Column*>& col_list )
{
	class Y{
	public:
		Y():ccnt_max(0),obj(NULL),cmatbeg(NULL),lb(NULL),ub(NULL),nzcnt_max(0),cmatind(NULL),cmatval(NULL){};
		~Y(){delete[] obj;delete[] cmatbeg;delete[] lb;delete[] ub;delete[] cmatind;delete[] cmatval;};
		int ccnt_max;
		double* obj;
		int* cmatbeg;
		double* lb;
		double* ub;
		int nzcnt_max;
		int* cmatind;
		double* cmatval;
	};

	// static init
	static Y y;
	// determine needed space
	int ccnt = (int)col_list.size();
	list<c_Column*>::const_iterator col;
	int nzcnt_estimation = ccnt * NumRows();
	/*
	for ( col=col_list.begin(); col!=col_list.end(); ++col )
		nzcnt+= (*col)->CPXnzcnt();
	*/
	// available memory too small?
	if ( y.ccnt_max < ccnt )
	{
		y.ccnt_max = 2*ccnt;
		delete[] y.obj;
		delete[] y.cmatbeg;
		delete[] y.lb;
		delete[] y.ub;
		y.obj = new double[y.ccnt_max];
		y.cmatbeg = new int[y.ccnt_max];
		y.lb= new double[y.ccnt_max];
		y.ub = new double[y.ccnt_max];
	}
	if ( y.nzcnt_max < nzcnt_estimation )
	{
		y.nzcnt_max = nzcnt_estimation;
		delete[] y.cmatind;
		delete[] y.cmatval;
		y.cmatind = new int[y.nzcnt_max];
		y.cmatval = new double[y.nzcnt_max];
	}
	// do insertion
	int col_no=0;
	int nzcnt = 0;
	for ( auto col : col_list )
	{
		y.cmatbeg[col_no] = nzcnt;		
		nzcnt += col->GetCPXColumn( y.obj[col_no], &y.cmatind[nzcnt], &y.cmatval[nzcnt], y.lb[col_no], y.ub[col_no], NULL );
		col_no++;
		// do statistics
		if ( Controller()->InfoStatisticsColumns() )
			Controller()->RecordStatisticsColumns( col->InfoColumnType() );
	}
	addcols( ccnt, nzcnt, y.obj, y.cmatbeg, y.cmatind, y.cmatval, y.lb, y.ub, NULL );
}

void c_RMP::GetReducedCosts( double c_red[] )
{
	getDj(c_red, 0, -1 );
}

void c_RMP::GetDualPrices( double pi[] )
{
	getPi(pi, 0, -1 );
}

void c_RMP::GetRHS( double rhs[] )
{
	getRHS(rhs, 0, -1 );
}

void c_RMP::RemoveAllBranchAndBoundConstraints()
{
}

bool c_RMP::UpdateStabilization( int iteration, int num_cols_pricing, c_DualVariables* dual )
{
	static int*         col_ind = 0;
	static char*        col_ul = 0;
	static double*  col_bnd = 0;
	static int*         obj_ind = 0;
	static double*  obj_val = 0;
	/*static */int max_size = 0;
	if ( max_size < (int)constraints.size() )
	{
		delete[] col_ind;
		delete[] col_ul;
		delete[] col_bnd;
		delete[] obj_ind;
		delete[] obj_val;
		max_size = 2* (int)constraints.size();
		col_ind = new int[max_size];
		col_ul  = new char[max_size];
		col_bnd = new double[max_size];
		obj_ind = new int[max_size];
		obj_val = new double[max_size];
	}

	bool last_update = true;

	// loop over all constraints...
	int col_cnt = 0;
	int obj_cnt = 0;
	vector< c_Constraint*>::iterator constraint;
	for ( constraint = constraints.begin(); constraint != constraints.end(); ++constraint )
	{
		c_Constraint* con = *constraint;
		// store old values
		double old_eps_minus = con->EpsMinus();
		double old_eps_plus  = con->EpsPlus();
		double old_delta_minus = con->DeltaMinus();
		double old_delta_plus    = con->DeltaPlus();

		// update
		double pi = (dual->CPXDual())[(*constraint)->Index()];
		bool erg = (*constraint)->UpdateStabilization( iteration, num_cols_pricing, pi );
		last_update = last_update && erg;

		// if eps or -eps have changed...
		if ( old_eps_minus != con->EpsMinus()
			&& con->YpsMinusColumn() )
		{
			col_ind[col_cnt] = con->YpsMinusColumn()->NumInRMP();
			col_bnd[col_cnt] = con->EpsMinus();
			col_ul[col_cnt]  = 'U';
			col_cnt++;
		}
		if ( old_eps_plus != con->EpsPlus()
			&& con->YpsPlusColumn() )
		{
			col_ind[col_cnt]    = con->YpsPlusColumn()->NumInRMP();
			col_bnd[col_cnt]    = con->EpsPlus();
			col_ul[col_cnt]     = 'U';
			col_cnt++;
		}

		// if delta y+ or delta y- has changed
		if ( old_delta_minus != con->DeltaMinus()
			&& con->YpsMinusColumn() )
		{
			obj_ind[obj_cnt] = con->YpsMinusColumn()->NumInRMP();
			obj_val[obj_cnt] = -con->DeltaMinus();
			obj_cnt++;
		}
		if ( old_delta_plus != con->DeltaPlus()
			&& con->YpsPlusColumn() )
		{
			obj_ind[obj_cnt] = con->YpsPlusColumn()->NumInRMP();
			obj_val[obj_cnt] = +con->DeltaPlus();
			obj_cnt++;
		}
	}
	chgbds( col_cnt, col_ind, col_ul, col_bnd );
	for ( int i=0;i<obj_cnt;i++ )
		chgcoef( -1, obj_ind[i], obj_val[i] );
	return last_update;
}

void c_RMP::Update( c_BranchAndBoundNode* node )
{
	// Info
	actual_iteration = -1;

	// remove all constraints
	RemoveAllBranchAndBoundConstraints();

	// loop over all constraints and eliminate those that are not valid here
	list<c_Constraint*> non_active;
	for (auto con:constraints )
		if ( !con->IsActiveAtNode( node ) )
			non_active.push_back( con );
	if ( !non_active.empty() )
		RemoveConstraints( non_active );

	num_removed_constraints += (int)non_active.size();

	list<c_BranchAndBoundConstraint*> constraint_list;
	node->GetConstraints( constraint_list );
	AddBranchAndBoundConstraints( constraint_list );

	num_added_constraints += (int)constraint_list.size();
}

void c_RMP::AddBranchAndBoundConstraints( const list<c_BranchAndBoundConstraint*>& constraint_list )
{
	class X{
	public:
		X():cnt_max(-1),indices(NULL),lu(NULL),lb(NULL),ub(NULL),cost(NULL){};
		~X(){delete[] indices;delete[] lu;delete[] lb;delete[] ub;delete[] cost;};
		int cnt_max;
		int* indices;
		char* lu;
		double* lb;
		double* ub;
		double* cost;
	};
	static X x;


	// more space needed than available?
	int cnt = numcols();
	if ( x.cnt_max < cnt )
	{
		x.cnt_max = 2*cnt;
		delete[] x.indices;
		delete[] x.lu;
		delete[] x.lb;
		delete[] x.ub;
		delete[] x.cost;
		x.indices = new int[x.cnt_max];
		x.lu = new char[x.cnt_max];
		x.lb = new double[x.cnt_max];
		x.ub = new double[x.cnt_max];
		x.cost = new double[x.cnt_max];
	}

	// loop over all column and check if lb or ub needs to be updated
	int i;
	int pos = 0;
	for ( i=0; i<cnt; i++ )
	{
		c_Column* col = p_controller->ColumnInRMP(i);
		if ( true /*col->IsTransferableToPool()*/ )
		{
			x.indices[pos] = i;
			x.lb[pos] = col->DefaultLowerBound();
			x.ub[pos] = col->DefaultUpperBound();
			x.cost[pos] = col->GetCPXObj();

			double new_lb = x.lb[pos];
			double new_ub = x.ub[pos];
			list<c_BranchAndBoundConstraint*>::const_iterator con;
			for ( auto con : constraint_list )
			{
				con->ComputeBounds( col, new_lb, new_ub );
				if (new_lb >= new_ub)
				{
					x.lb[pos] = new_lb;
					x.ub[pos] = new_lb;
					break;
				}

				if ( x.lb[pos] < new_lb )
					x.lb[pos] = new_lb;
				if ( x.ub[pos] > new_ub )
					x.ub[pos] = new_ub;
				/*
				if ( new_lb >= new_ub )
					break;
					*/
			}
			++pos;
		}
	}

	// set LB in RMP 
	for ( i=0; i<pos; i++ )
		x.lu[i] = 'L';
	chgbds( pos, x.indices, x.lu, x.lb );
	// set UB in RMP 
	for ( i=0; i<pos; i++ )
	{
		if ( x.ub[i] == 0.0 && x.lb[i]==0.0 )
		{
			x.cost[i] = Controller()->LocalInfty();
			x.ub[i] = Controller()->LocalInfty();
			//x.ub[i]       = 0; //TODO -> TEST

		}
		x.lu[i] = 'U';
	}
	chgbds( pos, x.indices, x.lu, x.ub );

	// change cost coefficient
	chgobj( pos, x.indices, x.cost );

	// RMP constraints in RMP
	list<c_Constraint*> rmp_constraints_to_add;
	for ( auto bb_con:constraint_list )
		for ( auto rmp_con : bb_con->RMPConstraints())
			if ( !IsConstraintInRMP( rmp_con ) )
				rmp_constraints_to_add.push_back( rmp_con );
	AddConstraints( rmp_constraints_to_add );
}

bool c_RMP::IsConstraintInRMP( const c_Constraint* con ) const
{
	if ( !con )
		return false;
	int n0, n1, n2, n3;
	con->Indices( n0, n1, n2, n3 );
	return ConstraintIndexExists( n0, n1, n2, n3 );
}

void c_RMP::RemoveConstraint( c_Constraint* con )
{
	if ( !IsConstraintInRMP( con ) )
	{
		cout << "throw - c_RMP::IsConstraintInRMP -> constraint is not in RMP, cannot be removed" << endl;
		throw;
	}

	int idx = con->Index();
	delrows( idx, idx );
	DeleteIndex( idx );

	//cout << "### " << constraints.size() << " constraints." << endl;
	for ( int i=idx; i<(int)constraints.size()-1; i++ )
	{
		constraints[i] = constraints[i+1];
		// cout << "### constraint " << constraints[i+1] << " is shifted from " << i+1 << " to " << i << "." << endl;
	}
	constraints.pop_back();

	if ( IsConstraintInRMP( con ) )
	{
		cout << "throw - c_RMP::RemoveConstraint -> Constraint is still in RMP" << endl;
		throw;
	}
}

void c_RMP::RemoveConstraints( list<c_Constraint*>& con_list )
{
	list<c_Constraint*>::const_iterator con;
	for ( auto con : con_list)
		RemoveConstraint( con );
}

int c_RMP::AddConstraint( c_Constraint* con )
{
	list<c_Constraint*> con_list;
	con_list.push_back( con );
	AddConstraints( con_list );
	return con->Index();
}

void c_RMP::AddConstraints( list<c_Constraint*>& con_list )
{
	class Z{
	public:
		Z():rcnt_max(-1),rhs(NULL),sense(NULL),rmatbeg(NULL),anz_entries_row(NULL),mmat_max(-1),rmatind(NULL),rmatval(NULL),mcnt_max(-1),ind(NULL),val(NULL),mncnt_max(-1),ind_row(NULL),val_row(NULL){};
		~Z(){delete[] rhs;delete[] sense;delete[] rmatbeg;delete[] anz_entries_row;delete[] rmatind;delete[] rmatval;delete[] ind;delete[] val;delete[] ind_row;delete[] val_row;};
		int rcnt_max;
		double* rhs;
		char* sense;
		int* rmatbeg;
		int* anz_entries_row;

		int mmat_max;
		int* rmatind;
		double* rmatval;

		int mcnt_max;
		int* ind;
		double* val;

		int mncnt_max;
		int* ind_row;
		double* val_row;
	};
	// static init
	static Z z;

	if ( con_list.size() == 0 )
		return;

	for ( auto con : con_list )
	{
#ifdef _DEBUG
		if ( IndexExists( con->_type, con->_n1, con->_n2, con->_n3 ) )
		{
			cout << "throw - c_RMP::AddConstraints -> Constraint already exists" << endl;
			throw;
		}
#endif
		constraints.push_back( con );
		NewIndex( con->_type, con->_n1, con->_n2, con->_n3 );
	}
	// Check, if there are already integrated columns
	int n = NumCols();
	if ( n < 1 )
		return;

	int ccnt = 0;                                            // number of new cols
	int rcnt = (int)con_list.size();     // number of new rows
	int nzcnt = 0;
	int mcnt = NumRows() + rcnt;
	if ( z.rcnt_max < rcnt )
	{
		z.rcnt_max = 2*rcnt;
		delete[] z.rhs;
		delete[] z.sense;
		delete[] z.rmatbeg;
		delete[] z.anz_entries_row;
		z.rhs = new double[ z.rcnt_max ];
		z.sense = new char[ z.rcnt_max ];
		z.rmatbeg = new int[ z.rcnt_max ];
		z.anz_entries_row = new int[ z.rcnt_max ];
	}
	if ( z.mmat_max < n*rcnt )
	{
		z.mmat_max = 2*n*rcnt;
		delete[] z.rmatind;
		delete[] z.rmatval;
		z.rmatind = new int[ z.mmat_max ];
		z.rmatval = new double[ z.mmat_max ];
	}
	if ( z.mcnt_max < mcnt )
	{
		z.mcnt_max =2*mcnt;
		delete[] z.ind;
		delete[] z.val;
		z.ind = new int[z.mcnt_max];
		z.val = new double[z.mcnt_max];
	}
	if ( z.mncnt_max < n*rcnt )
	{
		z.mncnt_max = 2*n*rcnt;
		delete[] z.ind_row;
		delete[] z.val_row;
		z.ind_row     = new int[z.mncnt_max];
		z.val_row = new double[z.mncnt_max];
	}
	double lb;
	double ub;

	// read new constraints
	rcnt = 0;
	for ( auto con : con_list )
	{
		z.rhs[rcnt] = con->Rhs();
		z.sense[rcnt] = con->Sense();
		z.anz_entries_row[rcnt] = 0;
		rcnt++;
	}

	for ( int j=0; j<NumCols(); j++ )
	{
		c_Column* col = Controller()->ColumnInRMP( j );
		double  cost;
		for ( int i=0; i<z.mcnt_max; i++ )
			z.ind[i] = -1;
		int col_nzcnt = col->GetCPXColumn(cost,z.ind,z.val,lb,ub,NULL);
		rcnt = 0;
		for ( auto con:con_list )
		{
			int idx = con->Index();
			// integrate additional values
			for ( int i=0; i<col_nzcnt; i++ )
			{
				if ( z.ind[i] == idx ) // if
				{
					int pos = z.anz_entries_row[rcnt];
					z.ind_row[ n * rcnt + pos ] = j;
					z.val_row[ n * rcnt + pos ] = z.val[i];
					z.anz_entries_row[rcnt]++;
					nzcnt++;
				}
				if ( z.ind[i] < 0 )
				{
					cout << "throw - c_RMP::AddConstraints -> ind[" << i << "] < 0" << endl;
					throw;
				}
			}
			rcnt++;
		}
	}

	// transfer into rmatbeg, rmatind, rmatval
	int pos = 0;
	for ( int i=0; i<(int)con_list.size(); i++ )
	{
		z.rmatbeg[i] = 0;
		for ( int j=0; j<i; j++ )
			z.rmatbeg[i] += z.anz_entries_row[j];

		for ( int k=0; k<z.anz_entries_row[i]; k++ )
		{
			z.rmatind[pos] = z.ind_row[ n*i + k ];
			z.rmatval[pos] = z.val_row[ n*i + k ];
			pos++;
		}
	}

	rcnt = (int)con_list.size();
	addrows( ccnt, rcnt, nzcnt, z.rhs, z.sense, z.rmatbeg, z.rmatind, z.rmatval, NULL, NULL );

}

int c_RMP::ConstraintIndex( int type, int n1, int n2, int n3 ) const
{
	return Index( type, n1, n2, n3 );
}

bool c_RMP::ConstraintIndexExists( int i1, int i2, int i3, int i4 ) const
{
	return IndexExists( i1, i2, i3, i4 ) ;
}

int c_RMP::NumConstraints(void) const
{
	return NumIndices();
}

c_Constraint* c_RMP::Constraint( int i1, int i2, int i3, int i4 ) const
{
	if ( !ConstraintIndexExists( i1, i2, i3, i4 ) )
		return NULL;
	else
		return constraints[Index( i1, i2, i3, i4)];
}

void c_RMP::ConstructEmptyRMP( CPLEXProblem::ObjSense objsen )
{
	int num_rows = NumConstraints();
	int num_cols = 0; // add cols later
	double* obj = new double[1];
	double* rhs = new double[ num_rows ];
	char* sense = new char[ num_rows ];
	int* cmatbeg = new int[1];
	int* cmatcnt = new int[1];
	int* cmatind = new int[1];
	double* cmatval = new double[1];
	double* lb = new double[1];
	double* ub = new double[1];
	for ( auto constraint : constraints )
	{
		rhs[ constraint->Index() ] = constraint->Rhs();
		sense[ constraint->Index() ] = constraint->Sense();
	}
	// format:  ...beg  cnt ind     val     lb      ub
	copylpdata( num_cols, num_rows, objsen, obj, rhs, sense, cmatbeg, cmatcnt, cmatind, cmatval, lb, ub );
	// add surplus and slack column for stabilized column generation
	list< c_Column* > col_list;
	for (auto constraint : constraints)
	{
		if (constraint->Stabilization())
		{
			constraint->CreateYpsColumns();
			if (constraint->YpsMinusColumn())
				col_list.insert(col_list.end(), constraint->YpsMinusColumn());
			if (constraint->YpsPlusColumn())
				col_list.insert(col_list.end(), constraint->YpsPlusColumn());
		}
	}
	Controller()->AddColumns( col_list );
	// clean memory
	delete[] obj;
	delete[] rhs;
	delete[] sense;
	delete[] cmatbeg;
	delete[] cmatcnt;
	delete[] cmatind;
	delete[] cmatval;
	delete[] lb;
	delete[] ub;
}

void c_RMP::OutputInOStream( ostream& s ) const
{
	s << "RMP: \n";
	for ( auto con : constraints )
	{
		s << con->Index() << ": " << *con << endl;
		if ( !IsConstraintInRMP( con ) )
			s << "#Error: constraint not in RMP."   << endl;
	}
}


/****************************************************************************

c_DualVariables

****************************************************************************/

// derived classes should allow a convenient access to duals
// pi(i), mu(k) etc.

c_DualVariables::c_DualVariables( c_Controller* controller, int dim )
	: p_controller( controller )
{
	dual_val = new double[ dim ];
}

c_DualVariables::~c_DualVariables( void )
{
	delete[] dual_val;
}


void c_DualVariables::OutputInOStream( ostream& s ) const
{
	s << "DualVariables: ( ";
	int i;
	for ( i=0; i<Controller()->RMP()->NumConstraints(); i++ )
		s << dual_val[i] << ", ";
	s   << " )\n";
}

/****************************************************************************

c_PricingProblem

****************************************************************************/

c_PricingProblem::c_PricingProblem( c_Controller* controller )
	: p_controller( controller )
{
}


void c_PricingProblem::OutputInOStream( ostream& s ) const
{
	s << "PricingProblem:\n";
}


/****************************************************************************

c_Column

****************************************************************************/

c_Column::c_Column( c_Controller* controller )
	: p_controller( controller ),
	is_in_rmp( false ),
	num_in_rmp( -1 )
{}

void c_Column::OutputInOStream( ostream& s ) const
{
	s << "Column: undefined Type";
}

double c_Column::ReducedCosts( const c_DualVariables& dual )
{
	class X{
	public:
		X():ind(NULL),val(NULL),nzcnt_max(0){};
		~X(){delete[] ind; delete[] val;};
		int* ind;
		double* val;
		int nzcnt_max;
	};
	// static init
	static X x;
	//int nzcnt_max = -1;
	//static int* ind = NULL;
	//static double* val = NULL;

	// enough memory available?
	double cost=0.0;
	int nzcnt_estimation = Controller()->RMP()->NumRows();
	if ( x.nzcnt_max < nzcnt_estimation )
	{
		x.nzcnt_max = 2*nzcnt_estimation;
		delete[] x.ind;
		delete[] x.val;
		x.ind = new int[ x.nzcnt_max ];
		x.val = new double[ x.nzcnt_max ];
	}
	double lb=0.0;
	double ub=0.0;
	char buffer[256];
	int nzcnt = GetCPXColumn( cost, x.ind, x.val, lb, ub, buffer );
	for ( int i=0; i<nzcnt; i++ )
		cost -= dual[x.ind[i]] * x.val[i];
	return cost;
}

double c_Column::CoutReducedCosts(const c_DualVariables& dual)
{
	class X {
	public:
		X() :ind(NULL), val(NULL), nzcnt_max(0) {};
		~X() { delete[] ind; delete[] val; };
		int* ind;
		double* val;
		int nzcnt_max;
	};
	// static init
	static X x;
	//int nzcnt_max = -1;
	//static int* ind = NULL;
	//static double* val = NULL;

	// enough memory available?
	double cost = 0.0;
	int nzcnt_estimation = Controller()->RMP()->NumRows();
	if (x.nzcnt_max < nzcnt_estimation)
	{
		x.nzcnt_max = 2 * nzcnt_estimation;
		delete[] x.ind;
		delete[] x.val;
		x.ind = new int[x.nzcnt_max];
		x.val = new double[x.nzcnt_max];
	}
	double lb = 0.0;
	double ub = 0.0;
	char buffer[256];
	int nzcnt = GetCPXColumn(cost, x.ind, x.val, lb, ub, buffer);
	for (int i = 0; i < nzcnt; i++)
	{
		cost -= dual[x.ind[i]] * x.val[i];
		cout << "Dual: " << dual[x.ind[i]] << " * " << x.val[i] << endl;
	}
		
	return cost;
}

const list<int> c_Column::InfoColumnType() const
{
	list<int> my_list; 
	my_list.push_back( (int)Controller()->PhaseInBaP() ); 
	return my_list;
}

void c_Column::ChangeObjCoeff( double obj )
{
	int j = (int) num_in_rmp;
	Controller()->RMP()->chgobj( 1, &j, &obj );
}


/***************************************************************************

c_BranchAndBoundConstraint

***************************************************************************/

c_BranchAndBoundConstraint::c_BranchAndBoundConstraint( c_Controller* controller )
	: p_controller( controller )
{}

c_BranchAndBoundConstraint::c_BranchAndBoundConstraint( c_Controller* controller, const list<c_Constraint*>& rmp_constraints )
	: p_controller( controller ),
	_rmp_constraints( rmp_constraints )
{}

c_BranchAndBoundConstraint::~c_BranchAndBoundConstraint()
{
	// do not delete c_Constraint* in _rmp_constraints
	// they are deleted by the controller
}

void c_BranchAndBoundConstraint::OutputInOStream( ostream& s ) const
{
	s << "B&B-Constraint: ";
	list<c_Constraint*>::const_iterator rmp_con;
	for ( auto rmp_con : _rmp_constraints )
		s << *rmp_con /*<< "\n"*/;
}

/***************************************************************************

c_BranchAndBoundConstraintSeparation

***************************************************************************/

inline string c_BranchAndBoundConstraintSeparation::TextInBranching() const
{
	stringstream ss;
	if (_rmp_constraints.size() > 0)
	{
		map<string, int> cut_names;
		for (auto con : _rmp_constraints)
			cut_names[con->TextInBranching()]++;
		bool first = true;
		for (auto elem : cut_names)
		{
			ss << (first ? "" : "+") << elem.first;
			first = false;
		}
	}
	else
		ss << "cut " << "Unknown";
	return ss.str();
}

/***************************************************************************

c_StrongBranchingCandidates

***************************************************************************/

c_StrongBranchingCandidates::c_StrongBranchingCandidates(c_Controller* controller)
	: c_BranchAndBoundConstraint(controller, list<c_Constraint*>() )
{}

c_StrongBranchingCandidates::~c_StrongBranchingCandidates() 
{
	// TODO: clean memory
}

void c_StrongBranchingCandidates::AddCandidate(const list<list< c_BranchAndBoundConstraint*> >& candidate)
{
	l_candidates.push_back(candidate);
}

/***************************************************************************

c_YpsMinusColumn

***************************************************************************/

// column for surplus in stabilized constraint
c_YpsMinusColumn::c_YpsMinusColumn( c_Controller* controller, c_Constraint* my_constraint )
	:   c_Column( controller ),
	constraint( my_constraint )
{}

int c_YpsMinusColumn::GetCPXColumn( double& cost, int* ind, double* val, double& lb, double& ub, char* name )
{
	cost   = -constraint->DeltaMinus();
	ind[0] = constraint->Index();
	val[0] = -1.0;
	lb = 0.0;
	ub = constraint->EpsMinus();
	name = NULL;
	return 1;
}

double c_YpsMinusColumn::GetCPXObj() {
	return -constraint->DeltaMinus();
}

double c_YpsMinusColumn::DefaultLowerBound()
{
	return 0.0;
}

double c_YpsMinusColumn::DefaultUpperBound()
{
	return constraint->EpsMinus();
}

void c_YpsMinusColumn::OutputInOStream(ostream& s) const
{
	
	s << "Yps-Minus-Col with value " << constraint->DeltaMinus();
}


/***************************************************************************

c_YpsPlusColumn

***************************************************************************/

// column for slack in stabilized constraint
c_YpsPlusColumn::c_YpsPlusColumn( c_Controller* controller, c_Constraint* my_constraint )
	:   c_Column( controller ),
	constraint( my_constraint )
{}

int c_YpsPlusColumn::GetCPXColumn( double& cost, int* ind, double* val, double& lb, double& ub, char* name )
{
	cost    = constraint->DeltaPlus();
	ind[0] = constraint->Index();
	val[0] = 1.0;
	lb = 0.0;
	ub = constraint->EpsPlus();
	name = NULL;
	return 1;
}

double c_YpsPlusColumn::GetCPXObj() {
	return constraint->DeltaPlus();
}

double c_YpsPlusColumn::DefaultLowerBound()
{
	return 0.0;
}

double c_YpsPlusColumn::DefaultUpperBound()
{
	return constraint->EpsPlus();
}

void c_YpsPlusColumn::OutputInOStream(ostream& s) const
{
	s << "Yps-Plus-Col " <<  " with value "<< constraint->DeltaPlus()  ;
}


/***************************************************************************

c_SeparationProblem

***************************************************************************/

c_SeparationProblem::c_SeparationProblem(c_Controller* controller )
	: p_controller( controller )
{
}

void c_SeparationProblem::OutputInOStream( ostream& s ) const
{
	s << "Separation problem (type undefined)";
}

/***************************************************************************

c_ReducedCostsFixing

***************************************************************************/

c_ReducedCostsFixing::c_ReducedCostsFixing( c_Controller* controller )
	: p_controller( controller )
{
}

void c_ReducedCostsFixing::OutputInOStream( ostream& s ) const
{
	s << "reduced costs fixing";
}


/***************************************************************************
c_Solution

***************************************************************************/

c_Solution::c_Solution( c_Controller* ctlr, c_RMP*, c_DualVariables* )
:	p_controller( ctlr ),
	obj(numeric_limits<double>::max())
{}

c_Solution::~c_Solution()
{
}

void c_Solution::OutputInOStream( ostream& s ) const
{
	s << "Solution (undef.)\n";
}

/***************************************************************************

c_PricingProblemHierarchy

***************************************************************************/

c_PricingProblemHierarchy::c_PricingProblemHierarchy( c_Controller* controller, int max_num_failures, int min_num_cols )
:	p_controller( controller ),
	i_pricing_start_level(0),
	i_max_num_failures( max_num_failures ),
	i_min_num_columns( min_num_cols ),
	i_num_failures(0),
	d_best_reduced_costs(0.0)
{}

void c_PricingProblemHierarchy::Add( c_PricingProblem* pricer )
{
	if ( !pricer )
		return;
	if ( !v_hierarchy.empty() && pricer->RHSofConvexityConstraint() != v_hierarchy[0]->RHSofConvexityConstraint() )
	{
		cout << "All pricing problems in hierarchy must have the same return value for RHSofConvexityConstraint()." << endl;
		cout << "Serious error. Stopping." << endl;
		throw;
	}
	v_hierarchy.push_back( pricer );
	p_controller->IncreasePricingStatisticsVectors();
	v_num_failures.push_back(0);
	pricer->p_my_hierarchy = this;
}

c_PricingProblemHierarchy::~c_PricingProblemHierarchy()
{
	for ( int i=0; i<(int)v_hierarchy.size(); i++ )
		delete v_hierarchy[i];
}

void c_PricingProblemHierarchy::Update( c_BranchAndBoundNode* node )
{
	for ( int i=0; i<(int)v_hierarchy.size(); i++ ){
		v_hierarchy[i]->Update(node);
		v_num_failures[i] = 0;
	}
	i_pricing_start_level = 0;
	//i_num_failures = 0;
}

void c_PricingProblemHierarchy::Update( c_DualVariables* dual )
{
	// default is: do nothing, pp->update(dual) is called directly before pricing
	//
	// This method might be used to prepare some information that is jointly used
	// by all pricers in the hierarchy.
}

int c_PricingProblemHierarchy::Go( c_DualVariables* dual, list<c_ColumnPricePair>& cols_and_prices, int NumPricersUsed )
{
	int num_generated_cols = 0;
	d_best_reduced_costs = -1000*Controller()->LocalInfty();

	int counter = 0;
	for ( int pricing_level=i_pricing_start_level; pricing_level<(int)v_hierarchy.size() && counter<NumPricersUsed ; pricing_level++,counter++ )
	{
		if( p_controller->TimeOut() )
			return 0;
		// call pricer
		c_TimeInfo timer_pricer_level;
		timer_pricer_level.Start();
		c_PricingProblem* pricer = v_hierarchy[pricing_level];
		pricer->Update(dual);
		double value = 0.0;
		int cols_before = (int) cols_and_prices.size();
		if ( p_controller->InfoLevel() >= 3 )
			p_controller->Info3() << "[" << pricing_level << "] ";
		pricer->Go( dual, cols_and_prices, value );
		num_generated_cols += ((int)cols_and_prices.size() - cols_before);
		timer_pricer_level.Stop();
		p_controller->SetInfoTimePricingLevel(pricing_level, timer_pricer_level.Seconds());
		p_controller->IncreaseInfoCallPricingLevel(pricing_level);

		//cout << p_controller->InfoTimePricingLevel(pricing_level) << endl;

		if(((int)cols_and_prices.size() - cols_before)>0)
			p_controller->IncreaseInfoSuccessPricingLevel(pricing_level);
		// update d_best_reduced_costs
		if ( pricing_level == (int)v_hierarchy.size()-1 )
		{
			d_best_reduced_costs = value;	
			if ( num_generated_cols == 0 && !p_controller->TimeOut() )
				d_best_reduced_costs = 0.0;
		}
		// sufficiently many cols generated?
		v_num_failures[pricing_level] = (num_generated_cols == 0) ? v_num_failures[pricing_level]+1 : 0;
		if ( num_generated_cols >= i_min_num_columns)
			break;
	}
	// update failures and start level
	for( int i = 0; i < (int)v_hierarchy.size()-1; i++){
		if( v_num_failures[i] >= i_max_num_failures ){
			i_pricing_start_level = i+1;
		}
	}
	return num_generated_cols;
}

double c_PricingProblemHierarchy::RHSofConvexityConstraint() const
{
	return v_hierarchy[0]->RHSofConvexityConstraint();
}

/***************************************************************************

class c_IndexInfo

***************************************************************************/

c_IndexInfo::c_IndexInfo( int first_index )
	: act_index( first_index ),
	indices()
{}

c_IndexInfo::~c_IndexInfo( )
{}

int c_IndexInfo::NewIndex(int i1, int i2, int i3, int i4)
{
	array<int, 4> key = { i1,i2,i3,i4 };
	indices[key] = act_index;
	return act_index++;
}

int c_IndexInfo::Index(int i1, int i2, int i3, int i4) const
{
	array<int, 4> key = { i1,i2,i3,i4 };
	unordered_map<array<int, 4>, int>::const_iterator i = indices.find(key);
	if (i != indices.end())
		return (*i).second;
	else
	{
		cout << "throw - c_IndexInfo::Index -> index too large" << endl;
		throw;
	}
}

bool c_IndexInfo::IndexExists( int i1, int i2, int i3, int i4 ) const
{
	array<int, 4> key = { i1,i2,i3,i4 };
	unordered_map<array<int, 4>, int>::const_iterator i = indices.find(key);
	if (i != indices.end())
		return true;
	else
		return false;
}

void c_IndexInfo::DeleteIndex(int index_to_del)
{
	unordered_map<array<int, 4>, int>::const_iterator i;
	for (i = indices.begin(); i != indices.end(); )
	{
		if ((*i).second == index_to_del)
			i = indices.erase(i);
		else
		{
			if ((*i).second > index_to_del)
				indices[i->first]--;
			++i;
		}
	}
	act_index--;
}

/***************************************************************************

class c_ConvergencyInfo

***************************************************************************/

c_ConvergencyInfo::c_ConvergencyInfo()
{
}

c_ConvergencyInfo::~c_ConvergencyInfo()
{
	for ( auto iter : l_values )
		delete iter;
	l_values.clear();
}

void c_ConvergencyInfo::AddPoint( double time_ms, double lb, double ub )
{
	// add new point
	if ( !l_values.empty() )
	{
		c_ConvergencyInfoPoint* pred = l_values.back();
		c_ConvergencyInfoPoint* point = new c_ConvergencyInfoPoint();
		point->_time_ms = time_ms - 0.001;
		point->_lb = pred->_lb;
		point->_ub = pred->_ub;
		l_values.push_back( point );
	}
	c_ConvergencyInfoPoint* point = new c_ConvergencyInfoPoint();
	point->_time_ms = time_ms;
	point->_lb	  = lb;
	point->_ub      = ub;
	l_values.push_back( point );
}

void c_ConvergencyInfo::WriteFile( const char* filename )
{
	std::ofstream out( filename );
	list<c_ConvergencyInfoPoint*>::iterator iter;
	for ( auto iter : l_values)
	{
		out << iter->_time_ms << " ";
		out << iter->_lb << " ";
		out << iter->_ub << endl;
	}
}

double c_ConvergencyInfo::BestLB() const
{
	if ( l_values.empty() )
		return numeric_limits<double>::infinity();
	else
		return l_values.back()->_lb;
}

double c_ConvergencyInfo::BestUB() const
{
	if ( l_values.empty() )
		return -numeric_limits<double>::infinity();
	else
		return l_values.back()->_ub;
}


/***************************************************************************

class c_FindKthElement

***************************************************************************/


c_FindKthElement::c_FindKthElement( int _max_size )
	: max_size( _max_size )
{
	elem = new double[ max_size ];
}

c_FindKthElement::~c_FindKthElement()
{
	delete[] elem;
}

void c_FindKthElement::SetElement( int index, double value )
{
	assert( (index>=0) && (index<max_size) );
	elem[index] = value;
}

int compare_less( const void *arg1, const void *arg2 )
{
	/* Compare all of both strings: */
	double* val1 = (double*) arg1;
	double* val2 = (double*) arg2;
	if ( *val1 < *val2 )
		return -1;
	if ( *val1 > *val2 )
		return 1;
	return 0;
}

int compare_greater( const void *arg1, const void *arg2 )
{
	/* Compare all of both strings: */
	double* val1 = (double*) arg1;
	double* val2 = (double*) arg2;
	if ( *val1 > *val2 )
		return -1;
	if ( *val1 < *val2 )
		return 1;
	return 0;
}

double c_FindKthElement::FindKthElementOfN( int k, int n, order ord )
{
	assert( (k>=0) && (k<n) && (n<=max_size) );

	// quick and dirty...
	if ( ord == increasing_order )
		qsort( elem, n, sizeof(double), compare_less );
	else
		qsort( elem, n, sizeof(double), compare_greater );
	return elem[k];
}


/***************************************************************************

globale Funktionen

***************************************************************************/

bool HasLargerFractionalPart( double x, double y )
{
	return ( x-floor(x) > y-floor(y) );
}

bool HasSmallerFractionalPart( double x, double y )
{
	return ( x-floor(x) < y-floor(y) );
}

bool IsMoreFractional( double x, double y )
{
	return ( FractionalValue(x) > FractionalValue( y ) );
}

bool IsFractional( double x )
{
	return ( (( ceil(x)-x > x-floor(x) ) ? x-floor(x) : ceil(x)-x) > CG_EPS );
}

double FractionalValue( double x )
{
	return ( ceil(x)-x > x-floor(x) ) ? x-floor(x) : ceil(x)-x;
}

double RelativeDifference( double x_100_percent, double value )
{
	if ( value > x_100_percent )
	{
		double merk = x_100_percent;
		x_100_percent = value;
		value = merk;
	}
	double erg = ( x_100_percent - value ) / x_100_percent;
	return erg;
}

/**************************************************************************
 Operatoren
// *************************************************************************/

ostream& operator<<( ostream& s, const c_Controller& ctlr )
{
	ctlr.OutputInOStream( s );
	return s;
}

ostream& operator<<( ostream& s, const c_Column& col )
{
	col.OutputInOStream( s );
	return s;
}

ostream& operator<<( ostream& s, const c_DualVariables& dual )
{
	dual.OutputInOStream( s );
	return s;
}

ostream& operator<<( ostream& s, const c_RMP& rmp )
{
	rmp.OutputInOStream( s );
	return s;
}

ostream& operator<<( ostream& s, const c_BranchAndBoundNode& node )
{
	node.OutputInOStream( s );
	return s;
}

ostream& operator<<( ostream& s, const c_BranchAndBoundConstraint& constraint )
{
	constraint.OutputInOStream( s );
	return s;
}

ostream& operator<<( ostream& s, const c_PricingProblem& pp )
{
	pp.OutputInOStream( s );
	return s;
}

ostream& operator<<( ostream& s, const c_Solution& sol )
{
	sol.OutputInOStream( s );
	return s;
}

ostream& operator<<( ostream& s, const c_Constraint& con )
{
	con.OutputInOStream( s );
	return s;
}


} // end of namespace CGBase 
