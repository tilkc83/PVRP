// Implementation of an example C++ class library for CPLEX

#include "cplexcpp.h"
#include <sstream>
#include <ostream>
#include <set>

namespace CGBase
{

	CPLEXEnvironment::CPLEXEnvironment(int ldwid, char *licenvstring)
	{
		int status = 0;
		env_ = CPXopenCPLEX(&status); // CPLEX 12.9 --> no serialnum required
		if (env_ == NULL) throw CPLEXException(env_, status, (char*)"CPXopenCPLEX");
	}

	void
		CPLEXEnvironment::terminate()
	{
		if (env_ != NULL)
		{
			int status = CPXcloseCPLEX(&env_);
			if (status)	throw CPLEXException(env_, status, (char*)"CPXcloseCPLEX");
			env_ = NULL;
		}
	}

	CPLEXEnvironment::~CPLEXEnvironment()
	{
		terminate();
	}

	void
		CPLEXEnvironment::setparam(int paramnum, int value)
	{
		int status = CPXsetintparam(env_, paramnum, value);
		if (status) throw CPLEXException(env_, status, (char*)"CPXsetintparam");
	}

	void
		CPLEXEnvironment::setparam(int paramnum, double value)
	{
		int status = CPXsetdblparam(env_, paramnum, value);
		if (status) throw CPLEXException(env_, status, (char*)"CPXsetdblparam");
	}

	void
		CPLEXEnvironment::setdefaults()
	{
		int status = CPXsetdefaults(env_);
		if (status) throw CPLEXException(env_, status, (char*)"CPXsetdefaults");
	}

	void
		CPLEXEnvironment::getParam(int paramnum, int& rvalue) const
	{
		int  value;
		int status = CPXgetintparam(env_, paramnum, &value);

		if (status) throw CPLEXException(env_, status, (char*)"CPXgetintparam");

		rvalue = value;
	}

	void
		CPLEXEnvironment::getParam(int paramnum, double& rvalue) const
	{
		double value;
		int status = CPXgetdblparam(env_, paramnum, &value);

		if (status) throw CPLEXException(env_, status, (char*)"CPXsetdblparam");

		rvalue = value;
	}

	typedef int(*callback_func_p)(CPXENVptr, void*, int, void*);

	CPLEXException::CPLEXException(CPXENVptr env, int status, char *funcname)
		: env_(env), status_(status), funcname_(funcname)
	{}

	const char *
		CPLEXException::what() const
	{
		return (CPXgeterrorstring(NULL, status_, (char *)buffer));
	}

	const char *
		CPLEXException::funcname() const
	{
		return (funcname_);
	}

	int
		CPLEXException::errcode() const
	{
		return (status_);
	}

	CPLEXProblem::CPLEXProblem(CPLEXEnvironment& renv, const char *probname)
	{
		int status = 0;
		env_ = renv.env_;
		lp_ = CPXcreateprob(env_, &status, probname);
		if (status != 0) throw CPLEXException(renv.env_, status, (char*)"CPXcreateprob");
	}

	CPLEXProblem::~CPLEXProblem()
	{
		CPXfreeprob(env_, &lp_);
	}

	void
		CPLEXProblem::read(const char *fname, const char *filetype)
	{
		int status = CPXreadcopyprob(env_, lp_, (char *)fname, (char *)filetype);
		if (status) throw CPLEXException(env_, status, (char*)"CPXreadcopyprob");
	}

	void
		CPLEXProblem::write(const char *fname, const char *filetype) const
	{
		int status = CPXwriteprob(env_, lp_, (char *)fname, (char *)filetype);
		if (status) throw CPLEXException(env_, status, (char*)"CPXwriteprob");
	}

	void
		CPLEXProblem::copylpdata(int numcols, int numrows,
			ObjSense objsen, double obj[],
			double rhs[], char sense[],
			int matbeg[], int matcnt[],
			int matind[], double matval[],
			double lb[], double ub[],
			double rngval[], char *colname[],
			char *rowname[])
	{
#ifdef _DEBUG	 
#if CPX_VERSION < 900 
		int st = CPXcheckcopylpwnames(env_, lp_, numcols, numrows, (int)objsen,
			obj, rhs, sense, matbeg, matcnt,
			matind, matval, lb, ub, rngval,
			colname, rowname);
		if (st >= 2)  throw CPLEXException(env_, st, "CPXcopylpwnames");
#endif
#endif
		int status = CPXcopylpwnames(env_, lp_, numcols, numrows, (int)objsen,
			obj, rhs, sense, matbeg, matcnt,
			matind, matval, lb, ub, rngval,
			colname, rowname);
		if (status) throw CPLEXException(env_, status, (char*)"CPXcopylpwnames");
	}


	void
		CPLEXProblem::chgprobtype(CPLEXProblem::ProbType cpx_prob)
	{
		int status = CPXchgprobtype(env_, lp_, cpx_prob);
		if (status) throw CPLEXException(env_, status, (char*)"CPXchgprobtype");
	}

	void
		CPLEXProblem::chgctype(int cnt, int indices[], char ctype[])
	{
		int status = CPXchgctype(env_, lp_, cnt, indices, ctype);
		if (status) throw CPLEXException(env_, status, (char*)"CPXchgctype");
	}


	void
		CPLEXProblem::newrows(int rcnt, double rhs[], char sense[],
			double rngval[], char *rowname[])
	{
		int status = CPXnewrows(env_, lp_, rcnt, rhs, sense, rngval, rowname);
		if (status) throw CPLEXException(env_, status, (char*)"CPXnewrows");
	}

	void
		CPLEXProblem::newcols(int ccnt, double obj[], double lb[],
			double ub[], char ctype[], char *colname[])
	{
		int status = CPXnewcols(env_, lp_, ccnt, obj, lb, ub, ctype, colname);
		if (status) throw CPLEXException(env_, status, (char*)"CPXnewcols");
	}

	void
		CPLEXProblem::addrows(int ccnt, int rcnt, int nzcnt,
			double rhs[], char sense[], int rmatbeg[],
			int rmatind[], double rmatval[],
			char *colname[], char *rowname[])
	{
#ifdef _DEBUG
#if CPX_VERSION < 900 
		int  st = CPXcheckaddrows(env_, lp_, ccnt, rcnt, nzcnt, rhs, sense,
			rmatbeg, rmatind, rmatval, colname, rowname);
		if (st)  throw CPLEXException(env_, st, "CPXaddrows");
#endif
#endif

		int status = CPXaddrows(env_, lp_, ccnt, rcnt, nzcnt, rhs, sense,
			rmatbeg, rmatind, rmatval, colname, rowname);
		if (status) throw CPLEXException(env_, status, (char*)"CPXaddrows");
	}

	void
		CPLEXProblem::addcols(int ccnt, int nzcnt, double obj[],
			int cmatbeg[], int cmatind[],
			double cmatval[], double lb[], double ub[],
			char *colname[])
	{
#ifdef _DEBUG
#if CPX_VERSION < 900 
		int  st = CPXcheckaddcols(env_, lp_, ccnt, nzcnt, obj,
			cmatbeg, cmatind, cmatval, lb, ub,
			colname);
		if (st)  throw CPLEXException(env_, st, "CPXaddcols");
#endif
#endif
		int status = CPXaddcols(env_, lp_, ccnt, nzcnt, obj,
			cmatbeg, cmatind, cmatval, lb, ub,
			colname);
		if (status) throw CPLEXException(env_, status, (char*)"CPXaddcols");
	}

	// NEW!!!
	void
		CPLEXProblem::chgbds(int cnt, int indices[], char lu[], double bd[])
	{
#ifdef _DEBUG
#if CPX_VERSION < 900 
		int st = CPXcheckvals(env_, lp_, cnt, NULL, indices, bd);
		if (st)
			throw CPLEXException(env_, st, "CPXchgbds");
#endif
#endif
		int status = CPXchgbds(env_, lp_, cnt, indices, lu, bd);
		if (status) throw CPLEXException(env_, status, (char*)"CPXchgbds");
	}

	void
		CPLEXProblem::chgobj(int cnt, int indices[], double values[])
	{
#ifdef _DEBUG
#if CPX_VERSION < 900 
		int st = CPXcheckvals(env_, lp_, cnt, NULL, indices, values);
		if (st)
			throw CPLEXException(env_, st, "CPXchgobj");
#endif
#endif
		int status = CPXchgobj(env_, lp_, cnt, indices, values);
		if (status) throw CPLEXException(env_, status, (char*)"CPXchgobj");
	}

	void
		CPLEXProblem::chgrhs(int cnt, int indices[], double values[])
	{
#ifdef _DEBUG
#if CPX_VERSION < 900 
		int st = CPXcheckvals(env_, lp_, cnt, indices, NULL, values);
		if (st)
			throw CPLEXException(env_, st, "CPXchgrhs");
#endif
#endif
		int status = CPXchgrhs(env_, lp_, cnt, indices, values);
		if (status) throw CPLEXException(env_, status, (char*)"CPXchgrhs");
	}

	void
		CPLEXProblem::chgobjsen(ObjSense objsen)
	{
		CPXchgobjsen(env_, lp_, (int)objsen);
	}

	void
		CPLEXProblem::chgcoef(int i, int j, double val)
	{
		int status = CPXchgcoef(env_, lp_, i, j, val);
		if (status) throw CPLEXException(env_, status, (char*)"CPXchgcoef");
	}

	void
		CPLEXProblem::chgcoeflist(int numcoefs, int rowlist[], int collist[],
			double vallist[])
	{
#ifdef _DEBUG
#if CPX_VERSION < 900 
		int  st = CPXcheckchgcoeflist(env_, lp_, numcoefs, rowlist,
			collist, vallist);

		if (st)  throw CPLEXException(env_, st, "CPXchgcoeflist");
#endif
#endif

		int status = CPXchgcoeflist(env_, lp_, numcoefs, rowlist, collist, vallist);
		if (status) throw CPLEXException(env_, status, (char*)"CPXchgcoeflist");
	}


	void
		CPLEXProblem::primopt()
	{
		int status = CPXprimopt(env_, lp_);
		if (status) throw CPLEXException(env_, status, (char*)"CPXprimopt");
	}

	void
		CPLEXProblem::dualopt()
	{
		int status = CPXdualopt(env_, lp_);
		if (status) throw CPLEXException(env_, status, (char*)"CPXdualopt");
	}

	void
		CPLEXProblem::baropt()
	{
		int status = CPXbaropt(env_, lp_);
		if (status) throw CPLEXException(env_, status, (char*)"CPXbaropt");
	}

	void
		CPLEXProblem::mipopt()
	{
		int status = CPXmipopt(env_, lp_);
		if (status)  throw CPLEXException(env_, status, (char*)"CPXmipopt");
	}

	void
		CPLEXProblem::hybbaropt(OptMethod method)
	{
		int status = CPXhybbaropt(env_, lp_, (char)method);
		if (status) throw CPLEXException(env_, status, (char*)"CPXhybbaropt");
	}

	void
		CPLEXProblem::hybnetopt(OptMethod method)
	{
		int status = CPXhybnetopt(env_, lp_, (char)method);
		if (status) throw CPLEXException(env_, status, (char*)"CPXhybnetopt");
	}

	int
		CPLEXProblem::stat() const
	{
		return (CPXgetstat(env_, lp_));
	}

	int
		CPLEXProblem::getitcnt()
	{
		int status = CPXgetitcnt(env_, lp_);
		if (status == -1) throw CPLEXException(env_, status, (char*)"CPXgetitcnt");
		return status;
	}

	double
		CPLEXProblem::objval() const
	{
		double objval;
		int status = CPXgetobjval(env_, lp_, &objval);
		if (status) throw CPLEXException(env_, status, (char*)"CPXgetobjval");
		return (objval);
	}

	CPLEXProblem::ProbType CPLEXProblem::probtype(void) const
	{
		return ((CPLEXProblem::ProbType) CPXgetprobtype(env_, lp_));
	}

	double
		CPLEXProblem::mipobjval() const
	{
		double objval;
		int status = CPXgetmipobjval(env_, lp_, &objval);
		if (status) throw CPLEXException(env_, status, (char*)"CPXgetmipobjval");
		return (objval);
	}

	void
		CPLEXProblem::_getX(double x[], int begin, int end) const
	{
		if (probtype() == CPLEXProblem::MIP)
			getmipX(x, begin, end);
		else
			getX(x, begin, end);
	}

	void
		CPLEXProblem::getX(double x[], int begin, int end) const
	{
		int  theend = (end < 0) ? numcols() - 1 : end;
		int status = CPXgetx(env_, lp_, x, begin, theend);
		if (status) throw CPLEXException(env_, status, (char*)"CPXgetx");
	}

	void
		CPLEXProblem::getmipX(double x[], int begin, int end) const
	{
		int  theend = (end < 0) ? numcols() - 1 : end;
		int status = CPXgetmipx(env_, lp_, x, begin, theend);
		if (status) throw CPLEXException(env_, status, (char*)"CPXgetmipx");
	}

	void
		CPLEXProblem::getobj(double val[], int begin, int end) const
	{
		int theend = (end < 0) ? numcols() - 1 : end;
		int status = CPXgetobj(env_, lp_, val, begin, theend);
		if (status) throw CPLEXException(env_, status, (char*)"CPXgetobj");
	}

	void
		CPLEXProblem::getSlack(double slack[], int begin, int end) const
	{
		int theend = (end < 0) ? numrows() - 1 : end;
		int status = CPXgetslack(env_, lp_, slack, begin, theend);
		if (status) throw CPLEXException(env_, status, (char*)"CPXgetslack");
	}

	void
		CPLEXProblem::getPi(double pi[], int begin, int end) const
	{
		int theend = (end < 0) ? numrows() - 1 : end;
		int status = CPXgetpi(env_, lp_, pi, begin, theend);
		if (status) throw CPLEXException(env_, status, (char*)"CPXgetpi");
	}

	void
		CPLEXProblem::getDj(double dj[], int begin, int end) const
	{
		int theend = (end < 0) ? numcols() - 1 : end;
		int status = CPXgetdj(env_, lp_, dj, begin, theend);
		if (status) throw CPLEXException(env_, status, (char*)"CPXgetdj");
	}

	void
		CPLEXProblem::getBase(int cstat[], int rstat[]) const
	{
		int status = CPXgetbase(env_, lp_, cstat, rstat);
		if (status) throw CPLEXException(env_, status, (char*)"CPXgetbase");
	}

	void
		CPLEXProblem::getRHS(double rhs[], int begin, int end)
	{
		int theend = (end < 0) ? numrows() - 1 : end;
		int status = CPXgetrhs(env_, lp_, rhs, begin, theend);
		if (status)  throw CPLEXException(env_, status, (char*)"CPXgetrhs");
	}

	int
		CPLEXProblem::numcols() const
	{
		return (CPXgetnumcols(env_, lp_));
	}

	int
		CPLEXProblem::numrows() const
	{
		return (CPXgetnumrows(env_, lp_));
	}

	void
		CPLEXProblem::delcols(int first, int last)
	{
		int status = CPXdelcols(env_, lp_, first, last);
		if (status) throw CPLEXException(env_, status, (char*)"CPXdelcols");
	}

	void
		CPLEXProblem::delrows(int first, int last)
	{
		int status = CPXdelrows(env_, lp_, first, last);
		if (status) throw CPLEXException(env_, status, (char*)"CPXdelrows");
	}

	void
		CPLEXProblem::copyStart(int cstat[], int rstat[], double cprim[], double rprim[], double cdual[], double rdual[])
	{
		int status = CPXcopystart(env_, lp_, cstat, rstat, cprim, rprim, cdual, rdual);
		if (status) throw CPLEXException(env_, status, (char*)"CPXcopystart");
	}

#if(CPX_VERSION < 12060000)
	void
		CPLEXProblem::copymipstart(int cnt, int* indices, double* value)
	{
		int status = CPXcopymipstart(env_, lp_, cnt, indices, value);
		if (status) throw CPLEXException(env_, status, "CPXcopymipstart");
	}
#endif

#if(CPX_VERSION >= 12060000)
	void
		CPLEXProblem::copymipstart(int cnt, int* indices, double* value)
	{
		int mcnt = 1;
		int nzcnt = cnt;
		int* beg = new int[1]; beg[0] = 0;
		int* varindices = indices;
		//double* value = value;
		int* effortlevel = NULL;
		char** mipstartname = NULL;

		int status = CPXaddmipstarts(env_, lp_, mcnt, nzcnt, beg, varindices, value, effortlevel, mipstartname);
		if (status) throw CPLEXException(env_, status, (char*)"CPXcopymipstart");
		delete[] beg;
	}
#endif

	void
		CPLEXProblem::copyctype(char ctype[])
	{
		int status = CPXcopyctype(env_, lp_, ctype);
		if (status) throw CPLEXException(env_, status, (char*)"CPXcopyctype");
	}

	// Zum Testen der LP-Matrix eingefuehrt
	// Rueckgabewert ist ein String, der alle Fehler enthaelt
	// method is like CPXcopylp()
	// except that numnzs (number of non-zeros is third parameter)
	std::string CPLEXProblem::CheckMatrix(int numcols, int numrows, int numnzs,
		ObjSense objsen, double obj[],
		double rhs[], char sense[], char ctype[],
		int matbeg[], int matcnt[],
		int matind[], double matval[],
		double lb[], double ub[],
		double rngval[], char *colname[],
		char *rowname[],
		const double d_minvalue,
		const double d_maxvalue)
	{
		std::ostringstream os;

		if (numcols < 1)
			os << "Anzahl Variablen: " << numcols << std::endl;
		if (numrows < 1)
			os << "Anzahl Restriktionen " << numrows << std::endl;
		if (obj == NULL)
			os << "obj ist NULL-Pointer" << std::endl;
		if (rhs == NULL)
			os << "rhs ist NULL-Pointer" << std::endl;
		if (sense == NULL)
			os << "sense ist NULL-Pointer" << std::endl;
		if (matbeg == NULL)
			os << "matbeg ist NULL-Pointer" << std::endl;
		if (matcnt == NULL)
			os << "matcnt ist NULL-Pointer" << std::endl;
		if (matind == NULL)
			os << "matind ist NULL-Pointer" << std::endl;
		if (matval == NULL)
			os << "matval ist NULL-Pointer" << std::endl;
		if (lb == NULL)
			os << "lb ist NULL-Pointer" << std::endl;
		if (ub == NULL)
			os << "ub ist NULL-Pointer" << std::endl;

		for (int i = 0; i < numrows; ++i)
		{
			if ((rhs[i] < d_minvalue) || (rhs[i] > d_maxvalue))
				os << "Warnung: rhs[" << i << "] = " << rhs[i] << std::endl;
			if ((sense[i] != 'L') && (sense[i] != 'E') && (sense[i] != 'G') && (sense[i] != 'R'))
				os << "Warnung: sense[" << i << "] = " << sense[i] << std::endl;
		}

		for (int i = 0; i < numcols; ++i)
		{
			if ((obj[i] < d_minvalue) || (obj[i] > d_maxvalue))
				os << "Warnung: obj[" << i << "] = " << obj[i] << std::endl;
			if ((lb[i] < d_minvalue) || (lb[i] > d_maxvalue))
				os << "Warnung: lb[" << i << "] = " << lb[i] << std::endl;
			if ((ub[i] < d_minvalue) || (ub[i] > d_maxvalue))
				os << "Warnung: ub[" << i << "] = " << ub[i] << std::endl;
			if (lb[i] > ub[i])
				os << "Fehler: lb[" << i << "] = " << lb[i] << " > ub[" << i << "] = " << ub[i] << std::endl;
			if (ctype != NULL &&
				ctype[i] != CPX_CONTINUOUS &&
				ctype[i] != CPX_BINARY     &&
				ctype[i] != CPX_INTEGER    // &&
										   //				 ctype[i] != CPX_SEMICONT   && // erst ab CPLEX 7.1
										   //				 ctype[i] != CPX_SEMIINT      
				)
				os << "Fehler: ctype[" << i << "] = " << ctype[i] << std::endl;
			if (matbeg[i] < 0)
				os << "Fehler: matbeg[" << i << "] = " << matbeg[i] << std::endl;
			if (matcnt[i] <= 0)
				os << "Fehler: matcnt[" << i << "] = " << matcnt[i] << std::endl;
			if (matbeg[i] + matcnt[i] > numnzs)
			{
				os << "Fehler: matbeg[" << i << "] + matcnt[" << i << "] > numnzs" << std::endl;
				os << "matbeg[" << i << "] = " << matbeg[i] << ", matcnt[" << i << "] = " << matcnt[i] << ", numnzs = " << numnzs << std::endl;
			}
			if ((i > 0) && (matbeg[i] != matbeg[i - 1] + matcnt[i - 1]))
				os << "Fehler: matbeg[" << i << "] = " << matbeg[i] << "!= matbeg[" << i - 1 << "] = " << matbeg[i - 1] << " + macnt[" << i - 1 << "] = " << matcnt[i - 1] << std::endl;
			std::set< int > col_rows;
			for (int k = matbeg[i]; k < matbeg[i] + matcnt[i]; ++k)
			{
				if (k < numnzs)
				{
					if (matind[k] < 0 || matind[k] >= numrows)
						os << "Fehler: Spalte " << i << " Zeile: " << matind[k] << " ausserhalb des zulaessigen Bereichs[0," << numrows - 1 << "]" << std::endl;
					if (col_rows.find(matind[k]) != col_rows.end())
						os << "Fehler: Spalte " << i << " Zeile: " << matind[k] << " mehrfach enthalten" << std::endl;
					col_rows.insert(matind[k]);
				}
				else
					os << "Fehler: Spalte " << i << " Eintrag: " << k << " ausserhalb des zulaessigen Bereichs[0," << numnzs - 1 << "]" << std::endl;
			}
		}

		for (int i = 0; i < numnzs; ++i)
		{
			if ((matval[i] < d_minvalue) || (matval[i] > d_maxvalue))
				os << "Warnung: matval[" << i << "] = " << matval[i] << std::endl;
		}

		return os.str();
	}

	// Gibt ein LP als Excel-Matrix im csv-Format aus
	// method is like CPXcopylp(), 
	std::string CPLEXProblem::DumpExcelMatrix(int numcols, int numrows,
		ObjSense objsen, double obj[],
		double rhs[], char sense[],
		int matbeg[], int matcnt[],
		int matind[], double matval[],
		double lb[], double ub[],
		double rngval[], char *colname[],
		char *rowname[])
	{
		std::ostringstream os;

		if (objsen == CPX_MAX)
			os << "Maximierung;";
		else
			os << "Minimierung;";
		os << "#Spalten;" << numcols << ";";
		os << "#Zeilen;" << numrows;
		os << std::endl;

		os << ";";
		for (int j = 0; j < numcols; ++j)
		{
			if (colname != NULL)
				os << colname[j] << ";";
			else
				os << j << ";";
		}
		os << std::endl;
		os << ";";
		for (int j = 0; j < numcols; ++j)
			os << obj[j] << ";";
		os << std::endl;
		for (int i = 0; i < numrows; ++i)
		{
			if (rowname != NULL)
				os << rowname[i] << ";";
			else
				os << i << ";";
			for (int j = 0; j < numcols; ++j)
			{
				int k = matbeg[j];
				bool found = false;
				while (k < matbeg[j] + matcnt[j])
				{
					if (matind[k] == i)
						os << matval[k];
					++k;
				}
				os << ";";
			}
			if (sense[i] == 'L')
				os << "<=;";
			else if (sense[i] == 'E')
				os << "=;";
			else if (sense[i] == 'G')
				os << ">=;";
			else if (sense[i] == 'R')
				os << "R;";
			else
				os << "Error;";
			os << rhs[i];
			os << std::endl;
		}
		os << "LB;";
		for (int j = 0; j < numcols; ++j)
			os << lb[j] << ";";
		os << std::endl;
		os << "UB;";
		for (int j = 0; j < numcols; ++j)
			os << ub[j] << ";";

		return os.str();
	}

	CPLEX_NET_Problem::CPLEX_NET_Problem(CPLEXEnvironment& renv, const char *probname)
	{
		int status = 0;
		env_ = renv.env_;
		net_ = CPXNETcreateprob(env_, &status, probname);
		if (status != 0) throw CPLEXException(renv.env_, status, (char*)"CPXNETcreateprob");
	}

	void
		CPLEX_NET_Problem::terminate()
	{
		if (env_ != NULL)
		{
			int status = CPXNETfreeprob(env_, &net_);
			if (status) throw CPLEXException(env_, status, (char*)"CPXNETaddarcs");
			env_ = NULL;
		}
	}

	CPLEX_NET_Problem::~CPLEX_NET_Problem()
	{
		terminate();
	}

	void
		CPLEX_NET_Problem::addarcs(int narcs, int *fromnode, int *tonode, double *low, double *up, double *obj, char **anames)
	{
		int status = CPXNETaddarcs(env_, net_, narcs, fromnode, tonode, low, up, obj, anames);
		if (status) throw CPLEXException(env_, status, (char*)"CPXNETaddarcs");
	}

	void
		CPLEX_NET_Problem::addnodes(int nnodes, double *supply, char **nnames)
	{
		int status = CPXNETaddnodes(env_, net_, nnodes, supply, nnames);
		if (status) throw CPLEXException(env_, status, (char*)"CPXNETaddnodes");
	}

	void
		CPLEX_NET_Problem::basewrite(char *filename)
	{
		int status = CPXNETbasewrite(env_, net_, filename);
		if (status) throw CPLEXException(env_, status, (char*)"CPXNETbasewrite");
	}

	/*
	void
	CPLEX_NET_Problem::checkcopynet ( int objsen, int nnodes, double *supply, char **nnames,
	int narcs, int *fromnode, int *tonode, double *low, double *up,
	double *obj, char **anames )
	{
	int status = 0;
	status = CPXNETcheckcopynet ( env_, net_, objsen, nnodes, supply, nnames, narcs, fromnode, tonode,
	low, up, obj, anames );
	if ( status )
	throw CPLEXException (env_, status, "CPXNETcheckcopynet");
	}
	*/

	void
		CPLEX_NET_Problem::chgarcname(int cnt, int *indices, char **anames)
	{
		int status = CPXNETchgarcname(env_, net_, cnt, indices, anames);
		if (status) throw CPLEXException(env_, status, (char*)"CPXNETchgarcname");
	}

	void
		CPLEX_NET_Problem::chgarcnodes(int cnt, int *index, int *fromnode, int *tonode)
	{
		int status = CPXNETchgarcnodes(env_, net_, cnt, index, fromnode, tonode);
		if (status) throw CPLEXException(env_, status, (char*)"CPXNETchgarcnodes");
	}

	void
		CPLEX_NET_Problem::chgbds(int cnt, int *index, char *lu, double *bd)
	{
		int status = CPXNETchgbds(env_, net_, cnt, index, lu, bd);
		if (status) throw CPLEXException(env_, status, (char*)"CPXNETchgbds");
	}

	void
		CPLEX_NET_Problem::chgname(int key, int index, char *name)
	{
		int status = CPXNETchgname(env_, net_, key, index, name);
		if (status) throw CPLEXException(env_, status, (char*)"CPXNETchgname");
	}

	void
		CPLEX_NET_Problem::chgnodename(int cnt, int *indices, char **name)
	{
		int status = CPXNETchgnodename(env_, net_, cnt, indices, name);
		if (status) throw CPLEXException(env_, status, (char*)"CPXNETchgnodename");
	}

	void
		CPLEX_NET_Problem::chgobj(int cnt, int *index, double *obj)
	{
		int status = CPXNETchgobj(env_, net_, cnt, index, obj);
		if (status) throw CPLEXException(env_, status, (char*)"CPXNETchgobj");
	}

	void
		CPLEX_NET_Problem::chgobjsen(int maxormin)
	{
		int status = CPXNETchgobjsen(env_, net_, maxormin);
		if (status) throw CPLEXException(env_, status, (char*)"CPXNETchgobjsen");
	}

	void
		CPLEX_NET_Problem::chgsupply(int cnt, int *index, double *supply)
	{
		int status = CPXNETchgsupply(env_, net_, cnt, index, supply);
		if (status) throw CPLEXException(env_, status, (char*)"CPXNETchgsupply");
	}

	void
		CPLEX_NET_Problem::copybase(int *astat, int *nstat)
	{
		int status = CPXNETcopybase(env_, net_, astat, nstat);
		if (status) throw CPLEXException(env_, status, (char*)"CPXNETcopybase");
	}

	void
		CPLEX_NET_Problem::copynet(int objsen, int nnodes, double *supply, char **nnames,
			int narcs, int *fromnode, int *tonode, double *low, double *up,
			double *obj, char **anames)
	{
		int status = CPXNETcopynet(env_, net_, objsen, nnodes, supply, nnames, narcs, fromnode, tonode,
			low, up, obj, anames);
		if (status) throw CPLEXException(env_, status, (char*)"CPXNETcopynet");
	}

	void
		CPLEX_NET_Problem::delarcs(int begin, int end)
	{
		int status = CPXNETdelarcs(env_, net_, begin, end);
		if (status) throw CPLEXException(env_, status, (char*)"CPXNETdelarcs");
	}

	void
		CPLEX_NET_Problem::delnodes(int begin, int end)
	{
		int status = CPXNETdelnodes(env_, net_, begin, end);
		if (status) throw CPLEXException(env_, status, (char*)"CPXNETdelnodes");
	}

	void
		CPLEX_NET_Problem::delset(int *whichnodes, int *whicharcs)
	{
		int status = CPXNETdelset(env_, net_, whichnodes, whicharcs);
		if (status) throw CPLEXException(env_, status, (char*)"CPXNETdelnset");
	}

	void
		CPLEX_NET_Problem::extract(CPLEXProblem *lp, int *colmap, int *rowmap)
	{
		int status = CPXNETextract(env_, net_, lp->lp_, colmap, rowmap);
		if (status) throw CPLEXException(env_, status, (char*)"CPXNETextract");
	}

	void
		CPLEX_NET_Problem::getarcindex(char *lname, int *index_p)
	{
		int status = CPXNETgetarcindex(env_, net_, lname, index_p);
		if (status) throw CPLEXException(env_, status, (char*)"CPXNETgetarcindex");
	}

	void
		CPLEX_NET_Problem::getarcname(char **nnames, char *namestore, int namespc, int *surplus_p, int begin, int end)
	{
		int status = CPXNETgetarcname(env_, net_, nnames, namestore, namespc, surplus_p, begin, end);
		if (status) throw CPLEXException(env_, status, (char*)"CPXNETgetarcname");
	}

	void
		CPLEX_NET_Problem::getarcnodes(int *fromnode, int *tonode, int begin, int end)
	{
		int status = CPXNETgetarcnodes(env_, net_, fromnode, tonode, begin, end);
		if (status) throw CPLEXException(env_, status, (char*)"CPXNETgetarcnodes");
	}

	void
		CPLEX_NET_Problem::getbase(int *astat, int *nstat)
	{
		int status = CPXNETgetbase(env_, net_, astat, nstat);
		if (status) throw CPLEXException(env_, status, (char*)"CPXNETgetbase");
	}

	void
		CPLEX_NET_Problem::getdj(double *dj, int begin, int end)
	{
		int status = CPXNETgetdj(env_, net_, dj, begin, end);
		if (status) throw CPLEXException(env_, status, (char*)"CPXNETgetdj");
	}

	int
		CPLEX_NET_Problem::getitcnt()
	{
		int status = CPXNETgetitcnt(env_, net_);
		if (status == -1) throw CPLEXException(env_, status, (char*)"CPXNETgetitcnt");
		return status;
	}

	void
		CPLEX_NET_Problem::getlb(double *low, int begin, int end)
	{
		int status = CPXNETgetlb(env_, net_, low, begin, end);
		if (status) throw CPLEXException(env_, status, (char*)"CPXNETgetlb");
	}

	void
		CPLEX_NET_Problem::getnodearcs(int *arccnt_p, int *arcbeg, int *arc, int arcspace, int *surplus_p, int begin, int end)
	{
		int status = CPXNETgetnodearcs(env_, net_, arccnt_p, arcbeg, arc, arcspace, surplus_p, begin, end);
		if (status) throw CPLEXException(env_, status, (char*)"CPXNETgetnodearcs");
	}

	void
		CPLEX_NET_Problem::getnodeindex(char *lname, int *index_p)
	{
		int status = CPXNETgetnodeindex(env_, net_, lname, index_p);
		if (status) throw CPLEXException(env_, status, (char*)"CPXNETgetnodeindex");
	}

	void
		CPLEX_NET_Problem::getnodename(char **nnames, char *namestore, int namespc, int *surplus_p, int begin, int end)
	{
		int status = CPXNETgetnodename(env_, net_, nnames, namestore, namespc, surplus_p, begin, end);
		if (status) throw CPLEXException(env_, status, (char*)"CPXNETgetnodename");
	}

	int
		CPLEX_NET_Problem::getnumarcs()
	{
		int status = CPXNETgetnumarcs(env_, net_);
		if (status == 0) throw CPLEXException(env_, status, (char*)"CPXNETgetnumarcs");
		return status;
	}

	int
		CPLEX_NET_Problem::getnumnodes()
	{
		int status = CPXNETgetnumnodes(env_, net_);
		if (status == 0) throw CPLEXException(env_, status, (char*)"CPXNETgetnodes");
		return status;
	}

	void
		CPLEX_NET_Problem::getobj(double *obj, int begin, int end)
	{
		int status = CPXNETgetobj(env_, net_, obj, begin, end);
		if (status) throw CPLEXException(env_, status, (char*)"CPXNETgetobj");
	}

	int
		CPLEX_NET_Problem::getobjsen()
	{
		int status = CPXNETgetobjsen(env_, net_);
		if (status == 0) throw CPLEXException(env_, status, (char*)"CPXNETgetobjsen");
		return status;
	}

	double
		CPLEX_NET_Problem::getobjval()
	{
		double objval;
		int status = CPXNETgetobjval(env_, net_, &objval);
		if (status) throw CPLEXException(env_, status, (char*)"CPXNETgetobjval");
		return objval;
	}

	int
		CPLEX_NET_Problem::getphase1cnt()
	{
		int status = CPXNETgetphase1cnt(env_, net_);
		if (status == -1) throw CPLEXException(env_, status, (char*)"CPXNETgetphase1cnt");
		return status;
	}

	void
		CPLEX_NET_Problem::getpi(double *pi, int begin, int end)
	{
		int status = CPXNETgetpi(env_, net_, pi, begin, end);
		if (status) throw CPLEXException(env_, status, (char*)"CPXNETgetpi");
	}

	void
		CPLEX_NET_Problem::getprobname(char *buf, int bufspace, int *surplus_p)
	{
		int status = CPXNETgetprobname(env_, net_, buf, bufspace, surplus_p);
		if (status) throw CPLEXException(env_, status, (char*)"CPXNETgetprobname");
	}

	void
		CPLEX_NET_Problem::getslack(double *slack, int begin, int end)
	{
		int status = CPXNETgetslack(env_, net_, slack, begin, end);
		if (status) throw CPLEXException(env_, status, (char*)"CPXNETgetslack");
	}

	int
		CPLEX_NET_Problem::getstat()
	{
		return CPXNETgetstat(env_, net_);
	}

	void
		CPLEX_NET_Problem::getsupply(double *supply, int begin, int end)
	{
		int status = CPXNETgetsupply(env_, net_, supply, begin, end);
		if (status) throw CPLEXException(env_, status, (char*)"CPXNETgetsupply");
	}

	void
		CPLEX_NET_Problem::getub(double *up, int begin, int end)
	{
		int status = CPXNETgetub(env_, net_, up, begin, end);
		if (status) throw CPLEXException(env_, status, (char*)"CPXNETgetub");
	}

	void
		CPLEX_NET_Problem::getx(double *x, int begin, int end)
	{
		int status = CPXNETgetx(env_, net_, x, begin, end);
		if (status) throw CPLEXException(env_, status, (char*)"CPXNETgetx");
	}

	void
		CPLEX_NET_Problem::primopt()
	{
		int status = CPXNETprimopt(env_, net_);
		if (status) throw CPLEXException(env_, status, (char*)"CPXNETprimopt");
	}

	void
		CPLEX_NET_Problem::readcopybase(char *filename)
	{
		int status = CPXNETreadcopybase(env_, net_, filename);
		if (status) throw CPLEXException(env_, status, (char*)"CPXNETreadcopybase");
	}

	void
		CPLEX_NET_Problem::readcopyprob(char *filename)
	{
		int status = CPXNETreadcopyprob(env_, net_, filename);
		if (status) throw CPLEXException(env_, status, (char*)"CPXNETreadcopyprob");
	}

	void CPLEX_NET_Problem::solution(int *netstat_p, double *objval_p, double *x, double *pi,
		double *slack, double *dj)
	{
		int status = CPXNETsolution(env_, net_, netstat_p, objval_p, x, pi, slack, dj);
		if (status) throw CPLEXException(env_, status, (char*)"CPXNETsolution");
	}

	void
		CPLEX_NET_Problem::writeprob(char *filename, char *format)
	{
		int status = CPXNETwriteprob(env_, net_, filename, format);
		if (status) throw CPLEXException(env_, status, (char*)"CPXNETwriteprob");
	}

} // end of namespace CGBase