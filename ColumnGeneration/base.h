#ifndef BASE_H
#define BASE_H

/****************************************************************************
Major Revisions:
- 2023-03-15: New and very clean mechanism for strong branching
        * provide:  virtual c_StrongBranchingCandidates* ComputeStrongBranchingCandidates(int max_k);
                    in class derived from c_BranchAndBoundNode
                    the new class c_StrongBranchingCandidates is a container for the candidate branching decisions
        * deprecated:   GetkthStrongBranchingCandidate
                        DecideonBranching
        * todo: we may integrate more parameters into the class c_StrongBranchingCandidates to 
                control the way in which the RMPs are evaluated (number of iterations, which type of pricing algorithms etc.)

- 2021-09-23: *SolveAsMip have been heavily modified such that preprocessing and dynamic search are used in CPLEX. 
               Therefore, the problem is written in file "MIP.lp" and read-in again with another instance of CPLEX and the Environment
              *If you want to use the SolveAsMip Functionality, you have to implement the function 'virtual c_Solution* CreateSolutionFromMIP(c_RMP* theRMP, vector<int,double>& columns_in_MIPsol)'
               in your problem-specific controller. 
              *To do so, the parameter columns_in_MIPsol contains the column_numbers and the value of all columns with positive value in the solution found by the MIP.
              * If the solution is infeasible (e.g., due to containing dummy columns), the function CreateSolutionFromMIP must return nullptr!
              * Parameter MIPScreenOutput controls  the screen output of cplex for the MIP
              * The MIP .lp-file has now a randomized name and this .lp-file is deleted in the destructor of the MIP with 'remove '

- 2021-03-30: Unordered_map instead of map in c_IndexInfo.

- 2019-02-05: Strong Branching Changed - All projects using Strong Branching must be recoded:
	
		*c_Controller has a vector v_undecidedBranching_nodes that saves completed node for which the branching candidate is not determined yet
			info_timer_StrongBranching counts the time that strong branching takes

		* c_BranchAndBoundNodeMVBP has two new functions that needs to be implemented 
		* and the derived class needs a member that saves the branching candidates 
		* (e.g., a vector<pair<init,int>> containing possible arc branching decisions)
		1) ComputeStrongBranchingCandidates ->  in that function the vector with candidates need to be filled 
		2) GetkthStrongBranchingCandidate -> returns the k-th candidate for branching

		*The new function DecideonBranching is called in SelectNextNode to decide on the branching candidates as late as possible



- 2017-06-07: 
		* INFTY and CG_EPS are now member of the class controller to deal with Nested CG
		* New Function "isFractional" also as a member of the controller 
				

- 2015-02-27: Memberfunctions of c_Column:

	*	The mechanism to represent a column (c_Column) has been simplified:
		old version
			virtual int CPXnzcnt() = 0;
			virtual void GetCPXColumn( double& cost, int* ind, double* val, double& lb, double& ub, char* name ) = 0;
		is now in one member function:
			virtual int GetCPXColumn( double& cost, int* ind, double* val, double& lb, double& ub, char* name ) = 0;
		i.e., the return value is the number of non-zeros

	*	double PrintReducedCosts( const c_DualVariables& dual );
		no longer supported, use ReducedCosts( const c_DualVariables& dual ); instead

	classes c_PricingProblem and c_PricingProblemHierarchy

	*	Misspelling corrected, old version
		virtual double RHSofKonvexityConstraint();
	is now
		virtual double RHSofConvexityConstraint();


	*	Misspelling corrected, old version
		virtual void GetBranchAndBoundContraints( list< list<c_BranchAndBoundConstraint*> >& con_list ) = 0;
	is now 
		virtual void GetBranchAndBoundConstraints( list< list<c_BranchAndBoundConstraint*> >& con_list ) = 0;

****************************************************************************/

#include <math.h>

#include <list>
#include <map>
#include <vector>
#include <iostream>
#include <list>
#include <limits>
#include <unordered_map>
#include <array>

using namespace std;
#include "cplexcpp.h"

//#include "../SharedFiles/timeinfo.h"
#include "../SharedFiles/stopwatch.h"
#include "../SharedFiles/settings_reader.h"

#include <assert.h>

typedef c_Stopwatch c_TimeInfo;

namespace CGBase
{
/******************* constants *********************************************/

const double INFTY = 10E8;
const double CG_EPS = 1e-4;
const int NO_ITERATION = -1;
const int MaxNumPricers = 100000;
/******************* class declarations ************************************/

class c_BranchAndBoundNode;
class c_BranchAndBoundConstraint;
class c_Column;
class c_ColumnPool;
class c_Constraint;
class c_Controller;
class c_ConvergencyInfo;
class c_DualVariables;
class c_FindKthElement;
class c_IndexInfo;
class c_PricingProblemHierarchy;
class c_PricingProblem;
class c_ReducedCostsFixing;
class c_RMP;
class c_SeparationProblem;
class c_Solution;
class c_StrongBranchingCandidates;
class c_YpsPlusColumn;
class c_YpsMinusColumn;

/****************** typedefs ***********************************************/

typedef pair<c_Column*,double> c_ColumnPricePair;

/****************** classes ************************************************/

class c_Controller : public c_SettingsReader {
public:
	enum e_phase_in_bap { initialization, root_node, tree };
private:
    CPLEXEnvironment* env;
    c_ColumnPool* p_pool;
    c_ReducedCostsFixing* p_red_costs_fixing;
    c_BranchAndBoundNode* p_currentNode;
    c_ConvergencyInfo* p_conv_info;
    int next_node_id;
    double ub;
    double aspiration_lb;
    double aspiration_ub;
    bool last_node_terminated;
    bool last_node_integer;
    map<list<int>, int> o_info_statistics_columns;
    map<int, string> s_info_column_type_names;
    list<c_BranchAndBoundNode*> active_nodes;
    list<c_BranchAndBoundNode*> inactive_nodes;
    vector<c_Column*> cols_in_rmp;
protected:
    c_BranchAndBoundNode* p_root_node;
    c_RMP* p_rmp;
    c_Solution* p_best_solution;
    // Info
    int info_number_of_rmp_iterations;
    int info_number_of_generated_columns;
    int info_number_of_solved_pricing_problems;
    int info_number_of_generated_cuts;
    int info_number_of_solved_nodes;

    double first_integer_solution;
    double info_time_first_integer_solution;
    double info_time_best_integer_solution;   

    double info_time_overall;
    double info_time_pricing;
    double info_time_separation;
    double info_time_reoptimization;
    double info_time_root_lb1;
    double info_time_root_lb2;
	double info_time_root_branching;
	double info_time_AddColumn;
	double info_time_Init;
	double info_time_NodeTerm;
	double info_time_NodeRun;
	double info_time_NodeUpdateUB;
	double info_time_MIPForUB;
	double info_timer_nodeSolve;
	double info_timer_ContUpdate;
	double info_timer_CreateDual;
	double info_timer_OrgPool;
	double info_timer_GetDual;
	double info_timer_RMPUpdate;
	double info_timer_PricingUpdate;
	double info_timer_StrongBranching;
    double info_timer_last_PP_root;
    ostream* info_1;
    ostream* info_2;
    ostream* info_3;
    ostream* info_err;
    ostream* info_statistics;
	e_phase_in_bap i_phase_in_bap;
    vector<double> v_info_time_pricing_level;
    vector<int> v_info_calls_pricing_level;
    vector<int> v_info_success_pricing_level;
    c_TimeInfo timer;
    c_TimeInfo GetBranchingInfo; 
protected:
	list<c_Constraint*> l_constraints;
	list<c_PricingProblemHierarchy*> pricing_problem_hierarchy;
	list<c_SeparationProblem*> separation_problems;
    bool LastNodeInteger() const { return last_node_integer; }
    bool LastNodeTerminated()  const { return last_node_terminated; }
	
	list<c_SeparationProblem*> feasibility_cutting_problems;
	// setter to overwrite pricing method
	void Set_number_of_solved_pricing_problems(int new_number){info_number_of_solved_pricing_problems=new_number;}
	void Set_number_of_generated_columns(int new_number){info_number_of_generated_columns=new_number;}
	void Set_number_of_rmp_iterations(int new_number){info_number_of_rmp_iterations=new_number;}
	void Set_timer_pricing_update(double new_number){info_timer_PricingUpdate=new_number;}
public:
    c_Controller( string settings_file = "settings.txt" );
    virtual ~c_Controller();

    double UB() const { return ub; }
    void SetUB( double val ) { ub = val; LBUpdateAllert(); }
    double AspirationLB() { return aspiration_lb; }
    void SetAspirationLB( double val ) { aspiration_lb = val; }
    double AspirationUB() { return aspiration_ub; }
    void SetAspirationUB( double val ) { aspiration_ub = val; }
    c_RMP* RMP() const { return p_rmp; }
    c_ColumnPool* ColumnPool() const { return p_pool; }
    int NumColumnsInRMP() const { return (int)cols_in_rmp.size(); };
    c_Column* ColumnInRMP( int position ) const
        {   if ( position>=(int)cols_in_rmp.size() ) return NULL; return cols_in_rmp[position]; }
    CPLEXEnvironment* CPLEXEnv() { return env; }
    void AddColumns( const list<c_Column*>& col_list );
    void TransferColumnsToPool( const list<c_Column*>& col_list );
    c_ReducedCostsFixing* ReducedCostsFixing() { return p_red_costs_fixing; }

    void AddNode( c_BranchAndBoundNode* node );
    void RemoveNode( c_BranchAndBoundNode* node );
	void AddConstraint( c_Constraint* con ) { l_constraints.push_back( con ); }
    void AddPricingProblem( c_PricingProblem* prob );
    void AddPricingProblemHierarchy( c_PricingProblemHierarchy* hierarchy );
    list<c_PricingProblemHierarchy*>& PricingProblemHierarchies() { return pricing_problem_hierarchy;}

    void AddSeparationProblem( c_SeparationProblem* );
    const list<c_SeparationProblem*>& SeparationProblems() const { return separation_problems; }

    void AddFeasibilityCuttingProblem( c_SeparationProblem* );
    const list<c_SeparationProblem*>& FeasibilityCuttingProblems() const { return feasibility_cutting_problems; }

    int NumBranchAndBoundNodes() const;
	int NumOpenBranchAndBoundNodes() const;
    int NumOpenStrongBranchingNodes() const;

	const list<c_BranchAndBoundNode*>& GetInactiveNodes() const { return inactive_nodes; }

    void TerminateNode( c_BranchAndBoundNode* node );
    enum e_branching { depth_first, best_first, fifo };
	void StatisticsBranching( map<string,int>& counter_for_branching );

	void LBUpdateAllert();
    // parameters
    c_BoolParameter DEBUG;
    c_StringParameter UseLPOptimizer;
    c_BoolParameter ReloadSettings;
    c_BoolParameter Stabilization;

    c_IntParameter InfoLevel;
    c_IntParameter InfoStatisticsLevel;
    c_BoolParameter InfoDottyOutput;
    c_BoolParameter InfoGnuplotOutput;
    c_BoolParameter InfoGraphMLOutput;
	c_BoolParameter InfoTikzOutput;
    c_BoolParameter InfoDottySuppressIntermediateNodes;
    c_StringParameter InfoInstance;
    c_BoolParameter InfoPrintOverallBestSolution;
    c_BoolParameter InfoPrintBestSolution;
    c_StringParameter InfoOutputFileName;
    c_BoolParameter InfoConvergency;
	c_BoolParameter InfoStatisticsColumns; 

    c_DoubleParameter MaxSolutionTimeSec;
    c_IntParameter MaxColumnsToAddInPricing;
    c_BoolParameter PricingWithColumnSelection;
	c_IntParameter PricingHierarchyMaxNumFailuresBeforeSwitchOff;
	c_IntParameter 	PricingHierarchyMinNumColsToGenerate;
    c_DoubleParameter MaxRatioColsToRows;
    c_DoubleParameter MaxRatioColsToRowsMIP;
    c_BoolParameter Branching;
    c_StringParameter BranchingNodeSelection;
    c_BoolParameter EarlyBranching;
    c_BoolParameter StrongBranching;
    c_IntParameter StrongBranchingUptoLevel;
    c_IntParameter StrongBranchingNumCandidates;
    c_IntParameter StrongBranchingMinNumCandidates;
    c_DoubleParameter StrongBranchingNumCandidatesDecreasePerLevel;

    c_BoolParameter CuttingPlanes;

    c_BoolParameter UseMIPSolverForUBs;
    c_IntParameter UseMIPSolverUptoLevel;
    c_IntParameter UseMIPSolverUpFromLevel;
    c_IntParameter MIPMaxSolutionTimeSec;
    c_BoolParameter MIPScreenOutput;
    c_BoolParameter MIPAccelerate; // Warning: This still causes a segmentation fault on Mogon II
    c_BoolParameter SolveAsMipAfterTimeOut;

    c_BoolParameter UseLagrangeanLB;
    c_BoolParameter ReducedCostVariableFixing;
	c_IntParameter StabFrequence;
	c_IntParameter FeasyCutFrequence; 
	c_DoubleParameter FeasyCutGap;
	c_IntParameter NumThreadsRMP;
	c_StringParameter NameDottyOutput;
    c_BoolParameter CheckForInftyAndLBError;

	c_StringParameter StrongBranchingRule;
	c_DoubleParameter StrongBranchingMu;
	c_IntParameter PricersUsedForStrongBranching;
    c_DoubleParameter LocalCG_EPS;
    c_DoubleParameter LocalInfty;
    // setters for parameters
    void SetDEBUG( bool val )                                { DEBUG.SetValue( val ); }
    void SetUseLPOptimizer( const char* txt) { UseLPOptimizer.SetValue( txt ); }
    void SetReloadSettings( bool val )           { ReloadSettings.SetValue( val ) ; }
    void SetStabilization( bool val )        { Stabilization.SetValue( val ); }
    void SetInfoLevel( int level )                   { InfoLevel.SetValue( level ); /* 0, 1, 2 or 3 */ }
    void SetInfoStatisticsLevel( int level ) { InfoStatisticsLevel.SetValue( level ); }
    void SetInfoDottyOutput(bool flag)       { InfoDottyOutput.SetValue( flag ); }
    void SetInfoGnuplotOutput(bool flag)     { InfoGnuplotOutput.SetValue( flag ); }
    void SetInfoGraphMLOutput(bool flag)     { InfoGraphMLOutput.SetValue( flag ); }
	void SetInfoDottySuppressIntermediateNodes(bool f) { InfoDottySuppressIntermediateNodes.SetValue(f); }
    void SetInfoPrintOverallBestSolution(bool f) { InfoPrintOverallBestSolution.SetValue(f); }
    void SetInfoPrintBestSolution(bool f)        { InfoPrintBestSolution.SetValue(f); }
    void SetInfoOutputFileName(const char* txt) { InfoOutputFileName.SetValue( txt ); }
    void SetInfoConvergencyNode( int v )      { InfoConvergency.SetValue(v); }
	void SetInfoStatisticsColumns( bool f )   { InfoStatisticsColumns.SetValue(f); }
    void SetMaxSolutionTimeSec( double sec ) { MaxSolutionTimeSec.SetValue( sec ); }
    void SetMaxColumnsToAddInPricing( int num )      { MaxColumnsToAddInPricing.SetValue( num ); }
	void SetPricingHierarchyMaxNumFailuresBeforeSwitchOff(int num ) { PricingHierarchyMaxNumFailuresBeforeSwitchOff.SetValue(num); }
	void SetPricingHierarchyMinNumColsToGenerate(int num ) { PricingHierarchyMinNumColsToGenerate.SetValue(num); }
    void SetMaxRatioColsToRows( double x )   { MaxRatioColsToRows.SetValue(x); }
    void SetMaxRatioColsToRowsMIP( double x ){ MaxRatioColsToRowsMIP.SetValue(x); }
    void SetBranching( bool flag )                   { Branching.SetValue( flag ); }
    void SetBranchingNodeSelection( const char* txt ) { BranchingNodeSelection.SetValue( txt ); }
    void SetEarlyBranching( bool flag )          { EarlyBranching.SetValue( flag ); }
    void SetStrongBranching( bool flag )     { StrongBranching.SetValue( flag ); }
    void SetStrongBranchingUptoLevel( int level )       { StrongBranchingUptoLevel.SetValue( level ); }
    void SetStrongBranchingNumCandidates( int val ) {   StrongBranchingNumCandidates.SetValue( val ); }
    void SetStrongBranchingMinNumCandidates( int v ) { StrongBranchingMinNumCandidates.SetValue( v ); }
    void SetStrongBranchingNumCandidatesDecreasePerLevel( double x ) { StrongBranchingNumCandidatesDecreasePerLevel.SetValue(x); }
    void SetCuttingPlanes( bool flag )           { CuttingPlanes.SetValue( flag ); }
    void SetInfoInstance( const char* txt )  { InfoInstance.SetValue( txt ); }
    void SetUseMIPSolverForUBs( bool flag )  { UseMIPSolverForUBs.SetValue( flag ); }
    void SetUseMIPSolverUptoLevel( int val ) { UseMIPSolverUptoLevel.SetValue( val ); }
    void SetMIPMaxSolutionTimeSec( int val ) { MIPMaxSolutionTimeSec.SetValue( val ); }
    void SetUseLagrangeanLB( bool flag )     { UseLagrangeanLB.SetValue( flag ); }
    void SetPricingWithColumnSelection( bool f ) { PricingWithColumnSelection.SetValue(f); }
    void SetReducedCostVariableFixing( bool f )  { ReducedCostVariableFixing.SetValue(f); }

    // Info streams
    ostream& Info1()        { return *info_1; }
    ostream& Info2()        { return *info_2; }
    ostream& Info3()        { return *info_3; }
    ostream& InfoErr()  { return *info_err; }
    ostream& InfoStatistics()   { return *info_statistics; }

    double LB() const;  // lower bound in general
    double LB1() const; // lower bound at root node
    double LB2() const; // lower bound at root node + cutting planes
    double InfoTimeOverall() const { return info_time_overall; }
    double InfoTimePricing() const { return info_time_pricing; }
    double InfoTimeSeparation() const { return info_time_separation; }
    double InfoTimeReoptimization() const { return info_time_reoptimization; }
    int   InfoNumberOfSimplexIterations() const;
    int   InfoNumberOfRMPIterations() const             { return info_number_of_rmp_iterations; }
    int     InfoNumberOfGeneratedColumns() const        { return info_number_of_generated_columns; }
    int     InfoNumberOfSolvedPricingProblems() const { return  info_number_of_solved_pricing_problems; }
    int     InfoNumberOfGeneratedCuts() const               { return info_number_of_generated_cuts; }
    int     InfoNumberOfSolvedNodes() const                 { return info_number_of_solved_nodes; }
    double  InfoTimeFirstIntegerSolution() const    { return info_time_first_integer_solution; }
    double  InfoTimeBestIntegerSolution() const     {return info_time_best_integer_solution; }
	double	InfoTimePricingLevel(int level) { double x=0.0; (level < (int)v_info_time_pricing_level.size()) ? x=v_info_time_pricing_level[level] : x=0.0; return x; }
	double	InfoCallPricingLevel(int level) { int x = 0; (level < (int)v_info_calls_pricing_level.size()) ? x = v_info_calls_pricing_level[level] : x = 0; return x; }
	double	InfoSuccessPricingLevel(int level) { int x = 0; (level < (int)v_info_success_pricing_level.size()) ? x = v_info_success_pricing_level[level] : x = 0; return x; }
    double  InfoTimeRootLB1() const                             { return info_time_root_lb1; }
    double  InfoTimeRootLB2() const                             { return info_time_root_lb2; }
    double  FirstIntegerSolution() const                    { return first_integer_solution; }
	double InfoTimeBranching() const {return info_time_root_branching; }
	double InfoTimeAddColum() const {return info_time_AddColumn; }
	double InfoTimeInit() const {return info_time_Init; }
	double InfoTimeNodeTerm() const {return info_time_NodeTerm; }
	double InfoTimeNodeRun() const {return info_time_NodeRun; }
	double InfoTimeNodeUpdateUB() const {return info_time_NodeUpdateUB; }
	double InfoTimeMIPForUB() const { return info_time_MIPForUB; }
	double InfoTimeNodeSolve() const {return info_timer_nodeSolve; }
	double InfoTimeContUpdate() const {return info_timer_ContUpdate; }
	double InfoTimeCreateDual() const {return info_timer_CreateDual; }
	double InfoTimeOrgPool() const {return info_timer_OrgPool; }
	double InfoTimeGetDual() const {return info_timer_GetDual; }
	double InfoTimeRMPUpdate() const {return info_timer_RMPUpdate; }
	double InfoTimePricingUpdate() const {return info_timer_PricingUpdate; }
	double InfoTimeStrongBranching() const { return info_timer_StrongBranching; }
	void SetTimeStrongBranching(double new_number) { info_timer_StrongBranching = new_number; }

	void RecordStatisticsColumns( const list<int>& col_info_type );
	void SetStatisticsColumnTypeName( int i, string s ) { s_info_column_type_names[i] = s; }
	e_phase_in_bap PhaseInBaP() const;
	int StatisticsColumns( const list<int>& col_info_type ) const;
	void PrintColumnStatistics();

	double InfoTimeLastPPRoot(){return info_timer_last_PP_root;}
	void SetInfoTimeLastPPRoot(double t){info_timer_last_PP_root=t;}
	void WriteConvergencyInfo( const char* filename );

    c_Solution* BestSolution() const { return p_best_solution; }
    void SetBestSolution( c_Solution* sol );
    bool SolutionIsFound() const { return ( p_best_solution != NULL ); }
    double SolutionTimeSec() const { return timer.Seconds(); }
    bool TimeOut() const { return ( timer.Seconds() > MaxSolutionTimeSec() ); }
	c_BranchAndBoundNode* GetCurrentNode() const { return p_currentNode; }

    void SetInfo1( ostream& out )       { info_1 = &out; }
    void SetInfo2( ostream& out )       { info_2 = &out; }
    void SetInfo3( ostream& out )       { info_3 = &out; }
    void SetInfoErr( ostream& out ) { info_err = &out; }
    void SetInfoStatistics( ostream& out ) { info_statistics = &out; }
    void SetInfoTimeOverall(double i) { info_time_overall = i; }
    void SetInfoTimePricing(double i) { info_time_pricing = i; }
    void SetInfoTimeSeparation(double i) { info_time_separation = i; }
    void SetInfoTimeReoptimization(double i) { info_time_reoptimization = i; }
	void SetInfoTimeAddColum(double i) { info_time_AddColumn = i; }
	void SetInfoTimeContUpdate(double i) { info_timer_ContUpdate = i; }
	void SetInfoTimeCreateDual(double i) { info_timer_CreateDual = i; }
	void SetInfoTimeOrgPool(double i) { info_timer_OrgPool = i; }
	void SetInfoTimeGetDual(double i) { info_timer_GetDual = i; }
	void SetInfoTimeMIPForUB(double i) { info_time_MIPForUB = i; }
	void IncreasePricingStatisticsVectors() {
		v_info_time_pricing_level.push_back(0.0); v_info_calls_pricing_level.push_back(0); v_info_success_pricing_level.push_back(0); }
	void SetInfoTimePricingLevel(int level, double i) { v_info_time_pricing_level[level] += i; }
	void IncreaseInfoCallPricingLevel(int level) { v_info_calls_pricing_level[level] ++; }
	void IncreaseInfoSuccessPricingLevel(int level) { v_info_success_pricing_level[level] ++; }
	bool local_IsFractional(double x);

    // we may overwrite these methods
    virtual void Go( void );
	virtual e_branching BranchingRule() const;
    virtual c_BranchAndBoundNode* SelectNextNode();
    virtual void OutputInOStream( ostream& s ) const;
    virtual void Update( c_BranchAndBoundNode* node );
	virtual void Pricing(int iteration, c_DualVariables* dual, list<c_Column*>& col_list, int NumPricersUsed = MaxNumPricers);
    virtual void ColumnSelectionInPricing( list<c_ColumnPricePair>& cols_and_prices, list<c_ColumnPricePair>& others ) { /*do nothing*/ };
    virtual void OrganizePool();
    virtual c_ReducedCostsFixing* CreateReducedCostsFixing() { return nullptr; }
    virtual void Separation( c_RMP* rmp, list<c_Constraint*>& cuts, int BaB_node_level );
    virtual void PerformBranching( c_BranchAndBoundNode* node );
    virtual void UpdateAllNodeInformations( c_BranchAndBoundNode* selected_node );
    virtual bool FeasibilityCutting();
	virtual void FixPartOfSolution();
	virtual void UnfixSolution();
	virtual void DeleteTree();

    // these methods must be provided by derived class
    virtual c_RMP* CreateNewRMP() = 0;
    virtual c_BranchAndBoundNode* CreateRootNode() = 0;
    virtual c_DualVariables* CreateDualVariables() = 0;
    virtual c_Solution* CreateSolution( c_RMP*, c_DualVariables* ) = 0;
    virtual c_Solution* CreateSolutionFromMIP(c_RMP* theRMP, vector<pair<int, double> >& columns_in_Mip_sol);
	virtual void AddPricingProblems() = 0;
	virtual void AddSeparationProblems() = 0;
};


class c_Column {
    friend class c_Controller;

    c_Controller* p_controller;
    bool is_in_rmp;
    long num_in_rmp;
    void SetIsInRMP( bool val ) { is_in_rmp=val; }
    void SetNumInRMP( long num ) { is_in_rmp=true; num_in_rmp=num; }
public:
    c_Column( c_Controller* controller );
    virtual ~c_Column() {};

    c_Controller* Controller() const { return p_controller; }
    bool IsInRMP() const { return is_in_rmp; }
    long NumInRMP() const { return num_in_rmp; }
    double ReducedCosts( const c_DualVariables& dual );
	double CoutReducedCosts(const c_DualVariables& dual);
	virtual void ChangeObjCoeff( double obj );

    // we may overwrite these methods
    virtual void OutputInOStream( ostream& s ) const;
    virtual double DefaultLowerBound() { return 0.0; }
    virtual double DefaultUpperBound() { return CPX_INFBOUND; }
	virtual const list<int> InfoColumnType() const; // idea: (phase-of-alg,col-typ,[additional param])
    // these methods must be provided by derived class
    virtual bool IsTransferableToPool() const = 0;
    virtual bool IsSystemColumn() const { return false; };
    // CPLEX format of columns
    virtual double GetCPXObj() = 0;
    virtual int GetCPXColumn( double& cost, int* ind, double* val, double& lb, double& ub, char* name ) = 0;
};


class c_DualVariables
{
    c_Controller* p_controller;
    double* dual_val;
public:
    c_DualVariables( c_Controller* controller, int dim );
    virtual ~c_DualVariables();

    c_Controller* Controller() const { return p_controller; }
    double* CPXDual() { return dual_val; }
	double* CPXDual() const { return dual_val; }
    double& operator[](int idx) { return dual_val[idx]; }
    double operator[](int idx) const { return dual_val[idx]; }

	// we may overwrite these methods
    virtual void OutputInOStream( ostream& s ) const;
};


struct eqlong
{
  bool operator()(const long s1, const long s2) const
  {
    return (s1 == s2);
  }
};


class c_ColumnPool {
    c_Controller* p_controller;
  list<c_Column*> cols_in_pool;
public:
    c_ColumnPool(c_Controller* controller) : p_controller( controller ) {};
    virtual ~c_ColumnPool();

    c_Controller* Controller() const { return p_controller; }
    void AddColumns( const list<c_Column*>& col_list );
    void RemoveColumns( c_DualVariables* dual, double threshold );
    void ReducePool( c_DualVariables* dual, int max_number_of_columns );
    void Pricing( c_DualVariables* dual, list<c_Column*>& col_list, list<double>& prices );
    void MoveAllColumnsToRMP();
};


class c_YpsPlusColumn : public c_Column {
    c_Constraint* constraint;
public:
    c_YpsPlusColumn( c_Controller* controller, c_Constraint* my_constraint );
    virtual ~c_YpsPlusColumn() {};

    // these methods must be provided by derived class
    virtual double GetCPXObj();
    virtual bool IsTransferableToPool() const { return false; }
    //virtual int CPXnzcnt() { return 1; }
    virtual int GetCPXColumn( double& cost, int* ind, double* val, double& lb, double& ub, char* name );
    virtual bool IsSystemColumn() const { return true; };

    virtual double DefaultLowerBound();
    virtual double DefaultUpperBound();

	 virtual void OutputInOStream( ostream& s ) const;
};


class c_YpsMinusColumn : public c_Column {
    c_Constraint* constraint;
public:
    c_YpsMinusColumn( c_Controller* controller, c_Constraint* my_constraint );
    virtual ~c_YpsMinusColumn() {};

    // these methods must be provided by derived class
    virtual double GetCPXObj();
    virtual bool IsTransferableToPool() const { return false; }
    //virtual int CPXnzcnt() { return 1; }
    virtual int GetCPXColumn( double& cost, int* ind, double* val, double& lb, double& ub, char* name );
    virtual bool IsSystemColumn() const { return true; };

    virtual double DefaultLowerBound();
    virtual double DefaultUpperBound();
	virtual void OutputInOStream( ostream& s ) const;
};


class hasher_fct {
public:
	int operator()(const array<int, 4>& a) const
	{
		int h = 0;
		for (int i = 0; i < 4; ++i)
		{
			h = h * 31 + a[i];
		}
		return h;
	}
}; 


class c_IndexInfo {
	int act_index;
	unordered_map<array<int, 4>, int, hasher_fct> indices;
public:
	c_IndexInfo( int first_index = 0 );
	virtual ~c_IndexInfo( void );

	// typedef enum { NO_INDEX = -1 };
	int NewIndex( int i1, int i2=0, int i3=0, int i4=0 );
	int Index( int i1, int i2=0, int i3=0, int i4=0 ) const;
	bool IndexExists( int i1, int i2=0, int i3=0, int i4=0 ) const;
	void DeleteIndex( int index_to_del );
	int NumIndices() const { return act_index; }
};


class c_Constraint {
friend class c_RMP;
    c_Controller* p_controller;
    int _type, _n1, _n2, _n3;
    int _sense;
    double _rhs;
    double _estimator_dual;
    c_YpsPlusColumn* col_yps_plus;
    c_YpsMinusColumn* col_yps_minus;
    c_BranchAndBoundNode* active_at_node; 
protected:
    // use only in the construction phase of derived classes
    void SetRhs( double rhs ) { _rhs = rhs; }
public:
    c_Constraint( c_Controller* controller,
                            char sense, double rhs, double estimator_dual,
                            int type, int n1=0, int n2=0, int n3=0 );
    virtual ~c_Constraint() {};

    // we may overwrite these methods
    c_Controller* Controller() const { return p_controller; }
    c_YpsPlusColumn* Y_Plus()       const { return col_yps_plus; }
    c_YpsMinusColumn* Y_Minus() const { return col_yps_minus; }
    char        Sense() const { return _sense; }
    double  Rhs() const { return _rhs; }
    int         Index() const;
    void Indices( int& type, int& n1, int& n2, int& n3 ) const { type=_type; n1=_n1; n2=_n2; n3=_n3; }
    bool IsActiveAtNode( c_BranchAndBoundNode* node ) const;
    void SetActiveAtNode( c_BranchAndBoundNode* node );
    c_YpsMinusColumn* YpsMinusColumn() { return col_yps_minus; }
    c_YpsPlusColumn* YpsPlusColumn() { return col_yps_plus; }
    void CreateYpsColumns();
    double EstimatorDual () const { return _estimator_dual; }

    // we may overwrite these methods
    virtual bool IsGloballyValid() const { return false; } // when added in B&B-tree
	virtual int Type() const { return _type; }
    virtual bool Stabilization() const { return false; }
    virtual c_YpsPlusColumn*    CreateYpsPlusColumn()   { return NULL; }
    virtual c_YpsMinusColumn* CreateYpsMinusColumn() { return NULL; }
    virtual bool UpdateStabilization( int iteration, int num_cols_pricing, double dual ) { return true; }
    virtual double EpsMinus()       const { return 1.0; }
    virtual double EpsPlus()            const { return 1.0; }
    virtual double DeltaMinus() const { return 1.0; }
    virtual double DeltaPlus()      const { return 1.0; }
	virtual string TextInBranching() const { return string("constraint"); }
    virtual void OutputInOStream( ostream& s ) const;
};


class c_RMP: public CPLEXProblem, private c_IndexInfo {
    c_Controller* p_controller;
protected:
    int    actual_iteration;
    double actual_obj_value;
    double d_mip_objval;
    vector< c_Constraint* > constraints;
    int info_number_of_simplex_iterations;
	int num_removed_constraints;
	int num_added_constraints;
    string help_mipname;
public:
    c_RMP( c_Controller* controller );
    virtual ~c_RMP();

    void Reset();

    c_Controller* Controller() const { return p_controller; }

    enum e_solver { ePRIMOPT, eDUALOPT, eBAROPT };
    double mipobjval() const;

    int ActualIteration() const { return actual_iteration; }
    int InfoNumberOfSimplexIterations() const { return info_number_of_simplex_iterations; }
    double ActualObjectiveValue() const { return actual_obj_value; }
    int NumCols() const { return numcols(); }
    int NumRows() const { return numrows(); }
    void GetDualVariables( c_DualVariables& dual ) const;
    void GetReducedCosts( double c_red[] );
    void GetDualPrices( double pi[] );
    void GetRHS( double rhs[] );
    void AddColumns( const list<c_Column*>& col_list );

    bool IsConstraintInRMP( const c_Constraint* con ) const;
    int AddConstraint( c_Constraint* con );
    void AddConstraints( list<c_Constraint*>& con_list );
    void RemoveConstraint( c_Constraint* con );
    void RemoveConstraints( list<c_Constraint*>& con_list );

    int ConstraintIndex( int type, int n1=0, int n2=0, int n3=0 ) const;
    bool ConstraintIndexExists( int i1, int i2=0, int i3=0, int i4=0 ) const;
    int NumConstraints( void ) const;
    c_Constraint* Constraint( int i1, int i2=0, int i3=0, int i4=0 ) const;
    void ConstructEmptyRMP( CPLEXProblem::ObjSense objsen = CPLEXProblem::MINIMIZE );
    const vector<c_Constraint*> Constraints() const { return constraints; }
	int GetNumRemCon() const { return num_removed_constraints; }
	int GetNumAddCon() const { return num_added_constraints; }
	void SetObjval(double the_val) { actual_obj_value = the_val; }

    // we may overwrite these methods
    virtual void Optimize( e_solver solver );
    virtual void OptimizeMIP( e_solver solver );
    virtual bool IsIntegral() const;    
	virtual bool IsFeasible() const;
    virtual bool UpdateStabilization( int iteration, int num_cols_pricing, c_DualVariables* dual );
	virtual double RoundLBTo(double lb) { return ceil(lb - Controller()->LocalCG_EPS() * Controller()->LocalCG_EPS()); }
    virtual void RemoveAllBranchAndBoundConstraints();
    virtual void Update( c_BranchAndBoundNode* node );
    virtual void OutputInOStream( ostream& s ) const;
	virtual void AddBranchAndBoundConstraints( const list<c_BranchAndBoundConstraint*>& constraint_list );
	virtual void SetRequiredVarsIntegral();

    virtual void SetConstraintSense(int contraintIndex, char sense);

    // these methods must be provided by derived class
    virtual void InitializeFirstRMP( ) = 0;
};


class c_BranchAndBoundNode {
    c_Controller* p_controller;
    c_BranchAndBoundNode* p_father;
    int i_node_number;
    int i_iteration;
    int i_node_number_solved;
    int i_num_iter_wo_feasibility_cut;
    bool b_is_removed;
    bool b_is_integral;
    bool b_is_feasible;
    bool b_terminated;
    double d_LB;
    double d_RMP_value;
    list<c_BranchAndBoundConstraint*> l_added_constraints;
    list<c_BranchAndBoundNode*> l_sons;
    // private methods
    void SetRMPValue( double val ) { d_RMP_value = val; }
    double ComputeLagrangeanLB();

    int i_level;
public:
    c_BranchAndBoundNode( c_Controller* controller );
    c_BranchAndBoundNode( c_BranchAndBoundNode* p_father, list<c_BranchAndBoundConstraint*>& add_constraints );
    virtual ~c_BranchAndBoundNode();
    // getter
    c_Controller* Controller() const { return p_controller; }
    int NodeNumber() const  { return i_node_number; }
    int NodeNumberSolved() const  { return i_node_number_solved; }
    c_BranchAndBoundNode* Father() const { return p_father; }
    const list<c_BranchAndBoundNode*>& Sons() const { return l_sons; }
    string UniquePattern() const;
    int NumSons() const { return (int)l_sons.size(); }
    bool IsRootNode() const { return ( p_father == NULL ) || ( p_father->NumSons() == 1 && p_father->IsRootNode() ); }
    int Level() const;
    int DetailedLevel() const;
    int GetLevel_new() const;
    double LB() const { return d_LB; }
    double SubtreeLB() const;
    double RMPValue() const { return d_RMP_value; }
    bool Terminated() const { return b_terminated; }
    bool IsSolved() const   { return (i_iteration > NO_ITERATION); }
    bool IsRemoved() const { return b_is_removed; }
    bool AnySonSolved() const;
    void GetConstraints( list<c_BranchAndBoundConstraint*>& ) const;
    const list<c_BranchAndBoundConstraint*>& AddedConstraints() const { return l_added_constraints; }   
    // setters
    void SetNodeNumber(int num) { i_node_number = num; }
    void SetNodeNumberSolved(int num) { i_node_number_solved = num; }
    void SetIsIntegral(bool val) { b_is_integral = val; }
    void SetLB(double val);
    void SetRemoved(bool removed) { b_is_removed = removed; }
    void SetUnsolved() { i_iteration = NO_ITERATION; }
    // non-const methods 
    c_DualVariables* Go(double termination_lb = numeric_limits<double>::infinity(), double termination_ub = -numeric_limits<double>::infinity(), int NumPricersUsed = MaxNumPricers); // returns the dual optimal solution
    void Terminate() { p_controller->TerminateNode(this); b_terminated = true; }
    void AddSon(c_BranchAndBoundNode* son);
    // we may overwrite these methods
    virtual void WriteFile();
    virtual double SolveAsMIP( long time_limit );
    virtual bool IsIntegral() const;
    virtual bool IsFeasible() const;
    virtual void OutputInOStream( ostream& s ) const;
    virtual bool EarlyBranching() { return false; }
    virtual bool GetSeparationConstraints( list<c_BranchAndBoundConstraint*>& con_list );
 	virtual void DecideOnStrongBranchingCandidate();
    // these methods must be provided by derived class
    virtual c_BranchAndBoundNode* CreateNewNode( list<c_BranchAndBoundConstraint*>& new_constraint_list )= 0;
    virtual void GetBranchAndBoundConstraints( list< list<c_BranchAndBoundConstraint*> >& con_list ) = 0;
	// this method must be provided by derived class if SB is used
	virtual c_StrongBranchingCandidates* ComputeStrongBranchingCandidates(int max_k);
};


class c_BranchAndBoundConstraint {
protected:
    c_Controller* p_controller;
    list<c_Constraint*> _rmp_constraints;
public:
	c_BranchAndBoundConstraint( c_Controller* controller );
    c_BranchAndBoundConstraint( c_Controller* controller, const list<c_Constraint*>& rmp_constraints );
    virtual ~c_BranchAndBoundConstraint() ;

    c_Controller* Controller() const { return p_controller; }
    const list<c_Constraint*>& RMPConstraints() const { return _rmp_constraints; }
	void AddConstraint( c_Constraint* con ) { _rmp_constraints.push_back(con); }
 
    // we may overwrite these methods
    virtual bool IsSeparationConstraint() const { return false; }
    virtual bool IsStrongBranchingCandidates() const { return false; }
    virtual void ComputeBounds( c_Column* col, double& lb, double& ub ) {} // do nothing
    // output
    virtual void OutputInOStream(ostream& s) const;
    virtual int GnuPlotLineStyle() { return 12; }
	virtual string TextInBranching() const { return string("branch"); }
};


class c_BranchAndBoundConstraintSeparation : public c_BranchAndBoundConstraint {
public:
    c_BranchAndBoundConstraintSeparation( c_Controller* controller, list<c_Constraint*>& rmp_constraints )
        : c_BranchAndBoundConstraint( controller, rmp_constraints ) {}
    virtual ~c_BranchAndBoundConstraintSeparation() {}

    virtual bool IsSeparationConstraint() const { return true; }
	virtual string TextInBranching() const;
};


class c_StrongBranchingCandidates : public c_BranchAndBoundConstraint {
    vector<list<list< c_BranchAndBoundConstraint*> > > l_candidates;
public:
    c_StrongBranchingCandidates(c_Controller* controller);
    virtual ~c_StrongBranchingCandidates();

    void AddCandidate(const list<list< c_BranchAndBoundConstraint*> >& candidate);
    bool IsStrongBranchingCandidates() const { return true; }
    auto Candidates() const { return l_candidates; }
    // we may overwrite these methods
    virtual int EstimatedNumBranchAndBoundNodes() const { return 2; }
    string TextInBranching() const { return string("strong br"); }
};

/*
the parameters have the following meaning:

* i_max_num_failures (default is 999999)
  If pricer0 fails for i_max_num_failures consecutive iterations then pricer0 is turned off
	If then pricer1 fails for i_max_num_failures consecutive iterations then pricer1 is turned off also
	etc.
	The last pricer<n> is never turned off

* i_min_num_columns (default is 1)
  pricer0, pricer1, ... must together deliver i_min_num_columns or more columns

* Update( dual )
  can be used if some preparation can be done jointly for all pricers in the hierarchy

*/
class c_PricingProblemHierarchy {
	c_Controller* p_controller;
protected:
	int i_pricing_start_level;
	int i_max_num_failures;
	int i_min_num_columns;
    int i_num_failures;
    double d_best_reduced_costs;
    vector<int> v_num_failures;
    vector<c_PricingProblem*> v_hierarchy; 
public:
	c_PricingProblemHierarchy( c_Controller*, int max_num_failures, int min_num_columns );
	virtual ~c_PricingProblemHierarchy();

	c_Controller* Controller() const { return p_controller; }
	void Add( c_PricingProblem* pricer );
	double BestReducedCosts() const { return d_best_reduced_costs; }
	double RHSofConvexityConstraint() const;
	int GetNumFailures() const {return i_num_failures;}
    int Size() const { return (int) v_hierarchy.size(); }
    c_PricingProblem* PricingProblem(int i) const { return v_hierarchy[i]; };

	// we may overwrite these methods
	virtual void Update( c_BranchAndBoundNode* node );
	virtual void Update( c_DualVariables* dual );
	virtual int Go(c_DualVariables* dual, list<c_ColumnPricePair>& cols_and_prices, int NumPricersUsed = MaxNumPricers);
};


class c_PricingProblem {
friend class c_PricingProblemHierarchy;
    c_Controller* p_controller;
	c_PricingProblemHierarchy* p_my_hierarchy;
public:
    c_PricingProblem( c_Controller* );
    virtual ~c_PricingProblem() {};

    c_Controller* Controller() const { return p_controller; }
	c_PricingProblemHierarchy* MyPricingProblemHierarchy() const { return p_my_hierarchy; }
	// we may overwrite these methods
    virtual void OutputInOStream( ostream& s ) const;
    // these methods must be provided by derived class
    virtual void Update( c_BranchAndBoundNode* node ) = 0;
    virtual void Update( c_DualVariables* dual ) = 0;
    virtual int Go( c_DualVariables* dual, list<c_ColumnPricePair>& cols_and_prices, double& best_val ) = 0;
	virtual double RHSofConvexityConstraint() = 0;
};


class c_SeparationProblem {
    c_Controller* p_controller;
public:
    c_SeparationProblem( c_Controller* );
    virtual ~c_SeparationProblem() {}

	c_Controller* Controller() const { return p_controller; }
    // we may overwrite these methods
    virtual void OutputInOStream( ostream& s ) const;
    // these methods must be provided by derived class
    virtual int Go( c_RMP* rmp, list<c_Constraint*>& cuts, int node_level ) = 0;
};


// optional class
class c_ReducedCostsFixing {
    c_Controller* p_controller;
public:
    c_ReducedCostsFixing( c_Controller* controller );
    virtual ~c_ReducedCostsFixing() {}

    // we may overwrite these methods
    c_Controller* Controller() const { return p_controller; }

    // we may overwrite these methods
    virtual void OutputInOStream( ostream& s ) const;

    // these methods must be provided by derived class
    virtual void ComputeLowerBounds( c_RMP* p_rmp, c_DualVariables* dual, double z_RMP, bool final_iteration ) = 0;
    virtual int CheckAgainstUB( double ub ) = 0;
};


class c_Solution {
    c_Controller* p_controller;
protected:
    double obj;
public:
    c_Solution( c_Controller*, c_RMP*, c_DualVariables* );
    virtual ~c_Solution();

    // we may overwrite these methods
    c_Controller* Controller() const { return p_controller; }
    double Objective() const { return obj; }

    // we may overwrite these methods
    virtual void OutputInOStream( ostream& s ) const;
};


class c_ConvergencyInfo {

	struct c_ConvergencyInfoPoint {
		double _time_ms;
		double _ub;
		double _lb;
	};
	list<c_ConvergencyInfoPoint*> l_values;
public:
	c_ConvergencyInfo();
	virtual ~c_ConvergencyInfo();

	int NumEntries() const { return (int)l_values.size(); }
	void AddPoint( double time_ms, double ub, double lb );
	double BestUB() const;
	double BestLB() const;

	void WriteFile( const char* filename );
};


class c_FindKthElement {
	int max_size;
	double* elem;
public:
	c_FindKthElement( int _max_size );
	~c_FindKthElement();

	typedef enum order { increasing_order, decreasing_order } order;
	void SetElement( int index, double value );
	double FindKthElementOfN( int k, int n, order ord=increasing_order );
};


// global functions

double FractionalValue( double x );
bool IsFractional( double x );
bool IsMoreFractional( double x, double y );
double RelativeDifference( double x_100_percent, double value );
bool HasLargerFractionalPart( double x, double y );
bool HasSmallerFractionalPart( double x, double y );


// output also for derived classes

ostream& operator<<( ostream& s, const c_Controller& );
ostream& operator<<( ostream& s, const c_Column& );
ostream& operator<<( ostream& s, const c_DualVariables& );
ostream& operator<<( ostream& s, const c_RMP& );
ostream& operator<<( ostream& s, const c_BranchAndBoundNode& );
ostream& operator<<( ostream& s, const c_BranchAndBoundConstraint&  );
ostream& operator<<( ostream& s, const c_PricingProblem& );
ostream& operator<<( ostream& s, const c_Solution& );
ostream& operator<<( ostream& s, const c_Constraint& );

} // end of namespace CGBase
#endif // of BASE_H