#ifndef _CPLEXCPP_H
#define _CPLEXCPP_H
// Header file for C++ example class library

#include <ilcplex/cplex.h>
#include <string>


// Note that for the CPLEXEnvironment and CPLEXProblem objects, we
//   explicitly prohibit copying and assignment of these arguments,
//   which forces them to be used by reference.  A copy constructor
//   or assignment operator for CPLEXEnvironment does not make 
//   conceptual sense.  
//   A copy constructor and assignment operator for a CPLEXProblem 
//   does make sense, but we've chosen not to implement it at this time.
//   This is because copying a CPLEXProblem would require querying the
//   internal representation of a problem, and creating a separate copy
//   of the problem data.  

namespace CGBase
{

	class CPLEXEnvironment
	{
	private:
		CPXENVptr env_;
		friend class CPLEXProblem;
		friend class CPLEX_NET_Problem;
		CPLEXEnvironment(const CPLEXEnvironment &rhs);
		CPLEXEnvironment& operator= (const CPLEXEnvironment& rhs);
	public:
		CPLEXEnvironment(int ldwid = 0, char *licenvstring = NULL);
		void terminate();
		virtual ~CPLEXEnvironment();

		// The setparam functions are the equivalent of CPXsetintparam() and
		// CPXsetdblparam(), but we use the C++ overloading feature
		// to define one function, and let the type of the argument
		// determine which parameter function is used.  This means
		// that the user is responsible for making sure that the CPLEX
		// parameters that require double arguments are given double
		// values as the "value" parameter

		void setparam(int paramnum, int value);
		void setparam(int paramnum, double value);
		void setdefaults();

		// The getParam functions are similar to the setparam functions
		// in that the arguments determine whether CPXgetintparam() or
		// CPXgetdblparam() are called.  Note that if each of the parameter
		// numbers became part of one of two enumerated types corresponding
		// to parameters that require integer values and parameters that
		// require double values, then the getParam() methods could return
		// int or double, and the first parameter to the method would
		// be the enumerated type.  We leave this as an exercise for
		// those who are so inclined!

		void getParam(int paramnum, int& rvalue) const;
		void getParam(int paramnum, double& rvalue) const;

		// Quick and Dirty, to use the C-Funktions "setmipcallbackfunc" and "setlpcallbackfunc"
		CPXENVptr getEnv() { return env_; }
	};

	// A class to handle exception handling.  When a CPLEX function fails,
	// the method that calls the CPLEX function will throw an exception 
	// of type CPLEXException (env_, status, funcname),
	// and the function what() will return the corresponding string
	// message.  The parameter env_ is required for the exception so that
	// CPLEX can quickly translate an error number to a string.  If the
	// parameter is NULL, then the string is still found, but the search
	// is significantly slower.


	class CPLEXException
	{
	private:
		CPXENVptr  env_;
		int        status_;
		const char *funcname_;
		char       buffer[512];
	public:
		CPLEXException(CPXENVptr env, int status, char *funcname = (char*)"unknown");
		const char *what() const;
		const char *funcname() const;
		int  errcode() const;
	};

	class CPLEXProblem
	{
		friend class CPLEX_NET_Problem;
	private:
		CPXLPptr   lp_;
		CPXENVptr  env_;
		CPLEXProblem(const CPLEXProblem &lp);
		CPLEXProblem& operator= (const CPLEXProblem& rhs);
	public:
		CPLEXProblem(CPLEXEnvironment& renv, const char *probname = "problem");
		virtual ~CPLEXProblem();

		void read(const char *filename, const char *filetype = 0);
		void write(const char *filename, const char *filetype = NULL) const;

		enum ObjSense { MAXIMIZE = CPX_MAX, MINIMIZE = CPX_MIN };

#if CPX_VERSION >= 900
		enum ProbType
		{
			LP = CPXPROB_LP,
			MIP = CPXPROB_MILP,
			RELAXED = CPXPROB_NODELP,
			FIXED = CPXPROB_FIXEDMILP,
			QP = CPXPROB_QP
		};
#else
		enum ProbType
		{
			LP = CPXPROB_LP,
			MIP = CPXPROB_MIP,
			RELAXED = CPXPROB_RELAXED,
			FIXED = CPXPROB_FIXED,
			QP = CPXPROB_QP
		};
#endif
		enum ColumnType { CONTINUES = 'C', BIN = 'B', INT = 'I' };
		enum ConstrSense { LESS = 'L', EQUAL = 'E', GREATER = 'G', RANGED = 'R' };
		enum BoundType { UPPER = 'U', LOWER = 'L', LOWER_AND_UPPER = 'B' };

		// copylpdata() method is like CPXcopylpwnames(), and CPXcopylp()
		void copylpdata(int numcols, int numrows,
			ObjSense objsen, double obj[],
			double rhs[], char sense[],
			int matbeg[], int matcnt[],
			int matind[], double matval[],
			double lb[], double ub[],
			double rngval[] = 0, char *colname[] = 0,
			char *rowname[] = 0);

		// copymipstart() method is like CPXcopymipstart()
		void copymipstart(int cnt, int* indices, double* value);

		// copyctype() method is like CPXcopyctype()
		void copyctype(char ctype[]);

		// newrows() method is like CPXnewrows()
		void newrows(int rcnt, double rhs[],
			char sense[], double rngval[] = 0,
			char *rowname[] = 0);

		// newcols() method is like CPXnewcols()
		void newcols(int ccnt, double obj[],
			double lb[], double ub[],
			char ctype[] = 0, char *colname[] = 0);

		// addrows() method is like CPXaddrows()
		void addrows(int ccnt, int rcnt, int nzcnt,
			double rhs[], char sense[], int rmatbeg[],
			int rmatind[], double rmatval[],
			char *colname[] = 0, char *rowname[] = 0);

		// addcols() method is like CPXaddcols()
		void addcols(int ccnt, int nzcnt, double obj[],
			int cmatbeg[], int cmatind[],
			double cmatval[], double lb[], double ub[],
			char *colname[] = 0);

		// delcols() method is like CPXdelCols()
		void delcols(int first, int last);

		// delrows() method is like CPXdelRows()
		void delrows(int first, int last);

		// chgctype is like CPXchgprobtype
		void chgprobtype(ProbType cpx_prob);

		// chgctype is like CPXchgctype
		void chgctype(int cnt, int indices[], char ctype[]);

		// chgobjsen() method is like CPXchgobjsen()
		void chgobjsen(ObjSense objsen);

		// chgcoef() method is like CPXchgcoef()
		void chgcoef(int i, int j, double val);

		// chgcoeflist() method is like CPXchgcoeflist()
		void chgcoeflist(int numcoefs, int rowlist[], int collist[],
			double vallist[]);

		// chgbds() method is like CPXchgbds
		void chgbds(int cnt, int indices[], char lu[], double bd[]);
		// chgbds() method is like CPXchgobj
		void chgobj(int cnt, int indices[], double values[]);
		// chgbds() method is like CPXchgrhs
		void chgrhs(int cnt, int indices[], double values[]);

		// primopt(), dualopt(), hybbaropt() are equivalents of
		// CPXprimopt(), CPXdualopt(), CPXhybbaropt() respectively
		void primopt();
		void dualopt();
		void baropt();
		void mipopt();

		// No of iterations
		int  getitcnt();
		// For the hybrid methods, define an enumerated type to hold
		// the possible simplex methods to be used after the initial
		// optimization.

		enum OptMethod { PRIMOPT = 'p', DUALOPT = 'd' };

		void hybbaropt(OptMethod = PRIMOPT);
		void hybnetopt(OptMethod = DUALOPT);

		// Solution routines.  stat() is CPXgetstat()
		int stat() const;

		double objval() const;
		double mipobjval() const;

		// The methods X, Slack, Pi, and Dj are like 
		// CPXgetx(), CPXgetslack(), CPXgetpi() and CPXgetdj(), respectively.
		// Note that if the delimiters (begin,end) = (0,-1), we'll assume
		// that means get the entire vector.  This allows those arguments
		// to not be required for the call.

		void getX(double x[], int begin = 0, int end = -1) const;
		void getmipX(double x[], int begin = 0, int end = -1) const;
		void _getX(double x[], int begin, int end) const; // both LP and MIP

		void getobj(double val[], int begin, int end) const;
		void getSlack(double slack[], int begin = 0, int end = -1) const;
		void getPi(double Pi[], int begin = 0, int end = -1) const;
		void getDj(double Dj[], int begin = 0, int end = -1) const;
		void getBase(int cstat[], int rstat[]) const;
		void getRHS(double rhs[], int begin = 0, int end = -1);

		// Some useful query functions
		int numcols(void) const;
		int numrows(void) const;
		ProbType probtype(void) const;

		// Methods to restart optimization
		void copyStart(int cstat[], int rstat[], double cprim[], double rprim[], double cdual[], double rdual[]);

		// Zum Testen der LP-Matrix eingefuehrt
		// Rueckgabewert ist ein String, der alle Fehler enthaelt
		std::string CheckMatrix(int numcols, int numrows, int numnzs,
			ObjSense objsen, double obj[],
			double rhs[], char sense[], char ctype[],
			int matbeg[], int matcnt[],
			int matind[], double matval[],
			double lb[], double ub[],
			double rngval[] = 0, char *colname[] = 0,
			char *rowname[] = 0,
			const double d_minvalue = -CPX_INFBOUND,
			const double d_maxvalue = CPX_INFBOUND);
		// Gibt ein LP als Excel-Matrix im csv-Format aus
		// method is like CPXcopylp(), 
		std::string DumpExcelMatrix(int numcols, int numrows,
			ObjSense objsen, double obj[],
			double rhs[], char sense[],
			int matbeg[], int matcnt[],
			int matind[], double matval[],
			double lb[], double ub[],
			double rngval[] = 0, char *colname[] = 0,
			char *rowname[] = 0);
	};


	class CPLEX_NET_Problem
	{
	private:
		CPXNETptr  net_;
		CPXENVptr  env_;
		CPLEX_NET_Problem(const CPLEX_NET_Problem &lp);
		CPLEX_NET_Problem& operator= (const CPLEX_NET_Problem& rhs);
	public:
		CPLEX_NET_Problem(CPLEXEnvironment& renv, const char *probname = "network problem");
		virtual ~CPLEX_NET_Problem();
		void terminate();

		void addarcs(int narcs, int *fromnode, int *tonode, double *low = NULL, double *up = NULL, double *obj = NULL, char **anames = NULL);
		void addnodes(int nnodes, double *supply = NULL, char **nnames = NULL);
		void basewrite(char *filename);
		void checkcopynet(int objsen, int nnodes, double *supply, char **nnames,
			int narcs, int *fromnode, int *tonode, double *low = NULL, double *up = NULL,
			double *obj = NULL, char **anames = NULL);
		void chgarcname(int cnt, int *indices, char **anames);
		void chgarcnodes(int cnt, int *index, int *fromnode, int *tonode);
		void chgbds(int cnt, int *index, char *lu, double *bd);
		void chgname(int key, int index, char *name);
		void chgnodename(int cnt, int *indices, char **name);
		void chgobj(int cnt, int *index, double *obj);
		void chgobjsen(int maxormin);
		void chgsupply(int cnt, int *index, double *supply);
		void copybase(int *astat, int *nstat);
		void copynet(int objsen, int nnodes, double *supply, char **nnames,
			int narcs, int *fromnode, int *tonode, double *low = NULL, double *up = NULL,
			double *obj = NULL, char **anames = NULL);
		void delarcs(int begin, int end);
		void delnodes(int begin, int end);
		void delset(int *whichnodes = NULL, int *whicharcs = NULL);
		void extract(CPLEXProblem *lp, int *colmap = NULL, int *rowmap = NULL);
		void getarcindex(char *lname, int *index_p);
		void getarcname(char **nnames, char *namestore, int namespc, int *surplus_p, int begin, int end);
		void getarcnodes(int *fromnode, int *tonode, int begin, int end);
		void getbase(int *astat, int *nstat);
		void getdj(double *dj, int begin, int end);
		int getitcnt();
		void getlb(double *low, int begin, int end);
		void getnodearcs(int *arccnt_p, int *arcbeg, int *arc, int arcspace, int *surplus_p, int begin, int end);
		void getnodeindex(char *lname, int *index_p);
		void getnodename(char **nnames, char *namestore, int namespc, int *surplus_p, int begin, int end);
		int getnumarcs();
		int getnumnodes();
		void getobj(double *obj, int begin, int end);
		int getobjsen();
		double getobjval();
		int getphase1cnt();
		void getpi(double *pi, int begin, int end);
		void getprobname(char *buf, int bufspace, int *surplus_p);
		void getslack(double *slack, int begin, int end);
		int getstat();
		void getsupply(double *supply, int begin, int end);
		void getub(double *up, int begin, int end);
		void getx(double *x, int begin, int end);
		void primopt();
		void readcopybase(char *filename);
		void readcopyprob(char *filename);
		void solution(int *netstat_p, double *objval_p = NULL, double *x = NULL, double *pi = NULL, double *slack = NULL, double *dj = NULL);
		void writeprob(char *filename, char *format = NULL);
	};

} // end of namespace CGBase

#endif // of _CPLEXCPP_H