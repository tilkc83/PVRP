#ifndef CVRP_INSTANCE_H
#define CVRP_INSTANCE_H

#include <string>
#include <bitset>
#include <vector>
#include <algorithm> 

#include <iostream>
#include "SharedFiles/matrix.h"
#include "SharedFiles/settings_reader.h"
#include "ColumnGeneration/base.h"
#include "data.h"

using namespace std;

const int pvrptw_bs_max_size = 200; //288*6=1728
typedef bitset<pvrptw_bs_max_size> pvrptw_bitset;
const int pvrptw_request_bs_max_size = 288;
typedef bitset<pvrptw_request_bs_max_size> customer_bitset;
const int timeScaleFactor = 10;

template <class T>
bool v_contains(std::vector<T> const &v, T const &x) { 
	return v.end() != std::find(v.begin(), v.end(), x);
}

//@Francesco: This file contains everythin relevant for the instance data, especially the class c_CVRPTW_Instance musst be adapted if necessary (and accordingly in the .cpp)
class c_CVRPTW_Instance {
	
	string instanceName;
	string scheduleName;

	int i_numNodes;
	int i_numCustomers;
	int i_numVehiclesPerDay; //if maximum is given by instance
	int i_origDepot;
	int i_destDepot;
	vector<int> v_maxDurationPerRoute; //for instances of Cordeau

	c_DoubleMatrix m_costs;
	c_IntMatrix m_times;
	//Perhaps the demand differs from day to day. Read it from input file?
	vector<double> v_demand; //demand per day. If customer is not visited every day, the demand of all following days without visit has to be fulfilled too
	vector<int> v_frequency; // just in case frequency is needed
	vector<int> v_serviceTime;
	vector<int> v_startTime; // lower TW border
	vector<int> v_endTime;   // upper TW border
	vector<double> v_xCoord;
	vector<double> v_yCoord;
	int i_capacity;

	////For every vertex (customers for every day)
	int i_origDepotVertex;
	int i_destDepotVertex;


	int i_planningHorizon;
	vector<vector<vector<bool>>> v_schedulesPerCustomer; //for every customer vector of all allowed schedule (a schedule is represented by bool vector saying if visit at the day ) //length of numNodes
	vector<c_SchedulePart*> v_scheduleParts;
	//Schedule Part Ids sorted 
	vector<vector<int>> v_schedulePartsPerCustomer; //for every customer vector of all start and end periods of visitation covering, length of num nodes
	vector<vector<vector<int>>> v_schedulePartsPerCustomerAndScheduleId; //for every customer and schedule-id vector of all parts
	
	int i_numVertices; //1 (startDepot) + i_planningHorizon * i_numCustomers + 1 (endDepot)
	vector<int> v_nodesOfVertices; //at position v is node corresponding to vertex v
	vector<int> v_daysOfVertices; //at position v is day corresponding to vertex v

	int i_numTasks;
	int i_startTask;
	int i_endTask;
	vector<int> v_nodesOfTasks; //at position v is node corresponding to vertex v
	vector<int> v_daysOfTasks; //at position v is day corresponding to vertex v
	
	bool LeseDouble( ifstream& file, double& i1 );
	bool LeseInteger( ifstream& file, int& i1 );
	bool LeseBoolean( ifstream& file, bool& b1 );
	bool LeseString( ifstream& file, const char* string, bool weglesen=true );
	bool LeseBez( ifstream& file, char* buffer );

public:
	c_CVRPTW_Instance( string instanceFilename, string scheduleFilename );

	string InstanceName() const { return instanceName;}
	string ScheduleName() const { return scheduleName;}
	int NumNodes() const { return i_numNodes;}
	int NumCustomers() const { return i_numCustomers;}
	int OrigDepot() const { return i_origDepot;}
	int DestDepot() const { return i_destDepot;}
	double Cost( int i, int j ) const { return m_costs(i,j);}
	int Time( int i, int j ) const { return m_times(i,j);}
	const double DemandPerDay( int i ) const { return v_demand[i];}
	const int Frequency( int i ) const { return v_frequency[i];}
	const int ServiceTime( int i ) const { return v_serviceTime[i];}
	const int AdditionalServiceTime() const { return 0 * 10; } //don't forget scaling factor 10 for times
	const int StartTime(int i) const { return v_startTime[i];}
	const int EndTime(int i) const { return v_endTime[i];}
	int Capacity() const { return i_capacity;}
	int RoundingDigits(){return i_roundingDigits;}
	const int NumVehiclesPerDay(){ return i_numVehiclesPerDay;}
	const double getDistToDepot(int node) const;
	const int MaxTourDuration(int day = 0){ return v_maxDurationPerRoute[day]; }

	vector<double> GetXCoords(){ return v_xCoord;}
	vector<double> GetYCoords(){ return v_yCoord;}
	
	vector<double> GetDemands(){ return v_demand;}
	vector<int> GetStarts(){ return v_startTime;}
	vector<int> GetEnds(){ return v_endTime;}
	vector<int> GetFrequencies(){ return v_frequency;}

	bool IsCustomerNode( int i ) const;
	int ArcExists( int n1, int n2 ) const; //refers to nodes
	bool ArcExistsOnDay( int v1, int v2, int day) const; //refers to vertices

	const int PlanningHorizon() const { return i_planningHorizon;}
	const vector<vector<bool>>& GetAllowedSchedules( int cust ) const { return v_schedulesPerCustomer[cust];}
	//Schedule parts
	const vector<int>& GetAllowedScheduleParts( int cust ) const { return v_schedulePartsPerCustomer[cust];}
	const vector<int>& GetScheduleParts( int customer, int scheduleId ) const { return v_schedulePartsPerCustomerAndScheduleId[customer][scheduleId]; }
	const c_SchedulePart* GetSchedulePart( int SPId ) const { return v_scheduleParts[SPId]; }
	const bool SPContainsDay( int SPId, int day ) const;
	const int GetSchedulePartId(int customer, int startDay, int endDay) const;
	const int GetNumScheduleParts() const { return (int)v_scheduleParts.size();}
	const bool IsAllowedSchedulePart( pair<int,int>& schedulepart, vector<bool>& schedule);

	const int DemandSchedulePart( int spId ) const { return GetSchedulePart( spId )->getDemand();}
	const int AdditionalServiceTimeSchedulePart( int spId ) const { return GetSchedulePart( spId )->getAdditionalServiceTime();} 

	const int NumVertices() const;
	const int GetVertex( int cust, int day, int endDay = -1 );
	//void createVertices();
	int OrigDepotVertex(int day = -1) const;
	int DestDepotVertex(int day = -1) const;
	const int GetNode( int vertex ) const;
	const int GetDay( int vertex ) const;

	const int NumTasks() const {return i_numTasks;}
	const int GetNodeOfTask( int task ) const;
	const int GetDayOfTask( int task ) const;
	const int GetTask( int cust, int day ) const;
	const bool TaskOfSubsetContainedInSP( const vector<int>& subset, int SPId ) const;
	const int GetNumTasksCoveredBySP(int SPId) const;
	const int GetNumTasksCovered(int startDay, int endDay) const;

	const int GetNumCustScheduleParts() const { return ((int)v_scheduleParts.size() - 2*PlanningHorizon());}
	const int NumCustomerVertices() const { return i_numVertices -2;}
	vector<int> GetDemandsForVertices() const;
	const int GetSepVertexNumber(int origVertexNumber) const;
	const int GetVertexOnOtherDay(int vertex, int day);

	double XCoord( int i ) const { return v_xCoord[i]; }
	double YCoord( int i ) const { return v_yCoord[i]; }

	virtual void Go( void );

	// Rounding
	const static int i_roundingDigits = 1;

	double RoundingFactor() { return pow( (double) 10, RoundingDigits() ); }
	virtual double OffsetForTriangleInequality() { return pow( (double) 10, -RoundingDigits() ); }

	void createScheduleParts();
	void storeVertexInfos(); //sets i_numVertices, v_nodesOfVertices, v_daysOfVertices
	
	vector<bool> createSchedule(int maxDaysUntilNextVisit, int startDay);


	void FixCustToFirstDay(int customer);
};





//@Francesco: c_CVRPTW_Instance_With_RDC should remain as it is at first
/*
This is an extended version of the instance which can store reduced costs, dual prices, and the ng-path neighborhoods and other stuff related to the solution method
*/
class c_CVRPTW_Instance_With_RDC : public c_CVRPTW_Instance {
	c_DoubleMatrix m_rdc;
	vector<int> activeCutIds;
	vector<double> dualsToSRIs;
	vector<vector<int> > v_subsetsOfCuts;
	std::vector<std::vector<int> > v_ng;
	std::vector<std::vector<int> > v_ng_index;
	
public:
	c_CVRPTW_Instance_With_RDC(string instanceFile, string scheduleFile );

	void InitializeRDCMatrix();

	void SetActiveCutIds(vector<int> cutIds){ activeCutIds = cutIds;}
	const vector<int>& getActiveCutIds() const {return activeCutIds;}

	void SetDualsToSRIs(vector<double> duals){ dualsToSRIs = duals;}
	double getSRIDual(int cutId) const {return dualsToSRIs[cutId];}
	void SetSRIs(const vector<vector<int> >& sris){ v_subsetsOfCuts = sris;}
	const vector<vector<int> >& getSRIs() const {return v_subsetsOfCuts;}

	void SetRDC( int i, int j, int day, double rdc );
	double RDC( int i, int j, int day) const;
	void SetNGNeighborhood( int i, const vector<int>& ng_i );
	int NGNeighborhoodSize( int i ) const;   // size of NG(i); k if NG(i) = \{ v_1, dots, v_k \}
	int NGNeighbor( int i, int idx ) const;  // returns v_{idx}
	int NGIndex( int i, int j ) const;       // returns idx for j=v_{idx}, -1 otherwise 
	bool AddToNeighborhood(int v, int nodeToBeAdded, int maxNgSize);
	const int NumberOfVehicles();
};

#endif // of CVRPTW_INSTANCE_H