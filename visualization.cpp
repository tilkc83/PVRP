#include "visualization.h"

#include <vector>
#include <fstream>

using namespace std;

c_TikzVisualization::c_TikzVisualization(const char* filename, int numNodes, vector<double> xCoord, vector<double> yCoord, vector<double> demands, vector<int> startTimes, vector<int> endTimes, vector<int> nodeTypes)
	: i_numNodes( numNodes ),
	v_xCoords( xCoord ),
	v_yCoords( yCoord ),
	v_demands ( demands ),
	v_startTimes ( startTimes ),
	v_endTimes ( endTimes )
{
	int depotStartTime = v_startTimes[0];
	int depotEndTime = v_endTimes[0];
	planningHorizon = depotEndTime - depotStartTime;
	
	s_filename = filename;
	ofstream tikzFile( filename);

	if(tikzFile.is_open())   
	{
		tikzFile << "\\begin{tikzpicture}[inner sep=0]" << endl;
		/*	tikzFile << "%Achsen zeichnen " << endl;
		tikzFile << "\\draw[->,thick] (0,0) -- (100.5,0) node[right] {$x$};" << endl;
		tikzFile << "\\draw[->,thick] (0,0) -- (0,100.5) node[above] {$y$};" << endl;
		tikzFile << "% Achsen beschriften" << endl;
		tikzFile << "\\foreach \\x in {0,10,20,30,40,50,60,70,80,90,100}" << endl;
		tikzFile << "\\draw (\\x,-.1) -- (\\x,.1) node[below=4pt] {$\\scriptstyle\\x$};" << endl; 
		tikzFile << "\\foreach \\y in {0,10,20,30,40,50,60,70,80,90,100}" << endl; 
		tikzFile << "\\draw (-.1,\\y) -- (.1,\\y) node[left=4pt] {$\\scriptstyle\\y$};" << endl;*/
		tikzFile << "%draw the nodes: " << endl;
		for (int i = 0; i < i_numNodes; i++){
			if( i == 0  ){
				tikzFile << "%draw the depot:" << endl;
				tikzFile << "\\node[circle, draw=black, minimum width = 20pt, fill= red] (" << i << ") at (" << v_xCoords[i] << "," << v_yCoords[i] << ") {\\textbf{" << i << "}};" << endl;
				/*tikzFile << "\\draw (" << v_xCoords[i]-halfPH << ", " << v_yCoords[i]+ptAbove << ") -- +(" << planningHorizonInPt << ", 0);" << endl;
				int twLength = planningHorizonInPt;
				int relativeStart = v_xCoords[i];
				tikzFile << "\\node[rectangle, draw=black, fill= white, fit={+(0,0) +(" << planningHorizonInPt<< ",1)}   ] at ("<< relativeStart <<", " << v_yCoords[i]+ptAbove << ") {};" << endl; */
			}
			else {
				tikzFile << "%draw node " << i << ":" << endl;
				string nodeColor ="green";
				if ( nodeTypes.size() != v_xCoords.size() ){
					nodeColor = "green";
				} else {
					switch( nodeTypes[i] ){
					case 1: 
						nodeColor = "yellow";
						break;
					case 2: 
						nodeColor = "green";
						break;
					case 3: 
						nodeColor = "cyan";
						break;
					case 4: 
						nodeColor = "blue";
						break;
					case 6: 
						nodeColor = "gray";
						break;
					default: nodeColor = "black";
					}
				}
				tikzFile << "\\node[circle, draw=black, minimum width = 20pt, fill=" << nodeColor <<  "] (" << i << ") at (" << v_xCoords[i] << "," << v_yCoords[i] << ") {\\textbf{" << i << "}};" << endl; //", label=below:{\\tiny" << v_demands[i] <<
				//tikzFile << "\\draw (" << v_xCoords[i]-halfPH << ", " << v_yCoords[i]+ptAbove << ") -- +(" << planningHorizonInPt << ", 0);" << endl;
				//double twLength = (v_endTimes[i]- v_startTimes[i])/planningHorizon * planningHorizonInPt;
				//double relativeStart = v_xCoords[i]-halfPH + ((v_startTimes[i]+(v_endTimes[i]-v_startTimes[i])/2.0)-depotStartTime)/planningHorizon * planningHorizonInPt; //middle of time window
				//tikzFile << "\\node[rectangle, draw=black, fill= white, fit={+(0,0) +(" << twLength << ",1)}   ] at ("<< relativeStart <<", " << v_yCoords[i]+ptAbove << ") {};" << endl; 			
			}
		}
		tikzFile << "\\end{tikzpicture}" << endl; 		       
	}
	tikzFile.close();
}

void c_TikzVisualization::addRoutes(vector<vector<int>> routes, vector<vector<int>> visitTimes)
{
	//create temporary copy of tikz output to remove last line
	string search_string = "\\end{tikzpicture}";
	string inbuf;
	fstream input_file(s_filename, ios::in);
	ofstream output_file( "Temporary.tikz" );

	while (!input_file.eof())
	{
		getline(input_file, inbuf);
		if( inbuf == search_string ) //stop before \end{tikzpicture}
			break;

		output_file << inbuf << endl;
	}
	input_file.close();
	//delete original file
	remove(s_filename);
	
	string colors[] = {"red", "green", "blue", "cyan", "magenta", "yellow", "gray", "brown", "orange", "purple"};
	//draw used arcs and mark arrival time
	for( int r = 0; r < routes.size(); r++){
		output_file << "%draw route " << r << ":" << endl;
		for( int i = 0; i < routes[r].size()-1; i++){
			output_file << "\\draw [->, " << colors[r%10] << ", thick] ("<< routes[r][i] << ") -- (" << routes[r][i+1] <<");" << endl;
			double xValOfTime = v_xCoords[routes[r][i]] - halfPH +  visitTimes[r][i]/planningHorizon * planningHorizonInPt;
			output_file << "\\draw[" << colors[r%10] << ", thick] (" << xValOfTime << ", " << v_yCoords[routes[r][i]] + ptAbove-0.75 << ") -- +(0, 1.5);" << endl;
		} 		
	}

	output_file << "\\end{tikzpicture}" << endl; 
	output_file.close();
	//Rename temporary file to finally replace the instance tikz file without routes
	rename( "Temporary.tikz", s_filename );
}
